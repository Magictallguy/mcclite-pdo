<?php
/*
MCCodes FREE
ad.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

define('NO_OUTPUT', true);
define('NONAUTH', true);
require_once __DIR__.'/lib/master.php';
$_GET['ad'] = array_key_exists('ad', $_GET) && ctype_digit($_GET['ad']) && $_GET['ad'] > 0 ? $_GET['ad'] : null;
if (null !== $_GET['ad']) {
    $db->query('SELECT adID, adURL FROM ads WHERE adID = ?');
    $db->execute([$_GET['ad']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        $db->query('UPDATE ads SET adCLICKS = adCLICKS + 1 WHERE adID = ?');
        $db->execute([$_GET['ad']]);
        exit(header('Location: '.$row['adURL']));
    } else {
        $_SESSION['error'] = 'The ad you selected doesn\'t exist';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid ad';
}
