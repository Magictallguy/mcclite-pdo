<?php
/*
MCCodes FREE
advsearch.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('PAGINATION', true);
require_once __DIR__.'/lib/master.php';
$nums = ['levelmin', 'levelmax', 'house', 'daysmin', 'daysmax'];
$_POST['gender'] = array_key_exists('gender', $_POST) && in_array($_POST['gender'], ['Male', 'Female']) ? $_POST['gender'] : null;
$_POST['name'] = array_key_exists('name', $_POST) && is_string($_POST['name']) && strlen($_POST['name']) > 0 ? $_POST['name'] : null;
$_POST['online'] = array_key_exists('online', $_POST) && in_array($_POST['online'], ['yes', 'no', 'either']) ? $_POST['online'] : null;
foreach ($nums as $num) {
    $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) ? $_POST[$num] : 0;
}
$db->query('SELECT hID, hNAME, hWILL FROM houses ORDER BY hWILL ASC');
$db->execute();
$houses = $db->fetch();
$wills = [];
$db->query('SELECT maxwill, COUNT(userid) AS cnt FROM users GROUP BY maxwill ORDER BY maxwill ASC');
$db->execute();
$willRows = $db->fetch();
if (null !== $willRows) {
    foreach ($willRows as $row) {
        $wills[$row['maxwill']] = $row['cnt'];
    }
}
$db->query('SELECT * FROM (
    (SELECT COUNT(userid) AS total FROM users) AS t,
    (SELECT COUNT(userid) AS males FROM users WHERE gender = "Male") AS g,
    (SELECT COUNT(userid) AS online FROM users WHERE laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)) AS onl
)');
$db->execute();
$users = $db->fetch(true);
$users['females'] = $users['total'] - $users['males'];
$users['offline'] = $users['total'] - $users['online']; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Advanced Search</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/advsearch.php" method="post" class="form">
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" id="name" class="form-control bg-dark text-light"<?php echo null !== $_POST['name'] ? ' value="'.$func->format($_POST['name']).'"' : ''; ?>>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="gender" class="form-label">Gender</label>
                        <select name="gender" id="gender" class="form-control bg-dark text-light">
                            <option value="0"<?php echo null === $_POST['gender'] ? ' selected' : ''; ?>>Either</option>
                            <option value="Male"<?php echo 'Male' == $_POST['gender'] ? ' selected' : ''; ?>>Male (<?php echo $func->format($users['males']); ?>)</option>
                            <option value="Female"<?php echo 'Female' == $_POST['gender'] ? ' selected' : ''; ?>>Female (<?php echo $func->format($users['females']); ?>)</option>
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="levelmin" class="form-label">Minimum Level</label>
                        <input type="number" name="levelmin" id="levelmin" class="form-control bg-dark text-light"<?php echo null !== $_POST['levelmin'] ? ' value="'.$_POST['levelmin'].'"' : ''; ?>>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="levelmax" class="form-label">Maximum Level</label>
                        <input type="number" name="levelmax" id="levelmax" class="form-control bg-dark text-light"<?php echo null !== $_POST['levelmax'] ? ' value="'.$_POST['levelmax'].'"' : ''; ?>>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-3">
                    <div class="form-group">
                        <label for="house" class="form-label">House</label>
                        <select name="house" id="house" class="form-control bg-dark text-light">
                            <option value="0"<?php echo null === $_POST['house'] ? ' selected' : ''; ?>>Any House</option><?php
            if (null !== $houses) {
                foreach ($houses as $house) {
                    printf('<option value="%u"%s>%s (%s)</option>', $house['hWILL'], $_POST['house'] == $house['hWILL'] ? ' selected' : '', $func->format($house['hNAME']), in_array($house['hWILL'], array_keys($wills)) ? $func->format($wills[$house['hWILL']]) : '0');
                }
            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label for="online" class="form-label">Online</label>
                        <select name="online" id="online" class="form-control bg-dark text-light">
                            <option value="either"<?php echo 'either' == $_POST['online'] ? ' selected' : ''; ?>>Either</option>
                            <option value="yes"<?php echo 'yes' == $_POST['online'] ? ' selected' : ''; ?>>Yes (<?php echo $func->format($users['online']); ?>)</option>
                            <option value="no"<?php echo 'no' == $_POST['online'] ? ' selected' : ''; ?>>No (<?php echo $func->format($users['offline']); ?>)</option>
                        </select>
                    </div>
                </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="daysmin" class="form-label">Minimum Age (days)</label>
                            <div class="input-group mb-3">
                                <input type="number" name="daysmin" id="daysmin" class="form-control bg-dark text-light" aria-label="Days"<?php echo null !== $_POST['daysmin'] ? ' value="'.$_POST['daysmin'].'"' : ''; ?>>
                                <div class="input-group-append">
                                    <span class="input-group-text">days</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group">
                            <label for="daysmax" class="form-label">Maximum Age</label>
                            <div class="input-group mb-3">
                                <input type="number" name="daysmax" id="daysmax" class="form-control bg-dark text-light" aria-label="Days"<?php echo null !== $_POST['daysmax'] ? ' value="'.$_POST['daysmax'].'"' : ''; ?>>
                                <div class="input-group-append">
                                    <span class="input-group-text">days</span>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-search"></span>
                    Search
                </button>
            </div>
        </form>
    </div>
</div><?php
if (array_key_exists('submit', $_POST)) {
                $query = '';
                $params = [];
                if ($_POST['levelmin'] > 0) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' level >= ?';
                    $params[] = $_POST['levelmin'];
                }
                if ($_POST['levelmax'] > 0) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' level <= ?';
                    $params[] = $_POST['levelmax'];
                }
                if (null !== $_POST['gender']) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' gender = ?';
                    $params[] = $_POST['gender'];
                }
                if (null != $_POST['name']) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' username LIKE ?';
                    $params[] = '%'.$_POST['name'].'%';
                }
                if ($_POST['house'] > 0) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' maxwill = ?';
                    $params[] = $_POST['house'];
                }
                if ($_POST['daysmin'] > 0) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' daysold >= ?';
                    $params[] = $_POST['daysmin'];
                }
                if ($_POST['daysmax'] > 0) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' daysold <= ?';
                    $params[] = $_POST['daysmax'];
                }
                if ('yes' == $_POST['online']) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)';
                } elseif ('no' == $_POST['online']) {
                    $query .= ('' != $query ? ' AND' : ' WHERE').' laston <= DATE_SUB(NOW(), INTERVAL 15 MINUTE)';
                }
                $db->query('SELECT COUNT(userid) FROM users'.$query);
                $db->execute($params);
                $cnt = $db->result();
                $pages = new Paginator($cnt);
                $db->query('SELECT userid, username, level, money, avatar, laston FROM users'.$query.$pages->limit);
                $db->execute($params);
                $rows = $db->fetch(); ?>
<div class="row">
    <div class="col">&nbsp;</div>
</div>
<div class="row">
    <div class="col">
        <p>
            <?php echo $cnt.' player'.$func->s($cnt); ?> found.
        </p>
        <?php echo $pages->display_pages(); ?>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Level</th>
                        <th>Money</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="3" class="center">No results found</td>
                    </tr><?php
    } else {
        $time = time();
        foreach ($rows as $row) {
            $laston = strtotime($row['laston']); ?>
                    <tr>
                        <td>
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" class="avatar-search img-fluid <?php echo $laston >= $time - 900 ? 'online-box' : 'offline-box'; ?>">
                            </a>
                            <?php echo $func->username($row['userid']); ?>
                        </td>
                        <td><?php echo $func->format($row['level']); ?></td>
                        <td><?php echo $func->money($row['money']); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
            <p>
                <?php echo $pages->display_pages(); ?>
            </p>
        </div>
    </div>
</div><?php
            }
