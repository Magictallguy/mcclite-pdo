<?php
/*
MCCodes FREE
attack.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('ATTACKING', true);
require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    exit("<font color='red'>WTF you doing, bro?</font></b>");
}
if ($_GET['ID'] == $userid) {
    exit("<font color='red'><b>Only the crazy attack themselves.</font></b>");
}
//get player data
$youdata = $ir;
$db->query('SELECT u.*, us.*
    FROM users AS u
    INNER JOIN userstats AS us ON u.userid = us.userid
    WHERE u.userid = ?
');
$db->execute([$_GET['ID']]);
$odata = $db->fetch(true);
if (null === $odata) {
    $_SESSION['attacking'] = 0;
    exit('<span class="bold red">This player does not exist.</span><br /><a href="index.php">&gt; Back</a>');
}
if (1 == $odata['hp']) {
    $_SESSION['attacking'] = 0;
    exit('<span class="bold red">This player is unconscious.</span><br /><a href="index.php">&gt; Back</a>');
}
if ($odata['hospital'] > 0 && 0 == $ir['hospital']) {
    $_SESSION['attacking'] = 0;
    exit('<span class="bold red">This player is in hospital.</span><br /><a href="index.php">&gt; Back</a>');
}
if ($ir['hospital'] > 0) {
    $_SESSION['attacking'] = 0;
    exit('<span class="bold red">You can not attack while in hospital.</span><br /><a href="hospital.php">&gt; Back</a>');
}
echo '<table width="100%"><tr><td colspan="2" class="center">';
$_GET['wepid'] = array_key_exists('wepid', $_GET) && ctype_digit($_GET['wepid']) && $_GET['wepid'] > 0 ? $_GET['wepid'] : null;
$myGen = [
    'his-her' => 'Male' == $ir['gender'] ? 'his' : 'her',
    'him-her' => 'Male' == $ir['gender'] ? 'him' : 'her',
    'he-she' => 'Male' == $ir['gender'] ? 'he' : 'she',
];
$oGen = [
    'his-her' => 'Male' == $odata['gender'] ? 'his' : 'her',
    'him-her' => 'Male' == $odata['gender'] ? 'him' : 'her',
    'he-she' => 'Male' == $odata['gender'] ? 'he' : 'she',
];
$myName = $func->username($ir['userid']);
$theirName = $func->username($odata['userid']);
$_GET['nextstep'] = array_key_exists('nextstep', $_GET) && ctype_digit($_GET['nextstep']) && $_GET['nextstep'] > 0 ? $_GET['nextstep'] : null;
$_SESSION['attacking'] = array_key_exists('attacking', $_SESSION) && ctype_digit($_SESSION['attacking']) && $_SESSION['attacking'] > 0 ? $_SESSION['attacking'] : null;
if (null !== $_GET['wepid']) {
    if (0 == $_SESSION['attacking']) {
        if ($youdata['energy'] >= $youdata['maxenergy'] / 2) {
            $youdata['energy'] -= $youdata['maxenergy'] / 2;
            $me = $youdata['maxenergy'] / 2;
            $db->query('UPDATE users SET energy = energy - ? WHERE userid = ?');
            $db->execute([$me, $userid]);
            $_SESSION['attacklog'] = '';
        } else {
            exit("<font color='red'><b>You can only attack someone when you have 50% energy</font></b>");
        }
    }
    $_SESSION['attacking'] = 1;
    //damage
    if (!$func->hasItem($_GET['wepid'])) {
        $db->query('UPDATE users SET exp = 0 WHERE userid = ?');
        $db->execute([$userid]);
        $_SESSION['attacking'] = 0;
        exit('<span class="bold red">Stop trying to abuse a game bug. You can lose all your EXP for that.</span><br /><a href="/index.php">&gt; Home</a>');
    }
    $db->query('SELECT i.itmid, i.itmname, w.damage
        FROM items AS i
        INNER JOIN weapons AS w ON i.itmid = w.item_id
        WHERE i.itmid = ?
    ');
    $db->execute([$_GET['wepid']]);
    $r1 = $db->fetch(true);
    if (null === $r1) {
        $_SESSION['attacking'] = 0;
        $_SESSION['error'] = 'The weapon you selected doesn\'t exist';
        exit(header('Location: /index.php'));
    }
    $mydamage = (int) (($r1['damage'] * $youdata['strength'] / $odata['guard']) * (mt_rand(8000, 12000) / 10000));
    $hitratio = min(50 * $ir['agility'] / $odata['agility'], 95);
    if (mt_rand(1, 100) <= $hitratio) {
        $db->query('SELECT a.Defence
            FROM inventory AS iv
            INNER JOIN items AS i ON iv.inv_itemid = i.itmid
            INNER JOIN armour AS a ON i.itmid = a.item_ID
            WHERE i.itmtype = 7 AND iv.inv_userid = ?
            ORDER BY RAND()
            LIMIT 1
        ');
        $db->execute([$odata['userid']]);
        $mydamage -= $db->result();
        if ($mydamage < 1) {
            $mydamage = 1;
        }
        $odata['hp'] -= $mydamage;
        if (1 == $odata['hp']) {
            $odata['hp'] = 0;
            ++$mydamage;
        }
        $db->query('UPDATE users SET hp = hp - ? WHERE userid = ?');
        $db->execute([$mydamage, $odata['userid']]);
        $str = '<span class="red">'.$_GET['nextstep'].'. Using {your.their} '.$func->format($r1['itmname']).', {attacker} hit {defender} doing '.$func->format($mydamage).' damage ('.$odata['hp'].')</span><br />';
        echo str_replace(['{your.their}', '{attacker}', '{defender}'], ['your', 'you', $theirName], $str);
        $_SESSION['attacklog'] .= str_replace('{your.their}', $myGen['his-her'], $str);
    } else {
        $str = '<span class="red">'.$_GET['nextstep'].'. {attacker} tried to hit {defender}, but missed ('.$odata['hp'].')</span><br />';
        echo str_replace(['{attacker}', '{defender}'], ['You', $theirName], $str);
        $_SESSION['attacklog'] .= $str;
    }
    if ($odata['hp'] <= 0) {
        $odata['hp'] = 0;
        $_SESSION['attackwon'] = $_GET['ID'];
        $db->query('UPDATE users SET hp = 0 WHERE userid = ?');
        $db->execute([$odata['userid']]); ?>
        <form action="/attackleave.php?ID=<?php echo $odata['userid']; ?>" method="post"><input type="submit" value="Leave Them" /></form>
        <form action="/attackmug.php?ID=<?php echo $odata['userid']; ?>" method="post"><input type="submit" value="Mug Them"></form>
        <form action="/attackhosp.php?ID=<?php echo $odata['userid']; ?>" method="post"><input type="submit" value="Hospitalize Them"></form><?php
    } else {
        // choose opp gun
        $db->query('SELECT i.itmname, w.damage
            FROM inventory AS iv
            INNER JOIN items AS i ON iv.inv_itemid = i.itmid
            INNER JOIN weapons AS w ON iv.inv_itemid = w.item_id
            WHERE iv.inv_userid = ? AND i.itmtype IN (3, 4)
        ');
        $db->execute([$odata['userid']]);
        $oWeps = $db->fetch();
        if (null === $oWeps) {
            $wep = 'Fists';
            $dam = (int) ((((int) ($odata['strength'] / 100)) + 1) * (mt_rand(8000, 12000) / 10000));
        } else {
            $cnt = 0;
            $enweps = [];
            foreach ($oWeps as $r) {
                $enweps[] = $r;
                ++$cnt;
            }
            $weptouse = mt_rand(0, $cnt - 1);
            $wep = $enweps[$weptouse]['itmname'];
            $dam = (int) (($enweps[$weptouse]['damage'] * $odata['strength'] / $youdata['guard']) * (mt_rand(8000, 12000) / 10000));
        }
        $hitratio = min(50 * $odata['agility'] / $ir['agility'], 95);
        if (1 == $odata['userid']) {
            $hitratio = 100;
        }
        if (mt_rand(1, 100) <= $hitratio) {
            $db->query('SELECT a.Defence
                FROM inventory AS iv
                INNER JOIN items AS i ON iv.inv_userid = i.itmid
                INNER JOIN armour AS a ON i.itmid = a.item_ID
                WHERE iv.inv_userid = ? AND i.itmtype = 7
                ORDER BY RAND()
                LIMIT 1
            ');
            $db->execute([$userid]);
            $dam -= $db->result();
            if ($dam < 1) {
                $dam = 1;
            }
            $youdata['hp'] -= $dam;
            $db->query('UPDATE users SET hp = hp - ? WHERE userid = ?');
            $db->execute([$dam, $userid]);
            $ns = $_GET['nextstep'] + 1;
            $str = '<span class="blue">'.$ns.'. Using '.$oGen['his-her'].' '.$func->format($wep).', {defender} hit {attacker} doing '.$func->format($dam).' damage ('.$youdata['hp'].')</span><br />';
            echo str_replace(['{attacker}', '{defender}'], ['you', $theirName], $str);
            $_SESSION['attacklog'] .= $str;
        } else {
            $ns = $_GET['nextstep'] + 1;
            $str = '<span class="blue">'.$ns.'. {defender} tried to hit {attacker}, but missed ('.$youdata['hp'].')</span><br />';
            echo str_replace(['{attacker}', '{defender}'], ['you', $theirName], $str);
            $_SESSION['attacklog'] .= $str;
        }
        if ($youdata['hp'] <= 0) {
            $youdata['hp'] = 0;
            $db->query('UPDATE users SET hp = 0 WHERE userid = ?');
            $db->execute([$userid]); ?>
            <form action="/attacklost.php?ID=<?php echo $odata['userid']; ?>" method="post"><input type="submit" value="Continue" /></form><?php
        }
    }
} elseif ($odata['hp'] < $odata['maxhp'] / 2) {
    exit('You can only attack those who have at least 1/2 their max health');
} elseif ($youdata['energy'] < $youdata['maxenergy'] / 2) {
    exit('You can only attack someone when you have 50% energy');
} elseif ($youdata['location'] != $odata['location']) {
    exit('You can only attack someone in the same location!');
}
echo '</td></tr>';
if ($youdata['hp'] <= 0 || $odata['hp'] <= 0) {
    echo '</table>';
} else {
    $db->query('SELECT i.itmid, i.itmname
        FROM inventory AS iv
        INNER JOIN items AS i ON iv.inv_itemid = i.itmid
        WHERE iv.inv_userid = ? AND i.itmtype IN (3, 4)
    ');
    $db->execute([$userid]);
    $weps = $db->fetch();
    $myhpperc = (int) ($youdata['hp'] / $youdata['maxhp'] * 100);
    $theirhpperc = (int) ($odata['hp'] / $odata['maxhp'] * 100);
    if ($myhpperc < 0) {
        $myhpperc = 0;
    } elseif ($myhpperc > 100) {
        $myhpperc = 100;
    }
    $myhpopp = 100 - $myhpperc;
    if ($theirhpperc < 0) {
        $theirhpperc = 0;
    } elseif ($theirhpperc > 100) {
        $theirhpperc = 100;
    }
    $theirhpopp = 100 - $myhpperc;
    if ($myhpperc <= 25) {
        $myhpclass = 'bg-danger';
    } elseif ($myhpperc > 25 && $myhpperc <= 50) {
        $myhpclass = 'bg-warning';
    } elseif ($myhpperc > 50 && $myhpperc <= 75) {
        $myhpclass = 'bg-success';
    } else {
        $myhpclass = 'bg-info';
    }
    if ($theirhpperc <= 25) {
        $theirhpclass = 'bg-danger';
    } elseif ($theirhpperc > 25 && $theirhpperc <= 50) {
        $theirhpclass = 'bg-warning';
    } elseif ($theirhpperc > 50 && $theirhpperc <= 75) {
        $theirhpclass = 'bg-success';
    } else {
        $theirhpclass = 'bg-info';
    } ?>
    <tr class="text-center">
        <td style="width:50%;">
            Your Health: <?php echo $func->format($youdata['hp']),'/',$func->format($youdata['maxhp']); ?><br>
            <div class="row">
                <div class="col-3">&nbsp;</div>
                <div class="col-6 text-center">
                    <div class="progress bg-dark position-relative" style="height:16px;" title="<?php echo $func->format($youdata['hp']),'/',$func->format($youdata['maxhp']),' ('.$func->format($myhpperc, 2).'%)'; ?>">
                        <div class="progress-bar <?php echo $myhpclass; ?>" style="width:<?php echo $myhpperc; ?>%"></div>
                        <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>My Health:</strong> <?php echo $func->format($myhpperc, 2); ?>%</span>
                    </div>
                </div>
                <div class="col-3">&nbsp;</div>
            </div>
        </td>
        <td>
            Opponents Health: <?php echo $func->format($odata['hp']),'/',$func->format($odata['maxhp']); ?><br>
            <div class="row">
                <div class="col-3">&nbsp;</div>
                <div class="col-6 text-center">
                    <div class="progress bg-dark position-relative" style="height:16px;" title="<?php echo $func->format($odata['hp']),'/',$func->format($odata['maxhp']),' ('.$func->format($theirhpperc, 2).'%)'; ?>">
                        <div class="progress-bar <?php echo $theirhpclass; ?>" style="width:<?php echo $theirhpperc; ?>%"></div>
                        <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>Their Health:</strong> <?php echo $func->format($theirhpperc, 2); ?>%</span>
                    </div>
                </div>
                <div class="col-3">&nbsp;</div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan=2 align='center'>Attack with:<br><?php
    if (null === $weps) {
        echo 'You don\'t have any weapons';
    } else {
        $ns = !$_GET['nextstep'] ? 1 : $_GET['nextstep'] + 2;
        $wepCnt = 0;
        $rowCnt = count($weps);
        foreach ($weps as $row) {
            ++$wepCnt; ?>
            <a href="/attack.php?nextstep=<?php echo $ns; ?>&amp;ID=<?php echo $odata['userid']; ?>&amp;wepid=<?php echo $row['itmid']; ?>"><?php echo $func->format($row['itmname']); ?></a><?php
            echo !($wepCnt % 6) ? '<br>' : ($rowCnt > $wepCnt ? ' &middot; ' : '');
        }
    }
    echo '</table>';
}
