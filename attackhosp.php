<?php
/*
MCCodes FREE
attackhosp.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';

if (null === $_GET['ID']) {
    exit('You didn\'t select a valid player');
}
if ($_SESSION['attackwon'] != $_GET['ID']) {
    exit('Cheaters don\'t get anywhere.');
}
$_SESSION['attacking'] = 0;
$db->query('SELECT userid, username, hp FROM users WHERE userid = ?');
$db->execute([$_GET['ID']]);
$odata = $db->fetch(true);
if (null === $odata) {
    exit('You beat Mr. non-existant!');
}
if (1 == $odata['hp']) {
    exit('What a cheater you are.');
}
$_SESSION['attackwon'] = 0;
$bots = [
    263 => 10000,
    264 => 10000,
    265 => 15500,
    2477 => 80000,
    2479 => 30000,
    2480 => 30000,
    2481 => 30000,
    0 => 100000,
    0 => 1400000,
    0 => 1400000,
    0 => 1400000,
    0 => 5000000,
    0 => 10000000,
];
$db->trans('start');
$db->query('UPDATE users SET hp = 1, hospital = hospital + 80 + (rand() * 230), hospreason = ? WHERE userid = ?');
$db->execute(['Hospitalized by <a href="/viewuser.php?u='.$userid.'">'.format($ir['username']).'</a>', $odata['userid']]);
$func->event_add($odata['userid'], '{user} hospitalized you.', $ir['userid']);
$db->query('INSERT INTO attacklogs (attacker, attacked, result, stole, attacklog) VALUES (?, ?, "won", -1, ?)');
$db->execute([$userid, $odata['userid'], $_SESSION['attacklog']]);
echo 'You beat '.$func->format($odata['username']).' and hospitalized them.';
if (in_array($odata['userid'], array_keys($bots))) {
    $db->query('SELECT COUNT(id) FROM challengesbeaten WHERE userid = ? AND npcid = ?');
    $db->execute([$userid, $pdata['userid']]);
    if (!$db->result()) {
        $gain = $bots[$odata['userid']];
        $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
        $db->execute([$gain, $userid]);
        $db->query('INSERT INTO challengesbeaten (userid, npcid) VALUES (?, ?)');
        $db->execute([$userid, $odata['userid']]);
        echo '<br /><br />Congrats, you have beaten the Challenge BOT '.$func->format($odata['username']).' and have earned '.$func->money($gain).'!';
    }
}
$db->trans('end');
