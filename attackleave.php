<?php
/*
MCCodes FREE
attackleave.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    exit('You didn\'t select a valid opponent');
}
$db->query('SELECT userid, username, hp, level FROM users WHERE userid = ?');
$db->execute([$_GET['ID']]);
$odata = $db->fetch(true);
if (null === $odata) {
    exit('You beat Mr. Non-existant! =O');
}
if (!array_key_exists('attackwon', $_SESSION) or $_SESSION['attackwon'] != $_GET['ID']) {
    exit('Cheaters don\'t get anywhere.');
}
if (1 == $odata['hp']) {
    exit('What a cheater u are.');
}
$_SESSION['attacking'] = 0;
$qe = $odata['level'] * $odata['level'] * $odata['level'];
$expgain = mt_rand($qe / 4, $qe / 2);
$expperc = (int) ($expgain / $ir['exp_needed'] * 100);
$name = '<a href="/viewuser.php?u='.$ir['userid'].'">'.$func->format($ir['username']).'</a>';
echo 'You beat <a href="/viewuser.php?u='.$odata['userid'].'">'.$func->format($odata['username']).'</a> and leave him on the ground and gained '.$expperc.'% EXP!';
$db->trans('start');
$db->query('UPDATE users SET exp = exp + ? WHERE userid = ?');
$db->execute([$expgain, $userid]);
$db->query('UPDATE users SET hp = 1, hospital = hospital + 20 + (rand()*20), hospreason = ? WHERE userid = ?');
$db->execute(['Attacked by '.$name, $odata['userid']]);
$func->event_add($odata['userid'], '{user} attacked you and left you lying on the street.', $ir['userid']);
$db->query('INSERT INTO attacklogs (attacker, attacked, result, attacklog) VALUES (?, ?, "won", ?)');
$db->execute([$ir['userid'], $odata['userid'], $_SESSION['attacklog']]);
$_SESSION['attackwon'] = 0;
$bots = [
    263 => 10000,
    264 => 10000,
    265 => 15000,
    536 => 100000,
    720 => 1400000,
    721 => 1400000,
    722 => 1400000,
    585 => 5000000,
    820 => 10000000,
    2477 => 80000,
    2479 => 30000,
    2480 => 30000,
    2481 => 30000,
];
if (in_array($odata['userid'], array_keys($bots))) {
    $db->query('SELECT COUNT(id) FROM challengesbeaten WHERE userid = ? AND npcid = ?');
    $db->execute([$userid, $pdata['userid']]);
    if (!$db->result()) {
        $gain = $bots[$odata['userid']];
        $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
        $db->execute([$gain, $userid]);
        $db->query('INSERT INTO challengesbeaten (userid, npcid) VALUES (?, ?)');
        $db->execute([$userid, $odata['userid']]);
        echo '<br /><br />Congrats, you have beaten the Challenge BOT '.$func->format($odata['username']).' and have earned '.$func->money($gain).'!';
    }
}
$db->trans('end');
