<?php
/*
MCCodes FREE
attacklost.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    exit('You didn\'t select a valid opponent');
}
$db->query('SELECT userid, username, level FROM users WHERE userid = ?');
$db->execute([$_GET['ID']]);
$odata = $db->fetch(true);
if (null === $odata) {
    exit('You lost to Mr. Non-existant! =O');
}
$_SESSION['attacking'] = 0;
$_SESSION['attacklost'] = 0;
$expgain = abs(($ir['level'] - $odata['level']) ^ 3);
$expgainp = $expgain / $ir['exp_needed'] * 100;
$oName = '<a href="/viewuser.php?u='.$odata['userid'].'">'.$func->format($odata['username']).'</a>';
$db->trans('start');
$db->query('UPDATE users SET exp = GREATEST(exp - ?, 0), hospital = 40 + (rand()*20), hospreason = ? WHERE userid = ?');
$db->execute(['Lost to '.$oName, $userid]);
$func->event_add($odata['userid'], '{user} attacked you and lost.', $ir['userid']);
$db->query('INSERT INTO attacklogs (attacker, attacked, result, stole, attacklog) VALUES (?, ?, "lost", 0, ?)');
$db->execute([$userid, $odata['userid'], $_SESSION['attacklog']]);
$db->trans('end');
echo 'You lost to '.$func->format($odata['username']).' and lost '.$expgainp.'% EXP!';
