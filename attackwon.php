<?php
/*
MCCodes FREE
attackwon.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    exit('You didn\'t select a valid opponent');
}
if ($_SESSION['attackwon'] != $_GET['ID']) {
    exit('Cheaters don\'t get anywhere.');
}
$db->query('SELECT userid, username, hp, level, money FROM users WHERE userid = ?');
$db->execute([$_GET['ID']]);
$odata = $db->fetch(true);
if (null === $odata) {
    exit('Your opponent doesn\'t exist');
}
if (1 == $odata['hp']) {
    exit('What a cheater u are.');
}
$_SESSION['attacking'] = 0;
$stole = (int) (mt_rand($odata['money'] / 500, $odata['money'] / 20));
$qe = $odata['level'] * $odata['level'] * $odata['level'];
$expgain = mt_rand($qe / 4, $qe / 2);
$expperc = (int) ($expgain / $ir['exp_needed'] * 100);
$name = '<a href="/viewuser.php?u='.$ir['userid'].'">'.$func->format($ir['username']).'</a>';
$_SESSION['attackwon'] = 0;
echo 'You beat '.$func->format($odata['username']).', stole '.$func->money($stole).' and gained '.$expperc.'% EXP!';
$db->trans('start');
$db->query('UPDATE users SET exp = exp + ?, money = money + ? WHERE userid = ?');
$db->execute([$expgain, $stole, $userid]);
$db->query('UPDATE users SET hp = 1, moeny = GREATEST(money - ?, 0) WHERE userid = ?');
$db->execute([$stole, $odata['userid']]);
$func->event_add($odata['userid'], '{user} attacked you and stole '.$func->money($stole), $ir['userid']);
$db->query('INSERT INTO attacklogs (attacker, attacked, result, stole, attacklog) VALUES (?, ?, "won", ?, ?)');
$db->execute([$ir['userid'], $odata['userid'], $stole, $_SESSION['attacklog']]);
$bots = [
    263 => 10000,
    264 => 10000,
    265 => 15000,
    536 => 100000,
    720 => 1400000,
    721 => 1400000,
    722 => 1400000,
    585 => 5000000,
    820 => 10000000,
    2477 => 80000,
    2479 => 30000,
    2480 => 30000,
    2481 => 30000,
];
if (in_array($odata['userid'], array_keys($bots))) {
    $db->query('SELECT COUNT(id) FROM challengesbeaten WHERE userid = ? AND npcid = ?');
    $db->execute([$userid, $pdata['userid']]);
    if (!$db->result()) {
        $gain = $bots[$odata['userid']];
        $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
        $db->execute([$gain, $userid]);
        $db->query('INSERT INTO challengesbeaten (userid, npcid) VALUES (?, ?)');
        $db->execute([$userid, $odata['userid']]);
        echo '<br /><br />Congrats, you have beaten the Challenge BOT '.$func->format($odata['username']).' and have earned '.$func->money($gain).'!';
    }
}
$db->trans('end');
