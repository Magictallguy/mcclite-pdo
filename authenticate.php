<?php
/*
MCCodes FREE
authenticate.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('NONAUTH', true);
define('NO_OUTPUT', true);
require_once __DIR__.'/lib/master.php';
$_POST['username'] = array_key_exists('username', $_POST) && is_string($_POST['username']) && strlen($_POST['username']) > 0 ? $_POST['username'] : null;
$_POST['password'] = array_key_exists('password', $_POST) && is_string($_POST['password']) && strlen($_POST['password']) > 0 ? $_POST['password'] : null;
if (null !== $_POST['username']) {
    if (null !== $_POST['password']) {
        $db->query('SELECT userid, userpass FROM users WHERE LOWER(login_name) = ?');
        $db->execute([strtolower($_POST['username'])]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if (password_verify($_POST['password'], $row['userpass'])) {
                session_regenerate_id();
                $_SESSION['loggedin'] = 1;
                $_SESSION['userid'] = $row['userid'];
                exit(header('Location: /loggedin.php'));
            } else {
                $_SESSION['error'] = 'Incorrect password';
            }
        } else {
            $_SESSION['error'] = 'Incorrect username';
        }
    } else {
        $_SESSION['error'] = 'Invalid password';
    }
} else {
    $_SESSION['error'] = 'Invalid username';
}
header('Location: /login.php');
