<?php
/*
MCCodes FREE
battletent.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (4 != $ir['location']) {
    $_SESSION['info'] = 'You can\'t challenge the Battle Tent because it\'s in the Industrial Sector.';
    exit(header('Location: /explore.php'));
}
$completed = [];
$times = [];
$db->query('SELECT userid, npcid FROM challengesbeaten');
$db->execute();
$beaten = $db->fetch();
if (null !== $beaten) {
    foreach ($beaten as $beat) {
        if ($beat['userid'] == $ir['userid']) {
            $completed[] = $beat['npcid'];
        }
        if (!array_key_exists($beat['npcid'], $times)) {
            $times[$beat['npcid']] = 1;
        } else {
            ++$times[$beat['npcid']];
        }
    }
}
$monies = [
    263 => 10000,
    264 => 10000,
    265 => 15000,
    536 => 100000,
    720 => 1400000,
    721 => 1400000,
    722 => 1400000,
    585 => 5000000,
    820 => 10000000,
    2477 => 80000,
    2479 => 30000,
    2480 => 30000,
    2481 => 30000,
];
$db->query('SELECT userid, username, level, hp, maxhp FROM users WHERE user_level = 0 ORDER BY level ASC');
$db->execute();
$bots = $db->fetch(); ?>
<h3>Battle Tent</h3>
<strong>Welcome to the battle tent! Here you can challenge NPCs for money.</strong>
<table class="table" width="75%">
    <thead>
        <tr>
            <th>Bot Name</th>
            <th>Level</th>
            <th>Times Owned</th>
            <th>Ready To Be Challenged?</th>
            <th>Money Won</th>
            <th>Challenge</th>
        </tr>
    </thead>
    <tbody><?php
if (null === $bots) {
    ?>
        <tr>
            <td colspan="6" class="center">There are no bots to challenge</td>
        </tr><?php
} else {
        foreach ($bots as $row) {?>
        <tr>
            <td><?php echo $func->format($row['username']); ?></td>
            <td><?php echo $func->format($row['level']); ?></td>
            <td><?php echo $func->format($times[$row['userid']]); ?></td>
            <td><?php echo $row['hp'] >= ($row['maxhp'] / 2) ? '<span class="text-success">Yes</span>' : '<span class="text-danger">No</span>'; ?></td>
            <td><?php echo $monies[$row['userid']]; ?></td>
            <td><?php echo in_array($row['userid'], $completed) ? '<em>Already</em>' : '<a href="/attack.php?ID='.$row['userid'].'">Attack</a>'; ?></td>
        </tr><?php
    }
    } ?>
    </tbody>
</table>
