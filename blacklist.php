<?php
/*
MCCodes FREE
blacklist.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if ($ir['donatordays'] < 1) {
    exit('This feature is for donators only.');
}
echo '<h3>Black List</h3>';
switch ($_GET['action']) {
    case 'add':
        add_enemy($db, $ir, $func);
        break;

    case 'remove':
        remove_enemy($db, $ir, $func);
        break;

    case 'ccomment':
        change_comment($db, $ir, $func);
        break;

    default:
        black_list($db, $ir, $func);
        break;
}

function black_list($db, $ir, $func)
{
    $db->query('SELECT COUNT(bl_ID) FROM blacklist WHERE bl_ADDED = ?');
    $db->execute([$ir['userid']]);
    $added = $db->result();
    $db->query('SELECT u.username, COUNT(bl.bl_ID) AS cnt, bl.bl_ADDED
        FROM blacklist AS bl
        INNER JOIN users AS u ON bl.bl_ADDED = u.userid
        GROUP BY bl.bl_ADDED
        ORDER BY cnt DESC
        LIMIT 5
    ');
    $db->execute();
    $tops = $db->fetch();
    $hated = '';
    if (null !== $tops) {
        foreach ($tops as $top) {
            $hated .= $func->username($top['bl_ADDED']).' | ';
        }
        if ('' != $hated) {
            $hated = substr($hated, 0, -3);
        }
    }
    $db->query('SELECT bl.bl_ID, bl.bl_COMMENT, u.userid, u.username, u.laston, u.donatordays
        FROM blacklist AS bl
        INNER JOIN users AS u ON bl.bl_ADDED = u.userid
        WHERE bl.bl_ADDER = ?
        ORDER BY u.username ASC
    ');
    $db->execute([$ir['userid']]);
    $rows = $db->fetch(); ?>
    <a href="/blacklist.php?action=add">&gt; Add an Enemy</a><br />
    These are the people on your black list.<br />
    Most hated: [<?php echo '' != $hated ? $hated : 'no-one'; ?>]
    <table class="table" width="90%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Attack</th>
                <th>Remove</th>
                <th>Comment</th>
                <th>Online?</th>
            </tr>
        </thead>
        <tbody><?php
    if (null === $rows) {
        ?>
            <tr>
                <td colspan="6" class="center">You haven't added anyone to your Blacklist</td>
            </tr><?php
    } else {
        $time = time();
        foreach ($rows as $row) {
            $on = $time - strtotime($row['laston']) - 900 ? '<span class="green bold">Online</span>' : '<span class="red bold">Offline</span>';
            $comment = $row['bl_COMMENT'] = '' ? 'N/A' : $func->format($row['bl_COMMENT'], true); ?>
            <tr>
                <td><?php echo $func->username($row['userid']); ?></td>
                <td><a href="/attack.php?ID=<?php echo $row['userid']; ?>">Attack</a></td>
                <td><a href="/blacklist.php?action=remove&amp;ID=<?php echo $row['bl_ID']; ?>">Remove</a></td>
                <td>
                    <?php echo $comment; ?><br /><br />
                    <span class="small">[<a href="/blacklist.php?action=ccomment&amp;ID=<?php echo $row['bl_ID']; ?>" class="italic">Change Comment</a>]</span>
                </td>
                <td><?php echo $on; ?></td>
            </tr><?php
        }
    } ?>
        </tbody>
    </table><?php
}

function add_enemy($db, $ir, $func)
{
    $_POST['ID'] = array_key_exists('ID', $_POST) && ctype_digit($_POST['ID']) && $_POST['ID'] > 0 ? $_POST['ID'] : null;
    if (null !== $_POST['ID']) {
        if ($_POST['ID'] != $ir['userid']) {
            $_POST['comment'] = array_key_exists('comment', $_POST) && is_string($_POST['comment']) && strlen($_POST['comment']) > 0 ? strip_tags(trim($_POST['comment'])) : '';
            $db->query('SELECT userid, username, user_level FROM users WHERE userid = ?');
            $db->execute([$_POST['ID']]);
            $row = $db->fetch(true);
            $backToForm = true;
            if (null !== $row) {
                $name = $func->username($row['userid']);
                if (1 == $row['user_level']) {
                    $db->query('SELECT COUNT(bl_ID) FROM blacklist WHERE bl_ADDER = ? AND bl_ADDED = ?');
                    $db->execute([$ir['userid'], $_POST['ID']]);
                    if (!$db->result()) {
                        $db->query('INSERT INTO blacklist (bl_ADDER, bl_ADDED, bl_COMMENT) VALUES (?, ?, ?)');
                        $db->execute([$ir['userid'], $row['userid'], $_POST['comment']]);
                        $_SESSION['success'] = 'You\'ve added '.$name.' to your Blacklist';
                        $backToForm = false;
                    } else {
                        $_SESSION['info'] = 'You\'ve already added '.$name.' to your Blacklist';
                    }
                } else {
                    $_SESSION['error'] = 'You can\'t add '.(!$row['user_level'] ? 'an NPC' : 'a member of staff').' to your Blacklist';
                }
            } else {
                $_SESSION['error'] = 'The player you selected doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You can\'t add yourself';
        }
        exit(header('Location: /blacklist.php'.(true === $backToForm ? '?action=add' : '')));
    } else { ?>
        Adding an enemy!
        <form action="blacklist.php?action=add" method="post">
            Enemy's ID: <input type="text" name="ID" value="<?php echo $_GET['ID']; ?>" /><br />
            Comment (optional): <br />
            <textarea name="comment" rows="7" cols="40"></textarea><br />
            <input type="submit" value="Add Enemy" />
        </form><?php
    }
}

function remove_enemy($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT b.bl_ID, b.bl_ADDED, b.bl_ADDER, u.username
            FROM blacklist AS b
            INNER JOIN users AS u ON b.bl_ADDED = u.username
            WHERE b.bl_ID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['bl_ADDER'] == $ir['userid']) {
                $db->query('DELETE FROM blacklist WHERE bl_ID = ?');
                $db->execute([$row['bl_ID']]);
                $_SESSION['success'] = 'You\'ve removed '.$func->username($row['bl_ADDED']).' from your Blacklist';
            } else {
                $_SESSION['error'] = 'That isn\'t your Blacklist entry';
            }
        } else {
            $_SESSION['error'] = 'The Blacklist entry you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid Blacklist entry';
    }
    exit(header('Location: /blacklist.php'));
}

function change_comment($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT b.bl_ID, b.bl_ADDED, b.bl_ADDER, b.bl_COMMENT, u.username
            FROM blacklist AS b
            INNER JOIN users AS u ON b.bl_ADDED = u.userid
            WHERE bl_ID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['bl_ADDER'] == $ir['userid']) {
                $name = $func->username($row['bl_ADDED']);
                if (array_key_exists('submit', $_POST)) {
                    $_POST['comment'] = array_key_exists('comment', $_POST) && is_string($_POST['comment']) && strlen($_POST['comment']) > 0 ? strip_tags(trim($_POST['comment'])) : '';
                    $db->query('UPDATE blacklist SET bl_COMMENT = ? WHERE bl_ID = ?');
                    $db->execute([$_POST['comment'], $row['bl_ID']]);
                    $_SESSION['success'] = 'You\'ve updated your comment for your Blacklist entry on '.$name;
                } else {
                    ?>
                    Changing your comment for Blacklist entry: <?php echo $name; ?><br />
                    <form action="/blacklist.php?action=ccomment&amp;ID=<?php echo $row['bl_ID']; ?>" method="post">
                        Comment:<br />
                        <textarea name="comment" rows="7" cols="40"><?php echo format($row['bl_COMMENT'], true); ?></textarea><br />
                        <input type="submit" name="submit" value="Change Comment" />
                    </form><?php
                }
            }
        }
    }
    exit(header('Location: /blacklist.php'));
}
