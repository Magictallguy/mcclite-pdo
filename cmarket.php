<?php
/*
MCCodes FREE
cmarket.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
echo '<h3>Crystal Market</h3>';
switch ($_GET['action']) {
    case 'buy':
        crystal_buy($db, $ir);
        break;

    case 'remove':
        crystal_remove($db, $ir);
        break;

    case 'add':
        crystal_add($db, $ir);
        break;

    default:
        cmarket_index($db, $ir);
        break;
}

function cmarket_index($db, $ir)
{
    $db->query('SELECT cm.cmID, cm.cmPRICE, cm.cmQTY, u.userid, u.username
        FROM crystalmarket AS cm
        INNER JOIN users AS u ON u.userid = cm.cmADDER
        ORDER BY cm.cmPRICE/cm.cmQTY ASC
    ');
    $db->execute();
    $rows = $db->fetch(); ?>
    <a href="/cmarket.php?action=add">&gt; Add A Listing</a><br /><br />
    Viewing all listings...
    <table class="table" width="75%">
        <thead>
            <tr>
                <th>Adder</th>
                <th>Qty</th>
                <th>Price each</th>
                <th>Price total</th>
                <th>Links</th>
            </tr>
        </thead>
        <tbody><?php
    if (null === $rows) {
        ?>
            <tr>
                <td colspan="5" class="center">There are no listings</td>
            </tr><?php
    } else {
        foreach ($rows as $row) {
            $each = (int) $row['cmPRICE'] / $row['cmQTY'];
            $which = $row['userid'] == $ir['userid'] ? 'remove' : 'buy';
            if ($row['userid'] == $ir['userid']); ?>
            <tr>
                <td><?php echo $func->username($row['userid']); ?></td>
                <td><?php echo $func->format($row['cmQTY']); ?></td>
                <td><?php echo $func->money($each); ?></td>
                <td><?php echo $func->money($row['cmPRICE']); ?></td>
                <td><a href="/cmarket.php?action=<?php echo $which; ?>&amp;ID=<?php echo $row['cmID']; ?>"><?php echo ucfirst($which); ?></a></td>
            </tr><?php
        }
    } ?>
        </tbody>
    </table><?php
}

function crystal_remove($db, $ir)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT cmID, cmADDER, cmQTY FROM crystalmarket WHERE cmID = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['cmADDER'] == $ir['userid']) {
                $db->trans('start');
                $db->query('UPDATE users SET crystals = crystals + ? WHERE userid = ?');
                $db->execute([$row['cmQTY'], $ir['userid']]);
                $db->query('DELETE FROM crystalmarket WHERE cmID = ?');
                $db->execute([$row['cmID']]);
                $db->trans('end');
                $_SESSION['success'] = 'You\'ve removed your '.$func->format($row['cmQTY']).'-crystal listing from the market';
            } else {
                $_SESSION['error'] = 'That isn\'t your listing';
            }
        } else {
            $_SESSION['error'] = 'The listing you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid listing';
    }
    exit(header('Location: /crystalmarket.php'));
}

function crystal_buy($db, $ir)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT c.cmID, c.cmADDER, c.cmPRICE, c.cmQTY, u.username
            FROM crystalmarket AS c
            INNER JOIN users AS u ON u.userid = c.cmADDER
            WHERE c.cmID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['cmADDER'] != $ir['userid']) {
                if ($ir['money'] >= $row['cmPRICE']) {
                    $db->trans('start');
                    $db->query('UPDATE users SET crystals = crystals + ?, money = money - ? WHERE userid = ?');
                    $db->execute([$row['cmQTY'], $row['cmPRICE'], $ir['userid']]);
                    $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
                    $db->execute([$row['cmPRICE'], $row['cmADDER']]);
                    $func->event_add($row['cmADDER'], '{user} purchased your '.$func->format($row['cmQTY']).'-crystal listing from the Crystal Market for '.$func->money($row['cmPRICE']), $ir['userid']);
                    $db->query('DELETE FROM crystalmarket WHERE cmID = ?');
                    $db->execute([$row['cmID']]);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve purchased '.$func->username($row['cmADDER']).'\'s '.$func->format($row['cmQTY']).'-crystal listing for '.$func->money($row['cmPRICE']);
                } else {
                    $_SESSION['error'] = 'You don\'t have enough money';
                }
            } else {
                $_SESSION['warning'] = 'You can\'t purchase your own listing';
            }
        } else {
            $_SESSION['error'] = 'The listing you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid listing';
    }
    exit(header('Location: /crystalmarket.php'));
}

function crystal_add($db, $ir)
{
    global $ir, $c, $userid, $h;
    if (array_key_exists('submit', $_POST)) {
        $_POST['amnt'] = array_key_exists('amnt', $_POST) && ctype_digit($_POST['amnt']) && $_POST['amnt'] > 0 ? $_POST['amnt'] : null;
        $_POST['price'] = array_key_exists('price', $_POST) && ctype_digit($_POST['price']) && $_POST['price'] > 0 ? $_POST['price'] : null;
        if (null !== $_POST['amnt']) {
            if (null !== $_POST['price']) {
                if ($ir['crystals'] >= $_POST['amnt']) {
                    $total = $_POST['amnt'] * $_POST['price'];
                    $db->trans('start');
                    $db->query('INSERT INTO crystalmarket (cmADDER, cmQTY, cmPRICE) VALUES (?, ?, ?)');
                    $db->execute([$ir['userid'], $_POST['amnt'], $_POST['price']]);
                    $db->query('UPDATE users SET crystals = crystals - ? WHERE userid = ?');
                    $db->execute([$_POST['amnt'], $ir['userid']]);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve added '.$func->crystals($_POST['amnt']).' to the market for a total price of '.$func->money($total);
                } else {
                    $_SESSION['error'] = 'You don\'t have that many crystals';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t enter a valid price';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid amount';
        }
        exit(header('Location: /crystalmarket.php'));
    } else {
        ?>
        <strong>Adding a listing...</strong><br /><br />
        You have <strong><?php echo $func->format($ir['crystals']); ?></strong> crystal<?php echo $func->s($ir['crystals']); ?> that you can add to the market.
        <form action="cmarket.php?action=add" method="post">
            <table width="50%" border="2">
                <tr>
                    <td>Crystals:</td>
                    <td><input type="text" name="amnt" value="<?php echo $ir['crystals']; ?>" /></td>
                </tr>
                <tr>
                    <td>Price Each:</td>
                    <td><input type="text" name="price" value="200" /></td>
                </tr>
                <tr>
                    <td colspan="2" class="center"><input type="submit" name="submit" value="Add To Market" /></td>
                </tr>
            </table>
        </form><?php
    }
}
