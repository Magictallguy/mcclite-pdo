<?php
/*
MCCodes FREE
criminal.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$groups = [];
$db->query('SELECT cgID, cgNAME FROM crimegroups ORDER BY cgORDER ASC');
$db->execute();
$cGroups = $db->fetch();
if (null === $cGroups) {
    $_SESSION['info'] = 'The crimes haven\'t been correctled configured';
    exit(header('Location: /index.php'));
}
foreach ($cGroups as $group) {
    $groups[$group['cgID']] = [];
    $groups[$group['cgID']]['group'] = $group['cgNAME'];
}
$db->query('SELECT crimeID, crimeNAME, crimeBRAVE, crimeGROUP FROM crimes ORDER BY crimeBRAVE ASC');
$db->execute();
$rows = $db->fetch();
if (null === $rows) {
    $_SESSION['info'] = 'There are no crimes to commit';
    exit(header('Location: /index.php'));
}
foreach ($rows as $row) {
    $groups[$row['crimeGROUP']][] = $row;
} ?>
<div class="row">
    <div class="col">
        <h3 class="page-title">Criminal Center</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th width="60%">Crime</th>
                        <th width="20%">Brave</th>
                        <th width="20%">Do</th>
                    </tr>
                </thead>
                <tbody>
                    <tr><?php
    $cnt = 0;
    $lt = '';
foreach ($groups as $group => $data) {
    if ($lt != $data['group']) {?>
                    <tr>
                        <td colspan="3" class="thead"><?php echo $func->format($data['group']); ?></td>
                    </tr><?php
        $lt = $data['group'];
    }
    foreach ($data as $row) {
        if (!is_array($row)) {
            continue;
        } ?>
                    <tr>
                        <td><?php echo $func->format($row['crimeNAME']); ?></td>
                        <td><?php echo $func->format($row['crimeBRAVE']); ?></td>
                        <td><?php echo $ir['brave'] >= $row['crimeBRAVE'] ? '<a href="/docrime.php?ID='.$row['crimeID'].'">Do</a>' : '<em>Not enough</em>'; ?></td>
                    </tr><?php
    }
} ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
