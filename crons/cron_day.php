<?php
/*
MCCodes FREE
crons/cron_day.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
if (!defined('CRON_OVERRIDE')) {
    define('NONAUTH', true);
    define('NO_OUTPUT', true);
    require_once dirname(__DIR__).'/lib/master.php';
    $cron_code = 'zv8tm9LlmWiXvLZa7NSLZVTvgQuxYW';
    if (isset($argv)) {
        $args = parse_str($argv[1], $param);
        $_GET['code'] = $param['code'];
    }
    if (!array_key_exists('code', $_GET) or $_GET['code'] != $cron_code) {
        exit;
    }
}
$db->query('UPDATE fedjail SET fed_days = fed_days - 1');
$db->execute();
$db->query('SELECT fed_userid FROM fedjail WHERE fed_days <= 0');
$db->execute();
$fedRows = $db->fetch();
$ids = [];
if (null !== $fedRows) {
    foreach ($fedRows as $row) {
        $ids[] = $row['fed_userid'];
    }
}
if (count($ids) > 0) {
    $db->query('UPDATE users SET fedjail = 0 WHERE userid IN('.implode(',', $ids).')');
    $db->execute();
}
$db->query('DELETE FROM fedjail WHERE fed_days <= 0');
$db->execute();
$db->query('UPDATE users SET
    daysold = daysold + 1,
    mailban = mailban - IF(mailban > 0, 1, 0),
    donatordays = donatordays - IF(donatordays > 0, 1, 0),
    cdays = cdays - IF(course > 0, 1, 0),
    bankmoney = bankmoney + IF(bankmoney > 0, bankmoney / 50, 0),
    cybermoney = cybermoney + IF(cybermoney > 0, cybermoney / 100 * 7, 0)
');
$db->execute();
$db->query('SELECT userid, course FROM users WHERE cdays <= 0 AND course > 0');
$db->execute();
$courseRows = $db->fetch();
$course_cache = [];
if (null !== $courseRows) {
    foreach ($courseRows as $row) {
        if (!array_key_exists($row['course'], $course_cache)) {
            $db->query('SELECT crSTR, crGUARD, crLABOUR, crAGIL, crIQ, crNAME FROM courses WHERE crID = ?');
            $db->execute([$row['course']]);
            $coud = $db->fetch(true);
            $course_cache[$row['course']] = $coud;
        } else {
            $coud = $course_cache[$row['course']];
        }
        $upd = '';
        $ev = '';
        $query = '';
        $params = [];
        if ($coud['crSTR'] > 0) {
            $upd .= ', us.strength = us.strength + ?';
            $params[] = $coud['crSTR'];
            $ev .= ', '.format($coud['crSTR']).' strength';
        }
        if ($coud['crGUARD'] > 0) {
            $upd .= ', us.guard = us.guard + ?';
            $params[] = $coud['crGUARD'];
            $ev .= ', '.format($coud['crGUARD']).' guard';
        }
        if ($coud['crLABOUR'] > 0) {
            $upd .= ', us.labour = us.labour + ?';
            $params[] = $coud['crLABOUR'];
            $ev .= ', '.format($coud['crLABOUR']).' labour';
        }
        if ($coud['crAGIL'] > 0) {
            $upd .= ', us.agility = us.agility + ?';
            $params[] = $coud['crAGIL'];
            $ev .= ', '.format($coud['crAGIL']).' agility';
        }
        if ($coud['crIQ'] > 0) {
            $upd .= ', us.IQ = us.IQ + ?';
            $params[] = $coud['crIQ'];
            $ev .= ', '.format($coud['crIQ']).' IQ';
        }
        $params[] = $row['userid'];
        $db->query('INSERT INTO coursesdone (userid, courseid) VALUES (?, ?)');
        $db->execute([$row['userid'], $row['course']]);
        $ev = substr($ev, 1);
        $db->query('UPDATE users AS u
            INNER JOIN userstats AS us ON u.userid = us.userid
            SET u.course = 0'.$query.'
            WHERE u.userid = ?
        ');
        $db->execute($params);
        event_add($row['userid'], 'Congratulations, you completed the '.format($coud['crNAME']).' and gained '.$ev);
    }
}
$db->query('TRUNCATE TABLE votes');
$db->execute();
$db->query('UPDATE cron_times SET last_run = NOW() WHERE cron = "day_one"');
$db->execute();
