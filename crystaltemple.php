<?php
/*
MCCodes FREE
crystaltemple.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$_GET['spend'] = array_key_exists('spend', $_GET) && in_array($_GET['spend'], ['refill', 'IQ', 'IQ2', 'money', 'money2']) ? $_GET['spend'] : null; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Crystal Temple</h3>
        <p>
            Welcome to the crystal temple!
        </p>
        <p>
            You have <strong><?php echo $func->format($ir['crystals']); ?></strong> crystal<?php echo $func->s($ir['crystals']); ?>.<br>
            What would you like to spend your crystals on?<br><br>
            <a href="/crystaltemple.php?spend=refill">Energy Refill - <?php echo $func->crystals(12); ?></a> &middot;
            <a href="/crystaltemple.php?spend=IQ">IQ - 5 IQ per crystal</a> &middot;
            <a href="/crystaltemple.php?spend=money">Money - <?php echo $func->money(200); ?> per crystal</a>
        </p>
    </div>
</div><?php
if ('refill' == $_GET['spend']) {
    if ($ir['crystals'] >= 12) {
        if ($ir['energy'] < $ir['maxenergy']) {
            $db->query('UPDATE users SET energy = maxenergy, crystals = crystals - 12 WHERE userid = ?');
            $db->execute([$ir['userid']]);
            $_SESSION['success'] = 'You\'ve paid 12 crystals to refill your energy bar.';
        } else {
            $_SESSION['info'] = 'You already have full energy.';
        }
    } else {
        $_SESSION['error'] = 'You don\'t have enough crystals!';
    }
    exit(header('Location: /crystaltemple.php'));
} elseif ('IQ' == $_GET['spend']) { ?>
<div class="row">
    <div class="col">
        <h4 class="page-subtitle">IQ</h4>
        Type in the amount of crystals you want to swap for IQ.<br>
        You have <strong><?php echo $func->format($ir['crystals']); ?></strong> crystal<?php echo $func->s($ir['crystals']); ?>.<br>
        One crystal = 5 IQ.
        <form action="/crystaltemple.php?spend=IQ2" method="post" class="form">
            <div class="form-group">
                <label for="crystals" class="form-label">Crystals</label>
                <input type="number" name="crystals" id="crystals" class="form-control bg-dark text-light" required autofocus>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-retweet"></span>
                    Swap
                </button>
            </div>
        </form>
    </div>
</div><?php
} elseif ('IQ2' == $_GET['spend']) {
    $_POST['crystals'] = array_key_exists('crystals', $_POST) && ctype_digit($_POST['crystals']) && $_POST['crystals'] > 0 ? $_POST['crystals'] : null;
    if (null !== $_POST['crystals']) {
        if ($ir['crystals'] >= $_POST['crystals']) {
            $iqgain = $_POST['crystals'] * 5;
            $db->query('UPDATE users AS u
                INNER JOIN userstats AS us ON u.userid = us.userid
                SET u.crystals = u.crystals - ?, us.IQ = us.IQ + ?
                WHERE u.userid = ?
            ');
            $db->execute([$_POST['crystals'], $iqgain, $ir['userid']]);
            $_SESSION['success'] = 'You\'ve traded '.$func->crystals($_POST['crystals']).' for '.$func->format($iqgain).' IQ';
        } else {
            $_SESSION['error'] = 'You don\'t have that many crystals';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid amount of crystals';
    }
    exit(header('Location: /crystaltemple.php'));
} elseif ('money' == $_GET['spend']) {
    ?>
<div class="row">
    <div class="col">
        <h4 class="page-subtitle">Money</h4>
        <p>
            Type in the amount of crystals you want to swap for $$$.<br>
            You have <strong><?php echo $func->format($ir['crystals']); ?></strong> crystal<?php echo $func->s($ir['crystals']); ?>.<br>
            One crystal = <?php echo $func->money(200); ?>.
        </p>
        <form action="/crystaltemple.php?spend=money2" method="post" class="form">
            <div class="form-group">
                <label for="crystals" class="form-label">Crystals</label>
                <input type="number" name="crystals" id="crystals" class="form-control bg-dark text-light" required autofocus>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-retweet"></span>
                    Swap
                </button>
            </div>
        </form>
    </div>
</div><?php
} elseif ('money2' == $_GET['spend']) {
        $_POST['crystals'] = array_key_exists('crystals', $_POST) && ctype_digit($_POST['crystals']) && $_POST['crystals'] > 0 ? $_POST['crystals'] : null;
        if (null !== $_POST['crystals']) {
            if ($ir['crystals'] >= $_POST['crystals']) {
                $gain = $_POST['crystals'] * 200;
                $db->query('UPDATE users SET crystals = crystals - ?, money = money + ? WHERE userid = ?');
                $db->execute([$_POST['crystals'], $gain, $ir['userid']]);
                $_SESSION['success'] = 'You\'ve traded '.$func->crystals($_POST['crystals']).' for '.$func->money($gain);
            } else {
                $_SESSION['error'] = 'You don\'t have that many crystals';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid amount of crystals';
        }
        exit(header('Location: /crystaltemple.php'));
    }
