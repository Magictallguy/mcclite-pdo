<?php
require_once __DIR__.'/lib/master.php';
$accountCost = 10000000;
$depFeePerc = 0.15;
$depFeeMax = 1500000;
$witFeePerc = 0.075;
$witFeeMax = 750000; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Cyber Bank</h3>
    </div>
</div><?php
if ($ir['cybermoney'] > -1) {
    switch ($_GET['action']) {
        case 'dep':
            $_POST['deposit'] = $ir['money'];
            // no break
        case 'deposit':
            deposit($db, $ir, $func, $depFeePerc, $depFeeMax);
            break;

        case 'withdraw':
            withdraw($db, $ir, $func, $witFeePerc, $witFeeMax);
            break;

        default:
            index($db, $ir, $func, $depFeePerc, $depFeeMax, $witFeePerc, $witFeeMax);
            break;
    }
} else {
    if (array_key_exists('buy', $_GET)) {
        if ($ir['money'] >= $accountCost) {
            $db->query('UPDATE users SET money = money - ?, cybermoney = 0 WHERE userid = ?');
            $db->execute([$accountCost, $ir['userid']]);
            $_SESSION['success'] = 'Congratulations, you bought a bank account for '.$func->money($accountCost).'!';
            exit(header('Location: /cyberbank.php'));
        } else {
            echo 'You don\'t have enough money to open an account. You require '.$func->money($accountCost).'<br><a href="explore.php">Back to town...</a>';
        }
    } else {
        echo 'Open a bank account today, just '.$func->money($accountCost).'!<br><a href="cyberbank.php?buy">&gt; Yes, sign me up!</a>';
    }
}

function index($db, $ir, $func, $depFeePerc, $depFeeMax, $witFeePerc, $witFeeMax)
{ ?>
<div class="row">
    <div class="col">
        <p>
            <strong>You currently have <?php echo $func->money($ir['cybermoney']); ?> in the bank.</strong><br>
            At the end of each day, your bank balance will go up by 2%.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <p>
            <strong>Deposit Money</strong><br><?php
    if ($depFeePerc > 0) {
        ?>
            It will cost you <?php echo $depFeePerc * 100; ?>% of the money you deposit, rounded up. <?php
        if ($depFeeMax > 0) {
            ?>
            The maximum fee is <?php echo $func->money($depFeeMax);
        }
    } else {
        ?>
            There is no fee on deposits<?php
    } ?>
        </p>
        <form action="cyberbank.php?action=deposit" method="post" class="form">
            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">&dollar;</span>
                    </div>
                    <input type="number" name="deposit" id="deposit" value="<?php echo $ir['money']; ?>" class="form-control bg-dark text-light" aria-label="Amount" required autofocus>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-dollar"></span>
                    Deposit
                </button>
            </div>
        </form>
    </div>
    <div class="col-6">
        <p>
            <strong>Withdraw Money</strong><br><?php
    if ($witFeePerc > 0) {
        ?>
            It will cost you <?php echo $witFeePerc * 100; ?>% of the money you withdraw, rounded up. <?php
        if ($witFeeMax > 0) {
            ?>
            The maximum fee is <?php echo $func->money($witFeeMax);
        }
    } else {
        ?>
            There is no fee on withdrawals<?php
    } ?>
        </p>
        <form action="cyberbank.php?action=withdraw" method="post" class="form">
            <div class="form-group">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">&dollar;</span>
                    </div>
                    <input type="number" name="withdraw" id="withdraw" value="<?php echo $ir['cybermoney']; ?>" class="form-control bg-dark text-light" aria-label="Amount" required autofocus>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-dollar"></span>
                    Withdaw
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function deposit($db, $ir, $func, $depFeePerc, $depFeeMax)
{
    $_POST['deposit'] = array_key_exists('deposit', $_POST) && ctype_digit($_POST['deposit']) && $_POST['deposit'] > 0 ? $_POST['deposit'] : null;
    if (null !== $_POST['deposit']) {
        if ($ir['money'] >= $_POST['deposit']) {
            $fee = $depFeePerc > 0 ? ceil($_POST['deposit'] * $depFeePerc) : 0;
            if ($depFeeMax > 0 && $fee > $depFeeMax) {
                $fee = $depFeeMax;
            }
            $gain = $_POST['deposit'] - $fee;
            $db->query('UPDATE users SET cybermoney = cybermoney + ?, money = money - ? WHERE userid = ?');
            $db->execute([$gain, $_POST['deposit'], $ir['userid']]);
            $_SESSION['success'] = 'You deposit '.$func->money($_POST['deposit']).' at the slip point.<br>'.
            ($fee > 0 ? 'Fee: '.$func->money($fee).'<br>' : '').
            $func->money($gain).' has been added to your cyber account';
        } else {
            $_SESSION['error'] = 'You don\'t have enough money to make that deposit';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid amount';
    }
    exit(header('Location: /cyberbank.php'));
}
function withdraw($db, $ir, $func, $witFeePerc, $witFeeMax)
{
    $_POST['withdraw'] = array_key_exists('withdraw', $_POST) && ctype_digit($_POST['withdraw']) && $_POST['withdraw'] > 0 ? $_POST['withdraw'] : null;
    if (null !== $_POST['withdraw']) {
        if ($ir['cybermoney'] >= $_POST['withdraw']) {
            $fee = $witFeePerc > 0 ? ceil($_POST['withdraw'] * $witFeePerc) : 0;
            if ($witFeeMax > 0 && $fee > $witFeeMax) {
                $fee = $witFeeMax;
            }
            $gain = $_POST['withdraw'] - $fee;
            $db->query('UPDATE users SET cybermoney = cybermoney - ?, money = money + ? WHERE userid = ?');
            $db->execute([$_POST['withdraw'], $gain, $ir['userid']]);
            $_SESSION['success'] = 'You request '.$func->money($_POST['withdraw']).' from your account.<br>'.
            ($fee > 0 ? 'Fee: '.$func->money($fee).'<br>' : '').
            $func->money($gain).' has been withdrawn. You now have '.$func->money($ir['money'] + $gain).' on hand';
        } else {
            $_SESSION['error'] = 'You don\'t have enough money to make that withdrawal';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid amount';
    }
    exit(header('Location: /cyberbank.php'));
}
