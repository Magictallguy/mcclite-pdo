<?php
/*
MCCodes FREE
dlarchive.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('NO_OUTPUT', true);
require_once __DIR__.'/lib/master.php';
$_GET['a'] = array_key_exists('a', $_GET) && in_array($_GET['a'], ['inbox', 'outbox']) ? $_GET['a'] : null;
if (null === $_GET['a']) {
    $_SESSION['error'] = 'You didn\'t select a valid box to export';
    exit(header('Location: /mailbox.php'));
}
if ('inbox' == $_GET['a']) {
    // We'll be outputting a PDF
    header('Content-type: text/html');

    // It will be called downloaded.pdf
    header('Content-Disposition: attachment; filename="inbox_archive_'.$userid.'_'.time().'.htm"');
    $db->query('SELECT m.mail_time, m.mail_subject, m.mail_text, u.userid, u.username
        FROM mail AS m
        LEFT JOIN users AS u ON m.mail_from = u.userid
        WHERE m.mail_to = ?
        ORDER BY mail_time DESC
    ');
    $db->execute([$ir['userid']]);
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'Your inbox is clear - nothing to export';
        exit(header('Location: /mailbox.php'));
    } ?>
    <table width="75%" border="2">
        <thead>
            <tr>
                <th>From</th>
                <th>Sent</th>
                <th>Subject/Message</th>
            </tr>
        </thead>
        <tbody><?php
    foreach ($rows as $row) {
        $date = new \DateTime($row['mail_time']); ?>
            <tr>
                <td><?php echo $row['userid'] > 0 ? $func->format($row['username']).' ['.$row['userid'].']' : '<strong>System</strong>'; ?></td>
                <td><?php echo $date->format('F j, Y, g:i:s a'); ?></td>
                <td>
                    Subject: <?php echo '' != $row['mail_subject'] ? $func->format($row['mail_subject']) : 'None'; ?><br />
                    <?php echo $func->format($row['mail_text'], true); ?>
                </td>
            </tr><?php
    } ?>
        </tbody>
    </table><?php
} elseif ('outbox' == $_GET['a']) {
        // We'll be outputting a PDF
        header('Content-type: text/html');

        // It will be called downloaded.pdf
        header('Content-Disposition: attachment; filename="outbox_archive_'.$userid.'_'.time().'.htm"');
        $db->query('SELECT m.mail_time, m.mail_subject, m.mail_text, u.userid, u.username
        FROM mail AS m
        LEFT JOIN users AS u ON m.mail_to = u.userid
        WHERE m.mail_from = ?
        ORDER BY mail_time DESC
    ');
        $db->execute([$ir['userid']]);
        $rows = $db->fetch();
        if (null === $rows) {
            $_SESSION['info'] = 'Your outbox is clear - nothing to export';
            exit(header('Location: /mailbox.php'));
        } ?>
    <table width="75%" border="2">
        <thead>
            <tr>
                <th>From</th>
                <th>Sent</th>
                <th>Subject/Message</th>
            </tr>
        </thead>
        <tbody><?php
    foreach ($rows as $row) {
        $date = new \DateTime($row['mail_time']); ?>
            <tr>
                <td><?php echo $row['userid'] > 0 ? $func->format($row['username']).' ['.$row['userid'].']' : '<strong>System</strong>'; ?></td>
                <td><?php echo $date->format('F j, Y, g:i:s a'); ?></td>
                <td>
                    Subject: <?php echo '' != $row['mail_subject'] ? $func->format($row['mail_subject']) : 'None'; ?><br />
                    <?php echo $func->format($row['mail_text'], true); ?>
                </td>
            </tr><?php
    } ?>
        </tbody>
    </table><?php
    }
