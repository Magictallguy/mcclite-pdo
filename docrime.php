<?php
/*
MCCodes FREE
docrime.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    $_SESSION['error'] = 'You didn\'t select a valid crime';
    exit(header('Location: /criminal.php'));
}
$db->query('SELECT * FROM crimes WHERE crimeID = ?');
$db->execute([$_GET['ID']]);
$row = $db->fetch(true);
if (null === $row) {
    $_SESSION['error'] = 'The crime you selected doesn\'t exist';
    exit(header('Location: /criminal.php'));
}
if ($row['crimeBRAVE'] > $ir['brave']) {
    $_SESSION['error'] = 'You\'re not brave enough to attempt that crime';
    exit(header('Location: /criminal.php'));
}
$ir['brave'] -= $row['crimeBRAVE'];
$ec = '$sucrate='.str_replace(['LEVEL', 'EXP', 'WILL', 'IQ'], [$ir['level'], $ir['exp'], $ir['will'], $ir['IQ']], $row['crimePERCFORM']).';';
eval($ec);
echo $func->format($row['crimeITEXT'], true);
$db->query('UPDATE users SET brave = brave - ? WHERE userid = ?');
$db->execute([$row['crimeBRAVE'], $ir['userid']]);
if (mt_rand(1, 100) <= $sucrate) {
    echo '<span class="text-success">',str_replace('{money}', $row['crimeSUCCESSMUNY'], $func->format($row['crimeSTEXT'], true)),'</span>';
    $money = $row['crimeSUCCESSMUNY'];
    $exp = (int) ($row['crimeSUCCESSMUNY'] / 8);
    $db->query('UPDATE users SET money = money + ?, exp = exp + ? WHERE userid = ?');
    $db->execute([$money, $exp, $ir['userid']]);
} else {
    echo '<span class="text-warning">',$func->format($row['crimeFTEXT'], true),'</span>';
}
if ($ir['brave'] >= $row['crimeBRAVE']) {
    ?>
<br><a href="/docrime.php?ID=<?php echo $row['crimeID']; ?>">Try Again</a><?php
} ?>
<br><a href="/criminal.php">Crimes</a>
