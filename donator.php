<?php
/*
MCCodes FREE
donator.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$game_url = $func->determine_game_urlbase(); ?>
<h3>Donations</h3>
<strong>[<a href="/willpotion.php">Buy Will Potions</a>]</strong><br />
If you become a donator to {GAME_NAME}, you will receive (each time you donate):<br />
<strong>1st Offer ($3.00):</strong>
<ul>
    <li><?php echo $func->money(5000); ?> game money</li>
    <li>50 crystals</li>
    <li>50 IQ, the hardest stat to get in the game!</li>
    <li>30 days Donator Status: Red name + cross next to your name</li>
    <li><strong>NEW!</strong> Friend and Black Lists</li>
    <li><strong>NEW!</strong> 17% Energy every 5 mins instead of 8%</li>
</ul><br />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="{PAYPAL}">
    <input type="hidden" name="item_name" value="{GAME_NAME} Donation for (<?php echo $ir['userid']; ?>) (Pack 1)">
    <input type="hidden" name="amount" value="3.00">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="https://<?php echo $game_url; ?>/donator.php?action=done&type=standard">
    <input type="hidden" name="cancel_return" value="https://<?php echo $game_url; ?>/donator.php?action=cancel">
    <input type="hidden" name="cn" value="Your Player ID">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="tax" value="0">
    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
</form>
<strong>2nd Offer ($3.00):</strong>
<ul>
    <li>100 crystals</li>
    <li>30 days Donator Status: Red name + cross next to your name</li>
    <li><strong>NEW!</strong> Friend and Black Lists</li>
    <li><strong>NEW!</strong> 17% Energy every 5 mins instead of 8%</li>
</ul><br />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="{PAYPAL}">
    <input type="hidden" name="item_name" value="{GAME_NAME} Donation for (<?php echo $ir['userid']; ?>) (Pack 2)">
    <input type="hidden" name="amount" value="3.00">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="https://<?php echo $game_url; ?>/donator.php?action=done&type=crystals">
    <input type="hidden" name="cancel_return" value="https://<?php echo $game_url; ?>/donator.php?action=cancel">
    <input type="hidden" name="cn" value="Your Player ID">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="tax" value="0">
    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
</form>
<strong>3rd Offer ($3.00):</strong>
<ul>
    <li>120 IQ, the hardest stat to get in the game!</li>
    <li>30 days Donator Status: Red name + cross next to your name</li>
    <li><strong>NEW!</strong> Friend and Black Lists</li>
    <li><strong>NEW!</strong> 17% Energy every 5 mins instead of 8%</li>
</ul><br />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="{PAYPAL}">
    <input type="hidden" name="item_name" value="{GAME_NAME} Donation for (<?php echo $ir['userid']; ?>) (Pack 3)">
    <input type="hidden" name="amount" value="3.00">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="https://<?php echo $game_url; ?>/donator.php?action=done&type=iq">
    <input type="hidden" name="cancel_return" value="https://<?php echo $game_url; ?>/donator.php?action=cancel">
    <input type="hidden" name="cn" value="Your Player ID">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="tax" value="0">
    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
</form>
<strong>4th Offer ($5.00 pack):</strong>
<ul>
    <li><?php echo $func->money(15000); ?> game money</li>
    <li>75 crystals</li>
    <li>80 IQ, the hardest stat to get in the game!</li>
    <li>55 days Donator Status: Red name + cross next to your name</li>
    <li><strong>NEW!</strong> Friend and Black Lists</li>
    <li><strong>NEW!</strong> 17% Energy every 5 mins instead of 8%</li>
</ul><br />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="{PAYPAL}">
    <input type="hidden" name="item_name" value="{GAME_NAME} Donation for (<?php echo $ir['userid']; ?>) (Pack 4)">
    <input type="hidden" name="amount" value="5.00">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="https://<?php echo $game_url; ?>/donator.php?action=done&type=fivedollars">
    <input type="hidden" name="cancel_return" value="https://<?php echo $game_url; ?>/donator.php?action=cancel">
    <input type="hidden" name="cn" value="Your Player ID">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="tax" value="0">
    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
</form>
<strong>5th Offer ($10.00 pack):</strong>
<ul>
    <li><?php echo $func->money(35000); ?> game money</li>
    <li>160 crystals</li>
    <li>180 IQ, the hardest stat to get in the game!</li>
    <li>A free Rifle valued at <?php echo $func->money(25000); ?></li>
    <li>115 days Donator Status: Red name + cross next to your name</li>
    <li><strong>NEW!</strong> Friend and Black Lists</li>
    <li><strong>NEW!</strong> 17% Energy every 5 mins instead of 8%</li>
</ul><br />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="{PAYPAL}">
    <input type="hidden" name="item_name" value="{GAME_NAME} Donation for (<?php echo $ir['userid']; ?>) (Pack 5)">
    <input type="hidden" name="amount" value="10.00">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="return" value="https://<?php echo $game_url; ?>/donator.php?action=done&type=tendollars">
    <input type="hidden" name="cancel_return" value="https://<?php echo $game_url; ?>/donator.php?action=cancel">
    <input type="hidden" name="cn" value="Your Player ID">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="tax" value="0">
    <input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but21.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
</form>
