<?php
/*
MCCodes FREE
education.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
echo '<h3>Schooling</h3>';
if ($ir['course'] > 0) {
    $db->query('SELECT crID, crNAME FROM courses WHERE crID = ?');
    $db->execute([$ir['course']]);
    $name = $db->result(1);
    exit('You are currently doing the '.$func->format($name).', you have '.$func->format($ir['cdays']).' day'.$func->s($ir['cdays']).' remaining.');
} else {
    if (null !== $_GET['ID']) {
        $db->query('SELECT crID, crNAME, crCOST, crDAYS FROM courses WHERE crID = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $db->query('SELECT COUNT(id) FROM coursesdone WHERE userid = ? AND courseid = ?');
            $db->execute([$ir['userid'], $_GET['ID']]);
            if (!$db->result()) {
                if ($ir['money'] >= $row['crCOST']) {
                    $db->query('UPDATE users SET course = ?, cdays = ?, money = money - ? WHERE userid = ?');
                    $db->execute([$_GET['ID'], $row['crDAYS'], $row['crCOST'], $ir['userid']]);
                    $_SESSION['success'] = 'You\'ve started the course: '.$func->format($row['crNAME']).', costing you '.$func->money($row['crCOST']).'.<br />It will be completed in '.$func->format($row['crDAYS']).' day'.$func->s($row['crDAYS']);
                } else {
                    $_SESSION['error'] = 'You don\'t have enough money';
                }
            } else {
                $_SESSION['info'] = 'You\'ve already completed the course: '.$func->format($row['crNAME']);
            }
        } else {
            $_SESSION['error'] = 'The course you selected doesn\'t exist';
        }
        exit(header('Location: /education.php'));
    } else {
        $completed = [];
        $db->query('SELECT id, courseid FROM coursesdone WHERE userid = ?');
        $db->execute([$ir['userid']]);
        $dones = $db->fetch();
        if (null !== $dones) {
            foreach ($dones as $done) {
                $completed[] = $done['courseid'];
            }
        }
        $db->query('SELECT crID, crNAME, crDESC, crCOST FROM courses ORDER BY crCOST ASC');
        $db->execute();
        $rows = $db->fetch(); ?>
        Here is a list of available courses.<br />
        <table class="table" width="75%">
            <thead>
                <tr>
                    <th>Course</th>
                    <th>Description</th>
                    <th>Cost</th>
                    <th>Take</th>
                </tr>
            </thead>
            <tbody><?php
        if (null === $rows) {
            ?>
                <tr>
                    <td colspan="4" class="center">There are no courses available</td>
                </tr><?php
        } else {
            foreach ($rows as $row) {
                $do = in_array($row['crID'], $completed) ? '<em>Done</em>' : ($ir['money'] >= $row['crCOST'] ? '<a href="/education.php?ID='.$row['crID'].'">Take</a>' : '<em>Can\'t afford</em>'); ?>
                <tr>
                    <td><?php echo $func->format($row['crNAME']); ?></td>
                    <td><?php echo $func->format($row['crDESC'], true); ?></td>
                    <td><?php echo $func->money($row['crCOST']); ?></td>
                    <td><?php echo $do; ?></td>
                </tr><?php
            }
        } ?>
            </tbody>
        </table><?php
    }
}
