<?php
/*
MCCodes FREE
estate.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$db->query('SELECT hNAME, hPRICE, hWILL FROM houses WHERE hWILL = ?');
$db->execute([$ir['maxwill']]);
$curr = $db->fetch(true);
if (null !== $_GET['ID']) {
    $db->query('SELECT hNAME, hPRICE, hWILL FROM houses WHERE hID = ?');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['hWILL'] > $curr['hWILL']) {
            $cost = $row['hPRICE'];
            $partEx = 0;
            if ($curr['hPRICE'] > 0) {
                $partEx = $curr['hPRICE'] * 0.6;
                $cost -= $partEx;
            }
            if ($ir['money'] >= $cost) {
                $db->query('UPDATE users SET money = money - ?, will = 0, maxwill = ? WHERE userid = ?');
                $db->execute([$cost, $row['hWILL'], $ir['userid']]);
                $_SESSION['success'] = 'Congrats, you\'ve bought the '.$func->format($row['hNAME']).' for '.$func->money($cost).($partEx > 0 ? '<br />Part-exchanged previous house for '.$func->money($partEx) : '');
            } else {
                $_SESSION['error'] = 'You don\'t have enough money.'.($partEx > 0 ? 'Including the part-exchange rate (60% of current house), y' : 'Y').'ou need '.$func->money($cost);
            }
        } else {
            $_SESSION['error'] = 'You can\'t downgrade your house';
        }
    } else {
        $_SESSION['error'] = 'The house you selected doesn\'t exist';
    }
    exit(header('Location: /estate.php'));
} elseif (array_key_exists('sellhouse', $_GET)) {
    if ($curr['hWILL'] > 100) {
        $db->query('SELECT hNAME, hPRICE FROM houses WHERE hWILL = ?');
        $db->execute([$ir['maxwill']]);
        $row = $db->fetch(true);
        $refund = $row['hPRICE'] * 0.6;
        $db->query('UPDATE users SET money = money + ?, will = 0, maxwill = 100 WHERE userid = ?');
        $db->execute([$refund, $ir['userid']]);
        $_SESSION['success'] = 'You\'ve sold your '.$func->format($row['hNAME']).' for 60% of its cost ('.$func->money($refund).')';
    } else {
        $_SESSION['error'] = 'You can\'t sell the lowest house';
    }
    exit(header('Location: /estate.php'));
} else {
    $db->query('SELECT hID, hNAME, hPRICE, hWILL FROM houses WHERE hWILL > ? ORDER BY hWILL ASC');
    $db->execute([$ir['maxwill']]);
    $rows = $db->fetch(); ?>
    Your current property: <strong><?php echo $func->format($curr['hNAME']); ?></strong><br />
    The houses you can buy are listed below. Click a house to buy it.<br /><?php
    if ($ir['maxwill'] > 100) {
        ?>
    <a href="/estate.php?sellhouse">Sell Your House</a><br /><?php
    } ?>
    <table class="table" width="75%">
        <thead>
            <tr>
                <th>House</th>
                <th>Cost</th>
                <th>Will</th>
            </tr>
        </thead>
        <tbody><?php
    if (null === $rows) {
        ?>
            <tr>
                <td colspan="3" class="center">There are no further house upgrades available</td>
            </tr><?php
    } else {
        foreach ($rows as $row) {
            ?>
            <tr>
                <td><a href="/estate.php?ID=<?php echo $row['hID']; ?>"><?php echo $func->format($row['hNAME']); ?></a></td>
                <td><?php echo $func->money($row['hPRICE']); ?></td>
                <td>
                    <?php echo $func->format($row['hWILL']); ?><br>
                    <span class="small italic green-dark">+<?php echo $func->format($row['hWILL'] - $curr['hWILL']); ?></span>
                </td>
            </tr><?php
        }
    } ?>
        </tbody>
    </table><?php
}
