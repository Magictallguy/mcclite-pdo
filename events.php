<?php
/*
MCCodes FREE
events.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('PAGINATION', true);
require_once __DIR__.'/lib/master.php'; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Events</h3>
    </div>
</div><?php
$_GET['delete'] = array_key_exists('delete', $_GET) && ctype_digit($_GET['delete']) && $_GET['delete'] > 0 ? $_GET['delete'] : null;
if (array_key_exists('delall', $_GET)) {
    if (array_key_exists('ans', $_GET)) {
        $db->query('DELETE FROM events WHERE evUSER = ? AND evREAD = 1');
        $db->execute([$ir['userid']]);
        $cnt = $db->affected();
        $_SESSION['success'] = $func->format($cnt).' read event'.$func->s($cnt).' deleted';
        exit(header('Location: /events.php'));
    } else {
        ?>
<div class="row">
    <div class="col">
        Confirm deletion of your read events?<br>
        <a href="/events.php?delall&amp;ans" class="btn btn-danger">
            <span class="fas fa-trash"></span>
            Delete them
        </a>
    </div>
</div><?php
    }
} elseif (null !== $_GET['delete']) {
    $db->query('SELECT evUSER FROM events WHERE evID = ?');
    $db->execute([$_GET['delete']]);
    if ($db->result() == $ir['userid']) {
        $db->query('DELETE FROM events WHERE evID = ?');
        $db->execute([$_GET['delete']]);
        $_SESSION['success'] = 'Event deleted';
    } else {
        $_SESSION['error'] = 'That isn\'t your event';
    }
    exit(header('Location: /events.php'));
}
$db->query('SELECT COUNT(evID) FROM events WHERE evUSER = ?');
$db->execute([$ir['userid']]);
$events = $db->result();
$pages = new Paginator($events);
$db->query('SELECT evID, evREAD, evTIME, evTEXT, evEXTRA FROM events WHERE evUSER = ? ORDER BY evTIME DESC'.$pages->limit);
$db->execute([$ir['userid']]);
$rows = $db->fetch(); ?>
<div class="row">
    <div class="col">
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>Event</th>
                        <th>Links</th>
                    </tr>
                </thead>
                <tbody><?php
if (null === $rows) {
    ?>
                    <tr>
                        <td colspan="3" class="center">You have no events</td>
                    </tr><?php
} else {
        $unread = [];
        $cache = [];
        if (count($rows) > 1) {
            ?>
                    <tr>
                        <td colspan="3" class="text-right">
                            <a href="/events.php?delall" class="btn btn-danger">
                                <span class="fas fa-trash"></span>
                                Delete All
                            </a>
                        </td>
                    </tr><?php
        }
        $evCnt = 0;
        foreach ($rows as $row) {
            ++$evCnt;
            $content = stripslashes($row['evTEXT']);
            if ('' != $row['evEXTRA']) {
                $content = $func->convertUserPlaceholder($row['evTEXT'], $row['exEXTRA']);
            }
            $date = new \DateTime($row['evTIME']); ?>
                    <tr>
                        <td>
                            <?php echo $date->format(DEFAULT_DATE_FORMAT);
            if (!$row['evREAD']) {
                $unread[] = $row['evID']; ?>
                            <br />
                            <strong>New!</strong><?php
            } ?>
                        </td>
                        <td><?php echo $content; ?></td>
                        <td><a href="/events.php?delete=<?php echo $row['evID']; ?>">Delete</a></td>
                    </tr><?php
        }
        if ($evCnt >= 10) {
            ?>
                    <tr>
                        <td colspan="3" class="text-right">
                            <a href="/events.php?delall" class="btn btn-danger">
                                <span class="fas fa-trash"></span>
                                Delete All
                            </a>
                        </td>
                    </tr><?php
        }
        if (count($unread) > 0) {
            $db->query('UPDATE events SET evREAD = 1 WHERE evID IN('.implode(',', $unread).')');
            $db->execute();
        }
    }?>
                </tbody>
            </table>
            <p>
                <?php echo $pages->display_pages(); ?>
            </p>
        </div>
    </div>
</div>
