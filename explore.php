<?php
/*
MCCodes FREE
explore.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$tresder = (int) mt_rand(100, 999);
$game_url = $func->determine_game_urlbase(); ?>
<strong>You begin exploring the area you're in, you see a bit that interests you.</strong><br>
<table class="table" width="75%">
    <tr height="100">
        <td valign="top">
            <span class="city-subtitle">Market</span><br>
            <a href="/shops.php">Shops</a><br>
            <a href="/itemmarket.php">Item Market</a><br>
            <a href="/cmarket.php">Crystal Market</a>
        </td>
        <td valign="top">
            <span class="city-subtitle">Serious</span><br>
            <a href="/monorail.php">Travel Agency</a><br>
            <a href="/estate.php">Estate Agent</a><br>
            <a href="/bank.php">City Bank</a>
        </td>
        <td valign="top"><?php
            if (5 == $ir['location']) {
                ?>
            <span class="city-subtitle">Cyber</span><br>
            <a href="/cyberbank.php">Cyber Bank</a><br><?php
            } ?>
        </td>
        <td valign="top">
            <span class="city-subtitle">Dark</span><br>
            <a href="/fedjail.php">Federal Jail</a><br>
            <a href="/slotsmachine.php?tresde=<?php echo $tresder; ?>">Slots Machine</a><br>
            <a href="/roulette.php?tresde=<?php echo $tresder; ?>">Roulette</a><br>
            <a href="/number.php?tresde=<?php echo $tresder; ?>">Number Game</a>
        </td>
    </tr>
    <tr height=100>
        <td valign="top">
            <span class="city-subtitle">Statistics</span><br>
            <a href="/userlist.php">User List</a><br>
            <a href="/stafflist.php">{GAME_NAME} Staff</a><br>
            <a href="/halloffame.php">Hall of Fame</a><br>
            <a href="/stats.php">Game Stats</a><br>
            <a href="/usersonline.php">Users Online</a><br>
            <a href="/itempedia.php">Itempedia</a>
        </td>
        <td valign="top">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        <td valign="top">
            <span class="city-subtitle">Mysterious</span><br>
            <a href="/crystaltemple.php">Crystal Temple</a><br><?php
            if (4 == $ir['location']) {
                ?>
            <a href="/battletent.php">Battle Tent</a><br><?php
            } ?>
        </td>
    </tr>
</table><br><br>
This is your referal link: http://<?php echo $game_url; ?>/register.php?REF=<?php echo $ir['userid']; ?><br>
Every signup from this link earns you two valuable crystals!
