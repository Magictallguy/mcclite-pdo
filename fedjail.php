<?php
/*
MCCodes FREE
fedjail.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$db->query('SELECT fed_userid, fed_days, fed_reason, fed_jailedby FROM fedjail ORDER BY fed_days ASC, fed_userid ASC');
$db->execute();
$feds = $db->fetch();
$db->query('SELECT userid, username, mailban, mb_reason FROM users WHERE mailban > 0 ORDER BY mailban ASC, userid ASC');
$db->execute();
$mails = $db->fetch();
$cache = []; ?>
<strong>Federal Jail</strong><br />
If you ever cheat the game your name will become part of this list...<br />
<table class="table">
    <thead>
        <tr>
            <th>Who</th>
            <th>Days</th>
            <th>Reason</th>
            <th>Jailer</th>
        </tr>
    </thead>
    <tbody><?php
if (null === $feds) {
    ?>
        <tr>
            <td colspan="4" class="center">There is no-one in Federal Jail</td>
        </tr><?php
} else {
        foreach ($rows as $row) {
            if (!in_array($row['fed_userid'], $cache)) {
                $cache[$row['fed_userid']] = $func->username($row['fed_userid']);
            }
            if (!in_array($row['fed_jailedby'], $cache)) {
                $cache[$row['fed_jailedby']] = $func->username($row['fed_jailedby']);
            } ?>
        <tr>
            <td><?php echo $cache[$row['fed_userid']]; ?></td>
            <td><?php echo $func->format($row['fed_days']); ?></td>
            <td><?php echo $func->format($row['fed_reason'], true); ?></td>
            <td><?php echo $cache[$row['fed_jailedby']]; ?></td>
        </tr><?php
        }
    } ?>
    </tbody>
</table><br /><br />
<strong>Mail Ban</strong></center><br />
If you ever swear or do bad things at your mail, your name will become part of this list...<br />
<table class="table">
    <thead>
        <tr>
            <th>Who</th>
            <th>Days</th>
            <th>Reason</th>
        </tr>
    </thead>
    <tbody><?php
if (null === $mails) {
        ?>
        <tr>
            <td colspan="3" class="center">No-one is currently mailbanned</td>
        </tr><?php
    } else {
        foreach ($rows as $row) {
            if (!in_array($row['userid'], $cache)) {
                $cache[$row['userid']] = $func->username($row['userid']);
            } ?>
        <tr>
            <td><?php echo $cache[$row['userid']]; ?></td>
            <td><?php echo $func->format($row['mailban']); ?></td>
            <td><?php echo $func->format($row['mb_reason'], true); ?></td>
        </tr><?php
        }
    } ?>
    </tbody>
</table>
