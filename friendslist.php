<?php
/*
MCCodes FREE
friendslist.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('PAGINATION', true);
define('BBCODE', true);
require_once __DIR__.'/lib/master.php';
if (0 == $ir['donatordays']) {
    exit('This feature is for donators only.');
}
echo '<h3>Friends List</h3>';
switch ($_GET['action']) {
    case 'add':
        add_friend($db, $ir, $func);
        break;

    case 'remove':
        remove_friend($db, $ir, $func);
        break;

    case 'ccomment':
        change_comment($db, $ir, $func);
        break;

    default:
        friend_list($db, $ir, $func, $parser);
        break;
}

function friend_list($db, $ir, $func, $parser)
{
    $db->query('SELECT COUNT(fl_ID) FROM friendslist WHERE fl_ADDER = ?');
    $db->execute([$ir['userid']]);
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT fl_ADDED, COUNT(fl_ID) AS cnt
        FROM friendslist
        GROUP BY fl_ADDED
        ORDER BY cnt DESC
        LIMIT 5
    ');
    $db->execute();
    $tops = $db->fetch();
    $liked = '';
    if (null !== $tops) {
        foreach ($tops as $top) {
            $liked .= '('.$top['cnt'].') '.$func->username($top['fl_ADDED']).' &middot; ';
        }
        if ('' != $liked) {
            $liked = substr($liked, 0, -10);
        }
    }
    $db->query('SELECT f.fl_ID, f.fl_COMMENT, u.userid, u.username, u.laston, u.donatordays, u.avatar
        FROM friendslist AS f
        INNER JOIN users AS u ON f.fl_ADDED = u.userid
        WHERE f.fl_ADDER = ?
        ORDER BY u.username ASC
    '.$pages->limit);
    $db->execute([$ir['userid']]);
    $rows = $db->fetch(); ?>
    <div class="row">
        <div class="col">
            <p>
                <a href="/friendslist.php?action=add"><span class="fas fa-plus"></span> Add a Friend</a><br>
                These are the people on your friends list.<br>
                Most liked: <?php echo '' != $liked ? $liked : 'no-one'; ?>
            </p>
            <?php echo $pages->display_pages(); ?>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Mail</th>
                            <th>Attack</th>
                            <th>Remove</th>
                            <th>Comment</th>
                        </tr>
                    </thead>
                    <tbody><?php
    if (null === $rows) {
        ?>
                        <tr>
                            <td colspan="5" class="text-center">You haven't added anyone to your Friends list</td>
                        </tr><?php
    } else {
        $time = time();
        foreach ($rows as $row) {
            $comment = $row['fl_COMMENT'] = '' ? 'N/A' : $func->format($row['fl_COMMENT'], true);
            $parser->parse($comment); ?>
                        <tr>
                            <td>
                                <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                    <img src="<?php echo $row['avatar']; ?>" class="avatar-friendslist img-fluid <?php echo strtotime($row['laston']) >= $time - 900 ? 'online-box' : 'offline-box'; ?>">
                                </a>
                                <?php echo $func->username($row['userid']); ?>
                            </td>
                            <td><a href="/mailbox.php?action=compose&amp;ID=<?php echo $row['userid']; ?>">Mail</a></td>
                            <td><a href="/attack.php?ID=<?php echo $row['userid']; ?>">Attack</a></td>
                            <td><a href="/friendslist.php?action=remove&amp;ID=<?php echo $row['fl_ID']; ?>">Remove</a></td>
                            <td>
                                <?php echo $parser->getAsHTML(); ?><br>
                                <small class="italic">[<a href="/friendslist.php?action=ccomment&amp;ID=<?php echo $row['fl_ID']; ?>">Change Comment</a>]</small>
                            </td>
                        </tr><?php
        }
    } ?>
                    </tbody>
                </table>
            </div>
            <?php echo $pages->display_pages(); ?>
        </div>
    </div><?php
}

function add_friend($db, $ir, $func)
{
    $_POST['ID'] = array_key_exists('ID', $_POST) && ctype_digit($_POST['ID']) && $_POST['ID'] > 0 ? $_POST['ID'] : null;
    if (null !== $_POST['ID']) {
        if ($_POST['ID'] != $ir['userid']) {
            $_POST['comment'] = array_key_exists('comment', $_POST) && is_string($_POST['comment']) && strlen($_POST['comment']) > 0 ? strip_tags(trim($_POST['comment'])) : '';
            $db->query('SELECT userid, username, user_level FROM users WHERE userid = ?');
            $db->execute([$_POST['ID']]);
            $row = $db->fetch(true);
            $backToForm = true;
            if (null !== $row) {
                $name = $func->username($row['userid']);
                $db->query('SELECT COUNT(fl_ID) FROM friendslist WHERE fl_ADDER = ? AND fl_ADDED = ?');
                $db->execute([$ir['userid'], $_POST['ID']]);
                if (!$db->result()) {
                    $db->trans('start');
                    $db->query('INSERT INTO friendslist (fl_ADDER, fl_ADDED, fl_COMMENT) VALUES (?, ?, ?)');
                    $db->execute([$ir['userid'], $row['userid'], $_POST['comment']]);
                    $func->event_add($row['userid'], '{user} has added you to their Friends List', $ir['userid']);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve added '.$name.' to your Friendslist';
                    $backToForm = false;
                } else {
                    $_SESSION['info'] = 'You\'ve already added '.$name.' to your Friendslist';
                }
            } else {
                $_SESSION['error'] = 'The player you selected doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You can\'t add yourself';
        }
        exit(header('Location: /friendslist.php'.(true === $backToForm ? '?action=add' : '')));
    } else {
        ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Add Friend</h3>
        <form action="friendslist.php?action=add" method="post" class="form">
            <div class="form-group">
                <label for="ID" class="form-label">Friend's ID</label>
                <input type="number" name="ID" id="ID" class="form-control bg-dark text-light" value="<?php echo $_GET['ID']; ?>" required autofocus>
            </div>
            <div class="form-group">
                <label for="comment" class="form-label">Comment</label>
                <textarea name="comment" id="comment" rows="7" class="form-control bg-dark text-light"></textarea>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-user-plus"></span>
                    Add Friend
                </button>
            </div>
        </form>
    </div>
</div><?php
    }
}

function remove_friend($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT f.fl_ID, f.fl_ADDED, f.fl_ADDER, u.username
            FROM friendslist AS b
            INNER JOIN users AS u ON f.fl_ADDED = u.username
            WHERE f.fl_ID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['fl_ADDER'] == $ir['userid']) {
                $db->query('DELETE FROM friendslist WHERE fl_ID = ?');
                $db->execute([$row['fl_ID']]);
                $_SESSION['success'] = 'You\'ve removed '.$func->username($row['fl_ADDED']).' from your Friendslist';
            } else {
                $_SESSION['error'] = 'That isn\'t your Friendslist entry';
            }
        } else {
            $_SESSION['error'] = 'The Friendslist entry you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid Friendslist entry';
    }
    exit(header('Location: /friendslist.php'));
}

function change_comment($db, $ir, $func)
{
    $redirect = true;
    if (null !== $_GET['ID']) {
        $db->query('SELECT f.fl_ID, f.fl_ADDED, f.fl_ADDER, f.fl_COMMENT, u.username
            FROM friendslist AS f
            INNER JOIN users AS u ON f.fl_ADDED = u.userid
            WHERE fl_ID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['fl_ADDER'] == $ir['userid']) {
                $name = $func->username($row['fl_ADDED']);
                if (array_key_exists('submit', $_POST)) {
                    $_POST['comment'] = array_key_exists('comment', $_POST) && is_string($_POST['comment']) && strlen($_POST['comment']) > 0 ? strip_tags(trim($_POST['comment'])) : '';
                    $db->query('UPDATE friendslist SET fl_COMMENT = ? WHERE fl_ID = ?');
                    $db->execute([$_POST['comment'], $row['fl_ID']]);
                    $_SESSION['success'] = 'You\'ve updated your comment for your Friendslist entry on '.$name;
                } else {
                    $redirect = false; ?>
            <div class="row">
                <div class="col">
                    <h3 class="page-subtitle">Change Comment</h3>
                    <p>
                        Changing your comment for Friendslist entry: <?php echo $name; ?>
                    </p>
                    <form action="/friendslist.php?action=ccomment&amp;ID=<?php echo $row['fl_ID']; ?>" method="post" class="form">
                        <div class="form-group">
                            <label for="comment" class="form-label">Comment</label>
                            <textarea name="comment" id="comment" rows="7" class="form-control bg-dark text-light"><?php echo $func->format($row['fl_COMMENT'], true); ?></textarea>
                        </div>
                        <div class="form-controls">
                            <button type="submit" name="submit" class="btn btn-primary">
                                <span class="fas fa-edit"></span>
                                Change Comment
                            </button>
                        </div>
                    </form>
                </div>
            </div><?php
                }
            } else {
                $_SESSION['error'] = 'That isn\'t your Friends list entry';
            }
        } else {
            $_SESSION['error'] = 'The Friends list enty you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid Friends list entry';
    }
    if (true === $redirect) {
        exit(header('Location: /friendslist.php'));
    }
}
