<?php
/*
MCCodes FREE
gym.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$out = '';
$stats = ['strength', 'agility', 'guard', 'labour'];
$_GET['times'] = array_key_exists('times', $_GET) && ctype_digit($_GET['times']) && $_GET['times'] > 0 ? $_GET['times'] : null;
$_GET['train'] = array_key_exists('train', $_GET) && in_array($_GET['train'], $stats) ? $_GET['train'] : null;
if (null !== $_GET['train']) {
    $tgain = 0;
    for ($i = 1; $i <= $_GET['times'] && $ir['energy'] > 0; ++$i) {
        if ($ir['energy'] > 0) {
            $gain = mt_rand(1, 3) / mt_rand(800, 1000) * mt_rand(800, 1000) * (($ir['will'] + 20) / 150);
            $tgain += $gain;
            $ir[$_GET['train']] += $gain;
            $egain = $gain / 10;
            $ts = $ir[$_GET['train']];
            $st = $_GET['train'];
            $wu = mt_rand(1, 3);
            $ir['will'] -= $wu;
            if ($ir['will'] < 0) {
                $ir['will'] = 0;
            }
            $db->query('UPDATE userstats AS us INNER JOIN users AS u ON us.userid = u.userid SET
                us.'.$st.' = us.'.$st.' + ? , u.energy = u.energy - 1, u.exp = exp + ?, u.will = GREATEST(u.will - ?, 0)
                WHERE us.userid = ?
            ');
            $db->execute([$gain, $egain, $wu, $ir['userid']]);
            --$ir['energy'];
            $ir['exp'] += $egain;
        } else {
            $out = 'You don\'t have enough energy to train.';
        }
    }
    $stat = $ir[$_GET['train']];
    --$i;
    if (1 == $_GET['times']) {
        $times = 'once';
    } elseif (2 == $_GET['times']) {
        $times = 'twice';
    } else {
        $times = $_GET['times'].' times';
    }
    $out = 'You begin training your '.$_GET['train'].'.<br />
    You have gained '.$func->format($tgain).' '.$_GET['train'].' by training it '.$times.'.<br />
    You now have '.$func->format($stat).' '.$_GET['train'].' and '.$ir['energy'].' energy left.<br /><br />';
} else {
    $out = '<h3 class="page-subtitle">Gym: Main Lobby<h3>';
}
$ts = $ir['strength'] + $ir['agility'] + $ir['guard'] + $ir['labour'] + $ir['IQ'];
$ir['strank'] = $func->get_rank($ir['strength'], 'strength');
$ir['agirank'] = $func->get_rank($ir['agility'], 'agility');
$ir['guarank'] = $func->get_rank($ir['guard'], 'guard');
$ir['labrank'] = $func->get_rank($ir['labour'], 'labour');
$ir['IQrank'] = $func->get_rank($ir['IQ'], 'IQ');
$tsrank = $func->get_rank($ts, 'strength+agility+guard+labour+IQ');
$ir['strength'] = $func->format($ir['strength']);
$ir['agility'] = $func->format($ir['agility']);
$ir['guard'] = $func->format($ir['guard']);
$ir['labour'] = $func->format($ir['labour']);
$ir['IQ'] = $func->format($ir['IQ']);
$ts = $func->format($ts); ?>
<div class="row">
    <div class="col">
        <?php echo $out; ?>
    </div>
</div>
<div class="row">
    <div class="col">
        Enter the amount of times you wish to train and choose the stat to train.<br />
        You can train up to <?php echo $ir['energy']; ?> time<?php echo $func->s($ir['energy']); ?>.<br />
        <form action="/gym.php" method="get">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="times" class="form-label">Times to train</label>
                        <input type="number" name="times" id="times" class="form-control bg-dark text-light" value="<?php echo $ir['energy']; ?>" required autofocus>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="train" class="form-label">Stat</label>
                        <select name="train" id="train" class="form-control bg-dark text-light"><?php
                        foreach ($stats as $theStat) {
                            printf('<option value="%s"%s>%s</option>', $theStat, $theStat == $_GET['train'] ? ' selected' : '', ucfirst($theStat));
                        } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-strong"></span>
                    Train
                </button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col">
        &nbsp;
    </div>
</div>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Stats Info</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td><strong>Strength:</strong> <?php echo $ir['strength']; ?> [Ranked: <?php echo $ir['strank']; ?>]</td>
                    <td><strong>Agility:</strong> <?php echo $ir['agility']; ?> [Ranked: <?php echo $ir['agirank']; ?>]</td>
                </tr>
                <tr>
                    <td><strong>Guard:</strong> <?php echo $ir['guard']; ?> [Ranked: <?php echo $ir['guarank']; ?>]</td>
                    <td><strong>Labour:</strong> <?php echo $ir['labour']; ?> [Ranked: <?php echo $ir['labrank']; ?>]</td>
                </tr>
                <tr>
                    <td><strong>IQ: </strong> <?php echo $ir['IQ']; ?> [Ranked: <?php echo $ir['IQrank']; ?>]</td>
                    <td><strong>Total stats:</strong> <?php echo $ts; ?> [Ranked: <?php echo $tsrank; ?>]</td>
                </tr>
            </table>
        </div>
    </div>
</div>
