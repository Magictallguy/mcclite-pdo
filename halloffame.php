<?php
/*
MCCodes FREE
halloffame.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

require_once __DIR__.'/lib/master.php';
$users = ['level', 'money', 'crystals'];
$stats = ['strength', 'agility', 'guard', 'labour'];
$_GET['action'] = array_key_exists('action', $_GET) && in_array($_GET['action'], array_merge($users, $stats, ['total'])) ? $_GET['action'] : 'level';
if ('total' == $_GET['action']) {
    $db->query('SELECT us.userid, u.username, u.level, u.avatar, u.gender, u.laston, (us.strength + us.agility + us.guard + us.labour) AS total
        FROM userstats AS us
        INNER JOIN users AS u ON us.userid = u.userid
        ORDER BY total DESC
        LIMIT 20
    ');
    $db->execute();
} elseif (in_array($_GET['action'], $stats)) {
    $db->query('SELECT us.userid, u.username, u.level, u.avatar, u.gender, u.laston
        FROM userstats AS us
        INNER JOIN users AS u ON us.userid = u.userid
        ORDER BY us.'.$_GET['action'].' DESC
        LIMIT 20
    ');
    $db->execute();
} elseif (in_array($_GET['action'], $users)) {
    $db->query('SELECT userid, level, avatar, gender, laston FROM users ORDER BY '.$_GET['action'].' DESC LIMIT 20');
    $db->execute();
}
$rows = $db->fetch();
$cnt = null !== $rows ? count($rows) : 0; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Hall Of Fame</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td><a href="/halloffame.php?action=level"<?php echo 'level' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>LEVEL</a></td>
                        <td><a href="/halloffame.php?action=money"<?php echo 'money' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>MONEY</a></td>
                        <td><a href="/halloffame.php?action=crystals"<?php echo 'crystals' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>CRYSTALS</a></td>
                        <td><a href="/halloffame.php?action=total"<?php echo 'total' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>TOTAL STATS</a></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><a href="/halloffame.php?action=strength"<?php echo 'strength' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>STRENGTH</a></td>
                        <td><a href="/halloffame.php?action=agility"<?php echo 'agility' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>AGILITY</a></td>
                        <td><a href="/halloffame.php?action=guard"<?php echo 'guard' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>GUARD</a></td>
                        <td><a href="/halloffame.php?action=labour"<?php echo 'labour' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>LABOUR</a></td>
                        <td><a href="/halloffame.php?action=iq"<?php echo 'iq' == $_GET['action'] ? ' class="selected-glow"' : ''; ?>>IQ</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        Showing the top <?php echo $cnt; ?> users with the highest <?php echo 'total' == $_GET['action'] ? 'total stats' : $_GET['action']; ?><br>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Position</th>
                        <th>User</th>
                        <th>Level</th>
                    </tr>
                </thead>
                <tbody><?php
if (null === $rows) {
    ?>
                    <tr>
                        <td colspan="3" class="center">There is no-one in the Hall of Fame for <?php echo $_GET['action']; ?><br>Wait .. wut?</td>
                    </tr><?php
} else {
        $pos = 0;
        $time = time();
        foreach ($rows as $row) {
            ++$pos;
            $laston = strtotime($row['laston']); ?>
                    <tr<?php echo $row['userid'] == $ir['userid'] ? ' class="bold"' : ''; ?>>
                        <td><?php echo $pos; ?></td>
                        <td>
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" title="avatar" alt="avatar" class="avatar-hof img-fluid <?php echo $laston >= $time - 900 ? 'online-box' : 'offline-box'; ?>">
                            </a>
                            <?php echo $func->username($row['userid']); ?> <span class="fas fa-<?php echo 'Male' == $row['gender'] ? 'mars' : 'venus'; ?>"></span>
                        </td>
                        <td><?php echo $func->format($row['level']); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
