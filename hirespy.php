<?php
/*
MCCodes FREE
hirespy.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (2 != $ir['user_level']) {
    $_SESSION['error'] = 'You dont have access';
    exit(header('Location: /index.php'));
}
if (null === $_GET['ID']) {
    $_SESSION['error'] = 'You didn\'t select a valid target';
    exit(header('Location: /index.php'));
}
if ($_GET['ID'] == $ir['userid']) {
    $_SESSION['error'] = 'You can\'t spy on yourself';
    exit(header('Location: /index.php'));
}
$db->query('SELECT us.strength, us.agility, us.guard, us.IQ, us.labour, u.userid, u.level, u.user_level, u.exp, u.gender
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = us.userid
    WHERE us.userid = ?
');
$db->execute([$_GET['ID']]);
$row = $db->fetch(true);
if (null === $row) {
    $_SESSION['error'] = 'Your intended target doesn\'t exist';
    exit(header('Location: /index.php'));
}
$target = $func->username($row['userid']);
if (2 == $row['user_level']) {
    $_SESSION['warning'] = 'The spy never came back. It was rumoured '.(1 == mt_rand(0, 1) ? 's' : '').'he was attacked by '.$target.' and pushed off a cliff.';
    exit(header('Location: /index.php'));
}
$cost = $row['level'] * 1000;
if ($ir['money'] >= $cost) {
    $_SESSION['error'] = 'You don\'t have enough cash. To hire a spy on '.$target.' at their current level, you need '.$func->money($cost);
    exit(header('Location: /index.php'));
}
if (array_key_exists('submit', $_POST)) {
    $db->query('UPDATE users SET money = money - ? WHERE userid = ?');
    $db->execute([$cost, $ir['userid']]);
    $db->query('SELECT iv.inv_itemid, iv.inv_qty, i.itmname, i.itmsellprice
        FROM inventory AS iv
        INNER JOIN items AS i ON iv.inv_itemid = i.itmid
        WHERE iv.inv_userid = ?
        ORDER BY i.itmbuyprice ASC, i.itmsellprice ASC
    ');
    $db->execute([$row['userid']]);
    $invs = $db->fetch();
    $exp = (int) ($row['exp'] / (($row['level'] + 1) * ($row['level'] + 1) * ($row['level'] + 1) * 2) * 100); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Hire Spy</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        You have hired a spy to get information on <strong><?php echo $target; ?></strong> at the cost of <?php echo $func->money($cost); ?>. Here is the info <?php echo 1 == mt_rand(0, 1) ? 's' : ''; ?>he retrieved:<br />
        Strength: <?php echo $func->format($row['strength']); ?><br />
        Agility: <?php echo $func->format($row['agility']); ?><br />
        Guard: <?php echo $func->format($row['guard']); ?><br />
        Labour: <?php echo $func->format($row['labour']); ?><br />
        IQ: <?php echo $func->format($row['IQ']); ?><br />
        Exp: <?php echo $exp; ?>%<br />
        Here is his/her inventory.<br /><?php
    if (null === $invs) {
        echo $target.' doesn\'t have any items';
    } else {
        $overallTotal = 0; ?>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Sell Value</th>
                        <th>Total Sell Value</th>
                    </tr>
                </thead>
                <tbody><?php
        foreach ($invs as $inv) {
            $total = $inv['itmsellprice'] * $inv['inv_qty'];
            $overallTotal += $total; ?>
                    <tr>
                        <td><?php echo $func->format($inv['itmname']).' x'.$func->format($inv['inv_qty']); ?></td>
                        <td><?php echo $func->money($inv['itmsellprice']); ?></td>
                        <td><?php echo $func->money($total); ?></td>
                    </tr><?php
        }
    } ?>
                    <tr>
                        <td colspan="3">Total Inventory Value: <?php echo $func->money($overallTotal); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
} else {
        ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Hire Spy</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        You are hiring a spy to spy on <strong><?php echo $target; ?></strong> at the cost of <?php echo $func->money($cost); ?>.<br />
        <form action="/hirespy.php?ID=<?php echo $_GET['ID']; ?>" method="post">
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-user-cog"></span>
                    Hire
                </button>
            </div>
        </form>
    </div>
</div><?php
    }
