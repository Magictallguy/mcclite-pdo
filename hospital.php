<?php
require_once __DIR__.'/lib/master.php';
$_GET['heal'] = array_key_exists('heal', $_GET) && ctype_digit($_GET['heal']) && $_GET['heal'] > 0 ? $_GET['heal'] : null;
echo '<h3><u>Hospital</u></h3>';
if (null !== $_GET['heal']) {
    $noRedirect = false;
    if ($ir['hospital'] < 1) {
        if ($_GET['heal'] != $ir['userid']) {
            $db->query('SELECT userid, username, hospital, level, gender FROM users WHERE userid = ?');
            $db->execute([$_GET['heal']]);
            $row = $db->fetch(true);
            if (null !== $row) {
                $name = '<a href="viewuser.php?u='.$row['userid'].'">'.format($row['username']).'</a>';
                if ($row['hospital'] > 0) {
                    $cost = $row['hospital'] * $row['level'];
                    $himHer = 'Male' == $row['gender'] ? 'him' : 'her';
                    $extra = 'To heal '.$name.' with '.$row['hospital'].' minute'.(1 == $row['hospital'] ? '' : 's').' remaining, it would cost '.money($cost);
                    if ($ir['money'] >= $cost) {
                        if (array_key_exists('ans', $_GET)) {
                            $db->trans('start');
                            $db->query('UPDATE users SET hospital = 0, hp = maxhp WHERE userid = ?');
                            $db->execute([$row['userid']]);
                            $db->query('UPDATE users SET money = money - ? WHERE userid = ?');
                            $db->execute([$cost, $ir['userid']]);
                            event_add($row['userid'], 'You were healed by <a href="viewuser.php?u='.$ir['userid'].'">'.$ir['username'].'</a>');
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve healed '.$name.' for $'.number_format($cost);
                        } else {
                            echo $extra.'<br>
                            <a href="hospital.php?heal='.$row['userid'].'&ans=yes">Ok, heal '.$himHer.'</a><br><br>';
                            $noRedirect = true;
                        }
                    } else {
                        $_SESSION['error'] = 'You don\'t have enough money. '.$extra;
                    }
                } else {
                    $_SESSION['error'] = $name.' isn\'t in hospital';
                }
            } else {
                $_SESSION['error'] = 'Your intended patient doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You can\'t heal yourself';
        }
    } else {
        $_SESSION['error'] = 'You can\'t heal others whilst in hospital';
    }
    if (false === $noRedirect) {
        exit(header('Location: /hospital.php'));
    }
}
$db->query('SELECT userid, username, hospital, hospreason, level FROM users WHERE hospital > 0 ORDER BY hospital DESC');
$db->execute();
$rows = $db->fetch(); ?>
<table class="table">
    <thead>
        <tr>
            <th>Patient</th>
            <th>Level</th>
            <th>Time</th>
            <th>Reason</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody><?php
if (null === $rows) {
    ?>
        <tr>
            <td colspan="5" class="center">There are currently no patients</td>
        </tr><?php
} else {
        foreach ($rows as $row) {
            ?>
        <tr>
            <td><a href="viewuser.php?u=<?php echo $row['userid']; ?>"><?php echo format($row['username']); ?></a></td>
            <td><?php echo format($row['level']); ?></td>
            <td><?php echo $row['hospital'].' minute'.s($row['hospital']); ?></td>
            <td><?php echo stripslashes($row['hospreason']); ?></td>
            <td><?php
            if (!$ir['hospital']) {
                ?>
                <a href="hospital.php?heal=<?php echo $row['userid']; ?>">Heal</a><?php
            } ?>
            </td>
        </tr><?php
        }
    } ?>
    </tbody>
</table>
