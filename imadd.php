<?php
/*
MCCodes FREE
imadd.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

require_once __DIR__.'/lib/master.php';
$redirect = true;
if (null !== $_GET['ID']) {
    $db->query('SELECT iv.inv_id, iv.inv_userid, iv.inv_itemid, i.itmname
        FROM inventory AS iv
        INNER JOIN items AS i ON iv.inv_itemid = i.itmid
        WHERE iv.inv_id = ?
    ');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['inv_userid'] == $ir['userid']) {
            $_POST['price'] = array_key_exists('price', $_POST) && ctype_digit($_POST['price']) && $_POST['price'] > 0 ? $_POST['price'] : null;
            if (null !== $_POST['price']) {
                $log = ' added '.aAn($row['itmname']).' '.format($row['itmname']).' to the Item Market for '.money($_POST['price']);
                $db->trans('start');
                $db->query('INSERT INTO itemmarket (imITEM, imADDER, imPRICE) VALUES (?, ?, ?)');
                $db->execute([$row['inv_itemid'], $ir['userid'], $_POST['price']]);
                takeItem($row['inv_itemid']);
                $db->query('INSERT INTO imarketaddlogs (imaITEM, imaPRICE, imaINVID, imaADDER, imaCONTENT) VALUES (?, ?, ?, ?, ?)');
                $db->execute([$row['inv_itemid'], $_POST['price'], $row['inv_id'], $ir['userid'], format($ir['username']).$log]);
                $db->trans('end');
                $_SESSION['success'] = 'You\'ve '.$log;
                exit(header('Location: /inventory.php'));
            } else {
                $redirect = false; ?>
                Adding an item to the item market...
                <form action="/imadd.php?ID=<?php echo $row['inv_id']; ?>" method="post" class="form">
                    <div class="form-group">
                        <label for="price" class="form-label">Price</label>
                        <input type="number" name="price" id="price" class="form-control bg-dark text-light" required autofocus>
                    </div>
                    <div class="form-controls">
                        <button type="submit" class="btn btn-primary">
                            <span class="fas fa-check"></span>
                            Add to Market
                        </button>
                    </div>
                </form><?php
            }
        } else {
            $_SESSION['error'] = 'That item isn\'t yours';
        }
    } else {
        $_SESSION['error'] = 'The item you selected doesn\'t exist';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid inventory entry';
}
if (true === $redirect) {
    exit(header('Location: /inventory.php'));
}
