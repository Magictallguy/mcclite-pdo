<?php
/*
MCCodes FREE
index.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$exp = (int) ($ir['exp'] / $ir['exp_needed'] * 100);
$ts = $ir['strength'] + $ir['agility'] + $ir['guard'] + $ir['labour'] + $ir['IQ'];
$db->query('SELECT SQL_CACHE * FROM (
    (SELECT COUNT(us.userid)+1 AS str
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = u.userid
    WHERE us.userid <> ? AND u.user_level > 0 AND ? <= us.strength) AS str,
    (SELECT COUNT(us.userid)+1 AS agi
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = u.userid
    WHERE us.userid <> ? AND u.user_level > 0 AND ? <= us.agility) AS agi,
    (SELECT COUNT(us.userid)+1 AS gua
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = u.userid
    WHERE us.userid <> ? AND u.user_level > 0 AND ? <= us.guard) AS gua,
    (SELECT COUNT(us.userid)+1 AS lab
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = u.userid
    WHERE us.userid <> ? AND u.user_level > 0 AND ? <= us.labour) AS lab,
    (SELECT COUNT(us.userid)+1 AS iq
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = u.userid
    WHERE us.userid <> ? AND u.user_level > 0 AND ? <= us.IQ) AS iq,
    (SELECT COUNT(us.userid)+1 AS allst
    FROM userstats AS us
    INNER JOIN users AS u ON us.userid = u.userid
    WHERE us.userid <> ? AND u.user_level > 0 AND ? <= (us.strength+us.agility+us.guard+us.labour+us.IQ)) AS allst
)');
$db->execute([$ir['userid'], $ir['strength'], $ir['userid'], $ir['agility'], $ir['userid'], $ir['guard'], $ir['userid'], $ir['labour'], $ir['userid'], $ir['IQ'], $ir['userid'], $ts]);
$ranks = $db->fetch(true);

$ir['strank'] = $ranks['str'];
$ir['agirank'] = $ranks['agi'];
$ir['guarank'] = $ranks['gua'];
$ir['labrank'] = $ranks['lab'];
$ir['IQrank'] = $ranks['iq'];
$tsrank = $ranks['allst'];
// $ir['strank'] = $func->get_rank($ir['strength'], 'strength');
// $ir['agirank'] = $func->get_rank($ir['agility'], 'agility');
// $ir['guarank'] = $func->get_rank($ir['guard'], 'guard');
// $ir['labrank'] = $func->get_rank($ir['labour'], 'labour');
// $ir['IQrank'] = $func->get_rank($ir['IQ'], 'IQ');
// $tsrank = $func->get_rank($ts, 'strength+agility+guard+labour+IQ');
$ir['strength'] = $func->format($ir['strength']);
$ir['agility'] = $func->format($ir['agility']);
$ir['guard'] = $func->format($ir['guard']);
$ir['labour'] = $func->format($ir['labour']);
$ir['IQ'] = $func->format($ir['IQ']);
$ts = $func->format($ts); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">General Info</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td><strong>Crystals:</strong> <?php echo $cm; ?></td>
                        <td><strong>Level:</strong> <?php echo $func->format($ir['level']); ?></td>
                    </tr>
                    <tr>
                        <td><strong>Exp:</strong> <?php echo $exp; ?>%</td>
                        <td><strong>Money:</strong> <?php echo $fm; ?></td>
                    </tr>
                    <tr>
                        <td><strong>HP:</strong> <?php echo $func->format($ir['hp']); ?>/<?php echo $func->format($ir['maxhp']); ?></td>
                        <td><strong>Property:</strong> <?php echo $func->format($ir['hNAME']); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Stats Info</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td><strong>Strength:</strong> <?php echo $ir['strength']; ?> [Ranked: <?php echo $ir['strank']; ?>]</td>
                    <td><strong>Agility:</strong> <?php echo $ir['agility']; ?> [Ranked: <?php echo $ir['agirank']; ?>]</td>
                </tr>
                <tr>
                    <td><strong>Guard:</strong> <?php echo $ir['guard']; ?> [Ranked: <?php echo $ir['guarank']; ?>]</td>
                    <td><strong>Labour:</strong> <?php echo $ir['labour']; ?> [Ranked: <?php echo $ir['labrank']; ?>]</td>
                </tr>
                <tr>
                    <td><strong>IQ: </strong> <?php echo $ir['IQ']; ?> [Ranked: <?php echo $ir['IQrank']; ?>]</td>
                    <td><strong>Total stats:</strong> <?php echo $ts; ?> [Ranked: <?php echo $tsrank; ?>]</td>
                </tr>
            </table>
        </div>
    </div>
</div>
