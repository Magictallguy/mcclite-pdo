<?php
/*
MCCodes FREE
inventory.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$db->query('SELECT inv.inv_id, inv.inv_qty, i.itmid, i.itmname, i.itmsellprice, it.itmtypename
    FROM inventory AS inv
    INNER JOIN items AS i ON inv.inv_itemid = i.itmid
    INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
    WHERE inv.inv_userid = ?
    ORDER BY it.itmtypename ASC, i.itmname ASC
');
$db->execute([$ir['userid']]);
$rows = $db->fetch();
if (null === $rows) {
    $_SESSION['info'] = 'You don\'t have any items';
    exit(header('Location: /index.php'));
}
$inventory = [];
foreach ($rows as $row) {
    if (!array_key_exists($row['itmtypename'], $inventory)) {
        $inventory[$row['itmtypename']] = 0;
    }
    if ($row['itmsellprice'] > 0) {
        $inventory[$row['itmtypename']] += $row['itmsellprice'] * $row['inv_qty'];
    }
}
$overallTotal = 0;
$lt = ''; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Inventory</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>Your items are listed below.</strong><br>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Sell Value</th>
                        <th>Total Sell Value</th>
                        <th>Links</th>
                    </tr>
                </thead>
                <tbody><?php
foreach ($rows as $row) {
    $total = $row['itmsellprice'] * $row['inv_qty'];
    $overallTotal += $total;
    if ($lt != $row['itmtypename']) {
        $toggle = false;
        $lt = $row['itmtypename']; ?>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="5">
                            <div class="float-left"><?php echo $func->format($lt); ?></div>
                            <div class="float-right">Subtotal: <?php echo $func->money($inventory[$lt]); ?></div>
                        </th>
                    </tr>
                </thead>
                <tbody><?php
    } ?>
                    <tr>
                        <td><?php echo ucwords($func->format($row['itmname'])); ?></td>
                        <td><?php echo $func->format($row['inv_qty']); ?></td>
                        <td><?php echo $row['itmsellprice'] > 0 ? $func->money($row['itmsellprice']) : '--'; ?></td>
                        <td><?php echo $total > 0 ? $func->money($total) : '--'; ?></td>
                        <td>
                            [<a href="/iteminfo.php?ID=<?php echo $row['itmid']; ?>">Info</a>]
                            [<a href="/itemsend.php?ID=<?php echo $row['inv_id']; ?>">Send</a>]
                            [<a href="/itemsell.php?ID=<?php echo $row['inv_id']; ?>">Sell</a>]
                            [<a href="/imadd.php?ID=<?php echo $row['inv_id']; ?>">Add to Market</a>] <?php
    if (in_array($row['itmtypename'], ['Food', 'Medical'])) {
        ?>
                            [<a href="/itemuse.php?ID=<?php echo $row['inv_id']; ?>">Use</a>]<?php
    } elseif ('Nuclear Bomb' == $row['itmname']) {
        ?>
                            [<a href="/nuclearbomb.php">Use</a>]<?php
    } ?>
                        </td>
                    </tr><?php
} ?>
                    <tr>
                        <th colspan="5">
                            <div class="float-right">
                                Total Inventory Resale Value: <?php echo $func->money($overallTotal); ?>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
