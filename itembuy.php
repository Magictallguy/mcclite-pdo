<?php
/*
MCCodes FREE
itembuy.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('NO_OUTPUT', true);
require_once __DIR__.'/lib/master.php';
$_POST['shop'] = array_key_exists('shop', $_POST) && ctype_digit($_POST['shop']) && $_POST['shop'] > 0 ? $_POST['shop'] : null;
$_POST['qty'] = array_key_exists('qty', $_POST) && ctype_digit($_POST['qty']) && $_POST['qty'] > 0 ? $_POST['qty'] : null;
if (null !== $_GET['ID']) {
    if (null !== $_POST['qty']) {
        $db->query('SELECT itmid, itmname, itmbuyprice, itmbuyable FROM items WHERE itmid = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['itmbuyable']) {
                $cost = $row['itmbuyprice'] * $_POST['qty'];
                if ($ir['money'] >= $cost) {
                    $log = ' bought '.$func->format($_POST['qty']).'x '.$func->format($row['itmname']).' for '.$func->money($cost);
                    $db->trans('start');
                    $func->giveItem($row['itmid'], $_POST['qty'], $ir['userid']);
                    $db->query('UPDATE users SET money = money - ? WHERE userid = ?');
                    $db->execute([$cost, $ir['userid']]);
                    $db->query('INSERT INTO itembuylogs (ibUSER, ibITEM, ibTOTALPRICE, ibQTY, ibCONTENT) VALUES (?, ?, ?, ?, ?)');
                    $db->execute([$ir['userid'], $row['itmid'], $cost, $_POST['qty'], $ir['username'].$log]);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve '.$log;
                } else {
                    $_SESSION['error'] = 'You don\'t have enough money to buy '.$func->format($_POST['qty']).' '.$func->s($_POST['qty'], '', $row['itmname']).'. You need '.$func->money($cost);
                }
            } else {
                $_SESSION['error'] = 'This item can\'t be bought!';
            }
        } else {
            $_SESSION['error'] = 'The item you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid quantity';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid item';
}
exit(header('Location: /shops.php'.(null !== $_POST['shop'] ? '?ID='.$_POST['shop'] : '')));
