<?php
/*
MCCodes FREE
iteminfo.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    $_SESSION['error'] = 'You didn\'t select a valid item';
    exit(header('/index.php'));
}
$db->query('SELECT i.itmid, i.itmname, i.itmdesc, i.itmbuyprice, i.itmsellprice, i.itmimage, it.itmtypename, it.itmtypecolor
    FROM items AS i
    INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
    WHERE i.itmid = ?
');
$db->execute([$_GET['ID']]);
$row = $db->fetch(true);
if (null === $row) {
    $_SESSION['error'] = 'The item you selected doesn\'t exist';
    exit(header('Location: /index.php'));
} ?>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2" class="bold">Looking up info on <?php echo $func->format($row['itmname']); ?></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            The <strong><?php echo $func->format($row['itmname']); ?></strong> is <?php echo $func->aAn($row['itmtypename']),' <span style="color:'.$row['itmtypecolor'].';">',$func->format($row['itmtypename']),'</span>'; ?> Item<br />
                            <em><?php echo $func->format($row['itmdesc'], true); ?></em>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center align-self-center">
                            <a href="<?php echo $row['itmimage']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['itmimage']; ?>" class="item-info img-fluid" style="box-shadow: 0 0 10px <?php echo $row['itmtypecolor']; ?>;">
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-6">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2">Item Info</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Item Buy Price</th>
                        <th>Item Sell Price</th>
                    </tr>
                    <tr>
                        <td><?php echo $row['itmbuyprice'] > 0 ? $func->money($row['itmbuyprice']) : 'N/A'; ?></td>
                        <td><?php echo $row['itmsellprice'] > 0 ? $func->money($row['itmsellprice']) : 'N/A'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
