<?php
/*
MCCodes FREE
itemmarket.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php'; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Item Market</h3>
    </div>
</div><?php
switch ($_GET['action']) {
    case 'buy':
        item_buy($db, $ir);
        break;

    case 'gift':
        item_gift($db, $ir);
        break;

    case 'edit':
        listing_edit($db, $ir);
        break;

    case 'remove':
        listing_remove($db, $ir);
        break;

    default:
        imarket_index($db, $ir);
        break;
}

function imarket_index($db, $ir)
{
    $db->query('SELECT im.imID, im.imADDER, im.imPRICE, i.itmid, i.itmname, it.itmtypename, u.userid, u.username
        FROM itemmarket AS im
        INNER JOIN items AS i ON im.imITEM = i.itmid
        INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
        INNER JOIN users AS u ON im.imADDER = u.userid
        ORDER BY i.itmtype ASC, i.itmname ASC
    ');
    $db->execute();
    $rows = $db->fetch();
    $lt = ''; ?>
<div class="row">
    <div class="col">
        Viewing all listings...<br>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Adder</th>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Links</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="4" class="center">There are currently no listings</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            if ($lt != $row['itmtypename']) {
                $lt = $row['itmtypename']; ?>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="4"><?php echo $func->format($lt); ?></th>
                    </tr>
                </thead>
                <tbody><?php
            } ?>
                    <tr>
                        <td><?php echo $func->username($row['imADDER']); ?></td>
                        <td><?php echo $func->format($row['itmname']); ?></td>
                        <td><?php echo $func->money($row['imPRICE']); ?></td>
                        <td>
                            [<a href="/iteminfo.php?ID=<?php echo $row['itmid']; ?>">Info</a>] <?php
            if ($row['userid'] == $ir['userid']) {
                ?>
                            [<a href="/itemmarket.php?action=edit&amp;ID=<?php echo $row['imID']; ?>">Edit</a>]
                            [<a href="/itemmarket.php?action=remove&amp;ID=<?php echo $row['imID']; ?>">Remove</a>]<?php
            } else {
                ?>
                            [<a href="/itemmarket.php?action=buy&amp;ID=<?php echo $row['imID']; ?>">Buy</a>]
                            [<a href="/itemmarket.php?action=gift&amp;ID=<?php echo $row['imID']; ?>">Gift</a>]<?php
            } ?>
                        </td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
}

function listing_edit($db, $ir)
{
    $redirect = true;
    if (null !== $_GET['ID']) {
        $db->query('SELECT im.imID, im.imADDER, im.imPRICE, i.itmid, i.itmname
            FROM itemmarket AS im
            INNER JOIN items AS i ON im.imITEM = i.itmid
            WHERE im.imID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['imADDER'] == $ir['userid']) {
                $item = format($row['itmname']);
                $aAnItem = aAn($row['itmname']).' '.$item;
                if (array_key_exists('submit', $_POST)) {
                    $_POST['price'] = array_key_exists('price', $_POST) && ctype_digit($_POST['price']) && $_POST['price'] > 0 ? $_POST['price'] : null;
                    if (null !== $_POST['price']) {
                        $db->query('UPDATE itemmarket SET imPRICE = ? WHERE imID = ?');
                        $db->execute([$_POST['price'], $row['imID']]);
                        $_SESSION['success'] = 'You\'ve altered the listed price for your '.$item;
                    } else {
                        $_SESSION['error'] = 'You didn\'t enter a valid price';
                    }
                } else {
                    $redirect = false; ?>
                    <div class="row">
                        <div class="col">
                            Editing your <?php echo $item; ?> listing, currently listed at <?php echo money($row['imPRICE']); ?><br>
                            <form action="/itemmarket.php?action=edit&amp;ID=<?php echo $row['imID']; ?>" method="post" class="form">
                                <div class="form-group">
                                    <label for="price" class="form-label">Price</label>
                                    <input type="number" name="price" id="price" class="form-control bg-dark text-light" required autofocus>
                                </div>
                                <div class="form-controls">
                                    <button type="submit" name="submit" class="btn btn-primary">
                                        <span class="fas fa-edit"></span>
                                        Edit listing
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><?php
                }
            } else {
                $_SESSION['error'] = 'That listing isn\'t yours';
            }
        } else {
            $_SESSION['error'] = 'The listing you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid listing';
    }
    if (true === $redirect) {
        exit(header('Location: /itemmarket.php'));
    }
}

function listing_remove($db, $ir)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT im.imID, im.imADDER, i.itmid, i.itmname
            FROM itemmarket AS im
            INNER JOIN items AS i ON im.imITEM = i.itmid
            WHERE im.imID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['imADDER'] == $ir['userid']) {
                $aAnItem = $func->aAn($row['itmname']).' '.$func->format($row['itmname']);
                $db->trans('start');
                $invID = $func->giveItem($row['itmid']);
                $db->query('INSERT INTO imremovelogs (imrITEM, imrADDER, imrREMOVER, imrIMID, imrINVID, imrCONTENT) VALUES (?, ?, ?, ?, ?, ?)');
                $db->execute([$row['itmid'], $row['imADDER'], $ir['userid'], $row['imID'], $invID, $func->format($ir['username']).' removed '.$aAnItem.' from the Item Market']);
                $db->query('DELETE FROM itemmarket WHERE imID = ?');
                $db->execute([$row['imID']]);
                $db->trans('end');
                $_SESSION['success'] = 'You\'ve removed '.$aAnItem.' from the Item Market';
            } else {
                $_SESSION['error'] = 'That listing isn\'t yours';
            }
        } else {
            $_SESSION['error'] = 'The listing you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid listing';
    }
    exit(header('Location: /itemmarket.php'));
}

function item_buy($db, $ir)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT im.imID, im.imADDER, im.imITEM, im.imPRICE, i.itmname
            FROM itemmarket AS im
            INNER JOIN items AS i ON im.imITEM = i.itmid
            WHERE im.imID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['imADDER'] != $ir['userid']) {
                if ($ir['money'] >= $row['imPRICE']) {
                    $item = $func->format($row['itmname']);
                    $aAnItem = $func->aAn($row['itmname']).' '.$item;
                    $log = 'bought '.$aAnItem.' from the Item Market for '.$func->money($row['imPRICE']);
                    $db->trans('start');
                    $invID = $func->giveItem($row['imITEM']);
                    $db->query('UPDATE users SET money = money - ? WHERE userid = ?');
                    $db->execute([$ir['userid']]);
                    $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
                    $db->execute([$row['imADDER']]);
                    $func->event_add($row['imADDER'], '<a href="/viewuser.php?u='.$ir['userid'].'">'.$func->format($ir['username']).'</a> bought your '.$item.' from the Item Market for '.$func->money($row['imPRICE']));
                    $db->query('INSERT INTO imbuylogs (imbITEM, imbADDER, imbBUYER, imbPRICE, imbIMID, imbINVID, imbCONTENT) VALUES (?, ?, ?, ?, ?, ?, ?)');
                    $db->execute([$row['imITEM'], $row['imADDER'], $ir['userid'], $row['imPRICE'], $row['imID'], $invID, $func->format($ir['username']).' '.$log]);
                    $db->query('DELETE FROM itemmarket WHERE imID = ?');
                    $db->execute([$row['imID']]);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve '.$log;
                } else {
                    $_SESSION['error'] = 'You don\'t have enough cash';
                }
            } else {
                $_SESSION['error'] = 'You can\'t purchase your own listing';
            }
        } else {
            $_SESSION['error'] = 'The listing you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid listing';
    }
    exit(header('Location: /itemmarket.php'));
}

function item_gift($db, $ir)
{
    $redirect = false;
    $gift = false;
    if (null !== $_GET['ID']) {
        $db->query('SELECT im.imID, im.imADDER, im.imPRICE, i.itmid, i.itmname
            FROM itemmarket AS im
            INNER JOIN items AS i ON im.imITEM = i.itmid
            WHERE im.imID = ?
        ');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['imADDER'] != $ir['userid']) {
                if ($ir['money'] >= $row['imPRICE']) {
                    $item = format($row['itmname']);
                    $aAnItem = aAn($row['itmname']).' '.$item;
                    if (array_key_exists('submit', $_POST)) {
                        $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
                        $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
                        $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
                        if (null !== $_POST['user']) {
                            if ($_POST['user'] != $ir['userid']) {
                                if ($_POST['user'] != $row['imADDER']) {
                                    $db->query('SELECT userid, username, user_level FROM users WHERE userid = ?');
                                    $db->execute([$_POST['user']]);
                                    $recip = $db->fetch(true);
                                    if (null !== $recip) {
                                        if ($recip['user_level'] > 0) {
                                            $log = 'purchased %s from the Item Market for '.$func->money($row['imPRICE']);
                                            $db->trans('start');
                                            $invID = $func->giveItem($row['itmid'], 1, $recip['userid']);
                                            $db->query('UPDATE users SET money = money - ? WHERE userid = ?');
                                            $db->execute([$row['imPRICE'], $ir['userid']]);
                                            $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
                                            $db->execute([$row['imPRICE'], $row['imADDER']]);
                                            $func->event_add($row['imADDER'], '{user} '.sprintf($log, 'your '.$item), $ir['userid']);
                                            $func->event_add($recip['userid'], '{user} '.sprintf($log, $aAnItem).' and gifted it to you', $ir['userid']);
                                            $db->query('INSERT INTO imbuylogs (imbITEM, imbADDER, imbBUYER, imbPRICE, imbIMID, imbINVID, imbCONTENT) VALUES (?, ?, ?, ?, ?, ?, ?)');
                                            $db->execute([$row['imITEM'], $row['imADDER'], $ir['userid'], $row['imPRICE'], $row['imID'], $invID, $func->format($ir['username']).' '.sprintf($log, $aAnItem).' and gifted it to '.$func->format($recip['username']).' ['.$recip['userid'].']']);
                                            $db->trans('end');
                                            $_SESSION['success'] = 'You\'ve '.sprintf($log, $aAnItem).' and gifted it to '.$func->username($recip['userid']);
                                        } else {
                                            $_SESSION['error'] = 'You can\'t send gifts to NPCs';
                                            $gift = $row['imID'];
                                        }
                                    } else {
                                        $_SESSION['error'] = 'Your intended recipient doesn\'t exist';
                                        $gift = $row['imID'];
                                    }
                                } else {
                                    $_SESSION['error'] = 'You can\'t gift this listing back to the seller';
                                    $gift = $row['imID'];
                                }
                            } else {
                                $_SESSION['error'] = 'You can\'t gift this listing to yourself';
                                $gift = $row['imID'];
                            }
                        } else {
                            $_SESSION['error'] = 'You didn\'t select a valid recipient';
                            $gift = $row['imID'];
                        }
                    } else {
                        $redirect = false; ?>
                        Gifting <?php echo $aAnItem; ?> for <?php echo $func->money($row['imPRICE']); ?>.<br>
                        Recipient:<br><br>
                        <form action="/itemmarket.php?action=gift&amp;ID=<?php echo $row['imID']; ?>" method="post" class="form">
                            <div class="form-group">
                                <label for="user1" class="form-label">Select</label>
                                <?php echo $func->user_dropdown('user1'); ?>
                            </div>
                            <div class="form-group">
                                <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                                <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                            </div>
                            <div class="form-controls">
                                <button type="submit" name="submit" class="btn btn-primary">
                                    <span class="fas fa-shopping-basket"></span>
                                    Purchase and gift
                                </button>
                            </div>
                        </form><?php
                    }
                } else {
                    $_SESSION['error'] = 'You don\'t have enough money';
                }
            } else {
                $_SESSION['error'] = 'You can\'t purchase your own listing';
            }
        } else {
            $_SESSION['error'] = 'The listing you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid listing';
    }
    if (true === $redirect) {
        exit(header('Location: /itemmarket.php'.(false !== $gift ? '?action=gift&ID='.$gift : '')));
    }
}
