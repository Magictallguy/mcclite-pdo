<?php

require_once __DIR__.'/lib/master.php';
$_GET['disp'] = array_key_exists('disp', $_GET) && in_array($_GET['disp'], ['table', 'grid']) ? $_GET['disp'] : 'table';
$owned = [];
$db->query('SELECT inv_itemid, inv_qty FROM inventory WHERE inv_userid = ?');
$db->execute([$ir['userid']]);
$myItems = $db->fetch();
if (null !== $myItems) {
    foreach ($myItems as $item) {
        $owned[$item['inv_itemid']] = $item['inv_qty'];
    }
}
$db->query('SELECT i.itmid, i.itmtype, i.itmname, i.itmdesc, i.itmimage, i.itmbuyprice, i.itmsellprice, i.itmbuyable, it.itmtypename, it.itmtypecolor
    FROM items AS i
    INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
    ORDER BY it.itmtypename ASC, i.itmname ASC
');
$db->execute();
$rows = $db->fetch();
if (null === $rows) {
    $_SESSION['info'] = 'There are no items';
    exit(header('Location: /explore.php'));
}
$inventory = [];
foreach ($rows as $row) {
    if (!array_key_exists($row['itmtypename'], $inventory)) {
        $inventory[$row['itmtypename']] = [
            'buy' => 0,
            'sell' => 0,
        ];
    }
    if ($row['itmbuyable'] > 0 && $row['itmbuyprice'] > 0) {
        $inventory[$row['itmtypename']]['buy'] += $row['itmbuyprice'];
    }
    if ($row['itmsellprice'] > 0) {
        $inventory[$row['itmtypename']]['sell'] += $row['itmsellprice'];
    }
}?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Itempedia</h3>
        <p>
            Display: <a href="/itempedia.php?disp=table"<?php echo 'table' == $_GET['disp'] ? ' class="bold"' : ''; ?>>Table</a> &middot;
            <a href="/itempedia.php?disp=grid"<?php echo 'grid' == $_GET['disp'] ? ' class="bold"' : ''; ?>>Grid</a>
        </p>
    </div>
</div>
<div class="row">
    <div class="col"><?php
if ('table' == $_GET['disp']) {
    ?>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Values</th>
                    </tr>
                </thead>
                <tbody><?php
        $lt = '';
    $totals = [
            'buy' => 0,
            'sell' => 0,
        ];
    foreach ($rows as $row) {
        if ($lt != $row['itmtypename']) {
            $lt = $row['itmtypename'];
            $totals['buy'] += $inventory[$lt]['buy'];
            $totals['sell'] += $inventory[$lt]['sell']; ?>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="2" style="color:<?php echo $row['itmtypecolor']; ?>"><?php echo $func->format($lt); ?></th>
                    </tr>
                    <tr>
                        <th colspan="2">
                            <div class="float-left">
                                Subtotal purchase value: <?php echo $inventory[$lt]['buy'] > 0 ? $func->money($inventory[$lt]['buy']) : '--'; ?>
                            </div>
                            <div class="float-right">
                                Subtotal resale value: <?php echo $inventory[$lt]['sell'] > 0 ? $func->money($inventory[$lt]['sell']) : '--'; ?>
                            </div>
                        </th>
                    </tr>
                </thead>
                <tbody><?php
        } ?>
                    <tr>
                        <td style="width:75%;">
                            <div class="float-left" style="padding-right:5px;">
                                <a href="<?php echo $row['itmimage']; ?>" data-toggle="lightbox">
                                    <img src="<?php echo $row['itmimage']; ?>" class="itempedia-item img-fluid" style="box-shadow:0 0 10px <?php echo $row['itmtypecolor']; ?>;">
                                </a>
                            </div>
                            <a href="/iteminfo.php?ID=<?php echo $row['itmid']; ?>"><?php echo ucwords($func->format($row['itmname'])); ?></a>
                            <?php echo array_key_exists($row['itmid'], $owned) ? ' <span title="You own '.$func->format($owned[$row['itmid']]).'"><span class="badge badge-info">'.$func->format($owned[$row['itmid']]).'</span></span>' : ''; ?><br>
                            <small class="italic"><?php echo $func->format(str_replace('****', "\n", $row['itmdesc']), true); ?></small>
                        </td>
                        <td style="width:25%;">
                            <?php echo !$row['itmbuyable'] ? 'Can\'t be purchased in the shops' : ($row['itmbuyprice'] > 0 ? 'Buy for '.$func->money($row['itmbuyprice']) : 'Free'); ?><br>
                            <?php echo $row['itmsellprice'] > 0 ? 'Sell for '.$func->money($row['itmsellprice']) : 'No sell value'; ?>
                        </td>
                    </tr><?php
    } ?>
                    <tr>
                        <th>Total purchase value: <?php echo $func->money($totals['buy']); ?></th>
                        <th>Total resale value: <?php echo $func->money($totals['sell']); ?></th>
                    </tr>
                </tbody>
            </table>
        </div><?php
} elseif ('grid' == $_GET['disp']) {
        $cnt = 0;
        $lt = ''; ?>
        <div class="row"><?php
        foreach ($rows as $row) {
            if ($lt != $row['itmtypename']) {
                $cnt = 0;
                $lt = $row['itmtypename']; ?>
        </div>
        <div class="row">
            <div class="col" style="margin-top:15px;margin-bottom:5px;">
                <h3 class="page-subtitle" style="color:<?php echo $row['itmtypecolor']; ?>;"><?php echo $func->format($lt); ?></h3>
            </div>
        </div>
        <div class="row"><?php
            }
            ++$cnt; ?>
            <div class="col-3 text-center">
                <a href="<?php echo $row['itmimage']; ?>" data-toggle="lightbox">
                    <img src="<?php echo $row['itmimage']; ?>" class="itempedia-item img-fluid" style="box-shadow:0 0 10px <?php echo $row['itmtypecolor']; ?>;">
                </a><br>
                <a href="/iteminfo.php?ID=<?php echo $row['itmid']; ?>"><?php echo $func->format($row['itmname']); ?></a><br>
                <small class="italic"><?php echo $func->format($row['itmdesc'], true); ?></small>
            </div><?php
        $div = $cnt % 4;
            if (!$div) {
                ?>
        </div>
        <div class="row">
            <div class="col">
                &nbsp;
            </div>
        </div>
        <div class="row"><?php
            }
        }
        if (1 == $div) {
            echo str_repeat('<div class="col-3">&nbsp;</div>', 3);
        } elseif (2 == $div) {
            echo str_repeat('<div class="col-3">&nbsp;</div>', 2);
        } elseif (3 == $div) {
            echo '<div class="col-3">&nbsp;</div>';
        } ?>
        </div><?php
    }?>
    </div>
</div>
