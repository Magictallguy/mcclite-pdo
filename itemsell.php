<?php
/*
MCCodes FREE
itemsell.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$redirect = true;
if (null !== $_GET['ID']) {
    $db->query('SELECT iv.inv_id, iv.inv_qty, iv.inv_userid, i.itmid, i.itmname, i.itmsellprice
        FROM inventory AS iv
        INNER JOIN items AS i ON iv.inv_itemid = i.itmid
        WHERE iv.inv_id = ?
    ');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['inv_userid'] == $ir['userid']) {
            $item = $func->format($row['itmname']);
            $_GET['qty'] = array_key_exists('qty', $_GET) && ctype_digit($_GET['qty']) && $_GET['qty'] > 0 ? $_GET['qty'] : null;
            if (null !== $_GET['qty']) {
                if ($row['inv_qty'] >= $_GET['qty']) {
                    $gain = $row['itmsellprice'] * $_GET['qty'];
                    $log = 'sold '.$func->format($_GET['qty']).'x '.$item.' for '.$func->money($gain);
                    $db->trans('start');
                    $func->takeItem($row['itmid'], $_GET['qty']);
                    $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
                    $db->execute([$gain, $ir['userid']]);
                    $db->query('INSERT INTO itemselllogs (isUSER, isITEM, isTOTALPRICE, isQTY, isCONTENT) VALUES (?, ?, ?, ?, ?)');
                    $db->execute([$ir['userid'], $row['itmid'], $gain, $_GET['qty'], $func->format($ir['username']).' '.$log]);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve '.$log;
                } else {
                    $_SESSION['error'] = 'You don\'t have '.$func->format($_GET['qty']).'x '.$item;
                }
            } else {
                $redirect = false; ?>
<div class="row">
    <div class="col">
        You have <?php echo $func->format($row['inv_qty']),'x ',$item; ?><br>
        Each is worth <?php echo $func->money($row['itmsellprice']); ?><br><?php
        if ($row['inv_qty'] > 1 && $row['itmsellprice'] > 0) {
            echo 'Total (for ',$func->format($row['inv_qty']),'): ',$func->money($row['itmsellprice'] * $row['inv_qty']),'<br>';
        } ?>
        <form action="/itemsell.php" method="get" class="form">
            <input type="hidden" name="ID" value="<?php echo $row['inv_id']; ?>">
            <div class="form-group">
                <label for="qty" class="form-label">Quantity</label>
                <input type="number" name="qty" id="qty" class="form-control bg-dark text-light" required autofocus>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-dollar"></span>
                    Sell
                </button>
            </div>
        </form>
    </div>
</div><?php
            }
        } else {
            $_SESSION['error'] = 'That inventory listing isn\'t yours'; // dafuq fam?
        }
    } else {
        $_SESSION['error'] = 'The inventory listing you selected doesn\'t exist';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid inventory listing';
}
if (true === $redirect) {
    exit(header('Location: /inventory.php'));
}
