<?php
/*
MCCodes FREE
itemsend.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$redirect = true;
$extra = '';
if (null !== $_GET['ID']) {
    $db->query('SELECT iv.inv_id, iv.inv_itemid, iv.inv_userid, iv.inv_qty, i.itmid, i.itmname
        FROM inventory AS iv
        INNER JOIN items AS i ON iv.inv_itemid = i.itmid
        WHERE iv.inv_id = ?
    ');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['inv_userid'] == $ir['userid']) {
            $item = $func->format($row['itmname']);
            $_GET['qty'] = array_key_exists('qty', $_GET) && ctype_digit($_GET['qty']) && $_GET['qty'] > 0 ? $_GET['qty'] : null;
            $_GET['user1'] = array_key_exists('user1', $_GET) && ctype_digit($_GET['user1']) && $_GET['user1'] > 0 ? $_GET['user1'] : null;
            $_GET['user2'] = array_key_exists('user2', $_GET) && ctype_digit($_GET['user2']) && $_GET['user2'] > 0 ? $_GET['user2'] : null;
            $_GET['user'] = null === $_GET['user2'] ? $_GET['user1'] : $_GET['user2'];
            if (null !== $_GET['qty'] && null !== $_GET['user']) {
                if ($row['inv_qty'] >= $_GET['qty']) {
                    $db->query('SELECT userid, username, user_level FROM users WHERE userid = ?');
                    $db->execute([$_GET['user']]);
                    $recip = $db->fetch(true);
                    if (null !== $recip) {
                        if ($recip['user_level'] > 0) {
                            $db->trans('start');
                            $func->takeItem($row['itmid'], $_GET['qty']);
                            $func->giveItem($row['itmid'], $_GET['qty'], $recip['userid']);
                            $func->event_add($recip['userid'], '{user} sent you '.$func->format($_GET['qty']).'x '.$item, $ir['userid']);
                            $db->query('INSERT INTO itemxferlogs (ixFROM, ixTO, ixITEM, ixQTY) VALUES (?, ?, ?, ?)');
                            $db->execute([$ir['userid'], $recip['userid'], $row['itmid'], $_GET['qty']]);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve sent '.$func->format($_GET['qty']).'x '.$item.' to '.$func->username($recip['userid']);
                        } else {
                            $_SESSION['error'] = 'You can\'t send items to NPCs';
                        }
                    } else {
                        $_SESSION['error'] = 'You\'re intended recipient doesn\'t exist';
                    }
                } else {
                    $_SESSION['error'] = 'You don\'t have '.format($_GET['qty']).'x '.$item;
                }
            } else {
                $redirect = false; ?>
                You have <?php echo $func->format($row['inv_qty']),'x ',$item; ?><br>
                <form action="/itemsend.php" method="get" class="form">
                    <input type="hidden" name="ID" value="<?php echo $row['inv_id']; ?>">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="user1" class="form-label">Select recipient</label>
                                <?php echo $func->user_dropdown('user1'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                                <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="qty" class="form-label">Quantity</label>
                                <input type="number" name="qty" id="qty" class="form-control bg-dark text-light" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-controls">
                        <button type="submit" class="btn btn-primary">
                            <span class="fas fa-user-check"></span>
                            Send
                        </button>
                    </div>
                </form><?php
            }
        } else {
            $_SESSION['error'] = 'That inventory listing isn\'t yours'; // srsly tho
        }
    } else {
        $_SESSION['error'] = 'That inventory listing doesn\'t exist';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid inventory listing';
}
if (true === $redirect) {
    exit(header('Location: /inventory.php'));
}
