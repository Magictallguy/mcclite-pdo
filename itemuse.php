<?php
/*
MCCodes FREE
itemuse.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('NO_OUTPUT', true);
require_once __DIR__.'/lib/master.php';
if (null !== $_GET['ID']) {
    $db->query('SELECT iv.inv_id, iv.inv_userid, i.itmid, i.itmname, it.itmtypename
        FROM inventory AS iv
        INNER JOIN items AS i ON iv.inv_itemid = i.itmid
        INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
        WHERE iv.inv_id = ?
    ');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['inv_userid'] == $ir['userid']) {
            $item = $func->format($row['itmname']);
            $type = strtolower($row['itmtypename']);
            if (in_array($type, ['food', 'medical'])) {
                if (!in_array(strtolower($row['itmname']), ['will potion', 'full restore'])) {
                    $cols = [
                        'medical' => [
                            'col-item' => 'health',
                            'col-users' => 'hp',
                        ],
                        'food' => [
                            'col-item' => 'energy',
                            'col-users' => 'energy',
                        ],
                    ];
                    $db->query('SELECT '.$cols[$type]['col-item'].' FROM '.$type.' WHERE item_id = ?');
                    $db->execute([$row['itmid']]);
                    $inc = $db->result();
                    if ($inc > 0) {
                        $db->trans('start');
                        $func->takeItem($row['itmid']);
                        $db->query('UPDATE users SET '.$cols[$type]['col-users'].' = LEAST('.$cols[$type]['col-users'].' + ?, max'.$cols[$type]['col-users'].') WHERE userid = ?');
                        $db->execute([$inc, $ir['userid']]);
                        $db->trans('end');
                        $_SESSION['success'] = 'You\'ve used '.$func->aAn($itemLower).' '.$item.'.';
                    } else {
                        $_SESSION['error'] = 'This item is bugged. Please report this.';
                    }
                } else {
                    $extra = '';
                    $itemLower = strtolower($row['itmname']);
                    if ('full restore' == $itemLower) {
                        $extra .= ', will = maxwill, brave = maxbrave';
                    } elseif ('will potion' == $itemLower) {
                        $extra .= ', will = maxwill';
                    }
                    $db->trans('start');
                    $func->takeItem($row['itmid']);
                    $db->query('UPDATE users SET username = username'.$extra.' WHERE userid = ?');
                    $db->execute([$ir['userid']]);
                    $db->trans('end');
                    $_SESSION['success'] = 'You sprayed a '.$item.' into your mouth and immediately felt the benefits';
                }
            } else {
                $_SESSION['error'] = 'The '.$item.' can\'t be used in this manner';
            }
        } else {
            $_SESSION['error'] = 'That inventory listing isn\'t yours'; // S T A H P
        }
    } else {
        $_SESSION['error'] = 'The inventory listing you selected doesn\'t exist';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid inventory listing';
}
exit(header('Location: /inventory.php'));
