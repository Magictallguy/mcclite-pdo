-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 04, 2019 at 12:47 AM
-- Server version: 10.3.17-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- --------------------------------------------------------

--
-- Table structure for table `adminlogs`
--

CREATE TABLE `adminlogs` (
  `adID` int(11) NOT NULL,
  `adUSER` int(11) NOT NULL DEFAULT 0,
  `adPOST` longtext NOT NULL,
  `adGET` longtext NOT NULL,
  `adTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `adID` int(11) NOT NULL,
  `adIMG` text NOT NULL,
  `adURL` text NOT NULL,
  `adVIEWS` int(11) NOT NULL DEFAULT 0,
  `adCLICKS` int(11) NOT NULL DEFAULT 0,
  `adLOGIN` varchar(191) NOT NULL DEFAULT '',
  `adPASS` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `armour`
--

CREATE TABLE `armour` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `item_ID` int(11) NOT NULL DEFAULT 0,
  `Defence` int(11) NOT NULL DEFAULT 0,
  INDEX(`item_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `armour`
--

INSERT INTO `armour` (`item_ID`, `Defence`) VALUES
(71, 1),
(72, 1000000),
(73, 2),
(74, 4),
(75, 7),
(76, 50),
(77, 10),
(79, 15),
(80, 20),
(81, 30),
(82, 40),
(83, 75),
(84, 100),
(85, 150),
(91, 40),
(92, 100),
(93, 150),
(94, 30),
(97, 400),
(98, 750);

-- --------------------------------------------------------

--
-- Table structure for table `attacklogs`
--

CREATE TABLE `attacklogs` (
  `log_id` int(11) NOT NULL,
  `attacker` int(11) NOT NULL DEFAULT 0,
  `attacked` int(11) NOT NULL DEFAULT 0,
  `result` enum('won','lost') NOT NULL DEFAULT 'won',
  `time_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `stole` int(11) NOT NULL DEFAULT 0,
  `attacklog` longtext NOT NULL,
  INDEX(`attacker`)
  INDEX(`attacked`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `blacklist`
--

CREATE TABLE `blacklist` (
  `bl_ID` int(11) NOT NULL,
  `bl_ADDER` int(11) NOT NULL DEFAULT 0,
  `bl_ADDED` int(11) NOT NULL DEFAULT 0,
  `bl_COMMENT` varchar(191) NOT NULL DEFAULT '',
  INDEX(`bl_ADDED`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cashxferlogs`
--

CREATE TABLE `cashxferlogs` (
  `cxID` int(11) NOT NULL,
  `cxFROM` int(11) NOT NULL DEFAULT 0,
  `cxTO` int(11) NOT NULL DEFAULT 0,
  `cxAMOUNT` int(11) NOT NULL DEFAULT 0,
  `cxFROMIP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `cxTOIP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `cxCONTENT` longtext NOT NULL,
  `cxTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `challengesbeaten`
--

CREATE TABLE `challengesbeaten` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `npcid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `cityid` int(11) NOT NULL,
  `cityname` varchar(191) NOT NULL DEFAULT '',
  `citydesc` longtext NOT NULL,
  `cityminlevel` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`cityid`, `cityname`, `citydesc`, `cityminlevel`) VALUES
(1, '{GAME_NAME} Central', 'This city has been the home of newcomers to {GAME_NAME} for many years... it is resourceful and provides many job opportunities.', 1),
(2, 'Country Farms', 'A cheap place to buy food, this is a peaceful place for pacifists but property is very expensive.', 5),
(3, 'El Ablo', 'The place of the truly strong.', 20),
(4, 'Industrial Sector', 'The industrial sector of {GAME_NAME} Central.', 1),
(5, 'Cyber State', 'One for those who are masters at the game', 50);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `crID` int(11) NOT NULL,
  `crNAME` varchar(191) NOT NULL DEFAULT '',
  `crDESC` text NOT NULL,
  `crCOST` int(11) NOT NULL DEFAULT 0,
  `crENERGY` int(11) NOT NULL DEFAULT 0,
  `crDAYS` int(11) NOT NULL DEFAULT 0,
  `crSTR` int(11) NOT NULL DEFAULT 0,
  `crGUARD` int(11) NOT NULL DEFAULT 0,
  `crLABOUR` int(11) NOT NULL DEFAULT 0,
  `crAGIL` int(11) NOT NULL DEFAULT 0,
  `crIQ` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`crID`, `crNAME`, `crDESC`, `crCOST`, `crENERGY`, `crDAYS`, `crSTR`, `crGUARD`, `crLABOUR`, `crAGIL`, `crIQ`) VALUES
(1, 'Computers course', 'Learn the basics of computers.', 500, 20, 7, 0, 0, 0, 0, 50),
(2, 'Intermediate Computer Course', 'Learn Intermediate Computer Course', 1500, 40, 10, 0, 0, 0, 0, 130),
(3, 'Massage therapy Course', 'Pretty self explainatory ^^', 5000, 55, 21, 500, 0, 500, 0, 180);

-- --------------------------------------------------------

--
-- Table structure for table `coursesdone`
--

CREATE TABLE `coursesdone` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `courseid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crimegroups`
--

CREATE TABLE `crimegroups` (
  `cgID` int(11) NOT NULL,
  `cgNAME` varchar(191) NOT NULL DEFAULT '',
  `cgORDER` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `crimegroups`
--

INSERT INTO `crimegroups` (`cgID`, `cgNAME`, `cgORDER`) VALUES
(1, 'Search for money', 1),
(2, 'Sell illegal CDs', 2),
(3, 'Stealing Cars', 7),
(4, 'Pickpocketing', 3),
(5, 'Larceny', 5),
(6, 'Murder', 6),
(7, 'Deal Drugs', 4),
(8, 'Illegal Betting', 8),
(9, 'Abduction', 9),
(10, 'misc.', 10),
(11, 'IQ CRIMES', 11);

-- --------------------------------------------------------

--
-- Table structure for table `crimes`
--

CREATE TABLE `crimes` (
  `crimeID` int(11) NOT NULL,
  `crimeNAME` varchar(191) NOT NULL DEFAULT '',
  `crimeBRAVE` int(11) NOT NULL DEFAULT 0,
  `crimePERCFORM` text NOT NULL,
  `crimeSUCCESSMUNY` int(11) NOT NULL DEFAULT 0,
  `crimeGROUP` int(11) NOT NULL DEFAULT 0,
  `crimeITEXT` text NOT NULL,
  `crimeSTEXT` text NOT NULL,
  `crimeFTEXT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `crimes`
--

INSERT INTO `crimes` (`crimeID`, `crimeNAME`, `crimeBRAVE`, `crimePERCFORM`, `crimeSUCCESSMUNY`, `crimeGROUP`, `crimeITEXT`, `crimeSTEXT`, `crimeFTEXT`) VALUES
(1, 'Near The Council', 1, '((WILL*0.8)/1)+(LEVEL/4)', 10, 1, '1. You head over to the council.\n\r\n2. You start scavenging for coins.\n', 'Result: You collect ${money} from the gutters!', 'Result: There\'s no change to be seen!'),
(2, 'Under a Hobo\'s Shack', 1, '((WILL*0.8)/1.5)+(LEVEL/4)', 20, 1, '1. You head over to the shack of well-known hobo, Kent.\n\r\n2. You creep underneath and start scavenging for money.\n', 'Result: You dig up ${money}!', 'Result: Kent finds you and says \"What yo doin here?\". You walk off briskly.'),
(3, 'Music CDS', 3, '((WILL*0.8)/3)+(LEVEL/4)', 30, 2, '1. You go down to the local market and buy some blank CDs.\n\r\n2. You head home and burn some music onto these CDs.\n\r\n3. You go back to the market, set up your stall, and wait for customers.\n', 'Result: You sell your stock making you ${money}!', 'Result: No-one wants to buy your worthless garbage!'),
(4, 'Video CDs', 3, '((WILL*0.8)/3.5)+(LEVEL/4)', 34, 2, '1. You go down to the local market and buy some blank CDs.\n\r\n2. You head home and burn some videos onto these CDs.\n\r\n3. You go back to the market, set up your stall, and wait for customers.\n', 'Result: You sell your stock making you ${money}!', 'Result: No-one wants to buy your worthless garbage!'),
(5, 'Honda Civic', 20, '((WILL*0.8)/20)+(LEVEL/4)', 7000, 3, '1. You head down to the local car yard.\n\r\n2. You spot a Honda Civic, pretty shiny you\'d say!\n\r\n3. You throw a rock at the window and hop in!\n', 'Result: You insert your fake key and rocket off to your buds, where you give him the car and he gives you ${money} for the job!', 'Result: There\'s no battery, so the car won\'t run! You hop out and run away.'),
(6, 'Ford Falcon', 25, '((WILL*0.7)/25)+(LEVEL/4)', 25000, 3, '1. You head down to the local car yard.\n\r\n2. You spot a Ford Falcon, ooh it looks neat!\n\r\n3. You throw a rock at the window, but then spy a security camera attached to the steering wheel.\n\r\n4. You jump in, make sure you\'re out of the camera\'s view, and attempt to work it out of its socket.\n', '5. The camera pops out, you smile and throw it away.\n\r\nResult: You insert your fake key and rocket off to your buds, where you give him the car and he gives you ${money} for the job!', 'Result: The camera won\'t budge. You abandon your attempt and walk home.'),
(7, 'Hobo', 6, '((WILL*0.8)/6)+(LEVEL/4)', 50, 4, '1. You go down the street searching for a hobo.\n\r\n2. You see a particularly nice-looking hobo dozing on the sidewalk.\n\r\n3. You bend down and stick your hand into his pocket.\n', '4. You grab some notes and run away swiftly.\n\r\nResult: You count up the notes, there\'s $50!', 'The hobo stirs. You dash away, not wanting to let him see you.'),
(8, 'Teenager', 6, '((WILL*0.8)/6.5)+(LEVEL/4)', 100, 4, '1. You go down the street searching for a teenager.\n\r\n2. You see a kid listening to his walkman walk down the sidewalk.\n\r\n3. You quietly go up to him and put a hand into his pocket.\n', '4. You grab a wallet and run away swiftly.\n\r\nResult: You open the wallet and count up the notes, there\'s $100!', 'Result: You hit his pocket protector, you walk away slowly.'),
(9, 'Shed', 10, '((WILL*0.8)/10)+(LEVEL/4)', 260, 5, '1. You head to a back alley where there are lots of sheds.\n\r\n2. You find a particularly nice-looking shed, check that there\'s no-one inside, and break open the door.\n\r\n3. You start searching the draws.\n', 'Result: You find $260 in a lower draw!', 'Result: You hear footsteps. You don\'t know if they are coming towards this shed, but you can\'t take the risk. You dash away with nothing in your pocket.'),
(10, 'Motel Room', 11, '((WILL*0.8)/11)+(LEVEL/4)', 400, 5, '1. You head over to your mates to pick up your assignment.\n\r\n2. He gives you a piece of paper with the address on it. You hop in your car and drive there.\n\r\n3. You throw a large rock at the window of the motel room and hop in.\n\r\n4. You start searching for the computer your mate told you to steal for him.\n', '5. You find the computer, it looks pretty new and powerful!\n\r\n6. You disconnect the computer from the power at the wall, disconnect the various wires, and pack it all into a cardboard box as tall as your head.\n\r\n7. You carry the box into your car and drive back to your mate\'s.\n\r\nResult: As promised, he gives you 20% of the computer\'s worth. You feel highly content with the $400 in your wallet.', 'Result: The alarm starts screeching. You get out as fast as possible and drive away.'),
(11, 'Horse Racing', 30, '((WILL*0.7)/30)+(LEVEL/5)', 100000, 8, '1. You set up the equipment required.\n\r\n2. The target comes in.\n\r\n3. You persuade him to make a bet of $100000 on a horse.\n\r\n4. Using delayed TV, you show him the race, knowing he has picked the wrong one.\n', 'Result: Operation Success!', 'Result: You misread the horse that won and made him bet on the winning horse! Doh!'),
(12, 'Working Man', 6, '((WILL*0.8)/6.9)+(LEVEL/4)', 150, 4, '1. You go down the street searching for a normal everyday working man.\n\r\n2. You see a man briskly walking, carrying a black bag.\n\r\n3. You quietly go up to him and reach a hand out for his bag.\n', '4. You grab it and dash off down an alleyway.\n\r\nResult: You open the bag and count up the money, there\'s $150!', 'Result: You cop one in the nose as he shoos you away.'),
(13, 'Cocaine', 8, '((WILL*0.8)/8)+(LEVEL/4)', 200, 7, '1. You pick up a load of cocaine from your mate\'s and drive south on the M1 highway.\n\r\n2. You see coppers chasing after you, you turn off to avoid them.\n', '3. You get off their tail and deliver the goods, collecting your fee.\n\r\nResult: You feel good with $200 in your pocket!', 'Result: As they pull nearer to you you leap out of the van and run off.'),
(14, 'Businessman', 15, '((WILL*0.8)/15)+(LEVEL/4)', 2000, 6, '1. You arm yourself with your trusty M16 and meet your accomplice at the bus station. He gives you the job.\n\r\n2. You drive to the target\'s mansion.\n\r\n3. He steps out of his car, you tense up and get ready to fire.\n', '4. You blow his head off.\n\r\nResult: You drive home to find $2,000 in your letterbox for the job!', 'Result: You wrongly identified the businessman, you blew a street walker\'s head off instead! Unlucky!'),
(15, 'Casino Fraud', 35, '((WILL*0.6)/35)+(LEVEL/5)', 150000, 8, 'Casino Fraud\r\n1. You set up the equipment required.\n\r\n2. You Start developing some fake casino Chips.\n\r\n3. You pack the Chips into the Suit case and Head of to the local Casino.\n\r\n4. Once inside u Bet $150000 in a single game of BlackJack.\n', 'Result: Operation Success!\r\n\r\nYou Won $150000 and Exhanges the Real Chips for Cash\r\n', 'Result: The Dealer Spots that your Chips are Fake and calls Security\r\n\r\nNot wishing to be Caught you Run of before the Security Guards Arrive.'),
(16, 'Drug Dealer', 25, '((WILL*0.7)/25)+(LEVEL/4)', 25000, 6, '1. You Recieved a call for a job downtown.\n\r\n2. You arm yourself with your trusty Sniper Rifle and head down to the Designated Area.\n\r\n3. The Target shows up about to get into his car.\n\r\n\r\n', '4. You Take his head off clean in 1 shot.\n\r\nResult: You drive home to find $25,000 in your LetterBox!', '4. the target steps into the car and prepares to drive off.\n\r\n5. Yours still trying to peice your gun together but forgot what part goes where.\n\r\n\r\nResult: He Drives off safely! Bad Luck!'),
(17, 'Vu GTS Commodore', 28, '((WILL*0.7)/28)+(LEVEL/4)', 80000, 3, '1. You walking down a streetof a rich neighbourhood.\n\r\n2. You see a red VU GTS Commodore with the window down.\n\r\n3. You stop to make sure no one is looking and you open it and attempt to hot wire it.\n \r\n', '4.You hear it crank over!\n\r\n\r\nResult: Operation Success.\r\n You speed off you take it to Jimmy and he give you $80000 for it!', '4. You wire it wrong and blow a fuse.\n\r\n5. The Alarm starts screeching.\n\r\n\r\nResult: you run of before somebody notices you. Bad Luck.'),
(18, 'Millionaire\'s Daughter', 45, '((WILL*0.5)/45)+(LEVEL/15)', 450000, 9, '1. You wait near the school where the daughter of a local millionaire attends.\n\r\n2. They see the Target Exiting the school.\n\r\n3. Then uou Follow the Target until you are alone.\n', '4. you put a cloth soaked with cloroform over her mouth and drag her to your Car.\n\r\n5. You make a call to the victim\'s family, requesting $450000.\n\r\nResult:Three anxious hours later, $450000 is delivered to the designated drop of point and \r\n\r\nthe exchange is made.', '4. Your turn around to check if there was anyone around you.\n\r\n5. You suddenly lost sight of the target. and notice her Bodyguards around\n\r\n\r\nResult: you decides to Give up, and try again tomorrow.'),
(19, 'Gang Ransom', 50, '((WILL*0.5)/50)+(LEVEL/20)', 750000, 10, '1. You happened to walk by a house and noticed there someone was home alone.\n\r\n2. You sneak in through a open window and sneaks up onto the target.\n\r\n3. You jump out and noticed your target was Nate, doing something obsence to himself.\n\r\n4. you happened to have a camera with you and you take a picture of him.\n', '4. Nate embarassed by the situation offers you 750k from the Gang Vault.\n\r\n5. You agree to his conditions and take the money and leave.\n\r\n\r\nResult:You walk home with $750000.', '4. You then Tie him up and make a phone call to Nates Gang demand Money his safety.\n\r\n5. Nates gang decided he is not worth the ammount just laughed over the phone and hangs up\n\r\n\r\nResult: You feel agitated and decided to Infect a pineapple up his rear end.'),
(20, 'Hijack a Train', 75, '((WILL*0.1)/75)', 1500000, 10, '1. You step onto the train. \n', '2. Then u pull out your gun and blow the train drivers head off and take over the train.\n\r\n3. You ring up the local authorities to request 1.5million.\n\r\n4. They agree and pay you the money.\n\r\n\r\nResult: Operation Success, You gain 1.5million dollars.', '2. And noticed thats no one one it =P.\n\r\n\r\n Result: Operation failed.'),
(21, 'Hijack Plane', 95, '((WILL*0.1)/95)', 3000000, 10, '1. brought yourself a plane ticket.\n\r\n2. You wait for the plane to take off. \n\r\n', '3. You take out your gun and hijack the plane.\n\r\n4. you demand 3 million for the satefy of the passangers.\n\r\n5. The city Mayor agrees to your demand and pays it into your swiss bank account.\n\r\n\r\nResult: operation Success. you jump out of the plane with a parachute.', '3. Once you are on the plane u search for your gun.\n\r\n4. you forgot you checked it in with the baggages.\n\r\n\r\nresult: Operation failed. YOu sit down and enjoy the rest of the flight.'),
(22, 'MC Lottery', 10, '((WILL*0.01)/30)+((LEVEL*0.01)/10)', 10000000, 10, '1. You go and by a lottery ticket hoping to win the 10million dollar draw.\n', '2. OMFG you won.\n\r\n3. you head down to the lottery commision and claim your 10 million dollar prize.\n', '2. When the draw comes out u find you didnt get any numbers right.\n\r\n3. Bad luck.\n'),
(23, 'Assisting Planned Robbery', 10, '((IQ*0.8)/10)', 250, 11, '1. You sit down and plan a shoplift.\n \r\n2. You finish off the plan.\n \r\n3. Your mate comes over and you give him the plan.\n', '4. Half an hour later your m8 comes back with the goods.\n \r\n Result: He gives you half of the profit, you received $250.', '4. Your mate doesnt come back.\n  Result: Your mate got arrested, unlucky.'),
(24, 'Simple Hacking', 20, '((IQ*0.8)/20)', 9500, 11, '1. You head down to the local net cafe\' and login to the internet.\n\r\n2. While no one is watching u look up Paypal and start inputting random email addresses.\n', '3. You happened to stumble upon a account with some decent amount of money in it.\n\r\n4. You decide to send the cash to 10 different accounts to avoid getting caught.\n\r\n5. You recieved $9500 in your account after spliting the money evenly.\n\r\n Result: Operation Success. You head down to the Bank and Withdraw the money.', '3. After many attempt u could not manage to match any username and passwords.\n\r\n4. The System logs your IP Address and locks your out of the Website.\n\r\n\r\n Result: Worried u might get caught, You turn of the computer and run out the Door. operation failed.');

-- --------------------------------------------------------

--
-- Table structure for table `crystalmarket`
--

CREATE TABLE `crystalmarket` (
  `cmID` int(11) NOT NULL,
  `cmQTY` int(11) NOT NULL DEFAULT 0,
  `cmADDER` int(11) NOT NULL DEFAULT 0,
  `cmPRICE` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `crystalxferlogs`
--

CREATE TABLE `crystalxferlogs` (
  `cxID` int(11) NOT NULL,
  `cxFROM` int(11) NOT NULL DEFAULT 0,
  `cxTO` int(11) NOT NULL DEFAULT 0,
  `cxAMOUNT` int(11) NOT NULL DEFAULT 0,
  `cxFROMIP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `cxTOIP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `cxCONTENT` longtext NOT NULL,
  `cxTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dps_process`
--

CREATE TABLE `dps_process` (
  `dp_id` int(11) NOT NULL,
  `dp_userid` int(11) NOT NULL DEFAULT 0,
  `dp_time` int(11) NOT NULL DEFAULT 0,
  `dp_type` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `evID` int(11) NOT NULL,
  `evUSER` int(11) NOT NULL DEFAULT 0,
  `evTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `evREAD` int(11) NOT NULL DEFAULT 0,
  `evTEXT` text NOT NULL,
  `evEXTRA` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fedjail`
--

CREATE TABLE `fedjail` (
  `fed_id` int(11) NOT NULL,
  `fed_userid` int(11) NOT NULL DEFAULT 0,
  `fed_days` int(11) NOT NULL DEFAULT 0,
  `fed_jailedby` int(11) NOT NULL DEFAULT 0,
  `fed_reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `energy` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `item_id`, `energy`) VALUES
(1, 1, 500),
(2, 5, 3),
(3, 6, 1),
(4, 15, 2147483647),
(5, 21, 10),
(6, 22, 10),
(7, 23, 25),
(8, 24, 50),
(9, 25, 100);

-- --------------------------------------------------------

--
-- Table structure for table `friendslist`
--

CREATE TABLE `friendslist` (
  `fl_ID` int(11) NOT NULL,
  `fl_ADDER` int(11) NOT NULL DEFAULT 0,
  `fl_ADDED` int(11) NOT NULL DEFAULT 0,
  `fl_COMMENT` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `houses`
--

CREATE TABLE `houses` (
  `hID` int(11) NOT NULL,
  `hNAME` varchar(191) NOT NULL DEFAULT '',
  `hPRICE` int(11) NOT NULL DEFAULT 0,
  `hWILL` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `houses`
--

INSERT INTO `houses` (`hID`, `hNAME`, `hPRICE`, `hWILL`) VALUES
(1, 'Garden Shed', 0, 100),
(2, 'Apartment', 12500, 150),
(3, 'Motel Room', 32000, 200),
(4, 'Semi-Detached House', 80000, 250),
(5, 'Detached House', 400000, 450),
(6, 'Chalet', 1000000, 750),
(7, 'Mansion', 15000000, 1200),
(8, 'Penthouse', 45000000, 2000),
(9, 'Castle', 125000000, 3500),
(10, 'Small Space Shuttle', 250000000, 5000),
(11, 'Luxurious Space Shuttle', 375000000, 7500),
(12, 'Artificial Orbiting Moon', 1000000000, 12500),
(13, 'Brothel', 2000000000, 20000);

-- --------------------------------------------------------

--
-- Table structure for table `imarketaddlogs`
--

CREATE TABLE `imarketaddlogs` (
  `imaID` int(11) NOT NULL,
  `imaITEM` int(11) NOT NULL DEFAULT 0,
  `imaPRICE` int(11) NOT NULL DEFAULT 0,
  `imaINVID` int(11) NOT NULL DEFAULT 0,
  `imaADDER` int(11) NOT NULL DEFAULT 0,
  `imaTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `imaCONTENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `imbuylogs`
--

CREATE TABLE `imbuylogs` (
  `imbID` int(11) NOT NULL,
  `imbITEM` int(11) NOT NULL DEFAULT 0,
  `imbADDER` int(11) NOT NULL DEFAULT 0,
  `imbBUYER` int(11) NOT NULL DEFAULT 0,
  `imbPRICE` int(11) NOT NULL DEFAULT 0,
  `imbIMID` int(11) NOT NULL DEFAULT 0,
  `imbINVID` int(11) NOT NULL DEFAULT 0,
  `imbTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `imbCONTENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `imremovelogs`
--

CREATE TABLE `imremovelogs` (
  `imrID` int(11) NOT NULL,
  `imrITEM` int(11) NOT NULL DEFAULT 0,
  `imrADDER` int(11) NOT NULL DEFAULT 0,
  `imrREMOVER` int(11) NOT NULL DEFAULT 0,
  `imrIMID` int(11) NOT NULL DEFAULT 0,
  `imrINVID` int(11) NOT NULL DEFAULT 0,
  `imrTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `imrCONTENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inv_id` int(11) NOT NULL,
  `inv_itemid` int(11) NOT NULL DEFAULT 0,
  `inv_userid` int(11) NOT NULL DEFAULT 0,
  `inv_qty` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `itembuylogs`
--

CREATE TABLE `itembuylogs` (
  `ibID` int(11) NOT NULL,
  `ibUSER` int(11) NOT NULL DEFAULT 0,
  `ibITEM` int(11) NOT NULL DEFAULT 0,
  `ibTOTALPRICE` int(11) NOT NULL DEFAULT 0,
  `ibQTY` int(11) NOT NULL DEFAULT 0,
  `ibTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `ibCONTENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `itemmarket`
--

CREATE TABLE `itemmarket` (
  `imID` int(11) NOT NULL,
  `imITEM` int(11) NOT NULL DEFAULT 0,
  `imADDER` int(11) NOT NULL DEFAULT 0,
  `imPRICE` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `itmid` int(11) NOT NULL,
  `itmtype` int(11) NOT NULL DEFAULT 0,
  `itmname` varchar(191) NOT NULL DEFAULT '',
  `itmdesc` text NOT NULL,
  `itmbuyprice` int(11) NOT NULL DEFAULT 0,
  `itmsellprice` int(11) NOT NULL DEFAULT 0,
  `itmbuyable` int(11) NOT NULL DEFAULT 0,
  `itmimage` varchar(191) NOT NULL DEFAULT '/src/images/default-item.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`itmid`, `itmtype`, `itmname`, `itmdesc`, `itmbuyprice`, `itmsellprice`, `itmbuyable`, `itmimage`) VALUES
(1, 1, 'Sack Lunch', 'Deliciously filled with nutrients. Even has a slice of your favorite cake!', 95000, 0, 1, '/src/images/default-item.png'),
(3, 5, 'Small Potion', 'Restores some health.', 500, 400, 1, '/src/images/default-item.png'),
(4, 5, 'First Aid Lotion', 'Heals a  considerable amount of health.', 1500, 750, 1, '/src/images/default-item.png'),
(5, 1, 'Hamburger', 'A scrumptious burger.', 30, 20, 1, '/src/images/default-item.png'),
(6, 1, 'Sugar Snake', 'A snake covered in sugar.', 10, 5, 1, '/src/images/default-item.png'),
(7, 3, 'Dagger', 'A small gold dagger.', 200, 100, 1, '/src/images/default-item.png'),
(8, 3, 'Kitchen Knife', 'A knife filled with dreaded spirits of dead animals.', 2500, 1500, 1, '/src/images/default-item.png'),
(9, 3, 'Chainsaw', 'Cut up your foes.', 13250, 10925, 1, '/src/images/default-item.png'),
(10, 4, 'Pistol', 'A small blue pistol.', 500, 400, 1, '/src/images/default-item.png'),
(11, 4, 'Colt', 'An average gun.', 5000, 3750, 1, '/src/images/default-item.png'),
(12, 4, 'Rifle', 'The standard in modern weaponry.', 25000, 17850, 1, '/src/images/default-item.png'),
(14, 4, 'Mini-Rocket Launcher', 'Blast your foes.', 99450, 78765, 1, '/src/images/default-item.png'),
(15, 1, 'SuperDuper Stick', 'restores 100% energy', 2147483647, 2147483647, 1, '/src/images/default-item.png'),
(16, 3, 'Bat', 'A cricket bat.', 75, 50, 1, '/src/images/default-item.png'),
(34, 5, 'Will Potion', 'Heals will to 100%', 0, 0, 0, '/src/images/default-item.png'),
(35, 4, 'Scout Sniper rifle', 'will hurt big time', 99393, 78353, 1, '/src/images/default-item.png'),
(36, 4, 'Minigun', 'Sheer power', 100000, 75000, 1, '/src/images/default-item.png'),
(37, 3, 'Diamond Dagger', 'Stabbing power to the max.', 450000, 275000, 1, '/src/images/default-item.png'),
(38, 4, 'Rocket Launcher', 'Boom.', 220000, 170000, 1, '/src/images/default-item.png'),
(39, 4, 'M16', '...', 49000, 38000, 1, '/src/images/default-item.png'),
(40, 6, 'Bomb Stew', 'Extremely rare collectors item.', 0, 0, 0, '/src/images/default-item.png'),
(42, 6, '\"Tit\"an Implant', 'Titans own way of saying hi.', 0, 0, 0, '/src/images/default-item.png'),
(43, 6, 'Arsons Zippo', 'Arsons odd fantasy.', 0, 0, 0, '/src/images/default-item.png'),
(44, 3, 'Demon Sword', 'Ultimate weapon.', 0, 0, 0, '/src/images/default-item.png'),
(45, 6, 'Gothic Warrior-Doll', 'gothdavid16s favorite toy', 0, 0, 0, '/src/images/default-item.png'),
(46, 2, 'Videogame Boy 2002', 'The ultimate in console action.', 0, 0, 0, '/src/images/default-item.png'),
(47, 6, 'JaggerDoll', 'Won in a test of wits.', 0, 0, 0, '/src/images/default-item.png'),
(48, 6, 'Ablemits Doll', 'If you have it then you are so lucky.', 0, 0, 0, '/src/images/default-item.png'),
(49, 4, 'Hunting Bow', 'Medium range bow, silent and efficient', 425, 210, 1, '/src/images/default-item.png'),
(50, 3, 'Foldable Chair', 'infamous folding chair as seen in WWF!', 750, 375, 1, '/src/images/default-item.png'),
(51, 3, 'Nail Filer', 'nail accessory', 300, 150, 1, '/src/images/default-item.png'),
(52, 6, 'MasturNATion Doll', 'You can sure put this doll to good use.', 0, 0, 0, '/src/images/default-item.png'),
(53, 6, 'Toonces Bouquet', '231 beautiful roses', 0, 0, 0, '/src/images/default-item.png'),
(55, 6, 'Nahdus Rubik Cube', '', 0, 0, 0, '/src/images/default-item.png'),
(56, 4, 'Calibre Machine Gun', 'Brute power', 175000, 130000, 1, '/src/images/default-item.png'),
(57, 4, 'benelli m1', 'Automatic Shotgun', 33000, 26500, 1, '/src/images/default-item.png'),
(58, 4, 'F90 Sub Machine Gun', 'extremely fast short to meduim distance gun', 40000, 30000, 1, '/src/images/default-item.png'),
(59, 6, 'Noobi Diapers', 'for newbies', 0, 0, 0, '/src/images/default-item.png'),
(60, 3, 'Gladius', 'superior dagger', 40000, 30000, 1, '/src/images/default-item.png'),
(61, 3, 'Katana', 'Japanese style sword', 95000, 78000, 1, '/src/images/default-item.png'),
(62, 3, 'Claymore', 'Mysterious Medievil Sword', 125000, 95000, 1, '/src/images/default-item.png'),
(63, 3, 'Ragnarok', 'mythical eastern Weapon', 175000, 125000, 1, '/src/images/default-item.png'),
(64, 3, 'Diamond Sword', 'Cutting power to the max.', 2000000, 1700000, 1, '/src/images/default-item.png'),
(65, 4, 'Battlements', 'Fully loaded battlements.', 5000000, 4000000, 1, '/src/images/default-item.png'),
(66, 6, 'Conerias Duck Tape', 'Coneria brand duck tape', 0, 0, 0, '/src/images/default-item.png'),
(67, 6, 'Cold Blooded Plushie', '-30C toy! What more could you want!', 0, 0, 0, '/src/images/default-item.png'),
(68, 6, 'Titanium NightVision Goggles', 'Increases your sight and accuracy at night', 0, 0, 0, '/src/images/default-item.png'),
(69, 6, 'Cyber-Surfboard', 'Netbois !!!!', 0, 0, 0, '/src/images/default-item.png'),
(70, 6, 'Nyuuubii Sword', 'Sword of the gods made for newbies', 0, 0, 0, '/src/images/default-item.png'),
(71, 7, 'Thick Jacket', 'Warm clothing for the long winter', 100, 75, 1, '/src/images/default-item.png'),
(73, 7, 'Trash Can Lid', 'Hard Round metal Lid', 200, 150, 1, '/src/images/default-item.png'),
(74, 7, 'Leather Jacket', 'Nice biker style jacket', 750, 500, 1, '/src/images/default-item.png'),
(75, 7, 'Riot Shield', 'standard issue shield', 3000, 2250, 1, '/src/images/default-item.png'),
(77, 7, 'Semi-bullet proof Vest', '', 8750, 6000, 1, '/src/images/default-item.png'),
(78, 6, 'Nuclear Bomb', 'Can kill anyone in one blow even though the guy is lvl 100000000 and has there stats higher than everyone! ****This weapon is restricted to poor people, but still can be buyable for a high enough prize****NOTE: This weapon can kill every member in {GAME_NAME} and can gain you heaps of .exp at once! The Deadlyest weapon in the whole Country', 1000000000, 0, 0, '/src/images/default-item.png'),
(79, 7, 'Helmet and Vest', '', 25000, 17850, 1, '/src/images/default-item.png'),
(80, 7, 'plated armour', 'heavy armour', 75000, 50000, 1, '/src/images/default-item.png'),
(83, 7, 'Mini-Tank', 'small tank almost bullet proof', 750000, 550000, 1, '/src/images/default-item.png'),
(86, 4, 'mini-Rail Gun', 'new Improve technology', 4000000, 3000000, 1, '/src/images/default-item.png'),
(87, 4, 'Mounted Rail Gun', 'usually mounted on tanks', 10000000, 7500000, 1, '/src/images/default-item.png'),
(89, 3, 'Light Saber', '', 50000000, 40000000, 1, '/src/images/default-item.png'),
(90, 4, 'Arsons FlameThrower', 'flamethrower', 150000, 115000, 1, '/src/images/default-item.png'),
(91, 7, 'Gothic Plate', '', 300000, 225000, 1, '/src/images/default-item.png'),
(92, 7, 'DBS Penguin Suit', 'Agile body suit with maximum protection', 1500000, 1100000, 1, '/src/images/default-item.png'),
(93, 7, 'DBS Emperor Penguin Suit', 'Advanced version: Agile body suit with maximum protection', 3250000, 2800000, 1, '/src/images/default-item.png'),
(94, 7, 'Conerias DuckTaped Suit', 'Made with superior brand of duck tape', 150000, 125000, 1, '/src/images/default-item.png'),
(95, 6, 'Fifty Cent Piece', 'A small piece of ancient {GAME_NAME} History.', 0, 0, 0, '/src/images/default-item.png'),
(96, 6, '1 Rupie', '1 freaking rupie.', 0, 0, 0, '/src/images/default-item.png'),
(97, 7, 'Plasma Shield', 'Battery powered arm guard', 25000000, 17500000, 1, '/src/images/default-item.png'),
(98, 7, 'Rynax Plasma Shield', 'Made form a new type of power source', 60000000, 40000000, 1, '/src/images/default-item.png'),
(99, 4, 'Plasma Gun', 'Fires Bolts of plasma Energy', 35000000, 22500000, 1, '/src/images/default-item.png'),
(100, 4, 'Plasma Rifle', 'Full automatic energy rifle', 75000000, 50000000, 1, '/src/images/default-item.png'),
(102, 4, 'Nuke Gun', '.... this is obvious', 200000000, 125000000, 1, '/src/images/default-item.png'),
(105, 6, 'Conerians', 'Awarded to helpful players. Can be redeemed for rewards.', 0, 0, 0, '/src/images/default-item.png'),
(107, 4, 'Bio Aeroactive 350', 'shoots green plasma', 500000000, 300000000, 1, '/src/images/default-item.png');

-- --------------------------------------------------------

--
-- Table structure for table `itemselllogs`
--

CREATE TABLE `itemselllogs` (
  `isID` int(11) NOT NULL,
  `isUSER` int(11) NOT NULL DEFAULT 0,
  `isITEM` int(11) NOT NULL DEFAULT 0,
  `isTOTALPRICE` int(11) NOT NULL DEFAULT 0,
  `isQTY` int(11) NOT NULL DEFAULT 0,
  `isTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `isCONTENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `itemtypes`
--

CREATE TABLE `itemtypes` (
  `itmtypeid` int(11) NOT NULL,
  `itmtypename` varchar(191) NOT NULL DEFAULT '',
  `itmtypecolor` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `itemtypes`
--

INSERT INTO `itemtypes` (`itmtypeid`, `itmtypename`, `itmtypecolor`) VALUES
(1, 'Food', '#e91f1f'),
(2, 'Electronics', '#f4df22'),
(3, 'Melee Weapon', '#76cc20'),
(4, 'Gun', '#5e9aeb'),
(5, 'Medical', '#af15f9'),
(6, 'Collectible', '#0fb382'),
(7, 'Armour', '#d6d6d6');

-- --------------------------------------------------------

--
-- Table structure for table `itemxferlogs`
--

CREATE TABLE `itemxferlogs` (
  `ixID` int(11) NOT NULL,
  `ixFROM` int(11) NOT NULL DEFAULT 0,
  `ixTO` int(11) NOT NULL DEFAULT 0,
  `ixITEM` int(11) NOT NULL DEFAULT 0,
  `ixQTY` int(11) NOT NULL DEFAULT 0,
  `ixTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jaillogs`
--

CREATE TABLE `jaillogs` (
  `jaID` int(11) NOT NULL,
  `jaJAILER` int(11) NOT NULL DEFAULT 0,
  `jaJAILED` int(11) NOT NULL DEFAULT 0,
  `jaDAYS` int(11) NOT NULL DEFAULT 0,
  `jaREASON` longtext NOT NULL,
  `jaTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `logs_staff`
--

CREATE TABLE `logs_staff` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ipaddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '127.0.0.1',
  `time_added` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mail`
--

CREATE TABLE `mail` (
  `mail_id` int(11) NOT NULL,
  `mail_read` int(11) NOT NULL DEFAULT 0,
  `mail_from` int(11) NOT NULL DEFAULT 0,
  `mail_to` int(11) NOT NULL DEFAULT 0,
  `mail_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `mail_subject` varchar(191) NOT NULL DEFAULT '',
  `mail_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `medical`
--

CREATE TABLE `medical` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `health` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `medical`
--

INSERT INTO `medical` (`id`, `item_id`, `health`) VALUES
(1, 2, 2147483647),
(2, 3, 5),
(3, 4, 18),
(4, 29, 100),
(5, 34, 0);

-- --------------------------------------------------------

--
-- Table structure for table `papercontent`
--

CREATE TABLE `papercontent` (
  `id` int(11) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `papercontent`
--

INSERT INTO `papercontent` (`id`, `content`) VALUES
(1, 'Post the latest news in your game here.');

-- --------------------------------------------------------

--
-- Table structure for table `preports`
--

CREATE TABLE `preports` (
  `prID` int(11) NOT NULL,
  `prREPORTER` int(11) NOT NULL DEFAULT 0,
  `prREPORTED` int(11) NOT NULL DEFAULT 0,
  `prTEXT` longtext NOT NULL,
  `prTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `refID` int(11) NOT NULL,
  `refREFER` int(11) NOT NULL DEFAULT 0,
  `refREFED` int(11) NOT NULL DEFAULT 0,
  `refTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `refREFERIP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `refREFEDIP` varchar(15) NOT NULL DEFAULT '127.0.0.1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `seclogs`
--

CREATE TABLE `seclogs` (
  `secID` int(11) NOT NULL,
  `secUSER` int(11) NOT NULL DEFAULT 0,
  `secPOST` longtext NOT NULL,
  `secGET` longtext NOT NULL,
  `secTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `shopitems`
--

CREATE TABLE `shopitems` (
  `sitemID` int(11) NOT NULL,
  `sitemSHOP` int(11) NOT NULL DEFAULT 0,
  `sitemITEMID` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shopitems`
--

INSERT INTO `shopitems` (`sitemID`, `sitemSHOP`, `sitemITEMID`) VALUES
(2, 2, 3),
(4, 2, 4),
(7, 3, 7),
(8, 3, 8),
(9, 3, 9),
(10, 3, 10),
(11, 3, 11),
(12, 3, 12),
(13, 3, 14),
(14, 3, 16),
(24, 6, 7),
(25, 6, 8),
(26, 6, 9),
(27, 6, 10),
(28, 6, 11),
(29, 6, 12),
(30, 6, 16),
(31, 6, 14),
(33, 7, 14),
(38, 8, 2),
(39, 9, 36),
(40, 9, 37),
(41, 9, 38),
(42, 3, 39),
(43, 6, 39),
(44, 6, 50),
(45, 3, 4),
(46, 3, 50),
(47, 3, 49),
(48, 6, 49),
(49, 6, 51),
(51, 9, 35),
(52, 9, 56),
(53, 6, 57),
(54, 6, 58),
(55, 3, 60),
(56, 9, 61),
(57, 9, 62),
(59, 6, 35),
(61, 9, 63),
(62, 9, 64),
(63, 9, 65),
(64, 9, 14),
(65, 6, 60),
(66, 6, 71),
(67, 3, 71),
(68, 3, 73),
(69, 6, 73),
(70, 3, 74),
(71, 6, 74),
(72, 6, 77),
(73, 6, 79),
(74, 6, 75),
(75, 9, 80),
(76, 9, 81),
(77, 9, 82),
(78, 4, 83),
(79, 9, 83),
(80, 9, 84),
(81, 9, 85),
(82, 9, 87),
(83, 9, 86),
(84, 6, 88),
(85, 9, 88),
(86, 10, 89),
(87, 10, 87),
(89, 10, 97),
(90, 10, 98),
(92, 10, 99),
(93, 10, 100),
(94, 2, 40),
(95, 10, 104),
(96, 9, 106);

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `shopID` int(11) NOT NULL,
  `shopLOCATION` int(11) NOT NULL DEFAULT 0,
  `shopNAME` varchar(191) NOT NULL DEFAULT '',
  `shopDESCRIPTION` text NOT NULL,
  `shopIMAGE` varchar(191) NOT NULL DEFAULT '/src/images/default-building.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`shopID`, `shopLOCATION`, `shopNAME`, `shopDESCRIPTION`, `shopIMAGE`) VALUES
(2, 1, 'MC Pharmacy', 'The one-stop medi-shop.', '/src/images/default-building.png'),
(3, 1, 'Weapons Central', 'The one place for all weps.', '/src/images/default-building.png'),
(4, 2, 'Drug store', 'we accept weed', '/src/images/default-building.png'),
(5, 1, 'Drug Store', 'we sell steriods', '/src/images/default-building.png'),
(6, 4, 'Industrial Weapons', 'All the weapons you could need', '/src/images/default-building.png'),
(9, 3, 'El Ablo Weapons', 'Only the truly powerful weapons.', '/src/images/default-building.png'),
(10, 5, 'Cyber weaponary', 'space age weaponary here', '/src/images/default-building.png');

-- --------------------------------------------------------

--
-- Table structure for table `staffdetlogs`
--

CREATE TABLE `staffdetlogs` (
  `sdID` int(11) NOT NULL,
  `sdUSER` int(11) NOT NULL DEFAULT 0,
  `sdTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `sdPOST` longtext NOT NULL,
  `sdGET` longtext NOT NULL,
  `sdACT` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `staffnews`
--

CREATE TABLE `staffnews` (
  `snID` int(11) NOT NULL,
  `snPOSTER` int(11) NOT NULL DEFAULT 0,
  `snIMPORTANCE` enum('low','medium','high') NOT NULL DEFAULT 'low',
  `snSUBJECT` varchar(191) NOT NULL DEFAULT '',
  `snTEXT` longtext NOT NULL,
  `snTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `staffnotelogs`
--

CREATE TABLE `staffnotelogs` (
  `snID` int(11) NOT NULL,
  `snCHANGER` int(11) NOT NULL DEFAULT 0,
  `snCHANGED` int(11) NOT NULL DEFAULT 0,
  `snTIME` timestamp NOT NULL DEFAULT current_timestamp(),
  `snOLD` longtext NOT NULL,
  `snNEW` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `surrenders`
--

CREATE TABLE `surrenders` (
  `surID` int(11) NOT NULL,
  `surWAR` int(11) NOT NULL DEFAULT 0,
  `surWHO` int(11) NOT NULL DEFAULT 0,
  `surTO` int(11) NOT NULL DEFAULT 0,
  `surMSG` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `txsused`
--

CREATE TABLE `txsused` (
  `txID` int(11) NOT NULL,
  `txTX` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `unjaillogs`
--

CREATE TABLE `unjaillogs` (
  `ujaID` int(11) NOT NULL,
  `ujaJAILER` int(11) NOT NULL DEFAULT 0,
  `ujaJAILED` int(11) NOT NULL DEFAULT 0,
  `ujaTIME` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(191) NOT NULL DEFAULT '',
  `userpass` text NOT NULL,
  `level` int(11) NOT NULL DEFAULT 1,
  `exp` decimal(11,4) NOT NULL DEFAULT 0.0000,
  `money` bigint(25) NOT NULL DEFAULT 0,
  `crystals` bigint(25) NOT NULL DEFAULT 0,
  `laston` timestamp NOT NULL DEFAULT current_timestamp(),
  `lastip` varchar(191) NOT NULL DEFAULT '',
  `energy` int(11) NOT NULL DEFAULT 12,
  `maxenergy` int(11) NOT NULL DEFAULT 12,
  `will` int(11) NOT NULL DEFAULT 100,
  `maxwill` int(11) NOT NULL DEFAULT 100,
  `brave` int(11) NOT NULL DEFAULT 5,
  `maxbrave` int(11) NOT NULL DEFAULT 5,
  `hp` int(11) NOT NULL DEFAULT 100,
  `maxhp` int(11) NOT NULL DEFAULT 100,
  `location` int(11) NOT NULL DEFAULT 1,
  `hospital` int(11) NOT NULL DEFAULT 0,
  `jail` int(11) NOT NULL DEFAULT 0,
  `fedjail` int(11) NOT NULL DEFAULT 0,
  `user_level` int(11) NOT NULL DEFAULT 1,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `daysold` int(11) NOT NULL DEFAULT 0,
  `signedup` timestamp NOT NULL DEFAULT current_timestamp(),
  `course` int(11) NOT NULL DEFAULT 0,
  `cdays` int(11) NOT NULL DEFAULT 0,
  `donatordays` int(11) NOT NULL DEFAULT 0,
  `email` varchar(191) NOT NULL DEFAULT '',
  `login_name` varchar(191) NOT NULL DEFAULT '',
  `display_pic` varchar(191) NOT NULL DEFAULT '/src/images/default-avatar.png',
  `duties` varchar(191) NOT NULL DEFAULT 'N/A',
  `bankmoney` bigint(25) NOT NULL DEFAULT -1,
  `cybermoney` bigint(25) NOT NULL DEFAULT -1,
  `staffnotes` longtext NOT NULL,
  `mailban` int(11) NOT NULL DEFAULT 0,
  `mb_reason` varchar(191) NOT NULL DEFAULT '',
  `hospreason` varchar(191) NOT NULL DEFAULT '',
  `avatar` varchar(191) NOT NULL DEFAULT '/src/images/default-user-avatar.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `userstats`
--

CREATE TABLE `userstats` (
  `userid` int(11) NOT NULL,
  `strength` decimal(11,4) NOT NULL DEFAULT 10.0000,
  `agility` decimal(11,4) NOT NULL DEFAULT 10.0000,
  `guard` decimal(11,4) NOT NULL DEFAULT 10.0000,
  `labour` decimal(11,4) NOT NULL DEFAULT 10.0000,
  `IQ` decimal(11,6) NOT NULL DEFAULT 10.000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE `votes` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `list` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `weapons`
--

CREATE TABLE `weapons` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT 0,
  `damage` decimal(11,4) NOT NULL DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `weapons`
--

INSERT INTO `weapons` (`id`, `item_id`, `damage`) VALUES
(1, 7, '3.0000'),
(2, 8, '6.2000'),
(3, 9, '10.0000'),
(4, 10, '5.0000'),
(5, 11, '7.5000'),
(6, 12, '12.0000'),
(7, 13, '0.0000'),
(8, 14, '25.0000'),
(9, 16, '1.5000'),
(10, 17, '50.0000'),
(11, 18, '3450.0000'),
(12, 19, '0.0000'),
(13, 20, '1050.0000'),
(14, 26, '1000.0000'),
(15, 27, '0.0000'),
(16, 28, '1.0000'),
(17, 30, '1250.0000'),
(18, 31, '0.0000'),
(19, 33, '0.0000'),
(20, 35, '20.0000'),
(21, 36, '25.0000'),
(22, 37, '75.0000'),
(23, 38, '50.0000'),
(24, 39, '28.0000'),
(25, 44, '1.0000'),
(26, 49, '4.0000'),
(27, 50, '5.0000'),
(28, 51, '0.2000'),
(29, 86, '210.0000'),
(30, 56, '40.0000'),
(31, 57, '14.0000'),
(32, 58, '16.0000'),
(33, 61, '20.0000'),
(34, 60, '15.0000'),
(35, 62, '30.0000'),
(36, 63, '40.0000'),
(37, 64, '120.0000'),
(38, 65, '240.0000'),
(39, 88, '10.0000'),
(40, 87, '180.0000'),
(41, 89, '750.0000'),
(42, 90, '35.0000'),
(43, 99, '400.0000'),
(44, 100, '900.0000'),
(45, 104, '5000.0000'),
(46, 102, '1500.0000'),
(47, 106, '2500.0000'),
(48, 107, '3000.0000'),
(49, 108, '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `willplogs`
--

CREATE TABLE `willplogs` (
  `wp_id` int(11) NOT NULL,
  `wp_userid` int(11) NOT NULL DEFAULT 0,
  `wp_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `wp_qty` varchar(191) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminlogs`
--
ALTER TABLE `adminlogs`
  ADD PRIMARY KEY (`adID`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`adID`);

--
-- Indexes for table `armour`
--
ALTER TABLE `armour`
  ADD KEY `item_ID` (`item_ID`);

--
-- Indexes for table `attacklogs`
--
ALTER TABLE `attacklogs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `attacker` (`attacker`),
  ADD KEY `attacked` (`attacked`);

--
-- Indexes for table `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`bl_ID`),
  ADD KEY `bl_ADDER` (`bl_ADDER`),
  ADD KEY `bl_ADDED` (`bl_ADDED`);

--
-- Indexes for table `cashxferlogs`
--
ALTER TABLE `cashxferlogs`
  ADD PRIMARY KEY (`cxID`),
  ADD KEY `cxFROM` (`cxFROM`),
  ADD KEY `cxTO` (`cxTO`);

--
-- Indexes for table `challengesbeaten`
--
ALTER TABLE `challengesbeaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `npcid` (`npcid`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`cityid`),
  ADD KEY `cityminlevel` (`cityminlevel`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`crID`);

--
-- Indexes for table `coursesdone`
--
ALTER TABLE `coursesdone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courseid` (`courseid`);

--
-- Indexes for table `crimegroups`
--
ALTER TABLE `crimegroups`
  ADD PRIMARY KEY (`cgID`);

--
-- Indexes for table `crimes`
--
ALTER TABLE `crimes`
  ADD PRIMARY KEY (`crimeID`),
  ADD KEY `crimeGROUP` (`crimeGROUP`);

--
-- Indexes for table `crystalmarket`
--
ALTER TABLE `crystalmarket`
  ADD PRIMARY KEY (`cmID`),
  ADD KEY `cmADDER` (`cmADDER`);

--
-- Indexes for table `crystalxferlogs`
--
ALTER TABLE `crystalxferlogs`
  ADD PRIMARY KEY (`cxID`),
  ADD KEY `cxFROM` (`cxFROM`),
  ADD KEY `cxTO` (`cxTO`);

--
-- Indexes for table `dps_process`
--
ALTER TABLE `dps_process`
  ADD PRIMARY KEY (`dp_id`),
  ADD KEY `dp_userid` (`dp_userid`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`evID`),
  ADD KEY `evUSER` (`evUSER`);

--
-- Indexes for table `fedjail`
--
ALTER TABLE `fedjail`
  ADD PRIMARY KEY (`fed_id`),
  ADD KEY `fed_jailedby` (`fed_jailedby`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `friendslist`
--
ALTER TABLE `friendslist`
  ADD PRIMARY KEY (`fl_ID`),
  ADD KEY `fl_ADDER` (`fl_ADDER`),
  ADD KEY `fl_ADDED` (`fl_ADDED`);

--
-- Indexes for table `houses`
--
ALTER TABLE `houses`
  ADD PRIMARY KEY (`hID`),
  ADD KEY `hWILL` (`hWILL`);

--
-- Indexes for table `imarketaddlogs`
--
ALTER TABLE `imarketaddlogs`
  ADD PRIMARY KEY (`imaID`);

--
-- Indexes for table `imbuylogs`
--
ALTER TABLE `imbuylogs`
  ADD PRIMARY KEY (`imbID`);

--
-- Indexes for table `imremovelogs`
--
ALTER TABLE `imremovelogs`
  ADD PRIMARY KEY (`imrID`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inv_id`),
  ADD KEY `inv_userid` (`inv_userid`),
  ADD KEY `inv_itemid` (`inv_itemid`);

--
-- Indexes for table `itembuylogs`
--
ALTER TABLE `itembuylogs`
  ADD PRIMARY KEY (`ibID`);

--
-- Indexes for table `itemmarket`
--
ALTER TABLE `itemmarket`
  ADD PRIMARY KEY (`imID`),
  ADD KEY `imITEM` (`imITEM`),
  ADD KEY `imADDER` (`imADDER`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`itmid`),
  ADD KEY `itmtype` (`itmtype`);

--
-- Indexes for table `itemselllogs`
--
ALTER TABLE `itemselllogs`
  ADD PRIMARY KEY (`isID`);

--
-- Indexes for table `itemtypes`
--
ALTER TABLE `itemtypes`
  ADD PRIMARY KEY (`itmtypeid`);

--
-- Indexes for table `itemxferlogs`
--
ALTER TABLE `itemxferlogs`
  ADD PRIMARY KEY (`ixID`),
  ADD KEY `ixFROM` (`ixFROM`),
  ADD KEY `ixTO` (`ixTO`);

--
-- Indexes for table `jaillogs`
--
ALTER TABLE `jaillogs`
  ADD PRIMARY KEY (`jaID`),
  ADD KEY `jaJAILER` (`jaJAILER`),
  ADD KEY `jaJAILED` (`jaJAILED`);

--
-- Indexes for table `logs_staff`
--
ALTER TABLE `logs_staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `mail`
--
ALTER TABLE `mail`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `medical`
--
ALTER TABLE `medical`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `papercontent`
--
ALTER TABLE `papercontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `preports`
--
ALTER TABLE `preports`
  ADD PRIMARY KEY (`prID`),
  ADD KEY `prREPORTER` (`prREPORTER`),
  ADD KEY `prREPORTED` (`prREPORTED`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`refID`),
  ADD KEY `refREFER` (`refREFER`),
  ADD KEY `refREFED` (`refREFED`);

--
-- Indexes for table `seclogs`
--
ALTER TABLE `seclogs`
  ADD PRIMARY KEY (`secID`),
  ADD KEY `secUSER` (`secUSER`);

--
-- Indexes for table `shopitems`
--
ALTER TABLE `shopitems`
  ADD PRIMARY KEY (`sitemID`),
  ADD KEY `sitemITEMID` (`sitemITEMID`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`shopID`),
  ADD KEY `shopLOCATION` (`shopLOCATION`);

--
-- Indexes for table `staffdetlogs`
--
ALTER TABLE `staffdetlogs`
  ADD PRIMARY KEY (`sdID`),
  ADD KEY `sdUSER` (`sdUSER`);

--
-- Indexes for table `staffnews`
--
ALTER TABLE `staffnews`
  ADD PRIMARY KEY (`snID`),
  ADD KEY `snPOSTER` (`snPOSTER`);

--
-- Indexes for table `staffnotelogs`
--
ALTER TABLE `staffnotelogs`
  ADD PRIMARY KEY (`snID`),
  ADD KEY `snCHANGER` (`snCHANGER`),
  ADD KEY `snCHANGED` (`snCHANGED`);

--
-- Indexes for table `surrenders`
--
ALTER TABLE `surrenders`
  ADD PRIMARY KEY (`surID`),
  ADD KEY `surWAR` (`surWAR`);

--
-- Indexes for table `txsused`
--
ALTER TABLE `txsused`
  ADD PRIMARY KEY (`txID`);

--
-- Indexes for table `unjaillogs`
--
ALTER TABLE `unjaillogs`
  ADD PRIMARY KEY (`ujaID`),
  ADD KEY `ujaJAILED` (`ujaJAILED`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `course` (`course`);

--
-- Indexes for table `userstats`
--
ALTER TABLE `userstats`
  ADD UNIQUE KEY `userid` (`userid`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `weapons`
--
ALTER TABLE `weapons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `willplogs`
--
ALTER TABLE `willplogs`
  ADD PRIMARY KEY (`wp_id`),
  ADD KEY `wp_userid` (`wp_userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adminlogs`
--
ALTER TABLE `adminlogs`
  MODIFY `adID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `adID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attacklogs`
--
ALTER TABLE `attacklogs`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `bl_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashxferlogs`
--
ALTER TABLE `cashxferlogs`
  MODIFY `cxID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `challengesbeaten`
--
ALTER TABLE `challengesbeaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `cityid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `crID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coursesdone`
--
ALTER TABLE `coursesdone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crimegroups`
--
ALTER TABLE `crimegroups`
  MODIFY `cgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `crimes`
--
ALTER TABLE `crimes`
  MODIFY `crimeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `crystalmarket`
--
ALTER TABLE `crystalmarket`
  MODIFY `cmID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `crystalxferlogs`
--
ALTER TABLE `crystalxferlogs`
  MODIFY `cxID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dps_process`
--
ALTER TABLE `dps_process`
  MODIFY `dp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `evID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fedjail`
--
ALTER TABLE `fedjail`
  MODIFY `fed_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `friendslist`
--
ALTER TABLE `friendslist`
  MODIFY `fl_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `houses`
--
ALTER TABLE `houses`
  MODIFY `hID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `imarketaddlogs`
--
ALTER TABLE `imarketaddlogs`
  MODIFY `imaID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imbuylogs`
--
ALTER TABLE `imbuylogs`
  MODIFY `imbID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imremovelogs`
--
ALTER TABLE `imremovelogs`
  MODIFY `imrID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itembuylogs`
--
ALTER TABLE `itembuylogs`
  MODIFY `ibID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itemmarket`
--
ALTER TABLE `itemmarket`
  MODIFY `imID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `itmid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `itemselllogs`
--
ALTER TABLE `itemselllogs`
  MODIFY `isID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `itemtypes`
--
ALTER TABLE `itemtypes`
  MODIFY `itmtypeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `itemxferlogs`
--
ALTER TABLE `itemxferlogs`
  MODIFY `ixID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jaillogs`
--
ALTER TABLE `jaillogs`
  MODIFY `jaID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs_staff`
--
ALTER TABLE `logs_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mail`
--
ALTER TABLE `mail`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medical`
--
ALTER TABLE `medical`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `papercontent`
--
ALTER TABLE `papercontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `preports`
--
ALTER TABLE `preports`
  MODIFY `prID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `refID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seclogs`
--
ALTER TABLE `seclogs`
  MODIFY `secID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shopitems`
--
ALTER TABLE `shopitems`
  MODIFY `sitemID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `shopID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `staffdetlogs`
--
ALTER TABLE `staffdetlogs`
  MODIFY `sdID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staffnews`
--
ALTER TABLE `staffnews`
  MODIFY `snID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staffnotelogs`
--
ALTER TABLE `staffnotelogs`
  MODIFY `snID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surrenders`
--
ALTER TABLE `surrenders`
  MODIFY `surID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `txsused`
--
ALTER TABLE `txsused`
  MODIFY `txID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unjaillogs`
--
ALTER TABLE `unjaillogs`
  MODIFY `ujaID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weapons`
--
ALTER TABLE `weapons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `willplogs`
--
ALTER TABLE `willplogs`
  MODIFY `wp_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
