<?php
/*
MCCodes FREE
global_func.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if (!defined('SITE_ENABLE')) {
    exit;
}
class global_func
{
    protected static $userCache = [];
    public static $inst = null;

    public static function getInstance()
    {
        if (null == self::$inst) {
            self::$inst = new self();
        }

        return self::$inst;
    }

    public function num_formatter($num)
    {
        $amnts = '';
        $num = (string) $num;
        if (strlen($num) <= 3) {
            return $num;
        }
        $dun = 0;
        for ($i = strlen($num); $i > 0; --$i) {
            if (0 == $dun % 3 && $dun > 0) {
                $amnts = ','.$amnts;
            }
            ++$dun;
            $amnts = $num[$i - 1].$amnts;
        }

        return $amnts;
    }

    public function money($muny, $symb = '$')
    {
        return $symb.$this->num_formatter($muny);
    }

    public function crystals($amnt)
    {
        return $this->num_formatter($amnt).' crystal'.$this->s($amnt);
    }

    public function itemtype_dropdown($ddname = 'item_type', $selected = null)
    {
        global $db;
        $db->query('SELECT itmtypeid, itmtypename FROM itemtypes ORDER BY itmtypename ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no item types</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['itmtypeid'], $row['itmtypeid'] == $selected ? ' selected' : '', $this->format($row['itmtypename']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function item_dropdown($ddname = 'item', $selected = null)
    {
        global $db;
        $db->query('SELECT itmid, itmname FROM items ORDER BY itmname ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no items</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['itmid'], $row['itmid'] == $selected ? ' selected' : '', $this->format($row['itmname']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function location_dropdown($ddname = 'location', $selected = null)
    {
        global $db;
        $db->query('SELECT cityid, cityname FROM cities ORDER BY cityname ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no cities</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['cityid'], $row['cityid'] == $selected ? ' selected' : '', $this->format($row['cityname']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function shop_dropdown($ddname = 'shop', $selected = null)
    {
        global $db;
        $db->query('SELECT shopID, shopNAME FROM shops ORDER BY shopNAME ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no shops</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['shopID'], $row['shopID'] == $selected ? ' selected' : '', $this->format($row['shopNAME']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function user_dropdown($ddname = 'user', $selected = null)
    {
        global $db;
        $db->query('SELECT userid, username FROM users ORDER BY username ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no users</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['userid'], $row['userid'] == $selected ? ' selected' : '', $this->format($row['username']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function city_dropdown($ddname = 'user', $selected = null)
    {
        global $db;
        $db->query('SELECT cityid, cityname FROM cities ORDER BY cityname ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no cities</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['cityid'], $row['cityid'] == $selected ? ' selected' : '', $this->format($row['cityname']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function fed_user_dropdown($ddname = 'user', $selected = null)
    {
        global $db;
        $db->query('SELECT userid, username FROM users WHERE fedjail = 1 ORDER BY username ASC');
        $db->execute();
        $rows = $db->fetch();
        if (null === $rows) {
            return '<em>There are no users</em>';
        }
        $ret = '<select name="'.$ddname.'" id="'.$ddname.'" class="form-control bg-dark text-light">
    <option value="0"'.(null === $selected ? ' selected' : '').' disabled>--- SELECT ---</option>';
        foreach ($rows as $row) {
            $ret .= sprintf('<option value="%u"%s>%s</option>', $row['userid'], $row['userid'] == $selected ? ' selected' : '', $this->format($row['username']));
        }
        $ret .= '</select>';

        return $ret;
    }

    public function event_add($id, $content, $extra = '')
    {
        global $db;
        $db->query('INSERT INTO events (evUSER, evTEXT, evEXTRA) VALUES (?, ?, ?)');
        $db->execute([$id, $content, $extra]);

        return true;
    }

    public function check_level()
    {
        global $db, $ir, $userid;
        $ir['exp_needed'] = (int) (($ir['level'] + 1) * ($ir['level'] + 1) * ($ir['level'] + 1) * 2.2);
        if ($ir['exp'] >= $ir['exp_needed']) {
            $expu = $ir['exp'] - $ir['exp_needed'];
            ++$ir['level'];
            $ir['exp'] = $expu;
            $ir['energy'] += 2;
            $ir['brave'] += 2;
            $ir['maxenergy'] += 2;
            $ir['maxbrave'] += 2;
            $ir['hp'] += 50;
            $ir['maxhp'] += 50;
            $ir['exp_needed'] = (int) (($ir['level'] + 1) * ($ir['level'] + 1) * ($ir['level'] + 1) * 2.2);
            $db->trans('start');
            $db->query('UPDATE users SET level = level + 1, exp = ?, maxenergy = maxenergy + 2, energy = maxenergy, maxbrave = maxbrave + 2, brave = maxbrave, maxhp = maxhp + 50, hp = maxhp WHERE userid = ?');
            $db->execute([$expu, $ir['userid']]);
            $this->event_add($ir['userid'], 'Congratulations! You\'ve reached level '.$this->format($ir['level']));
            $db->trans('end');
        }
    }

    public function get_rank($stat, $mykey)
    {
        global $db, $ir, $userid;
        $db->query('SELECT COUNT(us.userid)
            FROM userstats AS us
            INNER JOIN users AS u ON us.userid = u.userid
            WHERE us.userid <> ? AND u.user_level > 0 AND ? <= us.'.$mykey);
        $db->execute([$userid, $stat]);

        return $db->result() + 1;
    }

    public function microtime_float()
    {
        list($usec, $sec) = explode(' ', microtime());

        return (float) $usec + (float) $sec;
    }

    /**
     * @return string the URL of the game
     */
    public function determine_game_urlbase()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $turi = $_SERVER['REQUEST_URI'];
        $turiq = '';
        for ($t = strlen($turi) - 1; $t >= 0; --$t) {
            if ('/' != $turi[$t]) {
                $turiq = $turi[$t].$turiq;
            } else {
                break;
            }
        }
        $turiq = '/'.$turiq;
        $domain .= '/' == $turiq ? substr($turi, 0, -1) : str_replace($turiq, '', $turi);

        return $domain;
    }

    /**
     * Get the file size in bytes of a remote file, if we can.
     *
     * @param string $url The url to the file
     *
     * @return int the file's size in bytes, or 0 if we could
     *             not determine its size
     */
    public function get_filesize_remote($url)
    {
        // Retrieve headers
        if (strlen($url) < 8) {
            return 0; // no file
        }
        $is_ssl = false;
        if ('http://' == substr($url, 0, 7)) {
            $port = 80;
        } elseif ('https://' == substr($url, 0, 8) && extension_loaded('openssl')) {
            $port = 443;
            $is_ssl = true;
        } else {
            return 0; // bad protocol
        }
        // Break up url
        $url_parts = explode('/', $url);
        $host = $url_parts[2];
        unset($url_parts[2]);
        unset($url_parts[1]);
        unset($url_parts[0]);
        $path = '/'.implode('/', $url_parts);
        if (false !== strpos($host, ':')) {
            $host_parts = explode(':', $host);
            if (2 == count($host_parts) && ctype_digit($host_parts[1])) {
                $port = (int) $host_parts[1];
                $host = $host_parts[0];
            } else {
                return 0; // malformed host
            }
        }
        $request = "HEAD {$path} HTTP/1.1\r\n"."Host: {$host}\r\n"."Connection: Close\r\n\r\n";
        $fh = fsockopen(($is_ssl ? 'ssl://' : '').$host, $port);
        if (false === $fh) {
            return 0;
        }
        fwrite($fh, $request);
        $headers = array();
        $total_loaded = 0;
        while (!feof($fh) && $line = fgets($fh, 1024)) {
            if ("\r\n" == $line) {
                break;
            }
            if (false !== strpos($line, ':')) {
                list($key, $val) = explode(':', $line, 2);
                $headers[strtolower($key)] = trim($val);
            } else {
                $headers[] = strtolower($line);
            }
            $total_loaded += strlen($line);
            if ($total_loaded > 50000) {
                // Stop loading garbage!
                break;
            }
        }
        fclose($fh);
        if (!isset($headers['content-length'])) {
            return 0;
        }

        return (int) $headers['content-length'];
    }

    public function handleObLevels()
    {
        global $ir;
        $obLevel = ob_get_level();
        $contents = [];
        if ($obLevel > 0) {
            for ($i = 0; $i < $obLevel; ++$i) {
                $contents[] = ob_get_contents();
                ob_end_clean();
            }
            $contents = array_reverse($contents);
            $output = '';
            foreach ($contents as $content) {
                $output .= str_replace(SETTINGS_FIND, SETTINGS_REPL, $this->profileName($content));
            }
            if ($ir['maxenergy'] > 0) {
                $enperc = (int) ($ir['energy'] / $ir['maxenergy'] * 100);
                $wiperc = (int) ($ir['will'] / $ir['maxwill'] * 100);
                $experc = (int) ($ir['exp'] / $ir['exp_needed'] * 100);
                $brperc = (int) ($ir['brave'] / $ir['maxbrave'] * 100);
                $hpperc = (int) ($ir['hp'] / $ir['maxhp'] * 100);
                if ($enperc < 0) {
                    $enperc = 0;
                } elseif ($enperc > 100) {
                    $enperc = 100;
                }
                if ($wiperc < 0) {
                    $wiperc = 0;
                } elseif ($wiperc > 100) {
                    $wiperc = 100;
                }
                if ($experc < 0) {
                    $experc = 0;
                } elseif ($experc > 100) {
                    $experc = 100;
                }
                if ($brperc < 0) {
                    $brperc = 0;
                } elseif ($brperc > 100) {
                    $brperc = 100;
                }
                if ($hpperc < 0) {
                    $hpperc = 0;
                } elseif ($hpperc > 100) {
                    $hpperc = 100;
                }
                $enopp = 100 - $enperc;
                $wiopp = 100 - $wiperc;
                $exopp = 100 - $experc;
                $bropp = 100 - $brperc;
                $hpopp = 100 - $hpperc;
                if ($enperc <= 25) {
                    $enclass = 'bg-danger';
                } elseif ($enperc > 25 && $enperc <= 50) {
                    $enclass = 'bg-warning';
                } elseif ($enperc > 50 && $enperc <= 75) {
                    $enclass = 'bg-success';
                } else {
                    $enclass = 'bg-info';
                }
                if ($wiperc <= 25) {
                    $wiclass = 'bg-danger';
                } elseif ($wiperc > 25 && $wiperc <= 50) {
                    $wiclass = 'bg-warning';
                } elseif ($wiperc > 50 && $wiperc <= 75) {
                    $wiclass = 'bg-success';
                } else {
                    $wiclass = 'bg-info';
                }
                if ($experc <= 25) {
                    $exclass = 'bg-danger';
                } elseif ($experc > 25 && $experc <= 50) {
                    $exclass = 'bg-warning';
                } elseif ($experc > 50 && $experc <= 75) {
                    $exclass = 'bg-success';
                } else {
                    $exclass = 'bg-info';
                }
                if ($brperc <= 25) {
                    $brclass = 'bg-danger';
                } elseif ($brperc > 25 && $brperc <= 50) {
                    $brclass = 'bg-warning';
                } elseif ($brperc > 50 && $brperc <= 75) {
                    $brclass = 'bg-success';
                } else {
                    $brclass = 'bg-info';
                }
                if ($hpperc <= 25) {
                    $hpclass = 'bg-danger';
                } elseif ($hpperc > 25 && $hpperc <= 50) {
                    $hpclass = 'bg-warning';
                } elseif ($hpperc > 50 && $hpperc <= 75) {
                    $hpclass = 'bg-success';
                } else {
                    $hpclass = 'bg-info';
                }
                $find = [
                    '{ENERGY}', '{ENERGY_MAX}', '{ENERGY_PERC}', '{ENERGY_PERC_DEF}', '{ENERGY_BAR_CLASS}', '{ENERGY_FORMATTED}', '{ENERGY_MAX_FORMATTED}',
                    '{WILL}', '{WILL_MAX}', '{WILL_PERC}', '{WILL_PERC_DEF}', '{WILL_BAR_CLASS}', '{WILL_FORMATTED}', '{WILL_MAX_FORMATTED}',
                    '{EXP}', '{EXP_MAX}', '{EXP_PERC}', '{EXP_PERC_DEF}', '{EXP_BAR_CLASS}', '{EXP_FORMATTED}', '{EXP_MAX_FORMATTED}',
                    '{BRAVE}', '{BRAVE_MAX}', '{BRAVE_PERC}', '{BRAVE_PERC_DEF}', '{BRAVE_BAR_CLASS}', '{BRAVE_FORMATTED}', '{BRAVE_MAX_FORMATTED}',
                    '{HEALTH}', '{HEALTH_MAX}', '{HEALTH_PERC}', '{HEALTH_PERC_DEF}', '{HEALTH_BAR_CLASS}', '{HEALTH_FORMATTED}', '{HEALTH_MAX_FORMATTED}',
                    '{MONEY}', '{CRYSTALS}', '{LEVEL}',
                    '{PAW}',
                ];
                $repl = [
                    $ir['energy'], $ir['maxenergy'], $enperc, $enopp, $enclass, $this->format($ir['energy']), $this->format($ir['maxenergy']),
                    $ir['will'], $ir['maxwill'], $wiperc, $wiopp, $wiclass, $this->format($ir['will']), $this->format($ir['maxwill']),
                    $ir['exp'], $ir['exp_needed'], $experc, $exopp, $exclass, $this->format($ir['exp'], 4), $this->format($ir['exp_needed'], 4),
                    $ir['brave'], $ir['maxbrave'], $brperc, $bropp, $brclass, $this->format($ir['brave']), $this->format($ir['maxbrave']),
                    $ir['hp'], $ir['maxhp'], $hpperc, $hpopp, $hpclass, $this->format($ir['hp']), $this->format($ir['maxhp']),
                    $this->money($ir['money']), $this->format($ir['crystals']), $this->format($ir['level']),
                    '<span class="fas fa-paw"></span>',
                ];
                $output = str_replace($find, $repl, $output);
            }
            echo $output;
        }
    }

    public function profileName($text)
    {
        global $db;
        $pattern = '/\[profile\:(\d+?)\]/i';
        preg_match_all($pattern, $text, $matches);
        $cnt = is_array($matches) ? count($matches[0]) : 0;
        if ($cnt > 0) {
            $cache = [];
            for ($i = 0; $i <= $cnt - 1; ++$i) {
                if (!array_key_exists($matches[1][$i], $cache)) {
                    $cache[$matches[1][$i]] = $this->username($matches[1][$i]);
                }
                $text = str_ireplace('[profile:'.$matches[1][$i].']', $cache[$matches[1][$i]], $text);
            }
        }

        return $text;
    }

    public function autoVersion($file)
    {
        $filetime = 0 === strpos($file, '/') ? (file_exists($_SERVER['DOCUMENT_ROOT'].$file) ? filemtime($_SERVER['DOCUMENT_ROOT'].$file) : false) : false;
        if ($filetime) {
            return preg_replace('{\\.([^./]+)$}', ".$filetime.\$1", $file);
        } else {
            return $file;
        }
    }

    public function format($str, $dec = 0)
    {
        if (is_numeric($str)) {
            return number_format($str, $dec);
        } else {
            $str = stripslashes(htmlspecialchars($str));
            if (true === $dec) {
                $str = nl2br($str);
            }

            return $str;
        }
    }

    public function hasItem($item, $user = 0)
    {
        global $db, $ir;
        if (!$user) {
            if (!is_array($ir) or !array_key_exists('userid', $ir)) {
                return false;
            }
            $user = $ir['userid'];
        }
        $db->query('SELECT inv_qty FROM inventory WHERE inv_itemid = ? AND inv_userid = ?');
        $db->execute([$item, $user]);

        return $db->result();
    }

    public function takeItem($item, $qty = 1, $user = 0)
    {
        global $db, $ir;
        if (!$user) {
            if (!is_array($ir) or !array_key_exists('userid', $ir)) {
                return false;
            }
            $user = $ir['userid'];
        }
        $db->query('SELECT inv_qty FROM inventory WHERE inv_itemid = ? AND inv_userid = ?');
        $db->execute([$item, $user]);
        $owned = $db->result();
        if ($qty >= $owned) {
            $db->query('DELETE FROM inventory WHERE inv_itemid = ? AND inv_userid = ?');
            $db->execute([$item, $user]);
        } else {
            $db->query('UPDATE inventory SET inv_qty = inv_qty - ? WHERE inv_itemid = ? AND inv_userid = ?');
            $db->execute([$qty, $item, $user]);
        }

        return true;
    }

    public function giveItem($item, $qty = 1, $user = 0)
    {
        global $db, $ir;
        if (!$user) {
            $user = $ir['userid'];
        }
        $db->query('SELECT inv_id FROM inventory WHERE inv_itemid = ? AND inv_userid = ?');
        $db->execute([$item, $user]);
        $inv = $db->result();
        if (null === $inv) {
            $db->query('INSERT INTO inventory (inv_itemid, inv_qty, inv_userid) VALUES (?, ?, ?)');
            $db->execute([$item, $qty, $user]);
            $inv = $db->insert_id();
        } else {
            $db->query('UPDATE inventory SET inv_qty = inv_qty + ? WHERE inv_id = ?');
            $db->execute([$qty, $inv]);
        }

        return $inv;
    }

    public function aAn($str, $retWord = false)
    {
        return (in_array(substr(strtolower($str), 0, 1), ['a', 'e', 'i', 'o', 'u']) ? 'an' : 'a').(true === $retWord ? ' '.$str : '');
    }

    public function username($id, array $settings = [])
    {
        global $db, $ir;
        if (array_key_exists($id, self::$userCache)) {
            return self::$userCache[$id];
        }
        $defaults = [
            'display_id' => true,
            'display_donator_icon' => true,
            'display_status_icons' => true,
            'override_cache' => false,
        ];
        foreach ($defaults as $setting => $wut) {
            if (!in_array($setting, array_keys($settings))) {
                $settings[$setting] = $wut;
            }
        }
        $link = '<a href="/viewuser.php?u=%u"%s>%s</a>';
        if ($id == $ir['userid']) {
            $row = $ir;
        } else {
            $db->query('SELECT userid, username, user_level, donatordays, mailban, fedjail FROM users WHERE userid = ?');
            $db->execute([$id]);
            $row = $db->fetch(true);
            if (null === $row) {
                return '<em>System</em>';
            }
        }
        $style = '';
        if ($row['fedjail'] > 0) {
            $style .= 'color:#732f0d;text-decoration:line-through;';
        } elseif ($row['mailban'] > 0) {
            $style .= 'color:#7a5e38;';
        } elseif (2 == $row['user_level']) {
            $style .= 'color:#935ae4;';
        } elseif (3 == $row['user_level']) {
            $style .= 'color:#07d410;';
        } elseif (5 == $row['user_level']) {
            $style .= 'color:#e6de09;';
        } elseif ($row['donatordays'] > 0) {
            $style .= 'color:#df1414;';
        }
        $inline = '' != $style ? ' style="'.$style.'"' : '';
        $extra = '';
        if ($row['donatordays'] > 0 && $settings['display_donator_icon']) {
            $titleAlt = 'Donator: '.$this->time_format($row['donatordays'] * 86400, 'long', false).' remaining';
            $extra .= ' <span class="fas fa-gem text-warning" title="'.$titleAlt.'"></span>';
        }
        if (true === $settings['display_id']) {
            $extra .= ' <span class="badge badge-dark">#'.$row['userid'].'</span>';
        }
        if (true === $settings['display_status_icons']) {
            if ($row['mailban'] > 0) {
                $extra .= ' <span class="fas fa-envelope text-danger" title="Mailbanned for '.$this->time_format($row['mailban'] * 86400, 'long', false).'"></span>';
            }
        }
        $username = $this->format($row['username']);
        $ret = sprintf($link, $row['userid'], $inline, $username).$extra;
        if (true !== $settings['override_cache']) {
            self::$userCache[$id] = $ret;
        }

        return $ret;
    }

    public function ordinal($num)
    {
        if (!in_array(($num % 100), [11, 12, 13])) {
            switch ($num % 10) {
                case 1:
                    return $num.'st';
                    break;
                case 2:
                    return $num.'nd';
                    break;
                case 3:
                    return $num.'rd';
                    break;
            }
        }

        return 'th';
    }

    public function currency($num)
    {
        setlocale(LC_MONETARY, 'en_US');

        return money_format('%.2n', $num);
    }

    public function list_format(array $items = [], $oxfordComma = false, $char = ',')
    {
        $count = count($items);
        if (0 === $count) {
            return '';
        }
        if (1 === $count) {
            return $items[0];
        }

        return implode($char.' ', array_slice($items, 0, -1)).(true == $oxfordComma ? ', ' : '').' and '.end($items);
    }

    public function time_format($seconds, $mode = 'long', $ago = true, $display = 3)
    {
        if (!$seconds) {
            return $ago ? 'Now' : 'Never';
        }
        $names = [
            'long' => ['millenia', 'year', 'month', 'day', 'hour', 'minute', 'second'],
            'short' => ['mil', 'yr', 'mnth', 'day', 'hr', 'min', 'sec'],
        ];
        $seconds = floor($seconds);
        $minutes = intval($seconds / 60);
        $seconds -= $minutes * 60;
        $hours = intval($minutes / 60);
        $minutes -= $hours * 60;
        $days = intval($hours / 24);
        $hours -= $days * 24;
        $months = intval($days / 31);
        $days -= $months * 31;
        $years = intval($months / 12);
        $months -= $years * 12;
        $millenia = intval($years / 1000);
        $years -= $millenia * 1000;
        $disp = 0;
        $result = [];
        if ($millenia && $disp < $display) {
            $result[] = sprintf('%s %s', number_format($millenia), $names[$mode][0]);
            ++$disp;
        }
        if ($years && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($years), $names[$mode][1], $this->s($years));
            ++$disp;
        }
        if ($months && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($months), $names[$mode][2], $this->s($months));
            ++$disp;
        }
        if ($days && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($days), $names[$mode][3], $this->s($days));
            ++$disp;
        }
        if ($hours && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($hours), $names[$mode][4], $this->s($hours));
            ++$disp;
        }
        if ($minutes && $disp < $display) {
            $result[] = sprintf('%s %s%s', number_format($minutes), $names[$mode][5], $this->s($minutes));
            ++$disp;
        }
        if ($seconds && $disp < $display || !count($result)) {
            $result[] = sprintf('%s %s%s', number_format($seconds), $names[$mode][6], $this->s($seconds));
            ++$disp;
        }

        return implode(', ', $result).($ago ? ' ago' : '');
    }

    public function s($num, $type = '', $word = null, $apostrophe = false)
    {
        if (null === $word) {
            if (!$type) {
                return 1 == $num ? '' : (true === $apostrophe ? '\'' : '').'s';
            } else {
                $let = explode(':', $type);

                return 1 == $num ? $let[0] : (true === $apostrophe ? '\'' : '').$let[1];
            }
        } else {
            if (strstr($word, 'dice') || strstr($word, 'sheep')) {
                return '';
            }
            $last = substr($word, -1);
            if (!$type) {
                return $word.('s' != $last && 1 != $num ? (true === $apostrophe ? '\'' : '').'s' : (true === $apostrophe ? '\'' : '').'');
            } else {
                $let = explode('|', $type);

                return $word.(1 == $num ? $let[0] : (true === $apostrophe ? '\'' : '').$let[1]);
            }
        }
    }

    public function userExists($id)
    {
        global $db;
        $db->query('SELECT COUNT(userid) FROM users WHERE userid = ?');
        $db->execute([$id]);

        return $db->result(true);
    }

    public function array_flatten(array $array): array
    {
        $return = [];
        array_walk_recursive($array, function ($a) use (&$return) {
            $return[] = $a;
        });

        return $return;
    }

    public function convertUserPlaceholder($str, $extra)
    {
        $users = explode(':', $extra);
        $cnt = 0;
        preg_match_all('/(\{(user)(\d+?)?\})/i', $str, $matches);
        if (count($matches[0])) {
            foreach ($matches[0] as $match) {
                if (!array_key_exists($users[$cnt], self::$userCache)) {
                    $ev = $this->username($users[$cnt]);
                    self::$userCache[$users[$cnt]] = $ev;
                } else {
                    $ev = self::$userCache[$users[$cnt]];
                }
                $str = str_ireplace($match, $ev, $str);
                ++$cnt;
            }
        }

        return $str;
    }

    public function handleImageUpload($localPath, $attrName = 'image', $fileName = null)
    {
        $path = [];
        if (!empty($_FILES) && count($_FILES)) {
            $image = new \Bulletproof\Image($_FILES);
            $image->setMime(['jpeg', 'gif', 'png']);
            $image->setSize(1024, 1048576);
            // $image->setDimension(600, 600);
            $image->setLocation($localPath);
            $fileName = md5(microtime(true));
            $image->setName($fileName.'_'.date('d-m-y--H-i-s'));
            if ($image[$attrName]) {
                if ($image->upload()) {
                    $url = $this->determine_game_urlbase();
                    $path['url'] = 'https://'.$url.'/'.str_replace(PATH_ROOT, '', $image->getFullPath());
                } else {
                    $path['error'] = 'Couldn\'t upload image: '.$image->getError();
                }
            } else {
                $path['error'] = 'Couldn\'t upload image: '.$image->getError();
            }
        }

        return $path;
    }

    public function getDonatorPacks()
    {
        $packs = [
            1 => [
                'name' => 'Pack 1 (Standard)',
                'money' => 5000,
                'crystals' => 50,
                'IQ' => 50,
                'donatordays' => 30,
            ],
            2 => [
                'name' => 'Pack 2 (Crystals)',
                'crystals' => 100,
                'donatordays' => 30,
            ],
            3 => [
                'name' => 'Pack 3 (IQ)',
                'IQ' => 100,
                'donatordays' => 30,
            ],
            4 => [
                'name' => 'Pack 4 ($5.00)',
                'money' => 15000,
                'crystals' => 75,
                'IQ' => 80,
                'donatordays' => 55,
            ],
            5 => [
                'name' => 'Pack 5 ($10.00)',
                'money' => 35000,
                'crystals' => 160,
                'IQ' => 180,
                'donatordays' => 115,
                'items' => [[12, 1]],
            ],
        ];

        return $packs;
    }

    public function stafflog($log, $extra = '')
    {
        global $db, $ir;
        $db->query('INSERT INTO logs_staff (userid, content, extra, ipaddress) VALUES (?, ?, ?, ?)');
        $db->execute([$ir['userid'], $log, $extra, $this->ip()]);
    }

    public function detect_browser()
    {
        global $user_agent;

        $browser = 'Unknown Browser';

        $browser_array = [
            '/msie/i' => 'Internet Explorer',
            '/firefox/i' => 'Firefox',
            '/safari/i' => 'Safari',
            '/chrome/i' => 'Chrome',
            '/edge/i' => 'Edge',
            '/opera/i' => 'Opera',
            '/netscape/i' => 'Netscape',
            '/maxthon/i' => 'Maxthon',
            '/konqueror/i' => 'Konqueror',
            '/mobile/i' => 'Handheld Browser',
            '/android/i' => 'Android Browser',
            '/edge/i' => 'Microsoft Edge',
        ];

        foreach ($browser_array as $regex => $value) {
            if (preg_match($regex, $user_agent, $matches)) {
                return $value;
            }
        }

        return $browser;
    }

    public function detect_os()
    {
        if (!isset($user_agent) && isset($_SERVER['HTTP_USER_AGENT'])) {
            $user_agent = $_SERVER['HTTP_USER_AGENT'];
        }

        // https://stackoverflow.com/questions/18070154/get-operating-system-info-with-php
        $os_array = [
            'windows nt 10' => 'Windows 10',
            'windows nt 6.3' => 'Windows 8.1',
            'windows nt 6.2' => 'Windows 8',
            'windows nt 6.1|windows nt 7.0' => 'Windows 7',
            'windows nt 6.0' => 'Windows Vista',
            'windows nt 5.2' => 'Windows Server 2003/XP x64',
            'windows nt 5.1' => 'Windows XP',
            'windows xp' => 'Windows XP',
            'windows nt 5.0|windows nt5.1|windows 2000' => 'Windows 2000',
            'windows me' => 'Windows ME',
            'windows nt 4.0|winnt4.0' => 'Windows NT',
            'windows ce' => 'Windows CE',
            'windows 98|win98' => 'Windows 98',
            'windows 95|win95' => 'Windows 95',
            'win16' => 'Windows 3.11',
            'mac os x 10.1[^0-9]' => 'Mac OS X Puma',
            'macintosh|mac os x' => 'Mac OS X',
            'mac_powerpc' => 'Mac OS 9',
            'linux' => 'Linux',
            'ubuntu' => 'Linux - Ubuntu',
            'iphone' => 'iPhone',
            'ipod' => 'iPod',
            'ipad' => 'iPad',
            'android' => 'Android',
            'blackberry' => 'BlackBerry',
            'webos' => 'Mobile',

            '(media center pc).([0-9]{1,2}\.[0-9]{1,2})' => 'Windows Media Center',
            '(win)([0-9]{1,2}\.[0-9x]{1,2})' => 'Windows',
            '(win)([0-9]{2})' => 'Windows',
            '(windows)([0-9x]{2})' => 'Windows',

            // Doesn't seem like these are necessary...not totally sure though..
            // '(winnt)([0-9]{1,2}\.[0-9]{1,2}){0,1}'=>'Windows NT',
            // '(windows nt)(([0-9]{1,2}\.[0-9]{1,2}){0,1})'=>'Windows NT', // fix by bg

            'Win 9x 4.90' => 'Windows ME',
            '(windows)([0-9]{1,2}\.[0-9]{1,2})' => 'Windows',
            'win32' => 'Windows',
            '(java)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2})' => 'Java',
            '(Solaris)([0-9]{1,2}\.[0-9x]{1,2}){0,1}' => 'Solaris',
            'dos x86' => 'DOS',
            'Mac OS X' => 'Mac OS X',
            'Mac_PowerPC' => 'Macintosh PowerPC',
            '(mac|Macintosh)' => 'Mac OS',
            '(sunos)([0-9]{1,2}\.[0-9]{1,2}){0,1}' => 'SunOS',
            '(beos)([0-9]{1,2}\.[0-9]{1,2}){0,1}' => 'BeOS',
            '(risc os)([0-9]{1,2}\.[0-9]{1,2})' => 'RISC OS',
            'unix' => 'Unix',
            'os/2' => 'OS/2',
            'freebsd' => 'FreeBSD',
            'openbsd' => 'OpenBSD',
            'netbsd' => 'NetBSD',
            'irix' => 'IRIX',
            'plan9' => 'Plan9',
            'osf' => 'OSF',
            'aix' => 'AIX',
            'GNU Hurd' => 'GNU Hurd',
            '(fedora)' => 'Linux - Fedora',
            '(kubuntu)' => 'Linux - Kubuntu',
            '(ubuntu)' => 'Linux - Ubuntu',
            '(debian)' => 'Linux - Debian',
            '(CentOS)' => 'Linux - CentOS',
            '(Mandriva).([0-9]{1,3}(\.[0-9]{1,3})?(\.[0-9]{1,3})?)' => 'Linux - Mandriva',
            '(SUSE).([0-9]{1,3}(\.[0-9]{1,3})?(\.[0-9]{1,3})?)' => 'Linux - SUSE',
            '(Dropline)' => 'Linux - Slackware (Dropline GNOME)',
            '(ASPLinux)' => 'Linux - ASPLinux',
            '(Red Hat)' => 'Linux - Red Hat',
            // Loads of Linux machines will be detected as unix.
            // Actually, all of the linux machines I've checked have the 'X11' in the User Agent.
            // 'X11'=>'Unix',
            '(linux)' => 'Linux',
            '(amigaos)([0-9]{1,2}\.[0-9]{1,2})' => 'AmigaOS',
            'amiga-aweb' => 'AmigaOS',
            'amiga' => 'Amiga',
            'AvantGo' => 'PalmOS',
            // '(Linux)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3}(rel\.[0-9]{1,2}){0,1}-([0-9]{1,2}) i([0-9]{1})86){1}'=>'Linux',
            // '(Linux)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3}(rel\.[0-9]{1,2}){0,1} i([0-9]{1}86)){1}'=>'Linux',
            // '(Linux)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3}(rel\.[0-9]{1,2}){0,1})'=>'Linux',
            '(Linux)([0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3})' => 'Linux',
            '(webtv)/([0-9]{1,2}\.[0-9]{1,2})' => 'WebTV',
            'Dreamcast' => 'Dreamcast OS',
            'GetRight' => 'Windows',
            'go!zilla' => 'Windows',
            'gozilla' => 'Windows',
            'gulliver' => 'Windows',
            'ia archiver' => 'Windows',
            'NetPositive' => 'Windows',
            'mass downloader' => 'Windows',
            'microsoft' => 'Windows',
            'offline explorer' => 'Windows',
            'teleport' => 'Windows',
            'web downloader' => 'Windows',
            'webcapture' => 'Windows',
            'webcollage' => 'Windows',
            'webcopier' => 'Windows',
            'webstripper' => 'Windows',
            'webzip' => 'Windows',
            'wget' => 'Windows',
            'Java' => 'Unknown',
            'flashget' => 'Windows',

            // delete next line if the script show not the right OS
            // '(PHP)/([0-9]{1,2}.[0-9]{1,2})'=>'PHP',
            'MS FrontPage' => 'Windows',
            '(msproxy)/([0-9]{1,2}.[0-9]{1,2})' => 'Windows',
            '(msie)([0-9]{1,2}.[0-9]{1,2})' => 'Windows',
            'libwww-perl' => 'Unix',
            'UP.Browser' => 'Windows CE',
            'NetAnts' => 'Windows',
        ];

        // https://github.com/ahmad-sa3d/php-useragent/blob/master/core/user_agent.php
        $arch_regex = '/\b(x86_64|x86-64|Win64|WOW64|x64|ia64|amd64|ppc64|sparc64|IRIX64)\b/ix';
        $arch = preg_match($arch_regex, $user_agent) ? '64' : '32';

        foreach ($os_array as $regex => $value) {
            if (preg_match('{\b('.$regex.')\b}i', $user_agent)) {
                return $value.' x'.$arch;
            }
        }

        return 'Unknown';
    }

    public function isProxy()
    {
        $httpHeaders = [
            'HTTP_VIA',
            // 'HTTP_X_FORWARDED_FOR', // Set by CloudFlare. Comment out to disable check
            'HTTP_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_FORWARDED',
            'HTTP_CLIENT_IP',
            'HTTP_FORWARDED_FOR_IP',
            'VIA',
            'X_FORWARDED_FOR',
            'FORWARDED_FOR',
            'X_FORWARDED',
            'FORWARDED',
            'CLIENT_IP',
            'FORWARDED_FOR_IP',
            'HTTP_PROXY_CONNECTION',
        ];
        foreach ($httpHeaders as $header) {
            if (array_key_exists($header, $_SERVER) && isset($_SERVER[$header]) && strlen($_SERVER[$header]) > 0) {
                return true;
            }
        }

        return false;
    }

    /*
     * Function: _ip
     * Capture user's given IP address (not necessarily accurate)
     * Params: none
     */
    public function ip()
    {
        $check = $this->isCloudflare();

        if ($check) {
            return $_SERVER['HTTP_CF_CONNECTING_IP'];
        } else {
            $types = [
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'HTTP_X_FORWARDED',
                'HTTP_X_CLUSTER_CLIENT_IP',
                'HTTP_FORWARDED_FOR',
                'HTTP_FORWARDED',
                'REMOTE_ADDR',
            ];
            foreach ($types as $key) {
                if (true === array_key_exists($key, $_SERVER)) {
                    foreach (explode(',', $_SERVER[$key]) as $ip) {
                        $ip = trim($ip);
                        if (false !== filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) {
                            return $ip;
                        }
                    }
                }
            }
        }

        return '127.0.0.1';
    }

    public function subnetMatch($client_ip = false, $server_ip = false)
    {
        if (!$client_ip) {
            $client_ip = $this->ip();
        }
        if (!$server_ip) {
            $server_ip = $_SERVER['SERVER_ADDR'];
        }

        // Extract broadcast and netmask from ifconfig
        if (!($p = popen('ifconfig', 'r'))) {
            return false;
        }
        $out = '';
        while (!feof($p)) {
            $out .= fread($p, 1024);
        }
        fclose($p);

        // This is to avoid wrapping.
        $match = '/^.*'.$server_ip;
        $match .= ".*Bcast:(\d{1,3}\.\d{1,3}i\.\d{1,3}\.\d{1,3}).*";
        $match .= "Mask:(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$/im";
        if (!preg_match($match, $out, $regs)) {
            return false;
        }

        $bcast = ip2long($regs[1]);
        $smask = ip2long($regs[2]);
        $ipadr = ip2long($client_ip);
        $nmask = $bcast & $smask;

        return ($ipadr & $smask) == ($nmask & $smask);
    }

    // CLOUDFLARE
    private function ip_in_range($ip, $range)
    {
        if (false == strpos($range, '/')) {
            $range .= '/32';
        }

        // $range is in IP/CIDR format eg 127.0.0.1/24
        list($range, $netmask) = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
        $netmask_decimal = ~$wildcard_decimal;

        return ($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal);
    }

    private function _cloudflare_CheckIP($ip)
    {
        $cf_ips = [
            '199.27.128.0/21',
            '173.245.48.0/20',
            '103.21.244.0/22',
            '103.22.200.0/22',
            '103.31.4.0/22',
            '141.101.64.0/18',
            '108.162.192.0/18',
            '190.93.240.0/20',
            '188.114.96.0/20',
            '197.234.240.0/22',
            '198.41.128.0/17',
            '162.158.0.0/15',
            '104.16.0.0/12',
        ];
        $is_cf_ip = false;
        foreach ($cf_ips as $cf_ip) {
            if ($this->ip_in_range($ip, $cf_ip)) {
                $is_cf_ip = true;
                break;
            }
        }

        return $is_cf_ip;
    }

    private function _cloudflare_Requests_Check()
    {
        $flag = true;

        if (!isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $flag = false;
        }
        if (!isset($_SERVER['HTTP_CF_IPCOUNTRY'])) {
            $flag = false;
        }
        if (!isset($_SERVER['HTTP_CF_RAY'])) {
            $flag = false;
        }
        if (!isset($_SERVER['HTTP_CF_VISITOR'])) {
            $flag = false;
        }

        return $flag;
    }

    private function isCloudflare()
    {
        $ipCheck = $this->_cloudflare_CheckIP($_SERVER['REMOTE_ADDR']);
        $requestCheck = $this->_cloudflare_Requests_Check();

        return $ipCheck && $requestCheck;
    }
}
