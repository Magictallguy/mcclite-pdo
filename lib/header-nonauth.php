<?php
if (!defined('SITE_ENABLE')) {
    exit;
}
class headers_nonauth
{
    protected $func;
    protected $db;
    protected $onlineCnt;

    public function __construct()
    {
        global $db, $func;
        $this->db = $db;
        $this->func = $func;
        $this->db->query('SELECT COUNT(userid) FROM users WHERE laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)');
        $this->db->execute();
        $res = $this->db->result();
        $this->onlineCnt = $res > 0 ? $res : 0;
        $this->startHeaders();
    }

    public function startHeaders()
    {
        ?><!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="<?php echo $this->func->autoVersion('/src/css/bootstrap-4.3.1/bootstrap.min.css'); ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo $this->func->autoVersion('/src/font-awesome-5.10.0/css/all.min.css'); ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo $this->func->autoVersion('/src/css/game.min.css'); ?>" type="text/css" rel="stylesheet" />
        <script src="<?php echo $this->func->autoVersion('/src/js/jquery/3.3.1/jquery-slim.min.js'); ?>"></script>
        <script src="<?php echo $this->func->autoVersion('/src/js/popper.min.js'); ?>"></script>
        <script src="<?php echo $this->func->autoVersion('/src/js/bootstrap-4.3.1/bootstrap.bundle.min.js'); ?>"></script>
        <link rel="shortcut icon" type="image/icon" href="/src/images/favicon.ico">
        <title>{GAME_NAME}</title>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col">
                <img src="/src/images/logo-new.png" alt="{GAME_NAME} Logo">
            </div>
        </div>
        <div class="row">
            <div class="col"><?php
    }

    public function __destruct()
    {
        $year = date('Y');
        define('TIME_END', microtime(true)); ?>
            </div>
        </div>
        <footer class="footer">
            <div class="row">
                <div class="col-6">
                    Online [<?php echo $this->func->format($this->onlineCnt); ?>] &middot;
                    Page loaded in <?php echo number_format(TIME_END - TIME_START, 3); ?> &middot;
                    <?php echo $this->func->format($this->db->queryCount),' quer',$this->func->s($this->db->queryCount, 'y:ies'); ?>
                </div>
                <div class="col-6">
                    Copyright &copy; <?php echo $year; ?> {GAME_OWNER}.
                </div>
            </div>
        </footer>
    </div>
    </body>
    </html><?php
    $this->func->handleObLevels();
    }
}
