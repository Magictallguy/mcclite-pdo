<?php
/*
MCCodes FREE
header.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if (!defined('SITE_ENABLE')) {
    exit;
}

class headers
{
    private $func;
    private $fightRun = false;

    public function __construct()
    {
        global $func;
        ob_start();
        $this->func = $func;
        $this->startHeaders();
    }

    public function startHeaders()
    {
        ?><!DOCTYPE html>
       <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
           <meta charset="utf-8">
           <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
           <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet">
           <link href="<?php echo $this->func->autoVersion('/src/css/bootstrap-4.3.1/bootstrap.min.css'); ?>" type="text/css" rel="stylesheet">
           <link href="<?php echo $this->func->autoVersion('/src/font-awesome-5.10.0/css/all.min.css'); ?>" type="text/css" rel="stylesheet">
           <link href="<?php echo $this->func->autoVersion('/src/css/game.min.css'); ?>" type="text/css" rel="stylesheet">
           <link href="<?php echo $this->func->autoVersion('/src/ekko-lightbox/ekko.min.css'); ?>" type="text/css" rel="stylesheet">
           <script src="<?php echo $this->func->autoVersion('/src/js/jquery/3.3.1/jquery-slim.min.js'); ?>"></script>
           <script src="<?php echo $this->func->autoVersion('/src/js/popper.min.js'); ?>"></script>
           <script src="<?php echo $this->func->autoVersion('/src/js/bootstrap-4.3.1/bootstrap.bundle.min.js'); ?>"></script>
           <script src="<?php echo $this->func->autoVersion('/src/ekko-lightbox/ekko.min.js'); ?>"></script>
           <link rel="shortcut icon" type="image/icon" href="/src/images/favicon.ico">
           <title>{GAME_NAME}</title>
       </head>
       <body>
       <div class="container"><?php
    }

    public function userdata($ir, $lv, $fm, $cm, $dosessh = 1)
    {
        global $db, $userid;
        $ip = $this->func->ip();
        $db->query('UPDATE users SET laston = NOW(), lastip = ? WHERE userid = ?');
        $db->execute([$ip, $userid]);
        if (!$ir['email']) {
            exit('Your account may be broken. Please mail support@'.$func->determine_game_urlbase().' stating your username and player ID.');
        }
        $this->fightRun = false;
        if ($dosessh && isset($_SESSION['attacking'])) {
            if ($_SESSION['attacking'] > 0) {
                $this->fightRun = true;
                $db->query('UPDATE users SET exp = 0 WHERE userid = ?');
                $db->execute([$userid]);
                $_SESSION['attacking'] = 0;
            }
        }
        $db->query('SELECT adID, adIMG FROM ads ORDER BY RAND() LIMIT 1');
        $db->execute();
        $ad = $db->fetch(true); ?>
        <div class="row m-top-10 m-bottom-10">
            <div class="col-3 align-self-center">
                <a href="<?php echo $ir['avatar']; ?>" data-toggle="lightbox">
                    <img src="<?php echo $ir['avatar']; ?>" class="avatar-infobox img-fluid">
                </a>
                <?php echo $this->func->username($ir['userid']); ?><br>
                <strong>Money:</strong> {MONEY}<?php echo($ir['bankmoney'] > -1 && $ir['money'] > 0 ? ' <a href="bank.php?action=dep" title="Deposit into your bank"><span class="fas fa-coins"></span></a>' : '').($ir['cybermoney'] > -1 && $ir['money'] > 0 ? ' <a href="cyberbank.php?action=dep" title="Deposit into your cyberbank"><span class="fas fa-plug"></span></a>' : ''); ?><br>
                <strong>Crystals:</strong> {CRYSTALS}
            </div>
            <div class="col-6 align-self-center">
                <a href="/">
                    <img src="/src/images/logo-new.png" title="{GAME_NAME}" alt="{GAME_NAME}" class="img-fluid banner-top">
                </a>
            </div>
            <div class="col-3 align-self-center">
                <div class="row">
                    <div class="col-6">
                        <div class="progress bg-dark-red position-relative" title="{ENERGY_FORMATTED}/{ENERGY_MAX_FORMATTED} ({ENERGY_PERC}%)">
                            <div class="progress-bar progress-bar-striped progress-bar-animated {ENERGY_BAR_CLASS}" style="width:{ENERGY_PERC}%"></div>
                            <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>Energy:</strong> {ENERGY_PERC}%</span>
                        </div><br>
                    </div>
                    <div class="col-6">
                        <div class="progress bg-dark-red position-relative">
                            <div class="progress-bar progress-bar-striped progress-bar-animated {BRAVE_BAR_CLASS}" style="width:{BRAVE_PERC}%"></div>
                            <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>Brave:</strong> {BRAVE}/{BRAVE_MAX}</span>
                        </div><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="progress bg-dark-red position-relative" title="{WILL_FORMATTED}/{WILL_MAX_FORMATTED} ({WILL_PERC}%)">
                            <div class="progress-bar progress-bar-striped progress-bar-animated {WILL_BAR_CLASS}" style="width:{WILL_PERC}%"></div>
                            <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>Will:</strong> {WILL_PERC}%</span>
                        </div><br>
                    </div>
                    <div class="col-6">
                        <div class="progress bg-dark-red position-relative" title="{HEALTH_FORMATTED}/{HEALTH_MAX_FORMATTED} ({HEALTH_PERC}%)">
                            <div class="progress-bar progress-bar-striped progress-bar-animated {HEALTH_BAR_CLASS}" style="width:{HEALTH_PERC}%"></div>
                            <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>Health:</strong> {HEALTH_PERC}%</span>
                        </div><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="progress bg-dark-red position-relative" title="{EXP_FORMATTED}/{EXP_MAX_FORMATTED} ({EXP_PERC}%)">
                            <div class="progress-bar progress-bar-striped progress-bar-animated {EXP_BAR_CLASS}" style="width:{EXP_PERC}%"></div>
                            <span class="progress-bar-text justify-content-center d-flex position-absolute w-100"><strong>EXP:</strong> {EXP_PERC}%</span>
                        </div>
                    </div>
                    <div class="col-6">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div><?php
        if (null !== $ad) {
            $db->query('UPDATE ads SET adVIEWS = adVIEWS + 1 WHERE adID = ?');
            $db->execute([$ad['adID']]); ?>
        <div class="row">
            <div class="col text-center">
                <a href="/ad.php?ad=<?php echo $ad['adID']; ?>">
                    <img src="<?php echo $ad['adIMG']; ?>" alt="Paid Advertisement" title="Ad">
                </a>
            </div>
        </div><?php
        } ?>
        <div class="row">
            <div class="col-3<?php echo defined('ATTACKING') ? ' disabled' : ''; ?>"<?php echo defined('ATTACKING') ? ' disabled data-disabled="true"' : ''; ?>><?php
    }

    public function menuarea()
    {
        global $db, $ir;
        $ip = $_SERVER['REMOTE_ADDR'];
        $fedded = $ir['fedjail'] > 0;
        $ipBanned = file_exists(PATH_ROOT.'/ipbans/'.$ip);
        if (!$fedded && !$ipBanned) {
            $mainmenu = __DIR__.'/mainmenu.php';
            file_exists($mainmenu) ? require_once $mainmenu : trigger_error('Critical file missing: '.str_replace(__DIR__, '', $mainmenu), E_USER_ERROR);
        } ?>
            </div>
            <div class="col-9"><?php
        $alerts = ['success', 'info', 'warning', 'error'];
        foreach ($alerts as $alert) {
            if (array_key_exists($alert, $_SESSION)) {
                ?>
                <div class="alert alert-<?php echo $alert; ?>">
                    <a class="close" data-dismiss="alert">&times;</a>
                    <?php echo $_SESSION[$alert]; ?>
                </div><?php
                unset($_SESSION[$alert]);
            }
        }
        if (true === $this->fightRun) {
            ?>
            <div class="alert alert-warning">
                You lost all of your experience for running from the fight
            </div><?php
        }
        if ($fedded) {
            $find = ['{SENTENCE}', '{REASON}'];
            $db->query('SELECT fed_days, fed_reason FROM fedjail WHERE fed_userid = ?');
            $db->execute([$userid]);
            $fed = $db->fetch(true);
            $repl = null === $fed ? ['<em>(an unknown amount of time)</em>', '<em>(an unknown reason)</em>'] : [$this->func->time_format($fed['fed_days'] * 86400), $this->func->format($fed['fed_reason'])];
            exit(str_replace($find, $repl, '<div style="font-weight:700;font-size:1.4em;">You\'ve been put in the {GAME_NAME} Federal Jail for {SENTENCE}.<br>Reason: {REASON}</div>'));
        }
        if ($ipBanned) {
            exit('<div style="font-weight:700;font-size:1.4em;">Your IP has been banned</div>');
        }
    }

    public function __destruct()
    {
        if (defined('NO_FOOTER')) {
            return;
        }
        global $db;
        $db->query('SELECT COUNT(userid) FROM users WHERE laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)');
        $db->execute();
        $onlineUserCnt = $db->result();
        $year = date('Y');
        define('TIME_END', microtime(true)); ?>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <p>
                    <div class="row">
                        <div class="col">
                            <a href="/usersonline.php">Online [<?php echo $this->func->format($onlineUserCnt); ?>]</a> &middot; Page loaded in <?php echo number_format(TIME_END - TIME_START, 3); ?> &middot; <?php echo $this->func->format($db->queryCount),' quer',(1 == $db->queryCount ? 'y' : 'ies'); ?>
                        </div>
                        <div class="col">
                            Copyright &copy; <?php echo $year; ?> {GAME_OWNER}.
                        </div>
                    </div>
                </p>
            </div>
        </div>
    </div>
    <script>
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    </script>
    </body>
</html><?php
    $this->func->handleObLevels();
    }
}
