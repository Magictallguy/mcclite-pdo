<?php

namespace jBBCode;

require_once __DIR__.'/CodeDefinition.php';
require_once __DIR__.'/CodeDefinitionBuilder.php';
require_once __DIR__.'/CodeDefinitionSet.php';
require_once __DIR__.'/validators/CssColorValidator.php';
require_once __DIR__.'/validators/UrlValidator.php';
require_once __DIR__.'/validators/ImageValidator.php';
require_once __DIR__.'/validators/YouTubeValidator.php';

/**
 * Provides a default set of common bbcode definitions.
 *
 * @author jbowens
 */
class DefaultCodeDefinitionSet implements CodeDefinitionSet
{
    /* The default code definitions in this set. */
    protected $definitions = [];

    /**
     * Constructs the default code definitions.
     */
    public function __construct()
    {
        $urlValidator = new \JBBCode\validators\UrlValidator();
        $imageValidator = new \JBBCode\validators\ImageValidator();
        $youTubeValidator = new \JBBCode\validators\YouTubeValidator();
        $cssValidator = new \JBBCode\validators\CssColorValidator();
        /* [ul] unordered list tag */
        $builder = new CodeDefinitionBuilder('ul', '<ul>{param}</ul>');
        $this->definitions[] = $builder->build();
        /* [ol] ordered list tag */
        $builder = new CodeDefinitionBuilder('ol', '<ol>{param}</ol>');
        $this->definitions[] = $builder->build();
        /* [li] list item tag */
        $builder = new CodeDefinitionBuilder('li', '<li>{param}</li>');
        $this->definitions[] = $builder->build();
        /* [b] bold tag */
        $builder = new CodeDefinitionBuilder('b', '<strong>{param}</strong>');
        $this->definitions[] = $builder->build();
        /* [s] strike tag */
        $builder = new CodeDefinitionBuilder('s', '<span class="strike">{param}</span>');
        $this->definitions[] = $builder->build();
        /* [i] italics tag */
        $builder = new CodeDefinitionBuilder('i', '<em>{param}</em>');
        $this->definitions[] = $builder->build();
        /* [u] underline tag */
        $builder = new CodeDefinitionBuilder('u', '<u>{param}</u>');
        $this->definitions[] = $builder->build();
        /* [sub] underline tag */
        $builder = new CodeDefinitionBuilder('sub', '<sub>{param}</sub>');
        $this->definitions[] = $builder->build();
        /* [sup] underline tag */
        $builder = new CodeDefinitionBuilder('sup', '<sup>{param}</sup>');
        $this->definitions[] = $builder->build();
        /* [sup] underline tag */
        $builder = new CodeDefinitionBuilder('small', '<span class="small">{param}</span>');
        $this->definitions[] = $builder->build();
        /* [sup] underline tag */
        $builder = new CodeDefinitionBuilder('big', '<span class="big">{param}</span>');
        $this->definitions[] = $builder->build();
        /* [url] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{param}">{param}</a>');
        $builder->setUseOption(false)
            ->setParseContent(false)
            ->setBodyValidator($urlValidator);
        $this->definitions[] = $builder->build();
        /* [url=https://example.com] link tag */
        $builder = new CodeDefinitionBuilder('url', '<a href="{option}">{param}</a>');
        $builder->setUseOption(true)
            ->setParseContent(true)
            ->setOptionValidator($urlValidator);
        $this->definitions[] = $builder->build();
        /* [color] color tag */
        $builder = new CodeDefinitionBuilder('color', '<span style="color: {option}">{param}</span>');
        $builder->setUseOption(true)->setOptionValidator($cssValidator);
        $this->definitions[] = $builder->build();
        /* [center] align tag */
        $builder = new CodeDefinitionBuilder('center', '<span class="centre">{param}</span>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        /* [centre] align tag */
        $builder = new CodeDefinitionBuilder('centre', '<span class="centre">{param}</span>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        /* [size] font size tag */
        $builder = new CodeDefinitionBuilder('size', '<span style="font-size: {option}">{param}</span>');
        $builder->setUseOption(true)->setParseContent(true);
        $this->definitions[] = $builder->build();
        /* [font] font face tag */
        $builder = new CodeDefinitionBuilder('font', '<span style="font: {option}">{param}</span>');
        $builder->setUseOption(true)->setParseContent(true);
        $this->definitions[] = $builder->build();
        /* [quote] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<blockquote>{param}<br><p style="text-align:right;">~ {option}</p></blockquote>');
        $builder->setUseOption(true)->setParseContent(true);
        $this->definitions[] = $builder->build();
        /* [quote] quote tag */
        $builder = new CodeDefinitionBuilder('quote', '<blockquote>{param}</blockquote>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        /* [noparse] disable parsing tag */
        $builder = new CodeDefinitionBuilder('noparse', '{param}');
        $builder->setUseOption(false)->setParseContent(false);
        $this->definitions[] = $builder->build();
        // ---- MTG'S BBCODE HELPERS
        $builder = new CodeDefinitionBuilder('dev-success', '<div class="success">{param}</div>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        $builder = new CodeDefinitionBuilder('dev-info', '<div class="info">{param}</div>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        $builder = new CodeDefinitionBuilder('dev-warning', '<div class="warning">{param}</div>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        $builder = new CodeDefinitionBuilder('dev-error', '<div class="error">{param}</div>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        // Font Awesome
        $builder = new CodeDefinitionBuilder('fa', '<span class="fas fa-{param}"></span>');
        $builder->setUseOption(false)->setParseContent(true);
        $this->definitions[] = $builder->build();
        if (!defined('CHAT_FILE')) {
            /* [img] image tag */
            $builder = new CodeDefinitionBuilder('img', '<a href="{param}" data-toggle="lightbox"><img src="{param}" class="img-fluid img-thumbnail rounded" alt="" style="max-width:25%;max-height:25%;width:expression(this.width > 25% ? 25%: true);height: expression(this.height > 25% ? 25%: true);"></a>');
            $builder->setUseOption(false)
                ->setParseContent(false)
                ->setBodyValidator($imageValidator);
            $this->definitions[] = $builder->build();
            /* [img=alt text] image tag */
            $builder = new CodeDefinitionBuilder('img', '<a href="{param}" data-toggle="lightbox"><img src="{param}" class="img-fluid img-thumbnail" alt="{option}" style="max-width:25%;max-height:25%;width:expression(this.width > 25% ? 25%: true);height: expression(this.height > 25% ? 25%: true);"></a>');
            $builder->setUseOption(true)
                ->setParseContent(false)
                ->setBodyValidator($imageValidator);
            $this->definitions[] = $builder->build();
            $builder = new CodeDefinitionBuilder('youtube', '<iframe width="420" height="315" src="https://www.youtube.com/embed/{param}" style="border:none;"></iframe>');
            $builder->setUseOption(false)
                ->setParseContent(false)
                ->setBodyValidator($youTubeValidator);
            $this->definitions[] = $builder->build();
        } else {
            /* [img] image tag */
            $builder = new CodeDefinitionBuilder('img', '<a href="{param}" data-toggle="lightbox"><img src="{param}" alt="" style="max-width:600;max-height:25;width:expression(this.width > 25 ? 25: true);height: expression(this.height > 25 ? 25: true);" class="lazyload"></a>');
            $builder->setUseOption(false)
                ->setParseContent(false)
                ->setBodyValidator($imageValidator);
            $this->definitions[] = $builder->build();
            /* [img=alt text] image tag */
            $builder = new CodeDefinitionBuilder('img', '<a href="{param}" data-toggle="lightbox"><img src="{param}" alt="{option}" style="max-width:25;max-height:25;width:expression(this.width > 25 ? 25: true);height: expression(this.height > 25 ? 25: true);" class="lazyload"></a>');
            $builder->setUseOption(true)
                ->setParseContent(false)
                ->setBodyValidator($imageValidator);
            $this->definitions[] = $builder->build();
            $builder = new CodeDefinitionBuilder('youtube', '<a href="http://www.youtube.com/?v={param}">YouTube Video</a>');
            $builder->setUseOption(false)
                ->setParseContent(false)
                ->setBodyValidator($youTubeValidator);
            $this->definitions[] = $builder->build();
        }
    }

    /**
     * Returns an array of the default code definitions.
     */
    public function getCodeDefinitions()
    {
        return $this->definitions;
    }
}
