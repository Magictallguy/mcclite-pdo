<?php
namespace jBBCode\validators;

require_once dirname(__DIR__) . '/InputValidator.php';

class ImageValidator implements \JBBCode\InputValidator
{

    private $cache = [];

    /**
     * Returns true if $input is a valid image URL.
     *
     * @param $input the
     *            string to validate
     */
    public function validate($input)
    {
        global $func;
        if (in_array($input, $this->cache)) {
            return 'true' === $this->cache[$input] ? true : false;
        }
        try {
            $result = ! $func->isImage($input);
        } catch (Exception $e) {
            $this->cache[$input] = 'false';
            
            return false;
        }
        $this->cache[$input] = 'true';
        
        return true;
    }
}
