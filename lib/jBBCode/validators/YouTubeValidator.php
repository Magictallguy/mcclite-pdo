<?php
namespace jBBCode\validators;

require_once dirname(__DIR__) . DIRECTORY_SEPARATOR . 'InputValidator.php';

class YouTubeValidator implements \JBBCode\InputValidator
{

    public function validate($input)
    {
        global $func;
        $input = str_ireplace([
            '&autoplay=1?',
            'https://',
            'http://',
            'www.',
            'youtube.com/',
            'youtu.be/'
        ], '', $input);
        if (preg_match('/.*?(v=)?([^&]+).*?/im', $input)) {
            return true;
        }
        if (preg_match('/(v=)?([A-z0-9=\-]+?)(&.*)?$/i', $input)) {
            return true;
        }
        
        return false;
    }
}
