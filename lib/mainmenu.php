<?php
/*
MCCodes FREE
mainmenu.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if (!defined('SITE_ENABLE')) {
    exit;
}
global $db, $ir, $func;
$db->query('SELECT * FROM (
    (SELECT COUNT(evID) AS unreadEvents FROM events WHERE evUSER = ? AND evREAD = 0) AS ev,
    (SELECT COUNT(mail_id) AS unreadMail FROM mail WHERE mail_to = ? AND mail_read = 0) AS ma
)');
$db->execute([$ir['userid'], $ir['userid']]);
$data = $db->fetch(true);
$friendOnlineCnt = 0;
if ($ir['donatordays'] > 0) {
    $db->query('SELECT COUNT(u.userid)
        FROM friendslist AS f
        INNER JOIN users AS u ON f.fl_ADDED = u.userid
        WHERE f.fl_ADDER = ? AND u.laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)
    ');
    $db->execute([$ir['userid']]);
    $friendOnlineCnt = $db->result();
} ?>
<span class="page-subtitle">Menu</span><br>
<?php
if (!$ir['hospital']) { ?>
<a href="/index.php">Home</a><br>
<a href="/inventory.php">Items</a><br>
<a href="/explore.php">Explore</a><br>
<?php
} else {
    ?>
<a href="/hospital.php">Hospital</a><br>
<?php
} ?>
<a href="/events.php"<?php echo $data['unreadEvents'] > 0 ? ' class="bold new-notification"' : ''; ?>>Events [<?php echo $data['unreadEvents']; ?>]</a><br>
<a href="/mailbox.php"<?php echo $data['unreadMail'] > 0 ? ' class="bold new-notification"' : ''; ?>>Mail [<?php echo $data['unreadMail']; ?>]</a><br>
<?php
if (!$ir['hospital']) {
        ?>
<a href="/gym.php">Gym</a><br>
<a href="/criminal.php">Crimes</a><br>
<a href="/education.php">Local School</a><br>
<?php
    } ?>
<a href="/monopaper.php">Announcements</a><br>
<a href="/search.php">Search</a><br>
<a href="/advsearch.php">Advanced Search</a><br>
<?php
if ($ir['user_level'] > 1) { ?>
<hr>
<span class="page-subtitle">Staff Only</span><br>
<?php
    if ($ir['user_level'] < 6 && 4 != $ir['user_level']) {
        ?>
<a href="/new_staff.php">Staff Panel</a><br>
<?php
    }
    $db->query('SELECT userid, laston FROM users WHERE laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE) AND user_level > 1 ORDER BY user_level ASC, laston DESC, userid ASC');
    $db->execute();
    $staff = $db->fetch(); ?>
<hr>
<span class="page-subtitle">Staff Online:</span><br>
<?php
    if (null !== $staff) {
        $time = time();
        foreach ($staff as $row) {
            echo "\n",$func->username($row['userid']); ?> (<?php echo $func->time_format($time - strtotime($row['laston']), 'short', true, 1); ?>)<br>
            <?php
        }
    }
}
if ($ir['donatordays'] > 0) { ?>
<hr>
<span class="page-subtitle">Donators Only</span><br>
<a href="/friendslist.php">Friends List<?php echo $friendOnlineCnt > 0 ? ' ['.$friendOnlineCnt.']' : ''; ?></a><br>
<a href="/blacklist.php">Black List</a><?php
} ?>
<hr>
<span class="page-subtitle">Misc</span><br>
<a href="/preferences.php">Preferences</a><br>
<a href="/preport.php">Player Report</a><br>
<a href="/helptutorial.php">Help Tutorial</a><br>
<a href="/gamerules.php">Game Rules</a><br>
<a href="/viewuser.php?u=<?php echo $ir['userid']; ?>">My Profile</a><br>
<a href="/logout.php">Logout</a><br><br>
Time is now<br>
<?php echo date('F j, Y').'<br>'.date('g:i:s a');
