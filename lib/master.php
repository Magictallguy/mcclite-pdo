<?php

define('TIME_START', microtime(true));
ini_set('display_errors', 1);
error_reporting(E_ALL);
if (file_exists(dirname(__DIR__).'/vendor/autoload.php')) {
    require_once dirname(__DIR__).'/vendor/autoload.php';
    $whoops = new \Whoops\Run();
    $whoops->prependHandler(new \Whoops\Handler\PrettyPageHandler());
    $whoops->register();
}
if (!session_id()) {
    session_start();
}
define('SITE_ENABLE', true);
define('TIMEZONE_SITE', 'Africa/Lagos');
define('TIMEZONE_DB', '+01:00');
define('PATH_ROOT', dirname(__DIR__));
define('PATH_LIB', __DIR__);
define('PATH_CRON', PATH_ROOT.'/crons');
define('PATH_STAFF', PATH_ROOT.'/staff');
define('PATH_UPLOAD', PATH_ROOT.'/uploads');
define('PATH_UPLOAD_AVATAR', PATH_UPLOAD.'/avatars');
define('PATH_UPLOAD_ITEM', PATH_UPLOAD.'/items');
define('PATH_UPLOAD_SHOP', PATH_UPLOAD.'/shops');
define('DEFAULT_DATE_FORMAT', 'F j, Y, g:i:sa');

date_default_timezone_set(TIMEZONE_SITE);
$crits = [
    PATH_LIB.'/config.php',
    PATH_LIB.'/pdo.class.php',
    PATH_LIB.'/global_func.php',
    PATH_ROOT.'/redundancies/crons.php',
];
foreach ($crits as $path) {
    file_exists($path) ? require_once $path : trigger_error('Critical file missing: '.str_replace(PATH_ROOT.'/', '', $path), E_USER_ERROR);
}
if (defined('PAGINATION')) {
    file_exists(__DIR__.'/pagination.php') ? require_once __DIR__.'/pagination.php' : trigger_error('Critical file missing: pagination.php', E_USER_ERROR);
}
if (defined('BBCODE')) {
    file_exists(__DIR__.'/jBBCode/Parser.php') ? require_once __DIR__.'/jBBCode/Parser.php' : trigger_error('Critical file missing: jBBCode/Parser.php', E_USER_ERROR);
    $parser = new \jBBCode\Parser();
    $parser->addCodeDefinitionSet(new \jBBCode\DefaultCodeDefinitionSet());
}
$func = global_func::getInstance();
define('EMAIL_SUPPORT', 'support@'.$func->determine_game_urlbase());
define('EMAIL_FEDJAIL', 'fedjail@'.$func->determine_game_urlbase());
define('SETTINGS_FIND', [
    '{GAME_NAME}',
    '{GAME_OWNER}',
    '{GAME_DESCRIPTION}',
    '{PAYPAL}',
]);
define('SETTINGS_REPL', [
    'MCCLite: PDO',
    $func->username(1, ['display_donator_icon' => false, 'display_id' => false, 'override_cache' => true]),
    'Just a test rig',
    'magictallguy@hotmail.com',
]);
$_GET['ID'] = array_key_exists('ID', $_GET) && ctype_digit($_GET['ID']) && $_GET['ID'] > 0 ? $_GET['ID'] : null;
$_GET['action'] = array_key_exists('action', $_GET) ? $_GET['action'] : null;
if (!defined('NONAUTH')) {
    if (!array_key_exists('loggedin', $_SESSION) or !$_SESSION['loggedin']) {
        exit(header('Location: /login.php'));
    }
    $userid = array_key_exists('userid', $_SESSION) && is_numeric($_SESSION['userid']) && $_SESSION['userid'] > 0 ? $_SESSION['userid'] : null;
    if (!$userid) {
        exit(header('Location: /login.php'));
    }
    $db->query('SELECT u.*, us.*, h.hNAME
        FROM users AS u
        INNER JOIN userstats AS us ON u.userid = us.userid
        INNER JOIN houses AS h ON h.hWILL = u.maxwill
        WHERE u.userid = ?
    ');
    $db->execute([$userid]);
    $ir = $db->fetch(true);
    if (null === $ir) {
        session_unset();
        session_destroy();
        exit(header('Location: /login.php'));
    }
    unset($ir['userpass']);
    $func->check_level();
    if (!defined('NO_OUTPUT')) {
        file_exists(__DIR__.'/header.php') ? require_once __DIR__.'/header.php' : trigger_error('Critical file missing: header.php', E_USER_ERROR);
        $h = new headers();
        $fm = $func->money($ir['money']);
        $cm = $func->format($ir['crystals']);
        $lvDate = new \DateTime($ir['laston']);
        $lv = $lvDate->format(DEFAULT_DATE_FORMAT);
        $h->userdata($ir, $lv, $fm, $cm);
        if (!defined('NO_MENU')) {
            $h->menuarea();
        }
    }
    if (defined('STAFF_FILE')) {
        if (!in_array($ir['user_level'], [2, 3, 5])) {
            $_SESSION['error'] = 'You don\'t have access';
            exit(header('Location: /index.php'));
        }
    }
} else {
    if (!defined('NO_OUTPUT')) {
        file_exists(__DIR__.'/header-nonauth.php') ? require_once __DIR__.'/header-nonauth.php' : trigger_error('Critical file missing: header-nonauth.php', E_USER_ERROR);
        ob_start();
        $h = new headers_nonauth();
        $alerts = ['success', 'info', 'warning', 'error'];
        foreach ($alerts as $alert) {
            if (array_key_exists($alert, $_SESSION)) {
                echo '<div class="alert alert-'.$alert.'">'.$_SESSION[$alert].'</div>';
                unset($_SESSION[$alert]);
            }
        }
    }
}
