<?php
/**
 * PHP Pagination Class.
 *
 * @author admin@catchmyfame.com - http://www.catchmyfame.com
 *
 * @version 3.0.0
 * @date February 6, 2014
 *
 * @copyright (c) admin@catchmyfame.com (www.catchmyfame.com)
 * @license CC Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0) - http://creativecommons.org/licenses/by-sa/3.0/
 */
class Paginator
{
    public $current_page;
    public $items_per_page;
    public $limit_end;
    public $limit_start;
    public $num_pages;
    public $total_items;
    public $limit;
    protected $ipp_array;
    protected $mid_range;
    protected $querystring;
    protected $return;
    protected $get_ipp;

    public function __construct($total = 0, $mid_range = 7, $ipp_array = [24])
    {
        if (!is_numeric($total)) {
            $this->limit_start = 0;
            $this->limit_end = 0;
            $this->limit = '';

            return false;
        }
        $this->total_items = (int) $total;
        if ($this->total_items <= 0) {
            $this->limit_start = 0;
            $this->limit_end = 0;
            $this->limit = '';

            return false;
        }
        $this->mid_range = (int) $mid_range; // midrange must be an odd int >= 1
        if (0 == $this->mid_range % 2 or $this->mid_range < 1) {
            $this->limit_start = 0;
            $this->limit_end = 0;
            $this->limit = '';

            return false;
        }
        if (!is_array($ipp_array)) {
            $this->limit_start = 0;
            $this->limit_end = 0;
            $this->limit = '';

            return false;
        }
        $this->ipp_array = $ipp_array;
        $this->items_per_page = (isset($_GET['ipp'])) ? $_GET['ipp'] : $this->ipp_array[0];
        $this->default_ipp = $this->ipp_array[0];
        if (!is_numeric($this->items_per_page) or $this->items_per_page <= 0) {
            $this->items_per_page = $this->ipp_array[0];
        }
        $this->num_pages = ceil($this->total_items / $this->items_per_page);
        $this->current_page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1; // must be numeric > 0
        $this->limit = ' LIMIT '.(($this->current_page - 1) * $this->items_per_page).', '.$this->items_per_page;
        if ($_GET) {
            $args = explode('&', $_SERVER['QUERY_STRING']);
            foreach ($args as $arg) {
                $keyval = explode('=', $arg);
                if ('page' != $keyval[0] and 'ipp' != $keyval[0]) {
                    $this->querystring .= '&amp;'.$arg;
                }
            }
        }
        if ($_POST) {
            foreach ($_POST as $key => $val) {
                if ('page' != $key and 'ipp' != $key) {
                    $this->querystring .= '&amp;'.$key.'='.$val;
                }
            }
        }
        $this->return = '<ul class="pagination">';
        if ($this->num_pages > 10) {
            $this->return .= ($this->current_page > 1 and $this->total_items >= 10) ? '<li class="page-item"><a class="page-link" href="?page='.($this->current_page - 1).$this->querystring.'">Previous</a></li>' : '<li class="page-item disabled"><a href="#" class="page-link">Previous</a></li>';
            $this->start_range = $this->current_page - floor($this->mid_range / 2);
            $this->end_range = $this->current_page + floor($this->mid_range / 2);
            if ($this->start_range <= 0) {
                $this->end_range += abs($this->start_range) + 1;
                $this->start_range = 1;
            }
            if ($this->end_range > $this->num_pages) {
                $this->start_range -= $this->end_range - $this->num_pages;
                $this->end_range = $this->num_pages;
            }
            $this->range = range($this->start_range, $this->end_range);
            for ($i = 1; $i <= $this->num_pages; ++$i) {
                if ($this->range[0] > 2 and $i == $this->range[0]) {
                    $this->return .= '<li class="page-item disabled"><a href="#" class="page-link">&hellip;</a></li>';
                }
                // loop through all pages. if first, last, or in range, display
                if (1 == $i or $i == $this->num_pages or in_array($i, $this->range)) {
                    $this->return .= $i == $this->current_page ? '<li class="page-item active"><a title="Go to page '.$i.' of '.$this->num_pages.'" class="page-link" href="#">'.$i.'</a></li>'."\n" : '<li class="page-item"><a class="page-link" title="Go to page '.$i.' of '.$this->num_pages.'" href="?page='.$i.$this->querystring.'">'.$i.'</a></li>'."\n";
                }
                if ($this->range[$this->mid_range - 1] < $this->num_pages - 1 and $i == $this->range[$this->mid_range - 1]) {
                    $this->return .= '<li class="page-item disabled"><a href="#" class="page-link">&hellip;</a></li>';
                }
            }
            $this->return .= (($this->current_page < $this->num_pages and $this->total_items >= 10) and $this->current_page > 0) ? '<li class="page-item"><a class="page-link" href="?page='.($this->current_page + 1).$this->querystring.'">Next</a></li>'."\n" : '<li class="page-item disabled"><a class="page-link" href="#">Next</a></li>'."\n";
        } else {
            for ($i = 1; $i <= $this->num_pages; ++$i) {
                $this->return .= ($i == $this->current_page) ? '<li class="page-item active"><a class="page-link" href="#">'.$i.'</a></li>' : '<li class="page-item"><a class="page-link" href="?page='.$i.$this->querystring.'">'.$i.'</a></li>';
            }
        }
        $this->return .= '</ul>';
        $this->limit_start = ($this->current_page <= 0) ? 0 : ($this->current_page - 1) * $this->items_per_page;
        if ($this->current_page <= 0) {
            $this->items_per_page = 0;
        }
        $this->limit_end = (int) $this->items_per_page;
    }

    public function display_pages()
    {
        return $this->return;
    }
}
