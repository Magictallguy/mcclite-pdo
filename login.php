<?php
/*
MCCodes FREE
login.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

define('NONAUTH', true);
require_once __DIR__.'/lib/master.php';
$ip = $_SERVER['REMOTE_ADDR'];
$year = date('Y');
ob_start();
if (file_exists('ipbans/'.$ip)) {
    exit('<div style="font-weight:700;font-size:1.4em;">Your IP has been banned</div>');
} ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">{GAME_NAME}</h3>
        <p>
            {GAME_DESCRIPTION}
        </p>
    </div>
    <div class="col-3">
        <form action="/authenticate.php" method="post">
            <div class="form-group">
                <label for="username" class="form-label">Username</label>
                <input type="text" name="username" id="username" class="form-control bg-dark text-light" required autofocus>
            </div>
            <div class="form-group">
                <label for="password" class="form-label">Password</label>
                <input type="password" name="password" id="password" class="form-control bg-dark text-light" required>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-user-check"></span>
                    Login
                </button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col">
        <h4><a href="/register.php">REGISTER NOW!</a></h4>
    </div>
</div>
