<?php
/*
MCCodes FREE
logout.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('NO_OUTPUT', true);
require_once __DIR__.'/lib/master.php';
$_SESSION['attacking'] = array_key_exists('attacking', $_SESSION) && ctype_digit($_SESSION['attacking']) && $_SESSION['attacking'] > 0 ? $_SESSION['attacking'] : null;
if (null !== $_SESSION['attacking']) {
    $db->trans('start');
    $db->query('UPDATE users SET exp = 0 WHERE userid = ?');
    $db->execute([$ir['userid']]);
    $func->event_add($ir['userid'], 'You lost all of your EXP for running from the fight.');
    $db->trans('end');
    $_SESSION['attacking'] = 0;
}
session_unset();
session_destroy();
exit(header('Location: /login.php'));
