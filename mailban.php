<?php
/*
MCCodes FREE
jailuser.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
$_GET['userid'] = array_key_exists('userid', $_GET) && ctype_digit($_GET['userid']) && $_GET['userid'] > 0 ? $_GET['userid'] : null;
define('NO_OUTPUT', true);
require_once __DIR__.'/lib/master.php';
if (null !== $_GET['userid']) {
    $location = '/new_staff.php?action=mailform&ID='.$_GET['userid'];
} else {
    $_SESSION['error'] = 'You didn\'t select a valid player';
    $location = '/index.php';
}
exit(header('Location: '.$location));
