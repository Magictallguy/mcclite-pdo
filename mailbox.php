<?php
/*
MCCodes FREE
mailbox.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if ($ir['mailban'] > 0) {
    ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Mailban</h3>
        You're banned from the mail system for <?php echo $func->time_format($ir['mailban'] * 86400); ?><br>
        Reason: <?php echo $func->format($ir['mb_reason'], true); ?>
    </div>
</div><?php
} ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Mailbox</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td><a href="/mailbox.php?action=inbox">Inbox</a></td>
                        <td><a href="/mailbox.php?action=outbox">Sent</a></td>
                        <td><a href="/mailbox.php?action=compose">Compose</a></td>
                        <td><a href="/mailbox.php?action=delall">Delete All</a></td>
                        <td><a href="/mailbox.php?action=archive">Archive</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
switch ($_GET['action']) {
    case 'inbox':
        mail_inbox($db, $ir, $func);
        break;

    case 'outbox':
        mail_outbox($db, $ir, $func);
        break;

    case 'compose':
        mail_compose($db, $ir, $func);
        break;

    case 'delete':
        mail_delete($db, $ir, $func);
        break;

    case 'send':
        mail_send($db, $ir, $func);
        break;

    case 'delall':
        mail_delall($db, $ir, $func);
        break;

    case 'delall2':
        mail_delall2($db, $ir, $func);
        break;

    case 'archive':
        mail_archive($db, $ir, $func);
        break;

    default:
        mail_inbox($db, $ir, $func);
        break;
}

function mail_inbox($db, $ir, $func)
{
    $db->query('SELECT mail_id, mail_from, mail_subject, mail_text, mail_read, mail_time FROM mail WHERE mail_to = ? ORDER BY mail_time DESC LIMIT 25');
    $db->execute([$ir['userid']]);
    $rows = $db->fetch();
    $unread = []; ?>
<div class="row">
    <div class="col">
        Only the last 25 messages sent to you are visible.<br>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>From</th>
                        <th>Subject/Message</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="2" class="text-center">Your inbox is clear</td>
                    </tr><?php
    } else {
        $cache = [];
        foreach ($rows as $row) {
            if (!array_key_exists($row['mail_from'], $cache)) {
                $cache[$row['mail_from']] = $func->username($row['mail_from']);
            }
            $date = new \DateTime($row['mail_time']);
            if (!$row['mail_read']) {
                $unread[] = $row['mail_id'];
            } ?>
                    <tr>
                        <td><?php echo $cache[$row['mail_from']]; ?></td>
                        <td><?php echo(!$row['mail_read'] ? '<span class="fas fa-envelope" title="New"></span>  ' : '').$func->format($row['mail_subject']); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                        <td><?php echo $func->format($row['mail_text'], true); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a href="/mailbox.php?action=compose&amp;ID=<?php echo $row['mail_from']; ?>">Reply</a> &middot;
                            <a href="/mailbox.php?action=delete&amp;ID=<?php echo $row['mail_id']; ?>">Delete</a> &middot;
                            <a href="/preport.php?action=ID=<?php echo $row['mail_from']; ?>&amp;Fradulent mail: #<?php echo $row['mail_id']; ?>" class="text-danger">Report</a>
                        </td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
    if (count($unread) > 0) {
        $db->query('UPDATE mail SET mail_read = 1 WHERE mail_id IN ('.implode(',', $unread).')');
        $db->execute();
    }
}
function mail_outbox($db, $ir, $func)
{
    $db->query('SELECT mail_id, mail_to, mail_subject, mail_text, mail_read, mail_time FROM mail WHERE mail_from = ? ORDER BY mail_time DESC LIMIT 25');
    $db->execute([$ir['userid']]);
    $rows = $db->fetch(); ?>
<div class="row">
    <div class="col">
        Only the last 25 messages sent by you are visible.<br>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>To</th>
                        <th>Subject/Message</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="2" class="text-center">Your outbox is clear</td>
                    </tr><?php
    } else {
        $cache = [];
        foreach ($rows as $row) {
            if (!array_key_exists($row['mail_to'], $cache)) {
                $cache[$row['mail_to']] = $func->username($row['mail_to']);
            }
            $date = new \DateTime($row['mail_time']); ?>
                    <tr>
                        <td><?php echo $cache[$row['mail_to']]; ?></td>
                        <td><?php echo $func->format($row['mail_subject']); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                        <td><?php echo $func->format($row['mail_text'], true); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
}

function mail_compose($db, $ir, $func)
{
    $history = null;
    if (null !== $_GET['ID']) {
        if ($func->userExists($_GET['ID'])) {
            $db->query('SELECT mail_id, mail_from, mail_text, mail_time FROM mail WHERE (mail_from = ? AND mail_to = ?) OR (mail_from = ? AND mail_to = ?)');
            $db->execute([$_GET['ID'], $ir['userid'], $ir['userid'], $_GET['ID']]);
            $history = $db->fetch();
        }
    } ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Compose</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/mailbox.php?action=send" method="post" class="form">
            <div class="form-group">
                <label for="userid" class="form-label">ID</label>
                <input type="number" name="userid" id="userid" value="<?php echo $_GET['ID']; ?>" class="form-control bg-dark text-light" required>
            </div>
            <div class="form-group">
                <label for="subject" class="form-label">Subject</label>
                <input type="text" name="subject" id="subject" class="form-control bg-dark text-light">
            </div>
            <div class="form-group">
                <label for="message" class="form-label">Message</label>
                <textarea name="message" id="message" rows="5" class="form-control bg-dark text-light" required></textarea>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-envelope"></span>
                    Send
                </button>
            </div>
        </form>
    </div>
</div><?php
    if (null !== $history) {
        ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">History</h3>
        5-message limit<br>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Message</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
        $cache = [];
        foreach ($history as $row) {
            $date = new \DateTime($row['mail_time']);
            if (!array_key_exists($row['mail_from'], $cache)) {
                $cache[$row['mail_from']] = $func->username($row['mail_from']);
            } ?>
                    <tr>
                        <td><?php echo $cache[$row['mail_from']]; ?></td>
                        <td><?php echo $func->format($row['mail_text'], true); ?></td>
                        <td><?php echo $date->format('F j, Y g:i:sa'); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
}

function mail_send($db, $ir, $func)
{
    $_POST['userid'] = array_key_exists('userid', $_POST) && ctype_digit($_POST['userid']) && $_POST['userid'] > 0 ? $_POST['userid'] : null;
    $_POST['subject'] = array_key_exists('subject', $_POST) && is_string($_POST['subject']) && strlen($_POST['subject']) > 0 ? strip_tags(trim($_POST['subject'])) : '';
    $_POST['message'] = array_key_exists('message', $_POST) && is_string($_POST['message']) && strlen($_POST['message']) > 0 ? strip_tags(trim($_POST['message'])) : null;
    if (null !== $_POST['message']) {
        if (null !== $_POST['userid']) {
            $db->query('SELECT userid, user_level FROM users WHERE userid = ?');
            $db->execute([$_POST['userid']]);
            $recip = $db->fetch(true);
            if (null !== $recip) {
                if ($recip['user_level'] > 0) {
                    $target = $func->username($recip['userid']);
                    $db->query('INSERT INTO mail (mail_from, mail_to, mail_subject, mail_text) VALUES (?, ?, ?, ?)');
                    $db->execute([$ir['userid'], $recip['userid'], $_POST['subject'], $_POST['message']]);
                    $_SESSION['success'] = 'Your message to '.$target.' has been sent';
                } else {
                    $_SESSION['error'] = 'You can\'t message NPCs';
                }
            } else {
                $_SESSION['error'] = 'Your intended recipient doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t select a valid recipient';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid message';
    }
    exit(header('Location: /mailbox.php'));
}

function mail_delete($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT mail_id, mail_to FROM mail WHERE mail_id = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['mail_to'] === $ir['userid']) {
                $db->query('DELETE FROM mail WHERE mail_id = ?');
                $db->execute([$row['mail_id']]);
                $_SESSION['success'] = 'Message deleted';
            } else {
                $_SESSION['error'] = 'That isn\'t your message';
            }
        } else {
            $_SESSION['error'] = 'The message you\'ve selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid message';
    }
    exit(header('Location: /mailbox.php'));
}

function mail_delall($db, $ir, $func)
{
    ?>
    <div class="row">
        <div class="col">
            This will delete all the messages in your inbox.<br>
            There is <strong>NO</strong> undo, so be sure.<br>
            <a href="/mailbox.php?action=delall2">&gt; Yes, delete all messages</a><br>
            <a href="/mailbox.php">&gt; No, go back</a>
        </div>
    </div><?php
}

function mail_delall2($db, $ir, $func)
{
    $db->query('SELECT COUNT(mail_id) FROM mail WHERE mail_to = ? AND mail_read = 1');
    $db->execute([$ir['userid']]);
    $cnt = $db->result();
    if ($cnt > 0) {
        $db->query('DELETE FROM mail WHERE mail_to = ? AND mail_read = 1');
        $db->execute([$ir['userid']]);
        $_SESSION['success'] = $func->format($cnt).' message'.$func->s($cnt).' deleted';
    } else {
        $_SESSION['error'] = 'You don\'t have any unread messages to delete';
    }
    exit(header('Location: /mailbox.php'));
}

function mail_archive($db, $ir, $func)
{ ?>
    <div class="row">
        <div class="col">
            This tool will download an archive of all your messages.<br>
            <a href="/dlarchive.php?a=inbox">&gt; Download Inbox</a><br>
            <a href="/dlarchive.php?a=outbox">&gt; Download Outbox</a>
        </div>
    </div><?php
}
