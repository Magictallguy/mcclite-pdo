<?php
/*
MCCodes FREE
monopaper.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('BBCODE', true);
require_once __DIR__.'/lib/master.php';
$db->query('SELECT id, content FROM papercontent LIMIT 1');
$db->execute();
$content = $db->result(1);
$parser->parse($func->format($content, true)); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">The {GAME_NAME} Paper</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <a href="/gym.php">Local Gym</a>
    </div>
    <div class="col">
        <a href="/halloffame.php">Hall of Fame</a>
    </div>
</div>
<div class="row">
    <div class="col-2">
        Some banner.. maybe
    </div>
    <div class="col-10">
        <?php echo $parser->getAsHTML(); ?>
    </div>
</div>
