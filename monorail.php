<?php
/*
MCCodes FREE
monorail.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (null !== $_GET['ID']) {
    $db->query('SELECT cityid, cityname, cityminlevel FROM cities WHERE cityid = ?');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['cityid'] != $ir['location']) {
            if ($ir['level'] >= $row['cityminlevel']) {
                $cost = $row['cityminlevel'] * 1000;
                if (!$cost) {
                    $cost = 1000;
                }
                if ($ir['money'] >= $cost) {
                    $db->query('UPDATE users SET money = money - ?, location = ? WHERE userid = ?');
                    $db->execute([$cost, $row['cityid'], $ir['userid']]);
                    $_SESSION['success'] = 'You\'ve paid '.$func->money($cost).' and travelled to '.$func->format($row['cityname']);
                } else {
                    $_SESSION['error'] = 'You don\'t have enough cash. To travel to '.$func->format($row['cityname']).', you\'ll need '.$func->money($cost);
                }
            } else {
                $_SESSION['error'] = 'You\'re not experienced enough to travel to '.$func->format($row['cityname']);
            }
        } else {
            $_SESSION['info'] = 'You\'re already in '.$func->format($row['cityname']);
        }
    } else {
        $_SESSION['error'] = 'Your intended destination doesn\'t exist';
    }
    exit(header('Location: /monorail.php'));
}
$db->query('SELECT cityid, cityname, citydesc, cityminlevel FROM cities WHERE cityminlevel <= ? AND cityid <> ?');
$db->execute([$ir['level'], $ir['location']]);
$rows = $db->fetch(); ?>
<div class="row">
    <div class="col">
        Welcome to the Monorail Station. It costs <?php echo $func->money(1000); ?> x city's minimum level for a ticket.<br>
        Where would you like to travel today?<br>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Minimum Level</th>
                        <th>Go</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="4" class="text-center">There are no other destinations available to you</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            ?>
                    <tr>
                        <td><?php echo $func->format($row['cityname']); ?></td>
                        <td><?php echo $func->format($row['citydesc'], true); ?></td>
                        <td><?php echo $func->format($row['cityminlevel']); ?></td>
                        <td><a href="/monorail.php?ID=<?php echo $row['cityid']; ?>">Go</a></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
