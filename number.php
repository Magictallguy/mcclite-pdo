<?php
/*
MCCodes FREE
number.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$tresder = mt_rand(100, 999);
$maxbet = $ir['level'] * 1;
$_GET['tresde'] = array_key_exists('tresde', $_GET) && ctype_digit($_GET['tresde']) && $_GET['tresde'] > 0 ? $_GET['tresde'] : null;
$_SESSION['tresde'] = array_key_exists('tresde', $_SESSION) && ctype_digit($_SESSION['tresde']) && $_SESSION['tresde'] > 0 ? $_SESSION['tresde'] : null;
if (($_SESSION['tresde'] == $_GET['tresde']) || $_GET['tresde'] < 100) {
    $_SESSION['error'] = 'Error, you cannot refresh or go back on the slots, please use a side link to go somewhere else.';
    exit(header('Location: /explore.php'));
}
$_SESSION['tresde'] = $_GET['tresde'];
$_GET['bet'] = array_key_exists('bet', $_GET) && ctype_digit($_GET['bet']) && $_GET['bet'] > 0 ? $_GET['bet'] : null;
$_GET['number'] = array_key_exists('number', $_GET) && in_array($_GET['number'], [1, 2, 3]) ? $_GET['number'] : null;
$redirect = true;
$out = '';
if (null !== $_GET['bet']) {
    if ($ir['crystals'] >= $_GET['bet']) {
        if ($maxbet >= $_GET['bet']) {
            if (null !== $_GET['number']) {
                $slot = (int) mt_rand(1, 3);
                $out = 'You place <strong>'.$func->crystals($_GET['bet']).'</strong> into the slot and pull the pole.<br>
                You see the number: <strong>'.$slot.'</strong><br>';
                if ($slot == $_GET['number']) {
                    $won = $_GET['bet'] * 2;
                    $out .= '<span class="text-success">You win!</span><br>'.$func->crystals($won).' come flying out of the machine';
                } else {
                    $out .= '<span class="text-danger">You lost</span>';
                    $won = 0;
                }
                $db->query('UPDATE users SET crystals = crystals '.(!$won ? '-' : '+').' ? WHERE userid = ?');
                $db->execute([$_GET['bet'], $ir['userid']]);
                $tresder = mt_rand(100, 999);
                $redirect = false;
            } else {
                $_SESSION['error'] = 'Invalid number';
            }
        } else {
            $_SESSION['error'] = 'You can\'t bet that much';
        }
    } else {
        $_SESSION['error'] = 'You are trying to bet more than you have.';
    }
    if (true === $redirect) {
        exit(header('Location: /number.php?tresde='.$tresder));
    }
}
if ('' != $out) {
    ?>
<div class="row">
    <div class="col">
        <?php echo $out; ?>
    </div>
</div><?php
}?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Crystal Bet</h3>
        Ready to try your luck? Play today!<br />
        The maximum bet for your level is <?php echo $func->format($maxbet); ?>.<br />
        <form action="/number.php" method="get">
            <input type="hidden" name="tresde" value="<?php echo $tresder; ?>" />
            <div class="form-group">
                <label for="bet" class="form-label">Bet</label>
                <input type="number" name="bet" id="bet" value="5" class="form-control bg-dark text-light" required>
            </div>
            <div class="form-group">
                <label for="number" class="form-label">Pick (1-3)</label>
                <input type="number" name="number" id="number" class="form-control bg-dark text-light" value="<?php echo mt_rand(1, 3); ?>">
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-gem"></span>
                    Play
                </button>
            </div>
        </form>
    </div>
</div>
