<?php
/*
MCCodes FREE
oclog.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$redirect = true;
if (null !== $_GET['ID']) {
    $db->query('SELECT * FROM oclogs WHERE oclID = ?');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null !== $row) {
        if ($row['oclGANG'] == $ir['gang']) {
            $redirect = false;
            $date = new \DateTime($row['ocTIME']); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Organised Crime Log</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>Crime:</strong> <?php echo $func->format($row['ocCRIMEN']); ?><br>
        <strong>Time Executed:</strong> <?php echo $date->format('F j, Y g:i:sa'); ?><br>
        <strong>Result:</strong> <?php echo $func->format($row['oclLOG'], true); ?><br>
        <strong>Money Made:</strong> <?php echo $func->format($row['oclMONEY']); ?>
    </div>
</div><?php
        } else {
            $_SESSION['error'] = 'That log doesn\'t belong to your gang';
        }
    } else {
        $_SESSION['error'] = 'That log doesn\'t exist';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid log';
}
if (true === $redirect) {
    exit(header('Location: /events.php'));
}
