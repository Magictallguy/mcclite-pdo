<?php
/*
MCCodes FREE
preferences.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php'; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Preferences</h3>
    </div>
</div>
<div class="row">
    <div class="col-3">
        <a href="/preferences.php?action=namechange">Name Change</a>
    </div>
    <div class="col-3">
        <a href="/preferences.php?action=passchange">Password Change</a>
    </div>
    <div class="col-3">
        <a href="/preferences.php?action=picchange">Display Pic Change</a>
    </div>
    <div class="col-3">
        <a href="/preferences.php?action=sexchange">Sex Change</a>
    </div>
</div><?php
switch ($_GET['action']) {
    case 'sexchange2':
        do_sex_change($db, $ir, $func);
        break;

    case 'sexchange':
        conf_sex_change($db, $ir, $func);
        break;

    case 'passchange2':
        do_pass_change($db, $ir, $func);
        break;

    case 'passchange':
        pass_change($db, $ir, $func);
        break;

    case 'namechange2':
        do_name_change($db, $ir, $func);
        break;

    case 'namechange':
        name_change($db, $ir, $func);
        break;

    case 'picchange2':
        do_pic_change($db, $ir, $func);
        break;

    case 'picchange':
        pic_change($db, $ir, $func);
        break;
}

function conf_sex_change($db, $ir, $func)
{
    $g = 'Male' == $ir['gender'] ? 'Female' : 'Male'; ?>
<div class="row">
    <div class="col">
        Having the trans-gender costs 20 Crystals.<br>
        Are you sure you want to become a <?php echo $g; ?><br>
        <a href="/preferences.php?action=sexchange2">Yes</a> &middot; <a href="/preferences.php">No</a>
    </div>
</div><?php
}

function do_sex_change($db, $ir, $func)
{
    $cost = 20;
    if ($ir['crystals'] > 0) {
        $g = 'Male' == $ir['gender'] ? 'Female' : 'Male';
        $db->query('UPDATE users SET gender = ?, crystals = crystals - ? WHERE userid = ?');
        $db->execute([$g, $cost, $ir['userid']]);
        $_SESSION['success'] = 'You\'ve paid '.$func->crystals($cost).' to become '.$g;
    } else {
        $_SESSION['error'] = 'You don\'t have enough crystals!';
    }
    exit(header('Location: /preferences.php'));
}

function pass_change($db, $ir, $func)
{
    ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Password Change</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/preferences.php?action=passchange2" method="post" class="form">
            <div class="form-group">
                <label for="oldpw" class="form-label">Current Password</label>
                <input type="password" name="oldpw" id="oldpw" class="form-control bg-dark text-light">
            </div>
            <div class="form-group">
                <label for="newpw" class="form-label">New Password</label>
                <input type="password" name="newpw" id="newpw" class="form-control bg-dark text-light">
            </div>
            <div class="form-group">
                <label for="new2" class="form-label">Confirm Password</label>
                <input type="password" name="newpw2" id="newpw2" class="form-control bg-dark text-light" />
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-key"></span>
                    Change Password
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function do_pass_change($db, $ir, $func)
{
    $strs = ['oldpw', 'newpw', 'newpw2'];
    foreach ($strs as $str) {
        $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? $_POST[$str] : null;
    }
    if (null !== $_POST['oldpw']) {
        if (null !== $_POST['newpw']) {
            if (null !== $_POST['newpw2']) {
                $db->query('SELECT userid, userpass FROM users WHERE userid = ?');
                $db->execute([$ir['userid']]);
                $pass = $db->result(1);
                if (password_verify($_POST['oldpw'], $pass)) {
                    if ($_POST['newpw'] === $_POST['newpw2']) {
                        $db->query('UPDATE users SET userpass = ? WHERE userid = ?');
                        $db->execute([password_hash($_POST['newpw'], PASSWORD_BCRYPT), $ir['userid']]);
                        $_SESSION['success'] = 'Your password has been updated';
                    } else {
                        $_SESSION['error'] = 'The new passwords you entered didn\'t match - please note that passwords are case-sensitive';
                    }
                } else {
                    $_SESSION['error'] = 'Incorrect password';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t enter a confirmation password';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a new password';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter your current password';
    }
    exit(header('Location: /preferences.php'));
}

function name_change($db, $ir, $func)
{
    $cost = 3000; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Name Change</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        Changing your name costs <?php echo $func->money($cost); ?><br>
        Please note that you still use the same name to login, this procedure simply changes the name that is displayed.
        <form action="/preferences.php?action=namechange2" method="post" class="form">
            <div class="form-group">
                <label for="newname" class="form-label">New Name</label>
                <input type="text" name="newname" id="newname" class="form-control bg-dark text-light" required autofocus>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-info"></span>
                    Change Name
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function do_name_change($db, $ir, $func)
{
    $cost = 3000;
    $_POST['newname'] = array_key_exists('newname', $_POST) && is_string($_POST['newname']) && strlen($_POST['newname']) > 0 ? strip_tags(trim($_POST['newname'])) : null;
    if (null !== $_POST['newname']) {
        if (strtolower($_POST['newname']) != strtolower($ir['username'])) {
            if ($ir['money'] >= $cost) {
                $db->query('SELECT COUNT(userid) FROM users WHERE ? IN (LOWER(login_name), LOWER(username)) AND userid <> ?');
                $db->execute([strtolower($_POST['newname']), $ir['userid']]);
                if (!$db->result()) {
                    $db->query('UPDATE users SET username = ?, money = money - ? WHERE userid = ?');
                    $db->execute([$_POST['newname'], $cost, $ir['userid']]);
                    $_SESSION['success'] = 'You\'ve changed your name to '.$func->format($_POST['newname']);
                } else {
                    $_SESSION['error'] = 'That name is already in use';
                }
            } else {
                $_SESSION['error'] = 'You don\'t have enough money';
            }
        } else {
            $_SESSION['error'] = 'That\'s already your name';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a name';
    }
    exit(header('Location: /preferences.php'));
}

function pic_change($db, $ir, $func)
{
    ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Pic Change</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        All uploaded images will be automatically resized to fit where necessary. Preferred size: 150x150<br>
        <form action="/preferences.php?action=picchange2" method="post" class="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="newpic" class="form-label">Avatar</label>
                <input type="file" name="newpic" id="newpic" class="form-control bg-dark text-light" required>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-image"></span>
                    Upload
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function do_pic_change($db, $ir, $func)
{
    $path = $func->handleImageUpload(PATH_UPLOAD_AVATAR, 'newpic', $ir['userid']);
    if (isset($path['error'])) {
        $_SESSION['error'] = $path['error'];
    } else {
        $db->query('UPDATE users SET avatar = ? WHERE userid = ?');
        $db->execute([$path['url'], $ir['userid']]);
    }
    exit(header('Location: /preferences.php'));
}
