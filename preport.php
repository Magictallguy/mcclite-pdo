<?php
/*
MCCodes FREE
preport.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

require_once __DIR__.'/lib/master.php';
if (array_key_exists('submit', $_POST)) {
    $_POST['report'] = array_key_exists('report', $_POST) && is_string($_POST['report']) && strlen($_POST['report']) > 0 ? strip_tags(trim($_POST['report'])) : null;
    $_POST['player'] = array_key_exists('player', $_POST) && ctype_digit($_POST['player']) && $_POST['player'] > 0 ? $_POST['player'] : null;
    if (null !== $_POST['report']) {
        if (null !== $_POST['player']) {
            if ($func->userExists($_POST['player'])) {
                $db->query('SELECT COUNT(prID) FROM preports WHERE prREPORTER = ? AND prREPORTED = ? AND LOWER(prTEXT) = ?');
                $db->execute([$ir['userid'], $_POST['player'], strtolower($_POST['report'])]);
                if (!$db->result()) {
                    $db->trans('start');
                    $db->query('INSERT INTO preports (prREPORTER, prREPORTED, prTEXT) VALUES (?, ?, ?)');
                    $db->execute([$ir['userid'], $_POST['player'], $_POST['report']]);
                    $func->event_add(1, 'New player report from {user1} concerning {user2}', $ir['userid'].':'.$_POST['player']);
                    $db->trans('end');
                    $_SESSION['success'] = 'Your report concerning '.$func->username($_POST['player']).' has been sent';
                } else {
                    $_SESSION['error'] = 'You\'ve already reported '.$func->username($_POST['player']).' for that reason';
                }
            } else {
                $_SESSION['error'] = 'Your intended reportee doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid reportee';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a report';
    }
    exit(header('Location: /preport.php'));
}
$_GET['report'] = array_key_exists('report', $_GET) && is_string($_GET['report']) && strlen($_GET['report']) > 0 ? $_GET['report'] : null; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Player Report</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        Know of a player that's breaking the rules? Don't hesitate to report them. Reports are kept confidential.<br />
        <form action="/preport.php" method="post">
            <div class="form-group">
                <label for="player" class="form-label">Player ID</label>
                <input type="number" name="player" id="player" class="form-control bg-dark text-light"<?php echo null !== $_GET['ID'] ? ' value="'.$_GET['ID'].'"' : ''; ?> required autofocus>
            </div>
            <div class="form-group">
                <label for="report" class="form-label">Report</label>
                <textarea name="report" rows="5" class="form-control bg-dark text-light" required><?php echo null !== $_GET['report'] ? $func->format($_GET['report']) : ''; ?></textarea>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-danger">
                    <span class="fas fa-user-times"></span>
                    Report
                </button>
            </div>
        </form>
    </div>
</div>
