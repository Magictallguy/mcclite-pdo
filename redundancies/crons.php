<?php

if (!defined('SITE_ENABLE')) {
    exit;
}
$crons = [];
$db->query('SELECT id, cron, last_run FROM cron_times');
$db->execute();
$rows = $db->fetch();
if (null === $rows) {
    $db->query('INSERT INTO cron_times (cron) VALUES ("minute_one"), ("minute_five"), ("hour_one"), ("day_one")');
    $db->execute();

    return;
}
$durations = [
    'minute_one' => 60,
    'minute_five' => 300,
    'hour_one' => 3600,
    'day_one' => 86400,
];
foreach ($rows as $row) {
    $int = strtotime($row['last_run']);
    $crons[$row['cron']] = [
        'str' => $row['last_run'],
        'int' => $int,
        'add' => $durations[$row['cron']],
        'full' => $int + $durations[$row['cron']],
    ];
}
$time = time();
// minute: one
if ($time >= $crons['minute_one']['full']) {
    if (!defined('CRON_OVERRIDE')) {
        define('CRON_OVERRIDE', true);
    }
    require_once PATH_CRON.'/cron_minute.php';
    $sub = $crons['minute_one']['full'] - $time;
    $def = floor($sub / $crons['minute_one']['add']);
    $db->query('UPDATE cron_times SET last_run = NOW(), deficit = ? WHERE cron = "minute_one"');
    $db->execute([$def]);
}
// minute: five
if ($time >= $crons['minute_five']['full']) {
    if (!defined('CRON_OVERRIDE')) {
        define('CRON_OVERRIDE', true);
    }
    require_once PATH_CRON.'/cron_fivemins.php';
    $sub = $crons['minute_five']['full'] - $time;
    $def = floor($sub / $crons['minute_five']['add']);
    $db->query('UPDATE cron_times SET last_run = NOW(), deficit = ? WHERE cron = "minute_five"');
    $db->execute([$def]);
}
// hour: one
if ($time >= $crons['hour_one']['int'] + 3600) {
    if (!defined('CRON_OVERRIDE')) {
        define('CRON_OVERRIDE', true);
    }
    require_once PATH_CRON.'/cron_hour.php';
    $sub = $crons['hour_one']['full'] - $time;
    $def = floor($sub / $crons['hour_one']['add']);
    $db->query('UPDATE cron_times SET last_run = NOW(), deficit = ? WHERE cron = "hour_one"');
    $db->execute([$def]);
}
// day: one
if ($time >= $crons['day_one']['int'] + 86400) {
    if (!defined('CRON_OVERRIDE')) {
        define('CRON_OVERRIDE', true);
    }
    require_once PATH_CRON.'/cron_day.php';
    $sub = $crons['day_one']['full'] - $time;
    $def = floor($sub / $crons['day_one']['add']);
    $db->query('UPDATE cron_times SET last_run = NOW(), deficit = ? WHERE cron = "day_one"');
    $db->execute([$def]);
}
