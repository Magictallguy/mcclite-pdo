<?php
/*
MCCodes FREE
register.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

define('NONAUTH', true);
require_once __DIR__.'/lib/master.php';
$ip = $_SERVER['REMOTE_ADDR'];
ob_start();
if (file_exists('ipbans/'.$ip)) {
    exit('<span style="color:red;font-size:1.4em;font-weight:700">Your IP has been banned.</span>');
}
$error = null;
if (array_key_exists('username', $_POST)) {
    $_POST['promo'] = array_key_exists('promo', $_POST) && is_string($_POST['promo']) && strlen($_POST['promo']) > 0 ? $_POST['promo'] : null;
    $sm = 100;
    if ('Your Promo Code Here' == $_POST['promo']) {
        $sm += 100;
    }
    $_POST['username'] = array_key_exists('username', $_POST) && is_string($_POST['username']) && strlen($_POST['username']) > 0 ? $_POST['username'] : null;
    $_POST['password'] = array_key_exists('password', $_POST) && is_string($_POST['password']) && strlen($_POST['password']) > 0 ? $_POST['password'] : null;
    $_POST['cpassword'] = array_key_exists('cpassword', $_POST) && is_string($_POST['cpassword']) && strlen($_POST['cpassword']) > 0 ? $_POST['cpassword'] : null;
    $_POST['email'] = array_key_exists('email', $_POST) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? $_POST['email'] : null;
    if (null !== $_POST['username']) {
        if (null !== $_POST['password']) {
            if (null !== $_POST['cpassword']) {
                if ($_POST['password'] === $_POST['cpassword']) {
                    if (null !== $_POST['email']) {
                        $db->query('SELECT COUNT(userid) FROM users WHERE ? IN (LOWER(username), LOWER(login_name))');
                        $db->execute([strtolower($_POST['username'])]);
                        if (!$db->result()) {
                            $db->query('SELECT COUNT(userid) FROM users WHERE LOWER(email) = ?');
                            $db->execute([strtolower($_POST['email'])]);
                            if (!$db->result()) {
                                $_POST['ref'] = array_key_exists('ref', $_POST) && ctype_digit($_POST['ref']) && $_POST['ref'] > 0 ? $_POST['ref'] : null;
                                $proceed = true;
                                $ref = null;
                                if (null !== $_POST['ref']) {
                                    $db->query('SELECT userid, lastip FROM users WHERE userid = ?');
                                    $db->execute([$_POST['ref']]);
                                    $ref = $db->fetch(true);
                                    if (null === $ref or $ref['lastip'] == $ip) {
                                        $proceed = false;
                                    }
                                }
                                if (true === $proceed) {
                                    $db->trans('start');
                                    $db->query('INSERT INTO users (username, login_name, userpass, money, email, lastip, staffnotes) VALUES (?, ?, ?, ?, ?, ?, "")');
                                    $db->execute([$_POST['username'], $_POST['username'], password_hash($_POST['password'], PASSWORD_BCRYPT), $sm, $_POST['email'], $ip]);
                                    $id = $db->insert_id();
                                    $db->query('INSERT INTO userstats (userid) VALUES (?)');
                                    $db->execute([$id]);
                                    if (null !== $ref) {
                                        $db->query('UPDATE users SET crystals = crystals + 2 WHERE userid = ?');
                                        $db->execute([$ref['userid']]);
                                        $func->event_add($ref['userid'], 'For referring '.$_POST['username'].' to the game, you have earned 2 crystals!');
                                        $db->query('INSERT INTO referrals (refREFER, refREFED, refREFERIP, refREFEDIP) VALUES (?, ?, ?, ?)');
                                        $db->execute([$ref['userid'], $id, $ref['lastip'], $ip]);
                                    }
                                    $db->trans('end');
                                    $_SESSION['success'] = 'You\'ve signed up, enjoy the game!';
                                    exit(header('Location: /login.php'));
                                } else {
                                    $error = 'Either your referrer doesn\'t exist, or you\'re attempting to create a referral multi';
                                }
                            } else {
                                $error = 'That email address is already in use';
                            }
                        } else {
                            $error = 'That username is already in use';
                        }
                    } else {
                        $error = 'The passwords you entered didn\'t match. Please note that passwords are case-sensitive';
                    }
                } else {
                    $error = 'You didn\'t enter an email address';
                }
            } else {
                $error = 'You didn\'t enter a confirmation password';
            }
        } else {
            $error = 'You didn\'t enter a password';
        }
    } else {
        $error = 'You didn\'t enter a username';
    }
}
$_GET['REF'] = array_key_exists('REF', $_GET) && ctype_digit($_GET['REF']) && $_GET['REF'] > 0 ? $_GET['REF'] : null; ?>
<div class="row">
    <div class="col">
        <h3 class="page-title">{GAME_NAME} Registration</h3>
    </div>
</div><?php
if (null !== $error) {
    ?>
    <div class="alert alert-error">
        <?php echo $error; ?>
    </div><?php
} ?>
<div class="row">
    <div class="col">
        <form action="/register.php" method="post">
            <input type="hidden" name="ref" value="<?php echo $_GET['REF']; ?>">
            <div class="form-row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" name="username" id="username" class="form-control bg-dark text-light" required autofocus>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" name="email" id="email" class="form-control bg-dark text-light" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" id="password" class="form-control bg-dark text-light" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="cpassword" class="form-label">Confirm Password</label>
                        <input type="password" name="cpassword" id="cpassword" class="form-control bg-dark text-light" required>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="promo" class="form-label">Promo Code</label>
                        <input type="text" name="promo" id="promo" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-user-plus"></span>
                    Sign up
                </button> &middot;
                <a href="/login.php" class="btn btn-warning">
                    <span class="fas fa-arrow-left"></span>
                    Go back
                </a>
            </div>
        </form>
    </div>
</div>
