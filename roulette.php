<?php
/*
MCCodes FREE
roulette.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$tresder = mt_rand(100, 999);
$maxbet = $ir['level'] * 150;
$_GET['tresde'] = array_key_exists('tresde', $_GET) && ctype_digit($_GET['tresde']) && $_GET['tresde'] >= 100 && $_GET['tresde'] <= 999 ? $_GET['tresde'] : null;
$_SESSION['tresde'] = array_key_exists('tresde', $_SESSION) && ctype_digit($_SESSION['tresde']) && $_SESSION['tresde'] >= 100 && $_SESSION['tresde'] <= 999 ? $_SESSION['tresde'] : null;
if (($_SESSION['tresde'] == $_GET['tresde']) || $_GET['tresde'] < 100) {
    $_SESSION['error'] = 'Error, you cannot refresh or go back on the slots.';
    exit(header('Location: /roulette.php?tresde='.$tresder));
}
$_SESSION['tresde'] = $_GET['tresde'];
$_GET['bet'] = array_key_exists('bet', $_GET) && ctype_digit($_GET['bet']) && $_GET['bet'] > 0 ? $_GET['bet'] : null;
$_GET['number'] = array_key_exists('number', $_GET) && ctype_digit($_GET['number']) ? $_GET['number'] : null;
$out = null;
if (null !== $_GET['bet']) {
    if ($ir['money'] >= $_GET['bet']) {
        if ($maxbet >= $_GET['bet']) {
            if ($_GET['number'] >= 0 && $_GET['number'] <= 36) {
                $slot = mt_rand(0, 36);
                $out = 'You place <strong>'.$func->money($_GET['bet']).'</strong> into the slot and pull the pole<br>
                You see the number: <strong>'.$slot.'</strong><br>';
                if ($slot == $_GET['number']) {
                    $won = $_GET['bet'] * 37;
                    $gain = $_GET['bet'] * 36;
                    $out .= '<span class="text-success">You win!</span> You frantically catch the '.$func->money($won).' being fired out of the slot<br>';
                } else {
                    $won = 0;
                    $gain = $_GET['bet']; // well, $loss, really..
                    $out .= '<span class="text-danger">You lost</span> and you\'re down '.$func->money($gain).'<br>';
                }
                $db->query('UPDATE users SET money = money '.($won > 0 ? '+' : '-').' ? WHERE userid = ?');
                $db->execute([$gain, $ir['userid']]);
                $tresder = mt_rand(100, 999);
                $out .= '<br>
                <a href="/roulette.php?tresde='.$tresder.'&amp;number='.$_GET['number'].'&amp;bet='.$_GET['bet'].'">Again, same bet ('.$func->money($_GET['bet']).' on '.$_GET['number'].')</a>';
            } else {
                $_SESSION['error'] = 'The Numbers are only 0 - 36.';
                exit(header('Location: /roulette.php?tresde='.$tresder));
            }
        } else {
            $_SESSION['error'] = 'You have gone over the max bet.';
            exit(header('Location: /roulette.php?tresde='.$tresder));
        }
    } else {
        $_SESSION['error'] = 'You are trying to bet more than you have.';
        exit(header('Location: /roulette.php?tresde='.$tresder));
    }
} ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Roulette</h3>
    </div>
</div><?php
if (null !== $out) {
    ?>
<div class="row">
    <div class="col">
        <?php echo $out; ?>
    </div>
</div><?php
} ?>
<div class="row">
    <div class="col">
        &nbsp;
    </div>
</div>
<div class="row">
    <div class="col">
        Ready to try your luck? Play today!<br>
        The maximum bet for your level is <?php echo $func->money($maxbet); ?>.<br>
        <form action="/roulette.php" method="get" class="form">
            <input type="hidden" name="tresde" value="<?php echo $tresder; ?>">
            <div class="form-group">
                <label for="bet" class="form-label">Bet</label>
                <input type="text" name="bet" id="bet"<?php echo null !== $_GET['bet'] ? ' value="'.$_GET['bet'].'"' : ''; ?> class="form-control bg-dark text-light" required autofocus>
            </div>
            <div class="form-group">
                <label for="number" class="form-label">Pick (0-36)</label>
                <input type="number" name="number" id="number" value="<?php echo mt_rand(0, 36); ?>" class="form-control bg-dark text-light" required>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-dollar"></span>
                    Play
                </button>
            </div>
        </form>
    </div>
</div>
