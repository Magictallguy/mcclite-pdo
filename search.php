<?php
/*
MCCodes FREE
search.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$rows = null;
$_GET['name'] = array_key_exists('name', $_GET) && is_string($_GET['name']) && strlen($_GET['name']) > 0 ? strip_tags(trim($_GET['name'])) : null;
if (null !== $_GET['name']) {
    $db->query('SELECT u.userid, u.level, u.money, u.laston, u.avatar, c.cityname
        FROM users AS u
        INNER JOIN cities AS c ON u.location = c.cityid
        WHERE LOWER(username) LIKE ? OR LOWER(login_name) LIKE ?
    ');
    $db->execute(['%'.$_GET['name'].'%', '%'.$_GET['name'].'%']);
    $rows = $db->fetch();
} elseif (null !== $_GET['ID']) {
    if ($func->userExists($_GET['ID'])) {
        exit(header('Location: /viewuser.php?u='.$_GET['ID']));
    }
}?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Search</h3>
    </div>
</div><?php
if (null !== $rows) {
    ?>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Player</th>
                        <th>Info</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody><?php
    $time = time();
    foreach ($rows as $row) {
        $laston = strtotime($row['laston']); ?>
                    <tr>
                        <td class="text-center">
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" title="avatar" alt="avatar" class="avatar-userlist img-fluid <?php echo $laston >= $time - 900 ? 'online-box' : 'offline-box'; ?>">
                            </a><br>
                            <?php echo $func->username($row['userid']); ?>
                        </td>
                        <td>
                            <strong>Money:</strong> <?php echo $func->money($row['money']); ?><br>
                            <strong>Level:</strong> <?php echo $func->format($row['level']); ?><br>
                            <strong>Location:</strong> <?php echo $func->format($row['cityname']); ?>
                        </td>
                        <td>
                            [<a href="/mailbox.php?action=compose&amp;ID=<?php echo $row['userid']; ?>">Mail</a>] &middot;
                            [<a href="/attack.php?ID=<?php echo $row['userid']; ?>">Attack</a>]
                        </td>
                    </tr><?php
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
} elseif ((null !== $_GET['name'] or null !== $_GET['ID']) && null === $rows) {
        ?>
    <div class="alert alert-info">
        No results
    </div><?php
    } ?>
<div class="row">
    <div class="col">
        <div class="form-row">
            <div class="col-6">
                <form action="/search.php" method="get">
                    <div class="form-group">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" id="name" class="form-control bg-dark text-light" autofocus>
                    </div>
                    <div class="form-controls">
                        <button type="submit" class="btn btn-primary">
                            <span class="fas fa-search"></span>
                            Search by name
                        </button>
                    </div>
                </form>
            </div>
            <div class="col-6">
                <form action="/search.php" method="get">
                    <div class="form-group">
                        <label for="ID" class="form-label">ID</label>
                        <input type="number" name="ID" id="ID" class="form-control bg-dark text-light">
                    </div>
                    <div class="form-controls">
                        <button type="submit" class="btn btn-primary">
                            <span class="fas fa-search"></span>
                            Search by ID
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
