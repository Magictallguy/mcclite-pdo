<?php
/*
MCCodes FREE
sendcrystals.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$redirect = true;
$row = null;
if (null !== $_GET['ID']) {
    if ($_GET['ID'] != $ir['userid']) {
        $db->query('SELECT userid, username, avatar, user_level, lastip, laston FROM users WHERE userid = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['user_level'] > 0) {
                $names = [
                    $row['userid'] => [
                        'name' => $func->username($row['userid']),
                        'avatar' => $row['avatar'],
                        'indicator' => strtotime($row['laston']) >= time() - 900 ? 'online-box' : 'offline-box',
                    ],
                    $ir['userid'] => [
                        'name' => $func->username($ir['userid']),
                        'avatar' => $ir['avatar'],
                        'indicator' => strtotime($ir['laston']) >= time() - 900 ? 'online-box' : 'offline-box',
                    ],
                ];
                $_POST['crystals'] = array_key_exists('crystals', $_POST) && ctype_digit($_POST['crystals']) && $_POST['crystals'] > 0 ? $_POST['crystals'] : null;
                if (null !== $_POST['crystals']) {
                    if ($ir['crystals'] >= $_POST['crystals']) {
                        $db->trans('start');
                        $db->query('UPDATE users SET crystals = crystals - ? WHERE userid = ?');
                        $db->execute([$_POST['crystals'], $ir['userid']]);
                        $db->query('UPDATE users SET crystals = crystals + ? WHERE userid = ?');
                        $db->execute([$_POST['crystals'], $row['userid']]);
                        $func->event_add($row['userid'], 'You\'ve received '.$func->crystals($_POST['crystals']).' from {user}', $ir['userid']);
                        $db->query('INSERT INTO crystalxferlogs (cxFROM, cxTO, cxAMOUNT, cxFROMIP, cxTOIP, cxCONTENT) VALUES (?, ?, ?, ?, ?, ?)');
                        $db->execute([$ir['userid'], $row['userid'], $_POST['crystals'], $ir['lastip'], $row['lastip'], $func->format($ir['username']).' sent '.$func->crystals($_POST['crystals']).' to '.$func->format($row['username'])]);
                        $db->trans('end');
                        $_SESSION['success'] = 'You\'ve sent '.$func->crystals($_POST['crystals']).' to '.$names[$row['userid']]['name'];
                    } else {
                        $_SESSION['error'] = 'You don\'t have that much crystals';
                    }
                } else {
                    $db->query('SELECT cxID, cxFROM, cxTO, cxAMOUNT, cxTIME FROM crystalxferlogs WHERE (cxFROM = ? AND cxTO = ?) OR (cxFROM = ? AND cxTO = ?) ORDER BY cxTIME DESC LIMIT 5');
                    $db->execute([$row['userid'], $ir['userid'], $ir['userid'], $row['userid']]);
                    $history = $db->fetch();
                    $redirect = false; ?>
                    <div class="row">
                        <div class="col">
                            <h3 class="page-subtitle">Send Crystals</h3>
                            <p>
                                You're sending to <?php echo $names[$row['userid']]['name']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <form action="/sendcrystals.php?ID=<?php echo $row['userid']; ?>" method="post" class="form">
                                <div class="form-group">
                                    <label for="crystals" class="form-label">Crystals</label>
                                    <input type="number" name="crystals" id="crystals" class="form-control bg-dark text-light" required autofocus>
                                </div>
                                <div class="form-controls">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fas fa-dollar"></span>
                                        Send
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div><?php
                    if (null !== $history) {
                        ?>
                    <div class="row">
                        <div class="col">
                            &nbsp;
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <h3 class="page-subtitle">Latest Transfers</h3>
                            <p>
                                Between yourself and <?php echo $names[$row['userid']]['name']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sender</th>
                                            <th>Recipient</th>
                                            <th>Amount</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody><?php
                        foreach ($history as $hist) {
                            $date = new \DateTime($hist['cxTIME']); ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo $names[$hist['cxFROM']]['avatar']; ?>" data-toggle="lightbox">
                                                    <img src="<?php echo $names[$hist['cxFROM']]['avatar']; ?>" class="avatar-log-entry img-fluid <?php echo $names[$hist['cxFROM']]['indicator']; ?>">
                                                </a>
                                                <?php echo $names[$hist['cxFROM']]['name']; ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo $names[$hist['cxTO']]['avatar']; ?>" data-toggle="lightbox">
                                                    <img src="<?php echo $names[$hist['cxTO']]['avatar']; ?>" class="avatar-log-entry img-fluid <?php echo $names[$hist['cxTO']]['indicator']; ?>">
                                                </a>
                                                <?php echo $names[$hist['cxTO']]['name']; ?>
                                            </td>
                                            <td><?php echo $func->crystals($hist['cxAMOUNT']); ?></td>
                                            <td><?php echo $date->format('F j, Y g:i:sa'); ?></td>
                                        </tr><?php
                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><?php
                    }
                }
            } else {
                $_SESSION['error'] = 'You can\'t send crystals to an NPC';
            }
        } else {
            $_SESSION['error'] = 'Your intended recipient doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You can\'t send crystals to yourself';
    }
} else {
    $_SESSION['error'] = 'You didn\'t select a valid recipient';
}
if (true === $redirect) {
    exit(header('Location: '.(null !== $row ? '/viewuser.php?u='.$row['userid'] : '/index.php')));
}
