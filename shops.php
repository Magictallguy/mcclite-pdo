<?php
/*
MCCodes FREE
shops.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
if (null === $_GET['ID']) {
    $db->query('SELECT shopID, shopNAME, shopDESCRIPTION, shopIMAGE FROM shops WHERE shopLOCATION = ?');
    $db->execute([$ir['location']]);
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['warning'] = 'No shops have been set up. Please report this';
        exit(header('Location: /index.php'));
    }
    $cnt = 0; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops</h3>
        <p>
            You begin looking through town
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <tbody>
                    <tr><?php
    foreach ($rows as $row) {
        ++$cnt; ?>
                        <td class="text-center" style="width:25%">
                            <a href="<?php echo $row['shopIMAGE']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['shopIMAGE']; ?>" class="shop-front img-fluid">
                            </a><br>
                            <span class="italic"><?php echo $func->format($row['shopDESCRIPTION'], true); ?></span><br><br>
                            <a href="/shops.php?ID=<?php echo $row['shopID']; ?>">Browse</a>
                        </td><?php
        $div = $cnt % 4;
        if (!$div) {
            ?>
                    </tr>
                    <tr><?php
        }
    }
    if (1 == $div) {
        echo '<td></td><td></td><td></td>';
    } elseif (2 == $div) {
        echo '<td></td><td></td>';
    } elseif (3 == $div) {
        echo '<td></td>';
    } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
} else {
        $redirect = true;
        $db->query('SELECT shopID, shopNAME, shopDESCRIPTION, shopLOCATION FROM shops WHERE shopID = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            if ($row['shopLOCATION'] == $ir['location']) {
                $db->query('SELECT i.itmid, i.itmname, i.itmbuyprice, i.itmdesc, i.itmsellprice, i.itmimage, it.itmtypename
                FROM shopitems AS si
                INNER JOIN items AS i ON si.sitemITEMID = i.itmid
                INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
                WHERE si.sitemSHOP = ? AND i.itmbuyable = 1
                ORDER BY it.itmtypename ASC, i.itmbuyprice ASC, i.itmname ASC
            ');
                $db->execute([$row['shopID']]);
                $stocks = $db->fetch();
                if (null !== $stocks) {
                    $redirect = false;
                    $lt = ''; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle"><?php echo $func->format($row['shopNAME']); ?></h3>
        <p class="italic">
            <?php echo $func->format($row['shopDESCRIPTION'], true); ?>
        </p>
        <p>
            <a href="/shops.php">Back</a>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Cost</th>
                        <th>Sell</th>
                        <th>Purchase</th>
                    </tr>
                </thead>
                <tbody><?php
                foreach ($stocks as $stock) {
                    $canAfford = $ir['money'] > 0 && $stock['itmbuyprice'] > 0 ? floor($ir['money'] / $stock['itmbuyprice']) : 0;
                    if ($lt != $stock['itmtypename']) {
                        $lt = $stock['itmtypename']; ?>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="4"><?php echo $func->format($lt); ?></th>
                    </tr>
                </thead>
                <tbody><?php
                    } ?>
                    <tr>
                        <td>
                            <a href="<?php echo $stock['itmimage']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $stock['itmimage']; ?>" class="shop-listing img-fluid" alt="img">
                            </a>
                            <a href="/iteminfo.php?ID=<?php echo $stock['itmid']; ?>"><?php echo $func->format($stock['itmname']); ?></a><br>
                            <span class="italic"><?php echo $func->format($stock['itmdesc']); ?></span>
                        </td>
                        <td><?php echo $func->money($stock['itmbuyprice']); ?></td>
                        <td><?php echo $stock['itmsellprice'] > 0 ? $func->money($stock['itmsellprice']) : 'N/A'; ?></td>
                        <td>
                            <form action="/itembuy.php?ID=<?php echo $stock['itmid']; ?>" method="post" class="form form-inline">
                                <input type="hidden" name="shop" value="<?php echo $row['shopID']; ?>">
                                <div class="form-group">
                                    <input type="number" name="qty" id="qty" placeholder="Can afford <?php echo $func->format($canAfford); ?>" class="form-control bg-dark text-light" required>
                                </div>
                                <div class="form-controls">
                                    <button type="submit" class="btn btn-primary">
                                        <span class="fas fa-shopping-basket"></span>
                                        Purchase
                                    </button>
                                </div>
                            </form>
                        </td>
                    </tr><?php
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
                } else {
                    $_SESSION['info'] = $func->format($row['shopNAME']).'\'s completely out of stock';
                }
            } else {
                $db->query('SELECT cityid, cityname FROM cities WHERE cityid = ?');
                $db->execute([$row['shopLOCATION']]);
                $city = $db->result(1);
                $_SESSION['error'] = $func->format($row['shopNAME']).' is in '.$func->format($city).', you\'re not..';
            }
        } else {
            $_SESSION['error'] = 'The shop you selected doesn\'t exist';
        }
        if (true === $redirect) {
            exit(header('Location: /shops.php'));
        }
    }
