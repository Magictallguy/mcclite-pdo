<?php
/*
MCCodes FREE
slotsmachine.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$tresder = mt_rand(100, 999);
$maxbet = $ir['level'] * 250;
$_GET['tresde'] = array_key_exists('tresde', $_GET) && ctype_digit($_GET['tresde']) && $_GET['tresde'] >= 100 && $_GET['tresde'] <= 999 ? $_GET['tresde'] : null;
$_SESSION['tresde'] = array_key_exists('tresde', $_SESSION) && ctype_digit($_SESSION['tresde']) && $_SESSION['tresde'] >= 100 && $_SESSION['tresde'] <= 999 ? $_SESSION['tresde'] : null;
if (($_SESSION['tresde'] == $_GET['tresde']) || $_GET['tresde'] < 100) {
    $_SESSION['error'] = 'Error, you cannot refresh or go back on the slots, please use a side link to go somewhere else.';
    exit(header('Location: /slotsmachine.php?tresde='.$tresder));
}
$_SESSION['tresde'] = $_GET['tresde'];
$_GET['bet'] = array_key_exists('bet', $_GET) && ctype_digit($_GET['bet']) && $_GET['bet'] > 0 ? $_GET['bet'] : null;
$out = '';
if (null !== $_GET['bet']) {
    if ($_GET['bet'] > $ir['money']) {
        $_SESSION['error'] = 'You are trying to bet more than you have.';
        exit(header('Location: /slotsmachine.php?tresde='.$tresder));
    }
    if ($_GET['bet'] > $maxbet) {
        $_SESSION['error'] = 'You have gone over the max bet.';
        exit(header('Location: /slotsmachine.php?tresde='.$tresder));
    }
    $slot = [];
    $slot[1] = mt_rand(0, 9);
    $slot[2] = mt_rand(0, 9);
    $slot[3] = mt_rand(0, 9);
    $out = 'You place '.$func->money($_GET['bet']).' in the slot and pull the pole<br>
    You see the numbers: <strong>'.implode(' ', $slot).'</strong><br>';
    if ($slot[1] == $slot[2] && $slot[2] == $slot[3]) {
        $won = $_GET['bet'] * 26;
        $gain = $_GET['bet'] * 25;
        $out .= '<span class="text-success">You win with 3-of-a-kind!</span><br>You\'ve won '.$func->money($gain);
    } elseif ($slot[1] == $slot[2] || $slot[2] == $slot[3] || $slot[1] == $slot[3]) {
        $won = $_GET['bet'] * 3;
        $gain = $_GET['bet'] * 2;
        $out .= '<span class="text-success">You win with 2-of-a-kind!</span><br>You\'ve won '.$func->money($gain);
    } else {
        $won = 0;
        $gain = $_GET['bet'];
        $out .= '<span class="text-danger">You lost</span>';
    }
    $db->query('UPDATE users SET money = money '.($won > 0 ? '+' : '-').' ? WHERE userid = ?');
    $db->execute([$gain, $ir['userid']]);
    $tresder = mt_rand(100, 999);
    $out .= '<br><br>
    <a href="/slotsmachine.php?bet='.$_GET['bet'].'&amp;tresde='.$tresder.'">Again, same bet ('.$func->money($_GET['bet']).')</a>';
} ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Slots Machine</h3>
    </div>
</div><?php
if ('' != $out) {
    ?>
<div class="row">
    <div class="col">
        <?php echo $out; ?>
    </div>
</div><?php
} ?>
<div class="row">
    <div class="col">
        <p>
            Ready to try your luck? Play today!<br>
            The maximum bet for your level is <?php echo $func->money($maxbet); ?>.
        </p>
        <form action="/slotsmachine.php" method="get" class="form">
            <input type="hidden" name="tresde" value="<?php echo $tresder; ?>">
            <div class="form-group">
                <div class="form-group">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">&dollar;</span>
                        </div>
                        <input type="text" name="bet" id="bet" value="<?php echo $ir['money'] > $maxbet ? $maxbet : $ir['money']; ?>" class="form-control bg-dark text-light" aria-label="Amount" required autofocus>
                    </div>
                </div>
                <div class="form-controls">
                    <button type="submit" class="btn btn-primary">
                        <span class="fas fa-dollar"></span>
                        Play
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
