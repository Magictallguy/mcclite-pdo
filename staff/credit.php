<?php

if (!defined('STAFF_FILE')) {
    exit;
}
function credit_user_form($db, $ir, $func)
{
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Credit: Finances</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=creditsub" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select player</label>
                        <?php echo $func->user_dropdown('user1'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="italic">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="money" class="form-label">Money</label>
                        <input type="number" name="money" id="money" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="crystals" class="form-label">Crystals</label>
                        <input type="number" name="crystals" id="crystals" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-donate"></span>
                    Credit
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function credit_user_submit($db, $ir, $func)
{
    $toForm = true;
    $nums = ['money', 'crystals', 'user1', 'user2'];
    foreach ($nums as $num) {
        $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : null;
    }
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    if (null !== $_POST['user']) {
        $db->query('SELECT userid, user_level, fedjail FROM users WHERE userid = ?');
        $db->execute([$_POST['user']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $target = $func->username($row['userid']);
            if (!$row['fedjail']) {
                if ($row['user_level'] > 0) {
                    if (null !== $_POST['money'] or null !== $_POST['crystals']) {
                        $log = 'credited {user} with ';
                        $db->trans('start');
                        if (null !== $_POST['money'] && null !== $_POST['crystals']) {
                            $event = $func->money($_POST['money']).' and '.$func->crystals($_POST['crystals']);
                            $db->query('UPDATE users SET money = money + ?, crystals = crystals + ? WHERE userid = ?');
                            $db->execute([$_POST['money'], $_POST['crystals'], $row['userid']]);
                        } elseif (null !== $_POST['money'] && null === $_POST['crystals']) {
                            $event = $func->money($_POST['money']);
                            $db->query('UPDATE users SET money = money + ? WHERE userid = ?');
                            $db->execute([$_POST['money'], $row['userid']]);
                        } elseif (null === $_POST['money'] && null !== $_POST['crystals']) {
                            $event = $func->crystals($_POST['crystals']);
                            $db->query('UPDATE users SET crystals = crystals + ? WHERE userid = ?');
                            $db->execute([$_POST['crystals'], $row['userid']]);
                        }
                        $log .= $event;
                        $func->event_add($row['userid'], 'You\'ve been credited with '.$event.' by the Administration');
                        $func->stafflog(ucfirst($log), $row['userid']);
                        $db->trans('end');
                        $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                        $toForm = false;
                    } else {
                        $_SESSION['error'] = 'You didn\'t enter anything to credit';
                    }
                } else {
                    $_SESSION['error'] = 'You can\'t credit an NPC';
                }
            } else {
                $_SESSION['error'] = $target.' is in Federal Jail';
            }
        } else {
            $_SESSION['error'] = 'The player you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid player';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=creditform' : '')));
}
function give_item_form($db, $ir, $func)
{
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Credit: Item</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=giveitemsub" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select player</label>
                        <?php echo $func->user_dropdown('user1'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="italic">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="item" class="form-label">Item</label>
                        <?php echo $func->item_dropdown('item'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="qty" class="form-label">Quantity</label>
                        <input type="number" name="qty" id="qty" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-donate"></span>
                    Credit
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function give_item_submit($db, $ir, $func)
{
    $toForm = true;
    $nums = ['item', 'user1', 'user2'];
    foreach ($nums as $num) {
        $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : null;
    }
    $_POST['qty'] = array_key_exists('qty', $_POST) && ctype_digit($_POST['qty']) && $_POST['qty'] > 0 ? $_POST['qty'] : 1;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    if (null !== $_POST['user']) {
        $db->query('SELECT userid, user_level, fedjail FROM users WHERE userid = ?');
        $db->execute([$_POST['user']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $target = $func->username($row['userid']);
            if (!$row['fedjail']) {
                if ($row['user_level'] > 0) {
                    if (null !== $_POST['item']) {
                        $db->query('SELECT itmid, itmname FROM items WHERE itmid = ?');
                        $db->execute([$_POST['item']]);
                        $item = $db->fetch(true);
                        if (null !== $item) {
                            $log = 'credited {user} with '.$func->format($_POST['qty']).'x '.$func->format($item['itmname']);
                            $db->trans('start');
                            $func->giveItem($item['itmid'], $_POST['qty'], $row['userid']);
                            $func->event_add($row['userid'], 'You\'ve been credited '.$func->format($_POST['qty']).'x <a href="/iteminfo.php?ID='.$item['itmid'].'">'.$func->format($item['itmname']).'</a> by the Administration');
                            $func->stafflog(ucfirst($log), $row['userid']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                            $toForm = false;
                        } else {
                            $_SESSION['error'] = 'The item you selected doesn\'t exist';
                        }
                    } else {
                        $_SESSION['error'] = 'You didn\'t select a valid item';
                    }
                } else {
                    $_SESSION['error'] = 'You can\'t credit an NPC';
                }
            } else {
                $_SESSION['error'] = $target.' is in Federal Jail';
            }
        } else {
            $_SESSION['error'] = 'The player you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid player';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=giveitem' : '')));
}

function give_dp_form($db, $ir, $func)
{
    $packs = [
        1 => 'Pack 1 (Standard)',
        2 => 'Pack 2 (Crystals)',
        3 => 'Pack 3 (IQ)',
        4 => 'Pack 4 ($5.00)',
        5 => 'Pack 5 ($10.00)',
    ];
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Credit Donator Pack</h3>
        <p>
            The user will receive the benefits of one donator pack.
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=givedpsub" method="post" class="form">
            <div class="form-group">
                <label for="user" class="form-label">Select player</label>
                <?php echo $func->user_dropdown('user'); ?>
            </div>
            <div class="form-group"><?php
    foreach ($packs as $id => $disp) {
        ?>
                <div class="form-check">
                    <label for="type-<?php echo $id; ?>" class="form-check-label">
                        <input type="radio" name="type" id="type-<?php echo $id; ?>" value="<?php echo $id; ?>" class="form-check-input">
                        <?php echo $disp; ?>
                    </label>
                </div><?php
    } ?>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-donate"></span>
                    Credit Donator Pack
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function give_dp_submit($db, $ir, $func)
{
    $_POST['user'] = array_key_exists('user', $_POST) && ctype_digit($_POST['user']) && $_POST['user'] > 0 ? $_POST['user'] : null;
    if (null !== $_POST['user']) {
        $db->query('SELECT userid, fedjail FROM users WHERE userid = ?');
        $db->execute([$_POST['user']]);
        $user = $db->fetch(true);
        if (null !== $user) {
            $target = $func->username($user['userid']);
            $packs = $func->getDonatorPacks();
            $_POST['type'] = array_key_exists('type', $_POST) && in_array($_POST['type'], array_keys($packs)) ? $_POST['type'] : null;
            if (null !== $_POST['type']) {
                $pack = $packs[$_POST['type']];
                $log = 'credited {user} with 1x '.$pack['name'];
                $query = '';
                $params = [];
                $gains = [];
                if (isset($pack['money'])) {
                    $query .= 'u.money = u.money + ?, ';
                    $params[] = $pack['money'];
                    $gains[] = $func->money($pack['money']);
                }
                if (isset($pack['crystals'])) {
                    $query .= 'u.crystals = u.crystals + ?, ';
                    $params[] = $pack['crystals'];
                    $gains[] = $func->crystals($pack['crystals']);
                }
                if (isset($pack['donatordays'])) {
                    $query .= 'u.donatordays = u.donatordays + ?, ';
                    $params[] = $pack['donatordays'];
                    $gains[] = $func->time_format($pack['donatordays'] * 86400, 'long', false).' of donator status';
                }
                if (isset($pack['IQ'])) {
                    $query .= 'us.IQ = us.IQ + ?, ';
                    $params[] = $pack['IQ'];
                    $gains[] = $func->format($pack['IQ']).' IQ';
                }
                $params[] = $user['userid'];
                if (isset($pack['items'])) {
                    foreach ($pack['items'] as $key => $data) {
                        $db->query('SELECT itmid, itmname FROM items WHERE itmid = ?');
                        $db->execute([$data[0]]);
                        $item = $db->result(1);
                        $gains[] = $func->format($data[1]).'x '.$func->format($item);
                    }
                }
                $db->trans('start');
                $db->query('UPDATE users AS u
                    INNER JOIN userstats AS us ON u.userid = us.userid
                    SET '.substr($query, 0, -2).'
                    WHERE u.userid = ?
                ');
                $db->execute($params);
                if (isset($pack['items'])) {
                    foreach ($pack['items'] as $key => $data) {
                        $func->giveItem($data[0], $data[1], $user['userid']);
                    }
                }
                $func->event_add($user['userid'], 'You\'ve been credited with 1x '.$pack['name'].' by the Administration.<br>Benefits: '.$func->list_format($gains));
                $func->stafflog(ucfirst($log), $user['userid']);
                $db->trans('end');
                $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
            } else {
                $_SESSION['error'] = 'The pack you selected doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'The player you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid player';
    }
    exit(header('Location: /new_staff.php?action=givedpform'));
}
