<?php

if (!defined('STAFF_FILE')) {
    exit;
}
function donators_list($db, $ir, $func)
{
    $packs = [
        1 => 'Pack 1 (Standard)',
        2 => 'Pack 2 (Crystals)',
        3 => 'Pack 3 (IQ)',
        4 => 'Pack 4 ($5.00)',
        5 => 'Pack 5 ($10.00)',
    ];
    $db->query('SELECT COUNT(dp_id) FROM dps_process');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT dp_id, dp_userid, dp_type, dp_time FROM dps_process ORDER BY dp_time DESC'.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">List of Donations</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Player</th>
                        <th>Pack</th>
                        <th>Time</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="5" class="text-center">There are no pending donator packs</td>
                    </tr><?php
    } else {
        $cache = [];
        foreach ($rows as $row) {
            if (!array_key_exists($row['dp_userid'], $cache)) {
                $cache[$row['dp_userid']] = $func->username($row['dp_userid']);
            }
            $date = new \DateTime('@'.$row['dp_time']); ?>
                    <tr>
                        <td><?php echo $func->format($row['dp_id']); ?></td>
                        <td><?php echo $cache[$row['dp_userid']]; ?></td>
                        <td><?php echo $packs[$row['dp_type']]; ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                        <td>
                            <a href="/new_staff.php?action=acceptdp&amp;ID=<?php echo $row['dp_id']; ?>">Accept</a> &middot;
                            <a href="/new_staff.php?action=declinedp&amp;ID=<?php echo $row['dp_id']; ?>">Decline</a>
                        </td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
function accept_dp($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT dp_id, dp_userid, dp_type FROM dps_process WHERE dp_id = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $packs = $func->getDonatorPacks();
            $target = $func->username($row['dp_userid']);
            $pack = $packs[$row['dp_type']];
            $query = '';
            $params = [];
            $gains = [];
            if (isset($pack['money'])) {
                $query .= 'u.money = u.money + ?, ';
                $params[] = $pack['money'];
                $gains[] = $func->money($pack['money']);
            }
            if (isset($pack['crystals'])) {
                $query .= 'u.crystals = u.crystals + ?, ';
                $params[] = $pack['crystals'];
                $gains[] = $func->crystals($pack['crystals']);
            }
            if (isset($pack['donatordays'])) {
                $query .= 'u.donatordays = u.donatordays + ?, ';
                $params[] = $pack['donatordays'];
                $gains[] = $func->time_format($pack['donatordays'] * 86400, 'long', false).' of donator status';
            }
            if (isset($pack['IQ'])) {
                $query .= 'us.IQ = us.IQ + ?, ';
                $params[] = $pack['IQ'];
                $gains[] = $func->format($pack['IQ']).' IQ';
            }
            $params[] = $row['dp_userid'];
            if (isset($pack['items'])) {
                foreach ($pack['items'] as $key => $data) {
                    $db->query('SELECT itmid, itmname FROM items WHERE itmid = ?');
                    $db->execute([$data[0]]);
                    $item = $db->result(1);
                    $gains[] = $func->format($data[1]).'x <a href="/iteminfo.php?ID='.$data[0].'">'.$func->format($item).'</a>';
                }
            }
            $log = 'accepted the '.$pack['name'].' donation from {user}';
            $db->trans('start');
            $db->query('UPDATE users AS u
                INNER JOIN userstats AS us ON u.userid = us.userid
                SET '.substr($query, 0, -2).'
                WHERE u.userid = ?
            ');
            $db->execute($params);
            if (isset($pack['items'])) {
                foreach ($pack['items'] as $key => $data) {
                    $func->giveItem($data[0], $data[1], $row['dp_userid']);
                }
            }
            $func->event_add($row['dp_userid'], 'Your donation for '.$pack['name'].' has been accepted.<br>Benefits: '.$func->list_format($gains));
            $func->stafflog(ucfirst($log), $row['dp_userid']);
            $db->query('DELETE FROM dps_process WHERE dp_id = ?');
            $db->execute([$row['dp_id']]);
            $db->trans('end');
            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
        } else {
            $_SESSION['error'] = 'The donation entry you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid donation entry';
    }
    exit(header('Location: /new_staff.php?action=donator'));
}
function decline_dp($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT dp_id, dp_userid, dp_type FROM dps_process WHERE dp_id = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $packs = $func->getDonatorPacks();
            $pack = $packs[$row['dp_type']];
            $target = $func->username($row['dp_userid']);
            $log = 'declined the '.$pack['name'].' donation from {user}';
            $db->trans('start');
            $func->event_add($row['dp_userid'], 'Your donation for '.$pack['name'].' has been declined');
            $func->stafflog(ucfirst($log), $row['dp_userid']);
            $db->query('DELETE FROM dps_process WHERE dp_id = ?');
            $db->execute([$row['dp_id']]);
            $db->trans('end');
            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
        } else {
            $_SESSION['error'] = 'The donation entry you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid donation entry';
    }
    exit(header('Location: /new_staff.php?action=donator'));
}
