<?php
if (!defined('STAFF_FILE')) {
    exit;
}
// Stuff that all staff can do
$actions = [];
$actions['fedform'] = [
    'call' => 'fed_user_form',
    'file' => 'punishments',
];
$actions['fedsub'] = [
    'call' => 'fed_user_submit',
    'file' => 'punishments',
];
$actions['unfedform'] = [
    'call' => 'unfed_user_form',
    'file' => 'punishments',
];
$actions['unfedsub'] = [
    'call' => 'unfed_user_submit',
    'file' => 'punishments',
];
$actions['unmailform'] = [
    'call' => 'unmail_user_form',
    'file' => 'punishments',
];
$actions['unmailsub'] = [
    'call' => 'unmail_user_submit',
    'file' => 'punishments',
];
$actions['atklogs'] = [
    'call' => 'view_attack_logs',
    'file' => 'logs',
];
$actions['attackdetail'] = [
    'call' => 'view_attack_details',
    'file' => 'logs',
];
$actions['ipform'] = [
    'call' => 'ip_search_form',
    'file' => 'misc',
];
$actions['ipsub'] = [
    'call' => 'ip_search_submit',
    'file' => 'misc',
];
$actions['massjailip'] = [
    'call' => 'mass_jail',
    'file' => 'misc',
];
$actions['itmlogs'] = [
    'call' => 'view_itm_logs',
    'file' => 'logs',
];
$actions['cashlogs'] = [
    'call' => 'view_cash_logs',
    'file' => 'logs',
];
$actions['crystallogs'] = [
    'call' => 'view_crystal_logs',
    'file' => 'logs',
];
$actions['stafflogs'] = [
    'call' => 'view_staff_logs',
    'file' => 'logs',
];
// Stuff that a secretary or admin can do
if (in_array($ir['user_level'], [2, 3])) {
    $actions['giveitem'] = [
        'call' => 'give_item_form',
        'file' => 'credit',
    ];
    $actions['giveitemsub'] = [
        'call' => 'give_item_submit',
        'file' => 'credit',
    ];
    $actions['mailform'] = [
        'call' => 'mail_user_form',
        'file' => 'punishments',
    ];
    $actions['mailsub'] = [
        'call' => 'mail_user_submit',
        'file' => 'punishments',
    ];
    $actions['invbeg'] = [
        'call' => 'inv_user_begin',
        'file' => 'users',
    ];
    $actions['invuser'] = [
        'call' => 'inv_user_view',

        'file' => 'users',
    ];
    $actions['deleinv'] = [
        'call' => 'inv_delete',

        'file' => 'users',
    ];
    $actions['creditform'] = [
        'call' => 'credit_user_form',
        'file' => 'credit',
    ];
    $actions['creditsub'] = [
        'call' => 'credit_user_submit',
        'file' => 'credit',
    ];
    $actions['maillogs'] = [
        'call' => 'view_mail_logs',
        'file' => 'logs',
        'args' => ['parser'],
    ];
    $actions['reportsview'] = [
        'call' => 'reports_view',
        'file' => 'reports',
    ];
    $actions['repclear'] = [
        'call' => 'report_clear',
        'file' => 'reports',
    ];
}

// Stuff that only admins can do
if (2 == $ir['user_level']) {
    $actions['newuser'] = [
        'call' => 'new_user_form',
        'file' => 'users',
    ];
    $actions['newusersub'] = [
        'call' => 'new_user_submit',
        'file' => 'users',
    ];
    $actions['newitem'] = [
        'call' => 'new_item_form',
        'file' => 'items',
    ];
    $actions['newitemsub'] = [
        'call' => 'new_item_submit',
        'file' => 'items',
    ];
    $actions['killitem'] = [
        'call' => 'kill_item',
        'file' => 'items',
    ];
    $actions['edititem'] = [
        'call' => 'edit_item',
        'file' => 'items',
    ];
    $actions['newshop'] = [
        'call' => 'new_shop',
        'file' => 'shops',
    ];
    $actions['editshop'] = [
        'call' => 'edit_shop',
        'file' => 'shops',
    ];
    $actions['delshop'] = [
        'call' => 'delete_shop',
        'file' => 'shops',
    ];
    $actions['newstock'] = [
        'call' => 'new_stock',
        'file' => 'shops',
    ];
    $actions['edituser'] = [
        'call' => 'edit_user',
        'file' => 'users',
    ];
    $actions['maileform'] = [
        'call' => 'mail_edit_form',
        'file' => 'punishments',
    ];
    $actions['mailesub'] = [
        'call' => 'mail_edit_submit',
        'file' => 'punishments',
    ];
    $actions['fedeform'] = [
        'call' => 'fed_edit_form',
        'file' => 'punishments',
    ];
    $actions['fedesub'] = [
        'call' => 'fed_edit_submit',
        'file' => 'punishments',
    ];
    $actions['editnews'] = [
        'call' => 'newspaper',
        'file' => 'misc',
    ];
    $actions['editadnews'] = [
        'call' => 'adnewspaper',
        'file' => 'misc',
    ];
    $actions['donator'] = [
        'call' => 'donators_list',
        'file' => 'donators',
    ];
    $actions['acceptdp'] = [
        'call' => 'accept_dp',
        'file' => 'donators',
    ];
    $actions['declinedp'] = [
        'call' => 'decline_dp',
        'file' => 'donators',
    ];
    $actions['givedpform'] = [
        'call' => 'give_dp_form',
        'file' => 'credit',
    ];
    $actions['givedpsub'] = [
        'call' => 'give_dp_submit',
        'file' => 'credit',
    ];
    $actions['stafflist'] = [
        'call' => 'staff_list',
        'file' => 'staff',
    ];
    $actions['userlevel'] = [
        'call' => 'userlevel',
        'file' => 'staff',
    ];
    $actions['userlevelform'] = [
        'call' => 'userlevelform',
        'file' => 'staff',
    ];
    $actions['massmailer'] = [
        'call' => 'massmailer',
        'file' => 'misc',
    ];
    $actions['record'] = [
        'call' => 'admin_user_record',
        'file' => 'users',
    ];
    $actions['change_id'] = [
        'call' => 'admin_user_changeid',
        'file' => 'users',
    ];
}
$adminNews = PATH_STAFF.'/admin.news';
$news = file_exists($adminNews) ? nl2br(file_get_contents($adminNews)) : 'No news';

// Check their action
if (array_key_exists($_GET['action'], $actions)) {
    $act = $actions[$_GET['action']];
    $file = PATH_STAFF.'/'.$act['file'].'.php';
    if (file_exists($file)) {
        require_once $file;
    } else {
        $_SESSION['warning'] = 'File: <code>'.str_replace([PATH_STAFF.'/', '.php'], '', $file).'</code> is missing';
        exit(header('Location: /new_staff.php'));
    }
    if (function_exists($act['call'])) {
        if (array_key_exists('args', $act)) {
            if (in_array('parser', $act['args'])) {
                $act['call']($db, $ir, $func, $parser);
            }
        } else {
            $act['call']($db, $ir, $func);
        }
    } else {
        $_SESSION['warning'] = 'Function: <code>'.$act['call'].'()</code> is missing';
        exit(header('Location: /new_staff.php'));
    }
} else {
    switch ($ir['user_level']) {
        case 2:
            admin_index($db, $ir, $func, $news);
            break;
        case 3:
            sec_index($db, $ir, $func, $news);
            break;
        case 5:
            ass_index($db, $ir, $func, $news);
            break;
    }
}

function admin_index($db, $ir, $func, $news)
{
    $db->query('SELECT VERSION()');
    $db->execute();
    $mv = $db->result();
    $phpv = phpversion(); ?>
    <div class="row">
        <div class="col-2">
            Welcome to the {GAME_NAME} admin panel,<br>
            <?php echo $func->username($ir['userid'], ['display_id' => false, 'display_donator_icon' => false, 'override_cache' => true]); ?>!
        </div>
        <div class="col-6">
            <?php echo $news; ?>
        </div>
        <div class="col-4">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">System Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>PHP Version:</th>
                            <td><?php echo $phpv; ?></td>
                        </tr>
                        <tr>
                            <th>MySQL Version:</th>
                            <td><?php echo $mv; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div><?php
    adminMenu();
    displayStaffLogExcerpt($db, $func);
}

function sec_index($db, $ir, $func, $news)
{
    ?>
    <div class="row">
        <div class="col-4">
            Welcome to the {GAME_NAME} secretary panel,<br>
            <?php echo $func->username($ir['userid'], ['display_id' => false, 'display_donator_icon' => false, 'override_cache' => true]); ?>!
        </div>
        <div class="col-8">
            <?php echo $news; ?>
        </div>
    </div><?php
    secMenu();
    displayStaffLogExcerpt($db, $func);
}

function ass_index($db, $ir, $func, $news)
{
    ?>
    <div class="row">
        <div class="col-4">
            Welcome to the {GAME_NAME} assistant panel,<br>
            <?php echo $func->username($ir['userid'], ['display_id' => false, 'display_donator_icon' => false, 'override_cache' => true]); ?>!
        </div>
        <div class="col-8">
            <?php echo $news; ?>
        </div>
    </div><?php
    assMenu(); // mmm... ass menu
    displayStaffLogExcerpt($db, $func);
}
function displayStaffLogExcerpt($db, $func)
{
    $db->query('SELECT l.userid, l.content, l.extra, l.time_added, u.avatar, u.laston
        FROM logs_staff AS l
        INNER JOIN users AS u ON l.userid = u.userid
        ORDER BY l.time_added DESC
        LIMIT 10
    ');
    $db->execute();
    $rows = $db->fetch();
    if (null !== $rows) {
        $cnt = count($rows);
        $cache = []; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle"><?php echo $cnt; ?> Latest Staff Actions</h3>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Player</th>
                        <th>Action</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
        foreach ($rows as $row) {
            if (!array_key_exists($row['userid'], $cache)) {
                $cache[$row['userid']] = $func->username($row['userid']);
            }
            $content = $func->format($row['content']);
            if ('' != $row['extra']) {
                $content = $func->convertUserPlaceholder($content, $row['extra']);
            }
            $content = str_replace(['&amp;ldquo;', '&amp;rdquo;'], ['&ldquo;', '&rdquo;'], $content);
            $date = new \DateTime($row['time_added']);
            $laston = strtotime($row['laston']); ?>
                    <tr>
                        <td>
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" class="avatar-log-entry img-fluid">
                            </a>
                            <?php echo $cache[$row['userid']]; ?>
                        </td>
                        <td><?php echo $content; ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                    </tr><?php
        } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
    }
}
function adminMenu()
{
    ?>
    <div class="row">
        <div class="col">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="/new_staff.php">Staff</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownCredit" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Credit
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownCredit">
                                <a class="dropdown-item" href="/new_staff.php?action=givedpform">Donator Pack</a>
                                <a class="dropdown-item" href="/new_staff.php?action=creditform">Finances</a>
                                <a class="dropdown-item" href="/new_staff.php?action=giveitem">Item</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownDonator" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Donator
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownDonator">
                                <a class="dropdown-item" href="/new_staff.php?action=donator">Donator List</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownItems" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Items
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownItems">
                                <a class="dropdown-item" href="/new_staff.php?action=newitem">Add</a>
                                <a class="dropdown-item" href="/new_staff.php?action=edititem">Edit</a>
                                <a class="dropdown-item" href="/new_staff.php?action=killitem">Delete</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogs" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Logs
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownLogs">
                                <a class="dropdown-item" href="/new_staff.php?action=stafflogs">Staff</a>
                                <a class="dropdown-item" href="/new_staff.php?action=atklogs">Attacks</a>
                                <a class="dropdown-item" href="/new_staff.php?action=cashlogs">Cash Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=crystallogs">Crystal Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=itmlogs">Item Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=maillogs">Mail</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMisc" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Misc
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMisc">
                                <a class="dropdown-item" href="/new_staff.php?action=editadnews">Edit Admin News</a>
                                <a class="dropdown-item" href="/new_staff.php?action=editnews">Edit Newspaper</a>
                                <a class="dropdown-item" href="/new_staff.php?action=ipform">IP Search</a>
                                <a class="dropdown-item" href="/new_staff.php?action=massmailer">Mass Mailer</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPunishments" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Punishments
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownPunishments">
                                <a class="dropdown-item" href="/new_staff.php?action=mailform">Mail Ban</a>
                                <a class="dropdown-item" href="/new_staff.php?action=maileform">Edit Mail Ban</a>
                                <a class="dropdown-item" href="/new_staff.php?action=unmailform">Mail Unban</a>
                                <a class="dropdown-item" href="/new_staff.php?action=fedform">Fedjail</a>
                                <a class="dropdown-item" href="/new_staff.php?action=fedeform">Edit Fedjail Sentence</a>
                                <a class="dropdown-item" href="/new_staff.php?action=unfedform">Unfedjail</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownShops" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Shops
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownShops">
                                <a class="dropdown-item" href="/new_staff.php?action=newshop">Add Shop</a>
                                <a class="dropdown-item" href="/new_staff.php?action=editshop">Edit Shop</a>
                                <a class="dropdown-item" href="/new_staff.php?action=delshop">Delete Shop</a>
                                <a class="dropdown-item" href="/new_staff.php?action=newstock">Stock Shop</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownStaff" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Staff
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownStaff">
                                <a class="dropdown-item" href="/new_staff.php?action=stafflist">Staff List</a>
                                <a class="dropdown-item" href="/new_staff.php?action=userlevelform">Adjust User Level</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownUsers" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Users
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownUsers">
                                <a class="dropdown-item" href="/new_staff.php?action=newuser">Create New User</a>
                                <a class="dropdown-item" href="/new_staff.php?action=edituser">Edit User</a>
                                <a class="dropdown-item" href="/new_staff.php?action=invbeg">View User Inventory</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/new_staff.php?action=reportsview">Player Reports</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="/new_staff.php?action=record">User Record</a>
                                <a class="dropdown-item" href="/new_staff.php?action=change_id">Change User's ID</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div><?php
}
function secMenu()
{
    ?>
    <div class="row">
        <div class="col">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="/new_staff.php">Staff</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownCredit" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Credit
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownCredit">
                                <a class="dropdown-item" href="/new_staff.php?action=creditform">Finances</a>
                                <a class="dropdown-item" href="/new_staff.php?action=giveitem">Item</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogs" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Logs
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownLogs">
                                <a class="dropdown-item" href="/new_staff.php?action=stafflogs">Staff</a>
                                <a class="dropdown-item" href="/new_staff.php?action=atklogs">Attacks</a>
                                <a class="dropdown-item" href="/new_staff.php?action=cashlogs">Cash Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=crystallogs">Crystal Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=itmlogs">Item Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=maillogs">Mail</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMisc" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Misc
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMisc">
                                <a class="dropdown-item" href="/new_staff.php?action=ipform">IP Search</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPunishments" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Punishments
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownPunishments">
                                <a class="dropdown-item" href="/new_staff.php?action=mailform">Mail Ban</a>
                                <a class="dropdown-item" href="/new_staff.php?action=unmailform">Mail Unban</a>
                                <a class="dropdown-item" href="/new_staff.php?action=fedform">Fedjail</a>
                                <a class="dropdown-item" href="/new_staff.php?action=unfedform">Unfedjail</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownUsers" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Users
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownUsers">
                                <a class="dropdown-item" href="/new_staff.php?action=invbeg">View User Inventory</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div><?php
}
function assMenu()
{
    ?>
    <div class="row">
        <div class="col">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="/new_staff.php">Staff</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownLogs" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Logs
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownLogs">
                                <a class="dropdown-item" href="/new_staff.php?action=stafflogs">Staff</a>
                                <a class="dropdown-item" href="/new_staff.php?action=atklogs">Attacks</a>
                                <a class="dropdown-item" href="/new_staff.php?action=cashlogs">Cash Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=crystallogs">Crystal Transfers</a>
                                <a class="dropdown-item" href="/new_staff.php?action=itmlogs">Item Transfers</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMisc" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Misc
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownMisc">
                                <a class="dropdown-item" href="/new_staff.php?action=ipform">IP Search</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPunishments" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Punishments
                            </a>
                            <div class="dropdown-menu bg-dark" aria-labelledby="navbarDropdownPunishments">
                                <a class="dropdown-item" href="/new_staff.php?action=fedform">Fedjail</a>
                                <a class="dropdown-item" href="/new_staff.php?action=unfedform">Unfedjail</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div><?php
}
function staffMenu()
{
    global $ir;
    if (2 == $ir['user_level']) {
        adminMenu();
    } elseif (3 == $ir['user_level']) {
        secMenu();
    } elseif (5 == $ir['user_level']) {
        assMenu();
    } ?>
    <div class="row">
        <div class="col">
            &nbsp;
        </div>
    </div><?php
}
