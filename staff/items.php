<?php

if (!defined('STAFF_FILE')) {
    exit;
}
function new_item_form($db, $ir, $func)
{
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Items: Add</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=newitemsub" method="post" class="form" enctype="multipart/form-data">
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="itmname" class="form-label">Name</label>
                        <input type="text" name="itmname" id="itmname" class="form-control bg-dark text-light" required autofocus>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <label for="itmdesc" class="form-label">Description</label>
                        <textarea name="itmdesc" id="itmdesc" rows="5" class="form-control bg-dark text-light" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="itmtype" class="form-label">Type</label>
                        <?php echo $func->itemtype_dropdown('itmtype'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="itmimage" class="form-label">Image</label>
                        <input type="file" name="itmimage" id="itmimage" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        Buyable<br>
                        <label for="itmbuyable" class="switch switch-sm">
                            <input type="checkbox" name="itmbuyable" id="itmbuyable" class="form-check-input bg-dark text-light" checked>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="itmbuyprice" class="form-label">Price</label>
                        <input type="text" name="itmbuyprice" id="itmbuyprice" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="itmsellprice" class="form-label">Sell Value</label>
                        <input type="text" name="itmsellprice" id="itmsellprice" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <legend>Specialized</legend>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="energy" class="form-label">Energy Regen (food only)</label>
                        <input type="text" name="energy" id="energy" value="1" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="health" class="form-label">Health Regen (medical only)</label>
                        <input type="text" name="health" id="health" value="10" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="damage" class="form-label">Power (weapons only)</label>
                        <input type="text" name="damage" id="damage" value="10" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="Defence" class="form-label">Damage Off (armor only)</label>
                        <input type="text" name="Defence" id="Defence" value="10" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-plus"></span>
                    Add Item
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function new_item_submit($db, $ir, $func)
{
    $toForm = true;
    $strs = ['itmname', 'itmdesc'];
    $nums = ['itmbuyprice', 'itmsellprice', 'energy', 'health'];
    $decs = ['damage', 'Defence'];
    foreach ($strs as $str) {
        $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? strip_tags(trim($_POST[$str])) : null;
    }
    foreach ($nums as $num) {
        $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : 0;
    }
    foreach ($decs as $dec) {
        $_POST[$dec] = array_key_exists($dec, $_POST) && is_numeric($_POST[$dec]) && $_POST[$dec] > 0 ? $_POST[$dec] : 0;
    }
    $_POST['itmbuyable'] = array_key_exists('itmbuyable', $_POST) && isset($_POST['itmbuyable']) ? 1 : 0;
    $_POST['itmtype'] = array_key_exists('itmtype', $_POST) && ctype_digit($_POST['itmtype']) && $_POST['itmtype'] > 0 ? $_POST['itmtype'] : null;
    if (null !== $_POST['itmname']) {
        if (null !== $_POST['itmdesc']) {
            if ((1 == $_POST['itmbuyable'] && $_POST['itmbuyprice'] > 0) or !$_POST['itmbuyable']) {
                if (null !== $_POST['itmtype']) {
                    $db->query('SELECT COUNT(itmid) FROM items WHERE LOWER(itmname) = ?');
                    $db->execute([strtolower($_POST['itmname'])]);
                    if (!$db->result()) {
                        $db->query('SELECT itmtypeid, itmtypename FROM itemtypes WHERE itmtypeid = ?');
                        $db->execute([$_POST['itmtype']]);
                        $type = $db->result(1);
                        if (null !== $type) {
                            $log = 'added a new '.$func->format($type).' item: '.$func->format($_POST['itmname']);
                            $db->trans('start');
                            $db->query('INSERT INTO items (itmtype, itmname, itmdesc, itmbuyprice, itmsellprice, itmbuyable) VALUES (?, ?, ?, ?, ?, ?)');
                            $db->execute([$_POST['itmtype'], $_POST['itmname'], $_POST['itmdesc'], $_POST['itmbuyprice'], $_POST['itmsellprice'], $_POST['itmbuyable']]);
                            $id = $db->insert_id();
                            if (1 == $_POST['itmtype']) {
                                $db->query('INSERT INTO food (item_id, energy) VALUES (?, ?)');
                                $db->execute([$id, $_POST['energy']]);
                            } elseif (in_array($_POST['itmtype'], [3, 4])) {
                                $db->query('INSERT INTO weapons (item_ID, damage) VALUES (?, ?)');
                                $db->execute([$id, $_POST['damage']]);
                            } elseif (5 == $_POST['itmtype']) {
                                $db->query('INSERT INTO medical (item_id, health) VALUES (?, ?)');
                                $db->execute([$id, $_POST['health']]);
                            } elseif (7 == $_POST['itmtype']) {
                                $db->query('INSERT INTO armor (item_id, Defence) VALUES (?, ?)');
                                $db->execute([$id, $_POST['Defence']]);
                            }
                            $func->stafflog(ucfirst($log));
                            if (!empty($_FILES) && count($_FILES) > 0) {
                                $image = new \Bulletproof\Image($_FILES);
                                $image->setMime(['jpeg', 'gif', 'png']);
                                $image->setSize(1024, 1048576);
                                $image->setDimension(150, 150);
                                $image->setLocation(PATH_UPLOAD_ITEM);
                                $image->setName($id.'_'.date('d-m-y--H-i-s'));
                                if ($image['itmimage']) {
                                    if ($image->upload()) {
                                        $url = $func->determine_game_urlbase();
                                        $path = 'https://'.$url.'/'.str_replace(__DIR__, '', $image->getFullPath());
                                        $db->query('UPDATE items SET itmimage = ? WHERE itmid = ?');
                                        $db->execute([$path, $id]);
                                    } else {
                                        $_SESSION['warning'] = 'Couldn\'t upload image: '.$image->getError();
                                    }
                                } else {
                                    $_SESSION['warning'] = 'Couldn\'t upload image: '.$image->getError();
                                }
                            }
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.$log;
                            $toForm = false;
                        } else {
                            $_SESSION['error'] = 'The item type you selected doesn\'t exist';
                        }
                    } else {
                        $_SESSION['error'] = 'Another item with that name already exists';
                    }
                } else {
                    $_SESSION['error'] = 'You didn\'t select a valid item type';
                }
            } else {
                $_SESSION['error'] = 'You set this item to be buyable but didn\'t enter a price';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid description';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid name';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=newitem' : '')));
}
function edit_item($db, $ir, $func)
{
    $_GET['step'] = array_key_exists('step', $_GET) ? $_GET['step'] : null;
    $_POST['item'] = array_key_exists('item', $_POST) && ctype_digit($_POST['item']) && $_POST['item'] > 0 ? $_POST['item'] : null;
    switch ($_GET['step']) {
        case 2:
            $toForm = true;
            if (null !== $_POST['item']) {
                $db->query('SELECT itmid, itmname FROM items WHERE itmid = ?');
                $db->execute([$_POST['item']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $strs = ['itmname', 'itmdesc'];
                    $nums = ['itmbuyprice', 'itmsellprice', 'energy', 'health'];
                    $decs = ['damage', 'Defence'];
                    foreach ($strs as $str) {
                        $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? strip_tags(trim($_POST[$str])) : null;
                    }
                    foreach ($nums as $num) {
                        $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : 0;
                    }
                    foreach ($decs as $dec) {
                        $_POST[$dec] = array_key_exists($dec, $_POST) && is_numeric($_POST[$dec]) && $_POST[$dec] > 0 ? $_POST[$dec] : 0;
                    }
                    $_POST['itmbuyable'] = array_key_exists('itmbuyable', $_POST) && isset($_POST['itmbuyable']) ? 1 : 0;
                    $_POST['itmtype'] = array_key_exists('itmtype', $_POST) && ctype_digit($_POST['itmtype']) && $_POST['itmtype'] > 0 ? $_POST['itmtype'] : null;
                    if (null !== $_POST['itmname']) {
                        if (null !== $_POST['itmdesc']) {
                            if ((1 == $_POST['itmbuyable'] && $_POST['itmbuyprice'] > 0) or !$_POST['itmbuyable']) {
                                if (null !== $_POST['itmtype']) {
                                    $db->query('SELECT COUNT(itmid) FROM items WHERE LOWER(itmname) = ? AND itmid <> ?');
                                    $db->execute([strtolower($_POST['itmname']), $row['itmid']]);
                                    if (!$db->result()) {
                                        $db->query('SELECT itmtypeid, itmtypename FROM itemtypes WHERE itmtypeid = ?');
                                        $db->execute([$_POST['itmtype']]);
                                        $type = $db->result(1);
                                        if (null !== $type) {
                                            $log = 'edited '.$func->aAn($type, true).' item: '.$func->format($_POST['itmname']);
                                            $data = [
                                                'armour' => 'item_id',
                                                'food' => 'item_id',
                                                'medical' => 'item_id',
                                                'weapons' => 'item_ID',
                                            ];
                                            $db->trans('start');
                                            $db->query('UPDATE items SET itmtype = ?, itmname = ?, itmdesc = ?, itmbuyprice = ?, itmsellprice = ?, itmbuyable = ? WHERE itmid = ?');
                                            $db->execute([$_POST['itmtype'], $_POST['itmname'], $_POST['itmdesc'], $_POST['itmbuyprice'], $_POST['itmsellprice'], $_POST['itmbuyable'], $row['itmid']]);
                                            foreach ($data as $table => $col) {
                                                $db->query('DELETE FROM '.$table.' WHERE '.$col.' = ?');
                                                $db->execute([$row['itmid']]);
                                            }
                                            if (1 == $_POST['itmtype']) {
                                                $db->query('INSERT INTO food (item_id, energy) VALUES (?, ?)');
                                                $db->execute([$row['itmid'], $_POST['energy']]);
                                            } elseif (in_array($_POST['itmtype'], [3, 4])) {
                                                $db->query('INSERT INTO weapons (item_ID, damage) VALUES (?, ?)');
                                                $db->execute([$row['itmid'], $_POST['damage']]);
                                            } elseif (5 == $_POST['itmtype']) {
                                                $db->query('INSERT INTO medical (item_id, health) VALUES (?, ?)');
                                                $db->execute([$row['itmid'], $_POST['health']]);
                                            } elseif (7 == $_POST['itmtype']) {
                                                $db->query('INSERT INTO armour (item_id, Defence) VALUES (?, ?)');
                                                $db->execute([$row['itmid'], $_POST['Defence']]);
                                            }
                                            $func->stafflog(ucfirst($log));
                                            if (!empty($_FILES) && count($_FILES)) {
                                                try {
                                                    $image = new \Bulletproof\Image($_FILES);
                                                    $image->setMime(['jpeg', 'gif', 'png']);
                                                    $image->setSize(1024, 1048576);
                                                    $image->setDimension(300, 300);
                                                    $image->setLocation(PATH_UPLOAD_ITEM);
                                                    $image->setName($row['itmid'].'_'.date('d-m-y--H-i-s'));
                                                    if ($image['itmimage']) {
                                                        if ($image->upload()) {
                                                            $url = $func->determine_game_urlbase();
                                                            $path = 'https://'.$url.'/'.str_replace(PATH_ROOT, '', $image->getFullPath());
                                                            $db->query('UPDATE items SET itmimage = ? WHERE itmid = ?');
                                                            $db->execute([$path, $row['itmid']]);
                                                        } else {
                                                            $_SESSION['warning'] = 'Couldn\'t upload image: '.$image->getError();
                                                        }
                                                    } else {
                                                        $_SESSION['warning'] = 'Couldn\'t upload image: '.$image->getError();
                                                    }
                                                } catch (\Exception $e) {
                                                    $_SESSION['warning'] = 'Couldn\'t upload image: '.$e->getMessage();
                                                }
                                            }
                                            $db->trans('end');
                                            $_SESSION['success'] = 'You\'ve '.$log;
                                            $toForm = false;
                                        } else {
                                            $_SESSION['error'] = 'The item type you selected doesn\'t exist';
                                        }
                                    } else {
                                        $_SESSION['error'] = 'Another item with that name already exists';
                                    }
                                } else {
                                    $_SESSION['error'] = 'You didn\'t select a valid item type';
                                }
                            } else {
                                $_SESSION['error'] = 'You set this item to be buyable but didn\'t enter a price';
                            }
                        } else {
                            $_SESSION['error'] = 'You didn\'t enter a valid description';
                        }
                    } else {
                        $_SESSION['error'] = 'You didn\'t enter a valid name';
                    }
                } else {
                    $_SESSION['error'] = 'The item you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid item';
            }
            header('Location: /new_staff.php'.(true === $toForm ? '?action=edititem' : ''));
            break;
        case 1:
            if (null === $_POST['item']) {
                $_SESSION['error'] = 'You didn\'t select a valid item';
                exit(header('Location: /new_staff.php?action=edititem'));
            }
            $db->query('SELECT i.*, f.energy, m.health, a.Defence, w.damage
                FROM items AS i
                LEFT JOIN food AS f ON i.itmid = f.item_id
                LEFT JOIN medical AS m ON i.itmid = m.item_id
                LEFT JOIN armour AS a ON i.itmid = a.item_id
                LEFT JOIN weapons AS w ON i.itmid = w.item_ID
                WHERE i.itmid = ?
            ');
            $db->execute([$_POST['item']]);
            $row = $db->fetch(true);
            if (null === $row) {
                $_SESSION['error'] = 'The item you selected doesn\'t exists';
                exit(header('Location: /new_staff.php?action=edititem'));
            }
            staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Items: Edit <?php echo $func->format($row['itmname']); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=edititem&amp;step=2" method="post" class="form" enctype="multipart/form-data">
            <input type="hidden" name="item" value="<?php echo $row['itmid']; ?>">
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="itmname" class="form-label">Name</label>
                        <input type="text" name="itmname" id="itmname" value="<?php echo $row['itmname']; ?>" class="form-control bg-dark text-light" required autofocus>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <label for="itmdesc" class="form-label">Description</label>
                        <textarea name="itmdesc" id="itmdesc" rows="5" class="form-control bg-dark text-light" required><?php echo $func->format($row['itmdesc']); ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="itmtype" class="form-label">Type</label>
                        <?php echo $func->itemtype_dropdown('itmtype', $row['itmtype']); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="itmimage" class="form-label">Image</label>
                        <input type="file" name="itmimage" id="itmimage" class="form-control bg-dark text-light" aria-describedby="currentImagePath">
                        <small id="currentImagePath" class="form-text text-muted">Current: <?php echo $func->format($row['itmimage']); ?></small>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        Buyable<br>
                        <label for="itmbuyable" class="switch switch-sm">
                            <input type="checkbox" name="itmbuyable" id="itmbuyable" class="form-check-input bg-dark text-light"<?php echo 1 == $row['itmbuyable'] ? ' checked' : ''; ?>>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="itmbuyprice" class="form-label">Price</label>
                        <input type="text" name="itmbuyprice" id="itmbuyprice" value="<?php echo $row['itmbuyprice']; ?>" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="itmsellprice" class="form-label">Sell Value</label>
                        <input type="text" name="itmsellprice" id="itmsellprice" value="<?php echo $row['itmsellprice']; ?>" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <legend>Specialized</legend>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="energy" class="form-label">Energy Regen (food only)</label>
                        <input type="number" name="energy" id="energy" value="<?php echo $row['energy']; ?>" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="health" class="form-label">Health Regen (medical only)</label>
                        <input type="number" name="health" id="health" value="<?php echo $row['health']; ?>" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="damage" class="form-label">Power (weapons only)</label>
                        <input type="number" step="0.0001" name="damage" id="damage" value="<?php echo $row['damage']; ?>" class="form-control bg-dark text-light">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="Defence" class="form-label">Damage Off (armor only)</label>
                        <input type="number" step="0.0001" name="Defence" id="Defence" value="<?php echo $row['Defence']; ?>" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-check"></span>
                    Edit Item
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
        default:
            staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Items: Edit</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=edititem&amp;step=1" method="post" class="form">
            <div class="form-group">
                <label for="item" class="form-label">Select item</label>
                <?php echo $func->item_dropdown('item'); ?>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-edit"></span>
                    Begin Edits
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
    }
}
function kill_item($db, $ir, $func)
{
    $_GET['step'] = array_key_exists('step', $_GET) ? $_GET['step'] : null;
    $_POST['item'] = array_key_exists('item', $_POST) && ctype_digit($_POST['item']) && $_POST['item'] > 0 ? $_POST['item'] : null;
    switch ($_GET['step']) {
        case 2:
            $toForm = true;
            if (null !== $_POST['item']) {
                $db->query('SELECT i.itmid, i.itmname, i.itmimage, it.itmtypename
                    FROM items AS i
                    INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
                    WHERE i.itmid = ?
                ');
                $db->execute([$_POST['item']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $log = 'deleted '.$func->format($row['itmtypename']).' item: '.$func->format($row['itmname']);
                    $data = [
                        'armour' => 'item_ID',
                        'food' => 'item_id',
                        'imarketaddlogs' => 'imaITEM',
                        'imbuylogs' => 'imbITEM',
                        'imremovelogs' => 'imrITEM',
                        'inventory' => 'inv_itemid',
                        'itembuylogs' => 'ibITEM',
                        'itemmarket' => 'imITEM',
                        'items' => 'itmid',
                        'itemselllogs' => 'isITEM',
                        'itemxferlogs' => 'ixITEM',
                        'medical' => 'item_id',
                        'shopitems' => 'sitemITEMID',
                        'weapons' => 'item_id',
                    ];
                    $db->trans('start');
                    foreach ($data as $table => $col) {
                        $db->query('DELETE FROM '.$table.' WHERE '.$col.' = ?');
                        $db->execute([$row['itmid']]);
                    }
                    $func->stafflog(ucfirst($log));
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve '.$log;
                    $toForm = false;
                } else {
                    $_SESSION['error'] = 'The item you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid item';
            }
            header('Location: /new_staff.php'.(true === $toForm ? '?action=killitem' : ''));
            break;
        case 1:
            $redirect = true;
            if (null !== $_POST['item']) {
                $db->query('SELECT i.itmid, i.itmname, i.itmimage, it.itmtypename
                    FROM items AS i
                    INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
                    WHERE i.itmid = ?
                ');
                $db->execute([$_POST['item']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $redirect = false;
                    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Items: Delete</h3>
        <p>
            Item selected: <a href="<?php echo $row['itmimage']; ?>" data-toggle="lightbox">
                <img src="<?php echo $row['itmimage']; ?>" class="item-confirmation img-fluid">
            </a> <?php echo $func->format($row['itmname']); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=killitem&amp;step=2" method="post" class="form">
            <input type="hidden" name="item" value="<?php echo $row['itmid']; ?>">
            <div class="form-group">
                Confirm deletion<br>
                <label for="confirm" class="switch">
                    <input type="checkbox" name="confirm" id="confirm" class="form-check-input" required>
                    <span class="slider slider-danger round"></span>
                </label>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-trash"></span>
                    Delete Item
                </button>
            </div>
        </form>
    </div>
</div><?php
                } else {
                    $_SESSION['error'] = 'The item you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid item';
            }
            if (true === $redirect) {
                header('Location: /new_staff.php?action=killitem');
            }
            break;
        default:
            staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Items: Delete</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=killitem&amp;step=1" method="post" class="form">
            <div class="form-group">
                <label for="item" class="form-label">Select item</label>
                <?php echo $func->item_dropdown('item'); ?>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-trash"></span>
                    Delete Item
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
    }
}
