<?php

if (!defined('STAFF_FILE')) {
    exit;
}
function view_staff_logs($db, $ir, $func)
{
    $db->query('SELECT COUNT(id) FROM logs_staff');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT l.userid, l.content, l.extra, l.time_added, u.avatar, u.laston
        FROM logs_staff AS l
        INNER JOIN users AS u ON l.userid = u.userid
        ORDER BY l.time_added DESC
    '.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    $func->stafflog('Viewed the Staff Logs (Page: '.$pages->current_page.')');
    $cache = [];
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Staff</h3>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Player</th>
                        <th>Action</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
        if (null === $rows) {
            ?>
                    <tr>
                        <td colspan="4" class="text-center">There are no logs available</td>
                    </tr><?php
        } else {
            foreach ($rows as $row) {
                if (!array_key_exists($row['userid'], $cache)) {
                    $cache[$row['userid']] = $func->username($row['userid']);
                }
                $content = $func->format($row['content']);
                if ('' != $row['extra']) {
                    $content = $func->convertUserPlaceholder($content, $row['extra']);
                }
                $content = str_replace(['&amp;ldquo;', '&amp;rdquo;'], ['&ldquo;', '&rdquo;'], $content);
                $date = new \DateTime($row['time_added']);
                $laston = strtotime($row['laston']); ?>
                    <tr>
                        <td>
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" class="avatar-log-entry img-fluid">
                            </a>
                            <?php echo $cache[$row['userid']]; ?>
                        </td>
                        <td><?php echo $content; ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                    </tr><?php
            }
        } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
function view_attack_logs($db, $ir, $func)
{
    $cache = [];
    $db->query('SELECT COUNT(log_id) FROM attacklogs');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT log_id, attacker, attacked, result, time_added, stole FROM attacklogs ORDER BY time_added DESC'.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    $func->stafflog('Viewed the Attack Logs (Page: '.$pages->current_page.')');
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Attacks</h3>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Attacker</th>
                        <th>Defender</th>
                        <th>Winner</th>
                        <th>Result</th>
                        <th>Time</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="7" class="text-center">There are no logs available</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            if (!array_key_exists($row['attacker'], $cache)) {
                $cache[$row['attacker']] = $func->username($row['attacker']);
            }
            if (!array_key_exists($row['attacked'], $cache)) {
                $cache[$row['attacked']] = $func->username($row['attacked']);
            }
            $date = new \DateTime($row['time_added']);
            $winID = 'won' == $row['result'] ? $row['attacker'] : $row['defender']; ?>
                    <tr>
                        <td><?php echo $func->format($row['log_id']); ?></td>
                        <td><?php echo $cache[$row['attacker']]; ?></td>
                        <td><?php echo $cache[$row['attacked']]; ?></td>
                        <td><?php echo $cache[$winID]; ?></td>
                        <td>Stole: <?php echo $func->money($row['stole']); ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                        <td>
                            [<a href="/new_staff.php?action=attackdetail&amp;ID=<?php echo $row['log_id']; ?>">View Full Log</a>]
                        </td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
function view_attack_details($db, $ir, $func)
{
    if (null === $_GET['ID']) {
        $_SESSION['error'] = 'You didn\'t select a valid log entry';
        exit(header('Location: /new_staff.php?action=attacklogs'));
    }
    $db->query('SELECT log_id, attacker, attacked, time_added, attacklog FROM attacklogs WHERE log_id = ?');
    $db->execute([$_GET['ID']]);
    $row = $db->fetch(true);
    if (null === $row) {
        $_SESSION['error'] = 'The log entry you selected doesn\'t exist';
        exit(header('Location: /new_staff.php?action=attacklogs'));
    }
    $date = new \DateTime($row['time_added']);
    $cache = [];
    $cache[$row['attacker']] = $func->username($row['attacker']);
    $cache[$row['attacked']] = $func->username($row['attacked']);
    $func->stafflog('Viewed the details of attack ID #'.$func->format($row['log_id']));
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Attack: Details</h3>
        <p>
            Viewing the details of the attack by <?php echo $cache[$row['attacker']]; ?> to <?php echo $cache[$row['attacked']]; ?> on <?php echo $date->format(DEFAULT_DATE_FORMAT); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <?php echo str_replace(['{attacker}', '{defender}'], [$cache[$row['attacker']], $cache[$row['attacked']]], $row['attacklog']); ?>
    </div>
</div><?php
}

function view_cash_logs($db, $ir, $func)
{
    $cache = [];
    $db->query('SELECT COUNT(cxID) FROM cashxferlogs');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT cxID, cxFROM, cxTO, cxAMOUNT, cxTIME FROM cashxferlogs ORDER BY cxTIME DESC'.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    $func->stafflog('Viewed the Cash Transfer Logs (Page: '.$pages->current_page.')');
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Transfers: Cash</h3>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Amount</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="4" class="text-center">There are no logs available</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            if (!array_key_exists($row['cxFROM'], $cache)) {
                $cache[$row['cxFROM']] = $func->username($row['cxFROM']);
            }
            if (!array_key_exists($row['cxTO'], $cache)) {
                $cache[$row['cxTO']] = $func->username($row['cxTO']);
            }
            $date = new \DateTime($row['cxTIME']); ?>
                    <tr>
                        <td><?php echo $cache[$row['cxFROM']]; ?></td>
                        <td><?php echo $cache[$row['cxTO']]; ?></td>
                        <td><?php echo $func->money($row['cxAMOUNT']); ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
function view_crystal_logs($db, $ir, $func)
{
    $cache = [];
    $db->query('SELECT COUNT(cxID) FROM crystalxferlogs');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT cxID, cxFROM, cxTO, cxAMOUNT, cxTIME FROM crystalxferlogs ORDER BY cxTIME DESC'.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    $func->stafflog('Viewed the Crystal Transfer Logs (Page: '.$pages->current_page.')');
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Transfers: Crystals</h3>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Amount</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="4" class="text-center">There are no logs available</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            if (!array_key_exists($row['cxFROM'], $cache)) {
                $cache[$row['cxFROM']] = $func->username($row['cxFROM']);
            }
            if (!array_key_exists($row['cxTO'], $cache)) {
                $cache[$row['cxTO']] = $func->username($row['cxTO']);
            }
            $date = new \DateTime($row['cxTIME']); ?>
                    <tr>
                        <td><?php echo $cache[$row['cxFROM']]; ?></td>
                        <td><?php echo $cache[$row['cxTO']]; ?></td>
                        <td><?php echo $func->format($row['cxAMOUNT']); ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
function view_itm_logs($db, $ir, $func)
{
    $cache = [];
    $db->query('SELECT COUNT(ixID) FROM itemxferlogs');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT l.ixID, l.ixFROM, l.ixTO, l.ixQTY, l.ixITEM, l.ixTIME, i.itmname
        FROM itemxferlogs AS l
        INNER JOIN items AS i ON l.ixITEM = i.itmid
        ORDER BY l.ixTIME DESC
    '.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    $func->stafflog('Viewed the Item Transfer Logs (Page: '.$pages->current_page.')');
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Transfers: Items</h3>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Item</th>
                        <th>Amount</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="5" class="text-center">There are no logs available</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            if (!array_key_exists($row['ixFROM'], $cache)) {
                $cache[$row['ixFROM']] = $func->username($row['ixFROM']);
            }
            if (!array_key_exists($row['ixTO'], $cache)) {
                $cache[$row['ixTO']] = $func->username($row['ixTO']);
            }
            $date = new \DateTime($row['ixTIME']); ?>
                    <tr>
                        <td><?php echo $cache[$row['ixFROM']]; ?></td>
                        <td><?php echo $cache[$row['ixTO']]; ?></td>
                        <td><a href="/iteminfo.php?ID=<?php echo $row['ixITEM']; ?>"><?php echo $func->format($row['itmname']); ?></a></td>
                        <td><?php echo $func->format($row['ixQTY']); ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
function view_mail_logs($db, $ir, $func, $parser)
{
    $cache = [];
    $db->query('SELECT COUNT(mail_id) FROM mail');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT mail_id, mail_from, mail_to, mail_subject, mail_text, mail_read, mail_time FROM mail ORDER BY mail_time DESC'.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    $func->stafflog('Viewed the Mail Logs (Page: '.$pages->current_page.')');
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Logs: Mail</h3>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Subject/Message</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="4" class="text-center">There are no logs available</td>
                    </tr><?php
    } else {
        foreach ($rows as $row) {
            if (!array_key_exists($row['mail_from'], $cache)) {
                $cache[$row['mail_from']] = $func->username($row['mail_from']);
            }
            if (!array_key_exists($row['mail_to'], $cache)) {
                $cache[$row['mail_to']] = $func->username($row['mail_to']);
            }
            $date = new \DateTime($row['mail_time']);
            $parser->parse($func->format($row['mail_text'], true)); ?>
                    <tr>
                        <td><?php echo $cache[$row['mail_from']]; ?></td>
                        <td><?php echo $cache[$row['mail_to']]; ?></td>
                        <td>
                            <strong>Subject:</strong> <?php echo '' != $row['mail_subject'] ? $func->format($row['mail_subject']) : '<em>N/A</em>'; ?><br>
                            <?php echo $parser->getAsHTML(); ?>
                        </td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
        <p>
            <?php echo $pages->display_pages(); ?>
        </p>
    </div>
</div><?php
}
