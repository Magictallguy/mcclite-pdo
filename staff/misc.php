<?php

if (!defined('STAFF_FILE')) {
    exit;
}

function adnewspaper($db, $ir, $func)
{
    $file = __DIR__.'/admin.news';
    if (!file_exists($file)) {
        touch($file);
    }
    if (array_key_exists('submit', $_POST)) {
        $_POST['news'] = array_key_exists('news', $_POST) && is_string($_POST['news']) && strlen($_POST['news']) > 0 ? trim($_POST['news']) : '';
        $handle = fopen($file, 'w');
        fwrite($handle, stripslashes($_POST['news']));
        fclose($handle);
        $log = 'updated the Admin News';
        $func->stafflog(ucfirst($log));
        $_SESSION['success'] = 'You\'ve '.$log;
        exit(header('Location: /new_staff.php'));
    }
    $news = file_get_contents($file); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Edit Admin News</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=editadnews" method="post" class="form">
            <div class="form-group">
                <label for="news" class="form-label">News</label>
                <textarea name="news" id="news" rows="10" class="form-control bg-dark text-light"><?php echo str_replace(['[', ']'], ['&#91;', '&#93;'], htmlentities($news)); ?></textarea>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-check"></span>
                    Update Admin News
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function newspaper($db, $ir, $func)
{
    if (array_key_exists('submit', $_POST)) {
        $_POST['news'] = array_key_exists('news', $_POST) && is_string($_POST['news']) && strlen($_POST['news']) > 0 ? trim($_POST['news']) : '';
        $db->trans('start');
        $db->query('UPDATE papercontent SET content = ? WHERE id = 1');
        $db->execute([$_POST['news']]);
        $func->stafflog('Updated the newspaper');
        $db->trans('end');
        $_SESSION['success'] = 'News updated';
        exit(header('Location: /new_staff.php'));
    }
    $db->query('SELECT id, content FROM papercontent WHERE id = 1');
    $db->execute();
    $news = $db->result(1); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Edit News</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=editnews" method="post" class="form">
            <div class="form-group">
                <label for="news" class="form-label">News</label>
                <textarea name="news" id="news" rows="10" class="form-control bg-dark text-light"><?php echo str_replace(['[', ']'], ['&#91;', '&#93;'], $func->format($news)); ?></textarea>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-check"></span>
                    Update News
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function ip_search_form($db, $ir, $func, $ip = null)
{
    $_GET['ip'] = array_key_exists('ip', $_GET) && filter_var($_GET['ip'], FILTER_VALIDATE_IP) ? $_GET['ip'] : null;
    if (null !== $ip) {
        $_GET['ip'] = $ip;
    } ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">IP Search</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=ipsub" method="post" class="form">
            <div class="form-group">
                <label for="ip" class="form-label">IP</label>
                <input type="text" name="ip" id="ip" class="form-control bg-dark text-light"<?php echo null !== $_GET['ip'] ? ' value="'.$_GET['ip'].'"' : ''; ?> required autofocus>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-search"></span>
                    Search IP
                </button>
            </div>
        </form>
    </div>
</div><?php
}

function ip_search_submit($db, $ir, $func)
{
    $_POST['ip'] = array_key_exists('ip', $_POST) && filter_var($_POST['ip'], FILTER_VALIDATE_IP) ? $_POST['ip'] : null;
    ip_search_form($db, $ir, $func, $_POST['ip']); ?>
<div class="row">
    <div class="col">
        &nbsp;
    </div>
</div><?php
    $redirect = true;
    if (null !== $_POST['ip']) {
        $func->stafflog('Searched IP address: '.$_POST['ip']);
        $db->query('SELECT userid, laston, fedjail, user_level FROM users WHERE lastip = ?');
        $db->execute([$_POST['ip']]);
        $rows = $db->fetch();
        if (null !== $rows) {
            $redirect = false;
            $cnt = count($rows);
            $ids = [];
            $time = time(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle"><?php echo $cnt; ?> Result<?php echo $func->s($cnt); ?> Found</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Player</th>
                        <th>Last Seen</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody><?php
            foreach ($rows as $row) {
                if (!$row['fedjail'] && 1 == $row['user_level']) {
                    $ids[] = $row['userid'];
                }
                $date = new \DateTime($row['laston']);
                $laston = strtotime($row['laston']); ?>
                    <tr>
                        <td><?php echo $func->username($row['userid']); ?></td>
                        <td>
                            <?php echo $date->format(DEFAULT_DATE_FORMAT); ?><br>
                            <small class="italic"><?php echo $func->time_format($time - $laston, 'short'); ?></small>
                        </td>
                        <td>
                        </td>
                    </tr><?php
            } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
            $idCnt = count($ids);
            if ($idCnt > 0) {
                ?>
<div class="row">
    <div class="col">
        &nbsp;
    </div>
</div>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Mass Jail</h3>
        <p>
            This allows you to mass-fedjail all accounts found on <?php echo $_POST['ip']; ?>.<br>
            Staff members, NPCs, and accounts already in Federal Jail will be omitted
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=massjailip" method="post" class="form">
            <input type="hidden" name="ids" value="<?php echo implode(',', $ids); ?>">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="days" class="form-label">Days</label>
                        <input type="number" name="days" id="days" class="form-control bg-dark text-light" placeholder="365">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="reason" class="form-label">Reason</label>
                        <input type="text" name="reason" id="reason" class="form-control bg-dark text-light" placeholder="Same IP users, mail <?php echo EMAIL_FEDJAIL; ?>">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-ban"></span>
                    Ban <?php echo $func->format($idCnt),' player',$func->s($idCnt); ?>
                </button>
            </div>
        </form>
    </div>
</div><?php
            }
        } else {
            $_SESSION['info'] = 'No results for '.$_POST['ip'];
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid IP address';
    }
    if (true === $redirect) {
        exit(header('Location: /new_staff.php?action=ipform'));
    }
}

function mass_jail($db, $ir, $func)
{
    $_POST['ids'] = array_key_exists('ids', $_POST) && preg_match('/((\d+),?)+/', $_POST['ids']) ? $_POST['ids'] : null;
    $_POST['days'] = array_key_exists('days', $_POST) && ctype_digit($_POST['days']) && $_POST['days'] > 0 ? $_POST['days'] : 365;
    $_POST['reason'] = array_key_exists('reason', $_POST) && is_string($_POST['reason']) && strlen($_POST['reason']) > 0 ? strip_tags(trim($_POST['reason'])) : 'Same IP users, mail '.EMAIL_FEDJAIL;
    $ids = explode(',', $_POST['ids']);
    $idCnt = count($ids);
    $isNPC = [];
    $isStaff = [];
    $noExist = [];
    $permit = [];
    if ($idCnt > 0) {
        foreach ($ids as $id) {
            $db->query('SELECT userid, user_level, fedjail FROM users WHERE userid = ?');
            $db->execute([$id]);
            $row = $db->fetch(true);
            if (null !== $row) {
                if (1 == $row['user_level']) {
                    $permit[] = $id;
                } else {
                    $arr = !$row['user_level'] ? 'isNPC' : 'isStaff';
                    $arr[] = $id;
                }
            } else {
                $noExist[] = $id;
            }
        }
        $permitCnt = count($permit);
        $isNPCCnt = count($isNPC);
        $isStaffCnt = count($isStaff);
        $noExistCnt = count($noExist);
        if ($permitCnt > 0) {
            $permitImplode = implode(',', $permit);
            $insQuery = '';
            $insParams = [];
            $extra = '';
            $log = 'mass-fedjailed ';
            $tmpPermitCnt = 0;
            foreach ($permit as $user) {
                ++$tmpPermitCnt;
                $insQuery .= '(?, ?, ?, ?), ';
                $insParams[] = [$user, $_POST['days'], $_POST['reason'], $ir['userid']];
                $log .= '{user'.$tmpPermitCnt.'}, ';
                $extra .= $user.':';
            }
            $log = substr($log, 0, -2).' for '.$func->time_format($_POST['days'] * 86400, 'long', false).'; Reason: '.$func->format($_POST['reason']);
            $extra = substr($extra, 0, -1);
            $insParams = $func->array_flatten($insParams);
            $db->trans('start');
            $db->query('DELETE FROM fedjail WHERE fed_userid IN ('.$permitImplode.')');
            $db->execute();
            $db->query('UPDATE users SET fedjail = 1 WHERE userid IN ('.$permitImplode.')');
            $db->execute();
            $db->query('INSERT INTO fedjail (fed_userid, fed_days, fed_reason, fed_jailedby) VALUES '.substr($insQuery, 0, -2));
            $db->execute($insParams);
            $func->stafflog(ucfirst($log), $extra);
            $db->trans('end');
            $_SESSION['success'] = 'You\'ve '.$func->convertUserPlaceholder($log, $extra);
        }
        if ($isNPCCnt > 0) {
            $isNPCImplode = implode(':', $isNPC);
            $userLog = [];
            $tmpIsNPCCnt = 0;
            foreach ($isNPC as $user) {
                ++$tmpIsNPCCnt;
                $userLog[] = '{user'.$tmpIsNPCCnt.'}';
            }
            $log = $func->list_format($userLog);
            $_SESSION['info'] = $func->convertUserPlaceholder($log.(1 == $isNPCCnt ? 'is an NPC' : 'are NPCs').' and cannot be fedjailed', $isNPCImplode);
        }
        if ($isStaffCnt > 0) {
            $isStaffImplode = implode(':', $isStaff);
            $userLog = [];
            $tmpIsStaffCnt = 0;
            foreach ($isStaff as $user) {
                ++$tmpIsStaffCnt;
                $userLog[] = '{user'.$tmpIsStaffCnt.'}';
            }
            $log = $func->list_format($userLog);
            $_SESSION['warning'] = $func->convertUserPlaceholder($log.(1 == $isStaffCnt ? 'is a member of staff' : 'are members of staff').' and cannot be fedjailed', $isStaffImplode);
        }
        if ($noExistCnt > 0) {
            $noExistImplode = implode(':', $noExist);
            $userLog = [];
            $tmpIsStaffCnt = 0;
            foreach ($noExist as $user) {
                ++$tmpIsStaffCnt;
                $userLog[] = '{user'.$tmpIsStaffCnt.'}';
            }
            $log = $func->list_format($userLog);
            $_SESSION['error'] = 'IDs '.$func->list_format($ids).' don\'t exist';
        }
    } else {
        $_SESSION['error'] = 'No IDs detected';
    }
    exit(header('Location: /new_staff.php?action=ipform'));
}
function massmailer($db, $ir, $func)
{
    $_POST['message'] = array_key_exists('message', $_POST) && is_string($_POST['message']) && strlen($_POST['message']) > 0 ? strip_tags(trim($_POST['message'])) : null;
    $_POST['recip'] = array_key_exists('recip', $_POST) && ctype_digit($_POST['recip']) && $_POST['recip'] > 0 ? $_POST['recip'] : 'all';
    $redirect = false;
    if (null !== $_POST['message']) {
        $subject = 'Mass mail from the Administration';
        if ('all' == $_POST['recip']) {
            $db->query('SELECT userid FROM users WHERE user_level > 0 AND fedjail = 0 ORDER BY userid');
            $db->execute();
        }
        $rows = $db->fetch();
        if (null !== $rows) {
            $query = '';
            $params = [];
            $ids = [];
            foreach ($rows as $row) {
                $ids[] = $row['userid'];
                $query .= '(?, ?, ?), ';
                $params[] = [$row['userid'], $subject, $_POST['message']];
            }
            $idCnt = count($ids);
            $params = $func->array_flatten($params);
            $db->trans('start');
            $db->query('INSERT INTO mail (mail_to, mail_subject, mail_text) VALUES '.substr($query, 0, -2));
            $db->execute($params);
            $func->stafflog('Mass-mailed everyone');
            $db->trans('end');
            $_SESSION['success'] = 'Mass message sent to '.$func->format($idCnt).' player'.$func->s($idCnt);
            $redirect = true;
        } else {
            $_SESSION['error'] = 'There are no recipients';
            $redirect = true;
        }
    }
    if (true === $redirect) {
        exit(header('Location: /new_staff.php?action=massmailer'));
    } ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Mass Mail</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=massmailer" method="post" class="form">
            <div class="form-group">
                <label for="message" class="form-label">Message</label>
                <textarea name="message" id="message" rows="10" class="form-control bg-dark text-light" required autofocus></textarea>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-envelope"></span>
                    Send Message
                </button>
            </div>
        </form>
    </div>
</div><?php
}
