<?php

if (!defined('STAFF_FILE')) {
    exit;
}

function fed_user_form($db, $ir, $func)
{
    ?>
    <div class="row">
        <div class="col">
            <h3 class="page-subtitle">Jailing User</h3>
            <p>
                The user will be put in fed jail and will be unable to do anything in the game.
            </p>
            <form action="/new_staff.php?action=fedsub" method="post" class="form">
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="user1" class="form-label">Select user</label>
                            <?php echo $func->user_dropdown('user1', $_GET['ID']); ?>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                            <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="days" class="form-label">Days</label>
                            <input type="number" name="days" id="days" class="form-control bg-dark text-light" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="reason" class="form-label">Reason</label>
                            <input type="text" name="reason" id="reason" class="form-control bg-dark text-light" required>
                        </div>
                    </div>
                </div>
                <div class="form-controls">
                    <button type="submit" class="btn btn-primary">
                        <span class="fas fa-user-slash"></span>
                        Fed User
                    </button>
                </div>
            </form>
        </div>
    </div><?php
}
function fed_user_submit($db, $ir, $func)
{
    $toForm = true;
    $_POST['reason'] = array_key_exists('reason', $_POST) && is_string($_POST['reason']) && strlen($_POST['reason']) > 0 ? strip_tags(trim($_POST['reason'])) : null;
    $_POST['days'] = array_key_exists('days', $_POST) && ctype_digit($_POST['days']) && $_POST['days'] > 0 ? $_POST['days'] : null;
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    if (null !== $_POST['reason']) {
        if (null !== $_POST['days']) {
            if (null !== $_POST['user']) {
                $db->query('SELECT userid, user_level, fedjail FROM users WHERE userid = ?');
                $db->execute([$_POST['user']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $target = $func->username($row['userid']);
                    if (1 == $row['user_level']) {
                        if (!$row['fedjail']) {
                            $log = 'fedjailed {user} for '.$func->time_format($_POST['days'] * 86400, 'long', false).' with the reason: '.$func->format($_POST['reason']);
                            $db->trans('start');
                            $db->query('UPDATE users SET fedjail = 1 WHERE userid = ?');
                            $db->execute([$row['userid']]);
                            $db->query('INSERT INTO fedjail (fed_userid, fed_days, fed_reason, fed_jailedby) VALUES (?, ?, ?, ?)');
                            $db->execute([$row['userid'], $_POST['days'], $_POST['reason'], $ir['userid']]);
                            $func->stafflog(ucfirst($log), $row['userid']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                            $toForm = false;
                        } else {
                            $_SESSION['error'] = $target.' is already in Federal Jail';
                        }
                    } else {
                        $_SESSION['error'] = 'You can\'t fedjail '.(!$row['user_level'] ? 'NPCs' : 'members of staff');
                    }
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid player';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid amount of days';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid reason';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=fedform' : '')));
}
function fed_edit_form($db, $ir, $func)
{
    $db->query('SELECT u.userid, u.username, f.fed_days, f.fed_reason
        FROM users AS u
        LEFT JOIN fedjail AS f ON u.userid = f.fed_userid
        WHERE u.fedjail = 1
        ORDER BY u.userid ASC');
    $db->execute();
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'No-one is currently in Federal Jail, so there are no sentences to edit';
        exit(header('Location: /new_staff.php'));
    } ?>
    <div class="row">
        <div class="col">
            <h3 class="page-subtitle">Editing Fedjail Sentence</h3>
            <p>
                You are editing a player's sentence in Federal Jail
            </p>
            <form action="/new_staff.php?action=fedesub" method="post" class="form">
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="user1" class="form-label">Select user</label>
                            <select name="user1" id="user1" class="form-control bg-dark text-light">
                                <option value="0" disabled selected>---SELECT---</option><?php
    foreach ($rows as $row) {
        ?>
                                <option value="<?php echo $row['userid']; ?>"><?php echo $func->format($row['username']); ?> &middot; <?php echo $row['fed_days'] > 0 ? $func->time_format($row['fed_days'] * 86400, 'long', false).'; &ldquo;'.$func->format($row['fed_reason']).'&rdquo;' : '<em>Fed entry missing</em>'; ?></option>
                                    <?php
    } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                            <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="days" class="form-label">Days</label>
                            <input type="number" name="days" id="days" class="form-control bg-dark text-light" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="reason" class="form-label">Reason</label>
                            <input type="text" name="reason" id="reason" class="form-control bg-dark text-light" required>
                        </div>
                    </div>
                </div>
                <div class="form-controls">
                    <button type="submit" class="btn btn-primary">
                        <span class="fas fa-edit"></span>
                        Fed User
                    </button>
                </div>
            </form>
        </div>
    </div><?php
}
function fed_edit_submit($db, $ir, $func)
{
    $_POST['reason'] = array_key_exists('reason', $_POST) && is_string($_POST['reason']) && strlen($_POST['reason']) > 0 ? strip_tags(trim($_POST['reason'])) : null;
    $_POST['days'] = array_key_exists('days', $_POST) && ctype_digit($_POST['days']) && $_POST['days'] > 0 ? $_POST['days'] : null;
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    $toForm = true;
    if (null !== $_POST['reason']) {
        if (null !== $_POST['days']) {
            if (null !== $_POST['user']) {
                $db->query('SELECT userid, fedjail FROM users WHERE userid = ?');
                $db->execute([$_POST['user']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $target = $func->username($row['userid']);
                    if ($row['fedjail']) {
                        $db->query('SELECT fed_id, fed_days, fed_reason FROM fedjail WHERE fed_userid = ?');
                        $db->execute([$row['userid']]);
                        $fed = $db->fetch(true);
                        $act1 = null;
                        if ($_POST['days'] > $fed['fed_days']) {
                            $act1 = 'extending the duration by '.$func->time_format(($_POST['days'] - $fed['fed_days']) * 86400, 'long', false);
                        } elseif ($fed['fed_days'] > $_POST['days']) {
                            $act1 = 'reducing the duration by '.$func->time_format(($fed['fed_days'] - $_POST['days']) * 86400, 'long', false);
                        }
                        $act2 = strtolower($_POST['reason']) != strtolower($fed['fed_reason']) ? 'updating the reason to &ldquo;'.$func->format($_POST['reason']).'&rdquo;' : null;
                        if (null !== $act1 or null !== $act2) {
                            $log = 'edited the fedjail sentence of {user}, ';
                            if (null !== $act1 && null !== $act2) {
                                $log .= $act1.' and '.$act2;
                            } elseif (null !== $act1) {
                                $log .= $act1;
                            } else {
                                $log .= $act2;
                            }
                            $db->trans('start');
                            if (null !== $fed) {
                                $db->query('UPDATE fedjail SET fed_days = ?, fed_reason = ? WHERE fed_id = ?');
                                $db->execute([$_POST['days'], $_POST['reason'], $fed['fed_id']]);
                            } else {
                                $db->query('INSERT INTO fedjail (fed_userid, fed_days, fed_reason, fed_jailedby) VALUES (?, ?, ?, ?)');
                                $db->execute([$row['userid'], $_POST['days'], $_POST['reason'], $ir['userid']]);
                            }
                            $func->stafflog(ucfirst($log), $row['userid']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                            $toForm = false;
                        } else {
                            $_SESSION['error'] = 'No changes detected';
                        }
                    } else {
                        $_SESSION['error'] = $target.' isn\'t in Federal Jail';
                    }
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid player';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid amount of days';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid reason';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=fedeform' : '')));
}
function unfed_user_form($db, $ir, $func)
{
    $db->query('SELECT u.userid, u.username, f.fed_days, f.fed_reason
        FROM users AS u
        LEFT JOIN fedjail AS f ON u.userid = f.fed_userid
        WHERE u.fedjail = 1
        ORDER BY u.userid ASC');
    $db->execute();
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'No-one is currently in Federal Jail, so there are no sentences to revoke';
        exit(header('Location: /new_staff.php'));
    } ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Unfed User</h3>
        <p>
            You are lifting a player's account ban
        </p>
        <form action="/new_staff.php?action=unfedsub" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select user</label>
                        <select name="user1" id="user1" class="form-control bg-dark text-light">
                            <option value="0" disabled selected>---SELECT---</option><?php
foreach ($rows as $row) {
        ?>
                            <option value="<?php echo $row['userid']; ?>"><?php echo $func->format($row['username']); ?> &middot; <?php echo $row['fed_days'] > 0 ? $func->time_format($row['fed_days'] * 86400, 'long', false).'; &ldquo;'.$func->format($row['fed_reason']).'&rdquo;' : '<em>Fed entry missing</em>'; ?></option>
                                <?php
    } ?>
                        </select>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-smile"></span>
                    Revoke Sentence
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function unfed_user_submit($db, $ir, $func)
{
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    $toForm = true;
    if (null !== $_POST['user']) {
        $db->query('SELECT userid, fedjail FROM users WHERE userid = ?');
        $db->execute([$_POST['user']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $target = $func->username($row['userid']);
            if (1 == $row['fedjail']) {
                $log = 'revoked the fedjail sentence of {user}';
                $db->trans('start');
                $db->query('UPDATE users SET fedjail = 0 WHERE userid = ?');
                $db->execute([$row['userid']]);
                $db->query('DELETE FROM fedjail WHERE fed_userid = ?');
                $db->execute([$row['userid']]);
                $func->stafflog(ucfirst($log), $row['userid']);
                $db->trans('end');
                $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                $toForm = false;
            } else {
                $_SESSION['error'] = $target.' isn\'t in Federal Jail';
            }
        } else {
            $_SESSION['error'] = 'The player you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid player';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action==unfedform' : '')));
}

function mail_user_form($db, $ir, $func)
{
    $db->query('SELECT userid, username FROM users WHERE mailban = 0 ORDER BY username ASC');
    $db->execute();
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'Uh.. Everyone is mailbanned?';
        exit(header('Location: /new_staff.php'));
    } ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Mailban User</h3>
        <p>
            You are banning a player from the mail system
        </p>
        <form action="/new_staff.php?action=mailsub" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select user</label>
                        <select name="user1" id="user1" class="form-control bg-dark text-light">
                            <option value="0" disabled selected>---SELECT---</option><?php
    foreach ($rows as $row) {
        ?>
                            <option value="<?php echo $row['userid']; ?>"><?php echo $func->format($row['username']); ?></option>
                            <?php
    } ?>
                        </select>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="days" class="form-label">Days</label>
                        <input type="number" name="days" id="days" class="form-control bg-dark text-light" required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="reason" class="form-label">Reason</label>
                        <input type="text" name="reason" id="reason" class="form-control bg-dark text-light" required>
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-ban"></span>
                    Mailban User
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function mail_user_submit($db, $ir, $func)
{
    $toForm = true;
    $_POST['reason'] = array_key_exists('reason', $_POST) && is_string($_POST['reason']) && strlen($_POST['reason']) > 0 ? strip_tags(trim($_POST['reason'])) : null;
    $_POST['days'] = array_key_exists('days', $_POST) && ctype_digit($_POST['days']) && $_POST['days'] > 0 ? $_POST['days'] : null;
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    if (null !== $_POST['reason']) {
        if (null !== $_POST['days']) {
            if (null !== $_POST['user']) {
                $db->query('SELECT userid, user_level, mailban FROM users WHERE userid = ?');
                $db->execute([$_POST['user']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $target = $func->username($row['userid']);
                    if (1 == $row['user_level']) {
                        if (!$row['mailban']) {
                            $log = 'mailbanned {user} for '.$func->time_format($_POST['days'] * 86400, 'long', false).' with the reason: '.$func->format($_POST['reason']);
                            $db->trans('start');
                            $db->query('UPDATE users SET mailban = ?, mb_reason = ? WHERE userid = ?');
                            $db->execute([$_POST['days'], $_POST['reason'], $row['userid']]);
                            $func->stafflog(ucfirst($log), $row['userid']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                            $toForm = false;
                        } else {
                            $_SESSION['error'] = $target.' is already mailbanned';
                        }
                    } else {
                        $_SESSION['error'] = 'You can\'t mailban '.(!$row['user_level'] ? 'NPCs' : 'members of staff');
                    }
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid player';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid amount of days';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid reason';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=mailform' : '')));
}

function mail_edit_form($db, $ir, $func)
{
    $db->query('SELECT userid, username, mailban, mb_reason FROM users WHERE mailban > 0 ORDER BY mailban ASC');
    $db->execute();
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'No-one is currently mailbanned, so there are no sentences to edit';
        exit(header('Location: /new_staff.php'));
    } ?>
    <div class="row">
        <div class="col">
            <h3 class="page-subtitle">Editing Mailban Sentence</h3>
            <p>
                You are editing a player's mailban sentence
            </p>
            <form action="/new_staff.php?action=mailesub" method="post" class="form">
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="user1" class="form-label">Select user</label>
                            <select name="user1" id="user1" class="form-control bg-dark text-light">
                                <option value="0" disabled selected>---SELECT---</option><?php
    foreach ($rows as $row) {
        ?>
                                <option value="<?php echo $row['userid']; ?>"><?php echo $func->format($row['username']); ?> &middot; <?php echo $func->time_format($row['mailban'] * 86400, 'long', false).'; &ldquo;'.$func->format($row['mb_reason']).'&rdquo;'; ?></option>
                                    <?php
    } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                            <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="days" class="form-label">Days</label>
                            <input type="number" name="days" id="days" class="form-control bg-dark text-light" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="reason" class="form-label">Reason</label>
                            <input type="text" name="reason" id="reason" class="form-control bg-dark text-light" required>
                        </div>
                    </div>
                </div>
                <div class="form-controls">
                    <button type="submit" class="btn btn-primary">
                        <span class="fas fa-edit"></span>
                        Edit Sentence
                    </button>
                </div>
            </form>
        </div>
    </div><?php
}
function mail_edit_submit($db, $ir, $func)
{
    $_POST['reason'] = array_key_exists('reason', $_POST) && is_string($_POST['reason']) && strlen($_POST['reason']) > 0 ? strip_tags(trim($_POST['reason'])) : null;
    $_POST['days'] = array_key_exists('days', $_POST) && ctype_digit($_POST['days']) && $_POST['days'] > 0 ? $_POST['days'] : null;
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    $toForm = true;
    if (null !== $_POST['reason']) {
        if (null !== $_POST['days']) {
            if (null !== $_POST['user']) {
                $db->query('SELECT userid, mailban, mb_reason FROM users WHERE userid = ?');
                $db->execute([$_POST['user']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $target = $func->username($row['userid']);
                    if ($row['mailban'] > 0) {
                        $act1 = null;
                        if ($_POST['days'] > $row['mailban']) {
                            $act1 = 'extending the duration by '.$func->time_format(($_POST['days'] - $row['mailban']) * 86400, 'long', false);
                        } elseif ($row['mailban'] > $_POST['days']) {
                            $act1 = 'reducing the duration by '.$func->time_format(($row['mailban'] - $_POST['days']) * 86400, 'long', false);
                        }
                        $act2 = strtolower($_POST['reason']) != strtolower($row['mb_reason']) ? 'updating the reason to &ldquo;'.$func->format($_POST['reason']).'&rdquo;' : null;
                        if (null !== $act1 or null !== $act2) {
                            $log = 'edited the mailban sentence of {user}, ';
                            if (null !== $act1 && null !== $act2) {
                                $log .= $act1.' and '.$act2;
                            } elseif (null !== $act1) {
                                $log .= $act1;
                            } else {
                                $log .= $act2;
                            }
                            $db->trans('start');
                            $db->query('UPDATE users SET mailban = ?, mb_reason = ? WHERE userid = ?');
                            $db->execute([$_POST['days'], $_POST['reason'], $row['userid']]);
                            $func->stafflog(ucfirst($log), $row['userid']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                            $toForm = false;
                        } else {
                            $_SESSION['error'] = 'No changes detected';
                        }
                    } else {
                        $_SESSION['error'] = $target.' isn\'t mailbanned';
                    }
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid player';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid amount of days';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid reason';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=maileform' : '')));
}
function unmail_user_form($db, $ir, $func)
{
    $db->query('SELECT userid, username, mailban, mb_reason FROM users WHERE mailban > 0 ORDER BY mailban ASC');
    $db->execute();
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'No-one is currently mailbanned, so there are no sentences to revoke';
        exit(header('Location: /new_staff.php'));
    } ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Unmailban User</h3>
        <p>
            You are lifting a player's mail ban
        </p>
        <form action="/new_staff.php?action=unmailsub" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select user</label>
                        <select name="user1" id="user1" class="form-control bg-dark text-light">
                            <option value="0" disabled selected>---SELECT---</option><?php
foreach ($rows as $row) {
        ?>
                            <option value="<?php echo $row['userid']; ?>"><?php echo $func->format($row['username']); ?> &middot; <?php echo $func->time_format($row['mailban'] * 86400, 'long', false).'; &ldquo;'.$func->format($row['mb_reason']).'&rdquo;'; ?></option>
                                <?php
    } ?>
                        </select>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="underline">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-smile"></span>
                    Revoke Sentence
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function unmail_user_submit($db, $ir, $func)
{
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    $toForm = true;
    if (null !== $_POST['user']) {
        $db->query('SELECT userid, mailban FROM users WHERE userid = ?');
        $db->execute([$_POST['user']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $target = $func->username($row['userid']);
            if ($row['mailban'] > 0) {
                $log = 'revoked the mailban sentence of {user}';
                $db->trans('start');
                $db->query('UPDATE users SET mailban = 0, mb_reason = "" WHERE userid = ?');
                $db->execute([$row['userid']]);
                $func->stafflog(ucfirst($log), $row['userid']);
                $db->trans('end');
                $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                $toForm = false;
            } else {
                $_SESSION['error'] = $target.' isn\'t mailbanned';
            }
        } else {
            $_SESSION['error'] = 'The player you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid player';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action==unmailform' : '')));
}
