<?php

if (!defined('STAFF_FILE')) {
    exit;
}

function reports_view($db, $ir, $func)
{
    $db->query('SELECT COUNT(prID) FROM preports');
    $db->execute();
    $cnt = $db->result();
    $pages = new Paginator($cnt);
    $db->query('SELECT prID, prREPORTER, prREPORTED, prTEXT, prTIME FROM preports ORDER BY prID DESC'.$pages->limit);
    $db->execute();
    $rows = $db->fetch();
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Reports: Players</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Reporter</th>
                        <th>Offender</th>
                        <th>Report</th>
                        <th>Time</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody><?php
    if (null === $rows) {
        ?>
                    <tr>
                        <td colspan="5" class="text-center">There currently are no reports</td>
                    </tr><?php
    } else {
        $cache = [];
        foreach ($rows as $row) {
            if (!array_key_exists($row['prREPORTER'], $cache)) {
                $cache[$row['prREPORTER']] = $func->username($row['prREPORTER']);
            }
            if (!array_key_exists($row['prREPORTED'], $cache)) {
                $cache[$row['prREPORTED']] = $func->username($row['prREPORTED']);
            }
            $date = new \DateTime($row['prTIME']); ?>
                    <tr>
                        <td><?php echo $cache[$row['prREPORTER']]; ?></td>
                        <td><?php echo $cache[$row['prREPORTED']]; ?></td>
                        <td><?php echo $func->format($row['prTEXT'], true); ?></td>
                        <td><?php echo $date->format(DEFAULT_DATE_FORMAT); ?></td>
                        <td>
                            <a href="/new_staff.php?action=repclear&amp;ID=<?php echo $row['prID']; ?>">Clear</a>
                        </td>
                    </tr><?php
        }
    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
}

function report_clear($db, $ir, $func)
{
    if (null !== $_GET['ID']) {
        $db->query('SELECT prID, prREPORTER FROM preports WHERE prID = ?');
        $db->execute([$_GET['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $target = $func->username($row['prREPORTER']);
            $log = 'cleared player report #'.$func->format($row['prID']).' from {user}';
            $db->trans('start');
            $db->query('DELETE FROM preports WHERE prID = ?');
            $db->execute([$row['prID']]);
            $func->stafflog(ucfirst($log), $row['prREPORTER']);
            $db->trans('end');
            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
        } else {
            $_SESSION['error'] = 'The report you selected doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid report';
    }
    exit(header('Location: /new_staff.php?action=reportsview'));
}
