<?php

if (!defined('STAFF_FILE')) {
    exit;
}
function new_shop($db, $ir, $func)
{
    if (array_key_exists('submit', $_POST)) {
        $toForm = true;
        $strs = ['name', 'desc'];
        foreach ($strs as $str) {
            $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? strip_tags(trim($_POST[$str])) : null;
        }
        $_POST['location'] = array_key_exists('location', $_POST) && ctype_digit($_POST['location']) && $_POST['location'] > 0 ? $_POST['location'] : null;
        if (null !== $_POST['name']) {
            if (null !== $_POST['desc']) {
                if (null !== $_POST['location']) {
                    $db->query('SELECT cityid, cityname FROM cities WHERE cityid = ?');
                    $db->execute([$_POST['location']]);
                    $city = $db->result(1);
                    if (null !== $city) {
                        $db->query('SELECT COUNT(shopID) FROM shops WHERE LOWER(shopNAME) = ?');
                        $db->execute([strtolower($_POST['name'])]);
                        if (!$db->result()) {
                            $log = 'opened a new store &ldquo;'.$func->format($_POST['name']).'&rdquo; in '.$func->format($city);
                            $path = $func->handleImageUpload(PATH_UPLOAD_SHOP);
                            $db->trans('start');
                            $db->query('INSERT INTO shops (shopNAME, shopDESCRIPTION, shopLOCATION) VALUES (?, ?, ?)');
                            $db->execute([$_POST['name'], $_POST['desc'], $_POST['location']]);
                            $id = $db->insert_id();
                            if (isset($path['url'])) {
                                $db->query('UPDATE shops SET shopIMAGE = ? WHERE shopID = ?');
                                $db->execute([$path['url'], $id]);
                            } else {
                                $_SESSION['info'] = isset($path['error']) ? $path['error'] : 'No image uploaded';
                            }
                            $func->stafflog(ucfirst($log));
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.$log;
                            $toForm = false;
                            if (isset($path['error'])) {
                                $_SESSION['info'] = $path['error'];
                            }
                        } else {
                            $_SESSION['error'] = 'Another shop with that name already exists';
                        }
                    } else {
                        $_SESSION['error'] = 'The location you selected doesn\'t exist';
                    }
                } else {
                    $_SESSION['error'] = 'You didn\'t select a valid location';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t enter a valid description';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid name';
        }
        exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=newshop' : '')));
    }
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops: Add Shop</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=newshop" method="post" enctype="multipart/form-data" class="form">
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" id="name" class="form-control bg-dark text-light" required autofocus>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="location" class="form-label">Location</label>
                        <?php echo $func->city_dropdown('location'); ?>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="image" class="form-label">Image</label>
                        <input type="file" name="image" id="image" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="desc" class="form-label">Description</label>
                        <textarea name="desc" id="desc" rows="5" class="form-control bg-dark text-light" required></textarea>
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-plus"></span>
                    Add Shop
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function edit_shop($db, $ir, $func)
{
    $_GET['step'] = array_key_exists('step', $_GET) ? $_GET['step'] : null;
    switch ($_GET['step']) {
        case 2:
            $_POST['shop'] = array_key_exists('shop', $_POST) && ctype_digit($_POST['shop']) && $_POST['shop'] > 0 ? $_POST['shop'] : null;
            if (null !== $_POST['shop']) {
                $db->query('SELECT * FROM shops WHERE shopID = ?');
                $db->execute([$_POST['shop']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $toForm = true;
                    $strs = ['name', 'desc'];
                    foreach ($strs as $str) {
                        $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? strip_tags(trim($_POST[$str])) : null;
                    }
                    $_POST['location'] = array_key_exists('location', $_POST) && ctype_digit($_POST['location']) && $_POST['location'] > 0 ? $_POST['location'] : null;
                    if (null !== $_POST['name']) {
                        if (null !== $_POST['desc']) {
                            if (null !== $_POST['location']) {
                                $db->query('SELECT cityid, cityname FROM cities WHERE cityid = ?');
                                $db->execute([$_POST['location']]);
                                $city = $db->result(1);
                                if (null !== $city) {
                                    $db->query('SELECT COUNT(shopID) FROM shops WHERE LOWER(shopNAME) = ? AND shopID <> ?');
                                    $db->execute([strtolower($_POST['name']), $row['shopID']]);
                                    if (!$db->result()) {
                                        $log = 'rebranded store &ldquo;'.$func->format($_POST['name']).'&rdquo; in '.$func->format($city);
                                        $path = $func->handleImageUpload(PATH_UPLOAD_SHOP);
                                        if (!isset($path['error'])) {
                                            $db->trans('start');
                                            $db->query('UPDATE shops SET shopNAME = ?, shopDESCRIPTION = ?, shopLOCATION = ? WHERE shopID = ?');
                                            $db->execute([$_POST['name'], $_POST['desc'], $_POST['location'], $row['shopID']]);
                                            if (isset($path['url'])) {
                                                $db->query('UPDATE shops SET shopIMAGE = ? WHERE shopID = ?');
                                                $db->execute([$path['url'], $row['shopID']]);
                                            } else {
                                                $_SESSION['info'] = isset($path['error']) ? $path['error'] : 'No image uploaded';
                                            }
                                            $func->stafflog(ucfirst($log));
                                            $db->trans('end');
                                            $_SESSION['success'] = 'You\'ve '.$log;
                                            $toForm = false;
                                        } else {
                                            $_SESSION['error'] = $path['error'];
                                        }
                                    } else {
                                        $_SESSION['error'] = 'Another shop with that name already exists';
                                    }
                                } else {
                                    $_SESSION['error'] = 'The location you selected doesn\'t exist';
                                }
                            } else {
                                $_SESSION['error'] = 'You didn\'t select a valid location';
                            }
                        } else {
                            $_SESSION['error'] = 'You didn\'t enter a valid description';
                        }
                    } else {
                        $_SESSION['error'] = 'You didn\'t enter a valid name';
                    }
                } else {
                    $_SESSION['error'] = 'The shop you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid shop';
            }
            exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=editshop' : '')));
            break;
        case 1:
            $_POST['shop'] = array_key_exists('shop', $_POST) && ctype_digit($_POST['shop']) && $_POST['shop'] > 0 ? $_POST['shop'] : null;
            if (null === $_POST['shop']) {
                $_SESSION['error'] = 'You didn\'t select a valid shop';
                exit(header('Location: /new_staff.php?action=editshop'));
            }
            $db->query('SELECT * FROM shops WHERE shopID = ?');
            $db->execute([$_POST['shop']]);
            $row = $db->fetch(true);
            if (null === $row) {
                $_SESSION['error'] = 'The shop you selected doesn\'t exist';
                exit(header('Location: /new_staff.php?action=editshop'));
            }
            staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops: Edit Shop: <?php echo $func->format($row['shopNAME']); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=editshop&amp;step=2" method="post" enctype="multipart/form-data" class="form">
            <input type="hidden" name="shop" value="<?php echo $row['shopID']; ?>">
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" name="name" id="name" value="<?php echo $func->format($row['shopNAME']); ?>" class="form-control bg-dark text-light" required autofocus>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="location" class="form-label">Location</label>
                        <?php echo $func->city_dropdown('location', $row['shopLOCATION']); ?>
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="image" class="form-label">Image</label>
                        <input type="file" name="image" id="image" class="form-control bg-dark text-light">
                        <small class="form-text text-muted">Current: <?php echo $func->format($row['shopIMAGE']); ?></small>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <div class="form-group">
                        <label for="desc" class="form-label">Description</label>
                        <textarea name="desc" id="desc" rows="5" class="form-control bg-dark text-light" required><?php echo $func->format($row['shopDESCRIPTION']); ?></textarea>
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-edit"></span>
                    Edit Shop
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
        default:
            ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops: Edit Shop</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=editshop&amp;step=1" method="post" class="form">
            <div class="form-group">
                <label for="shop" class="form-label">Shop</label>
                <?php echo $func->shop_dropdown('shop'); ?>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-edit"></span>
                    Begin Edits
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
    }
}
function delete_shop($db, $ir, $func)
{
    $_GET['step'] = array_key_exists('step', $_GET) ? $_GET['step'] : null;
    switch ($_GET['step']) {
        case 2:
            $toForm = true;
            $_POST['shop'] = array_key_exists('shop', $_POST) && ctype_digit($_POST['shop']) && $_POST['shop'] > 0 ? $_POST['shop'] : null;
            if (null !== $_POST['shop']) {
                $db->query('SELECT s.shopID, s.shopNAME, c.cityname
                    FROM shops AS s
                    INNER JOIN cities AS c ON s.shopLOCATION = c.cityid
                    WHERE s.shopID = ?
                ');
                $db->execute([$_POST['shop']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $log = 'closed store &ldquo;'.$func->format($row['shopNAME']).'&rdquo; in '.$func->format($row['cityname']);
                    $db->trans('start');
                    $db->query('DELETE FROM shops WHERE shopID = ?');
                    $db->execute([$row['shopID']]);
                    $db->query('DELETE FROM shopitems WHERE sitemSHOP = ?');
                    $db->execute([$row['shopID']]);
                    $func->stafflog(ucfirst($log));
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve '.$log;
                    $toForm = false;
                } else {
                    $_SESSION['error'] = 'The shop you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid shop';
            }
            exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=delshop' : '')));
            break;
        case 1:
            $toForm = true;
            $redirect = true;
            $_POST['shop'] = array_key_exists('shop', $_POST) && ctype_digit($_POST['shop']) && $_POST['shop'] > 0 ? $_POST['shop'] : null;
            if (null !== $_POST['shop']) {
                $db->query('SELECT s.shopID, s.shopNAME, c.cityname
                    FROM shops AS s
                    INNER JOIN cities AS c ON s.shopLOCATION = c.cityid
                    WHERE s.shopID = ?
                ');
                $db->execute([$_POST['shop']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $toForm = false;
                    $redirect = false;
                    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops: Delete Shop: <?php echo $func->format($row['shopNAME']); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=delshop&amp;step=2" method="post" class="form">
            <input type="hidden" name="shop" value="<?php echo $row['shopID']; ?>">
            <div class="form-group">
                Confirm deletion<br>
                <label for="confirm" class="switch">
                    <input type="checkbox" name="confirm" id="confirm" class="form-check-input" required>
                    <span class="slider slider-danger round"></span>
                </label>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-trash"></span>
                    Delete Shop
                </button>
            </div>
        </form>
    </div>
</div><?php
                } else {
                    $_SESSION['error'] = 'The shop you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid shop';
            }
            if (true === $redirect) {
                header('Location: /new_staff.php'.(true === $toForm ? '?action=delshop' : ''));
            }
            break;
        default:
            ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops: Delete Shop</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=delshop&amp;step=1" method="post" class="form">
            <div class="form-group">
                <label for="shop" class="form-label">Shop</label>
                <?php echo $func->shop_dropdown('shop'); ?>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-trash"></span>
                    Delete Shop
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
    }
}
function new_stock($db, $ir, $func)
{
    $toForm = true;
    $redirect = true;
    if (array_key_exists('submit', $_POST)) {
        $nums = ['item', 'shop'];
        foreach ($nums as $num) {
            $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : null;
        }
        if (null !== $_POST['item']) {
            if (null !== $_POST['shop']) {
                $db->query('SELECT itmid, itmname, itmbuyable FROM items WHERE itmid = ?');
                $db->execute([$_POST['item']]);
                $item = $db->fetch(true);
                if (null !== $item) {
                    if (1 == $item['itmbuyable']) {
                        $db->query('SELECT shopID, shopNAME FROM shops WHERE shopID = ?');
                        $db->execute([$_POST['shop']]);
                        $shop = $db->fetch(true);
                        if (null !== $shop) {
                            $db->query('SELECT COUNT(sitemID) FROM shopitems WHERE sitemSHOP = ? AND sitemITEMID = ?');
                            $db->execute([$_POST['shop'], $_POST['item']]);
                            if (!$db->result()) {
                                $log = 'added item: '.$func->format($item['itmname']).' to shop: '.$func->format($shop['shopNAME']);
                                $db->trans('start');
                                $db->query('INSERT INTO shopitems (sitemSHOP, sitemITEMID) VALUES (?, ?)');
                                $db->execute([$_POST['shop'], $_POST['item']]);
                                $func->stafflog(ucfirst($log));
                                $db->trans('end');
                                $_SESSION['success'] = 'You\'ve '.$log;
                                $toForm = false;
                            }
                        } else {
                            $_SESSION['error'] = 'The shop you selected doesn\'t exist';
                        }
                    } else {
                        $_SESSION['error'] = 'The item you selected is set to unbuyable';
                    }
                } else {
                    $_SESSION['error'] = 'The item you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid shop';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t select a valid item';
        }
        if (true === $redirect) {
            exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=newstock' : '')));
        }
    }
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Shops: Add Stock</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=newstock" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="item" class="form-label">Item</label>
                        <?php echo $func->item_dropdown('item'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="shop" class="form-label">Shop</label>
                        <?php echo $func->shop_dropdown('shop'); ?>
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-shopping-basket"></span>
                    Add Stock
                </button>
            </div>
        </form>
    </div>
</div><?php
}
