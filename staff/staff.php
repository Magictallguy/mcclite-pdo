<?php

if (!defined('STAFF_FILE')) {
    exit;
}
function staff_list($db, $ir, $func)
{
    staffMenu();
    $time = time();
    $staff = [
        'administrators' => [],
        'secretaries' => [],
        'assistants' => [],
    ];
    $db->query('SELECT userid, laston, level, money, user_level, avatar FROM users WHERE user_level IN(2, 3, 5) ORDER BY user_level ASC, userid ASC');
    $db->execute();
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = 'There are no staff members :o';
        exit(header('Location: /explore.php'));
    }
    foreach ($rows as $row) {
        if (2 == $row['user_level']) {
            $level = 'administrators';
        } elseif (3 == $row['user_level']) {
            $level = 'secretaries';
        } elseif (5 == $row['user_level']) {
            $level = 'assistants';
        }
        $staff[$level][] = $row;
    }
    $admin = '<a style="color:#935ae4;" href="/new_staff.php?action=userlevel&amp;level=2&amp;ID={USERID}&amp;fromStaff">Administrator</a>';
    $sec = '<a style="color:#07d410;" href="/new_staff.php?action=userlevel&amp;level=3&amp;ID={USERID}&amp;fromStaff">Secretary</a>';
    $ass = '<a style="color:#e6de09;" href="/new_staff.php?action=userlevel&amp;level=5&amp;ID={USERID}&amp;fromStaff">Assistant</a>';
    foreach ($staff as $rank => $data) {
        if (count($data)) { ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle"><?php echo ucfirst($rank); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Member</th>
                        <th>Last Seen</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody><?php
        foreach ($data as $row) {
            $links = [];
            if (2 == $row['user_level']) {
                $links[] = $sec;
                $links[] = $ass;
            } elseif (3 == $row['user_level']) {
                $links[] = $admin;
                $links[] = $ass;
            } elseif (5 == $row['user_level']) {
                $links[] = $admin;
                $links[] = $sec;
            }
            $links[] = '<a href="/new_staff.php?action=userlevel&amp;level=1&amp;ID={USERID}&amp;fromStaff">Member</a>';
            $laston = strtotime($row['laston']);
            $timeCalc = $laston >= $time - 900; ?>
                    <tr>
                        <td>
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" class="staff-management-list img-fluid <?php echo $timeCalc ? 'online-box' : 'offline-box'; ?>">
                            </a>
                            <?php echo $func->username($row['userid']); ?>
                        </td>
                        <td><?php echo $func->time_format($time - $laston, 'short', true, 2); ?></td>
                        <td>
                            <?php echo str_replace('{USERID}', $row['userid'], implode(' &middot; ', $links)); ?>
                        </td>
                    </tr><?php
        } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">&nbsp;</div>
</div><?php
        }
    }
}
function userlevel($db, $ir, $func)
{
    $ranks = [
        1 => 'Member',
        2 => 'Administrator',
        3 => 'Secretary',
        5 => 'Assistant',
    ];
    $fromStaff = array_key_exists('fromStaff', $_GET);
    $fromForm = array_key_exists('fromULForm', $_GET);
    $_GET['level'] = array_key_exists('level', $_GET) && in_array($_GET['level'], [1, 2, 3, 5]) ? $_GET['level'] : null;
    if (null !== $_GET['level']) {
        if (null !== $_GET['ID']) {
            $db->query('SELECT userid, user_level, fedjail FROM users WHERE userid = ?');
            $db->execute([$_GET['ID']]);
            $row = $db->fetch(true);
            if (null !== $row) {
                $target = $func->username($row['userid']);
                if ($row['user_level'] > 0) {
                    if (!$row['fedjail']) {
                        if ($_GET['level'] != $row['user_level']) {
                            $log = 'granted {user} the rank of '.$ranks[$_GET['level']];
                            $db->trans('start');
                            $db->query('UPDATE users SET user_level = ? WHERE userid = ?');
                            $db->execute([$_GET['level'], $row['userid']]);
                            $func->event_add($row['userid'], 'You\'ve been granted the rank of '.$ranks[$_GET['level']].' by the Administration');
                            $func->stafflog(ucfirst($log), $row['userid']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                        } else {
                            $_SESSION['error'] = $target.' is already '.$func->aAn($ranks[$_GET['level']], true);
                        }
                    } else {
                        $_SESSION['error'] = $target.' is in Federal Jail';
                    }
                } else {
                    $_SESSION['error'] = $target.' is an NPC. You ca\'t modify their staff rank';
                }
            } else {
                $_SESSION['error'] = 'The player you selected doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t select a valid player';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid staff rank';
    }
    $location = '';
    if (true === $fromStaff or true === $fromForm) {
        $location .= '?action='.(true === $fromStaff ? 'stafflist' : '').(true === $fromForm ? 'userlevelform' : '');
    }
    exit(header('Location: /new_staff.php'.$location));
}
function userlevelform($db, $ir, $func)
{
    $ranks = [
        1 => 'Member',
        2 => 'Administrator',
        3 => 'Secretary',
        5 => 'Assistant',
    ]; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Setting Staff Rank</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php" method="get" class="form">
            <input type="hidden" name="action" value="userlevel">
            <input type="hidden" name="fromULForm" value="true">
            <div class="form-group">
                <label for="ID" class="form-label">Select player</label>
                <?php echo $func->user_dropdown('ID'); ?>
            </div>
            <div class="form-group">
                <label for="level" class="form-label">Rank</label>
                <select name="level" id="level" class="form-control bg-dark text-light">
                    <option value="0" disabled selected>---SELECT---</option><?php
    foreach ($ranks as $rank => $disp) {
        ?>
                    <option value="<?php echo $rank; ?>"><?php echo $disp; ?></option>
                    <?php
    } ?>
                </select>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-user"></span>
                    Set Rank
                </button>
            </div>
        </form>
    </div>
</div><?php
}
