<?php

if (!defined('STAFF_FILE')) {
    exit;
}
$defaults = [
    'level' => 1,
    'money' => 100,
    'crystals' => 0,
    'donatordays' => 0,
    'strength' => 10,
    'agility' => 10,
    'guard' => 10,
    'labour' => 10,
    'IQ' => 10,
];
function inv_user_begin($db, $ir, $func)
{
    staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">View User Inventory</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=invuser" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select player</label>
                        <?php echo $func->user_dropdown('user1'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="italic">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-search"></span>
                    View
                </button>
            </div>
        </form>
    </div>
</div><?php
}
function inv_user_view($db, $ir, $func)
{
    $_GET['user'] = array_key_exists('user', $_GET) && ctype_digit($_GET['user']) && $_GET['user'] > 0 ? $_GET['user'] : null;
    $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
    $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
    $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
    if (null !== $_GET['user']) {
        $_POST['user'] = $_GET['user'];
    }
    if (null === $_POST['user']) {
        $_SESSION['error'] = 'You didn\'t select a valid player';
        exit(header('Location: /new_staff.php?action=invbeg'));
    }
    $db->query('SELECT userid, user_level FROM users WHERE userid = ?');
    $db->execute([$_POST['user']]);
    $user = $db->fetch(true);
    if (null === $user) {
        $_SESSION['error'] = 'The player you selected doesn\'t exist';
        exit(header('Location: /new_staff.php?action=invbeg'));
    }
    $func->stafflog('Viewed the inventory of {user}', $user['userid']);
    $target = $func->username($user['userid']);
    $db->query('SELECT inv.inv_id, inv.inv_qty, i.itmid, i.itmname, i.itmsellprice, it.itmtypename
        FROM inventory AS inv
        INNER JOIN items AS i ON inv.inv_itemid = i.itmid
        INNER JOIN itemtypes AS it ON i.itmtype = it.itmtypeid
        WHERE inv.inv_userid = ?
        ORDER BY it.itmtypename ASC, i.itmname ASC
    ');
    $db->execute([$user['userid']]);
    $rows = $db->fetch();
    if (null === $rows) {
        $_SESSION['info'] = $target.' doesn\'t have any items';
        exit(header('Location: /new_staff.php?action=invbeg'));
    }
    $inventory = [];
    foreach ($rows as $row) {
        if (!array_key_exists($row['itmtypename'], $inventory)) {
            $inventory[$row['itmtypename']] = 0;
        }
        if ($row['itmsellprice'] > 0) {
            $inventory[$row['itmtypename']] += $row['itmsellprice'] * $row['inv_qty'];
        }
    }
    $overallTotal = 0;
    $lt = ''; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">View Inventory: <?php echo $target; ?></h3>
        <p>
            <a href="/new_staff.php?action=invbeg">Back</a>
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Qty</th>
                        <th>Sell Value</th>
                        <th>Total Sell Value</th>
                        <th>Links</th>
                    </tr>
                </thead>
                <tbody><?php
    foreach ($rows as $row) {
        $total = $row['itmsellprice'] * $row['inv_qty'];
        $overallTotal += $total;
        if ($lt != $row['itmtypename']) {
            $toggle = false;
            $lt = $row['itmtypename']; ?>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="5">
                            <div class="float-left"><?php echo $func->format($lt); ?></div>
                            <div class="float-right">Subtotal: <?php echo $func->money($inventory[$lt]); ?></div>
                        </th>
                    </tr>
                </thead>
                <tbody><?php
        } ?>
                    <tr>
                        <td><?php echo ucwords($func->format($row['itmname'])); ?></td>
                        <td><?php echo $func->format($row['inv_qty']); ?></td>
                        <td><?php echo $row['itmsellprice'] > 0 ? $func->money($row['itmsellprice']) : '--'; ?></td>
                        <td><?php echo $total > 0 ? $func->money($total) : '--'; ?></td>
                        <td>
                            [<a href="/new_staff.php?action=deleinv&amp;ID=<?php echo $row['itmid']; ?>&amp;user=<?php echo $user['userid']; ?>">Delete</a>]
                        </td>
                    </tr><?php
    } ?>
                    <tr>
                        <th colspan="5">
                            <div class="float-right">
                                Total Inventory Resale Value: <?php echo $func->money($overallTotal); ?>
                            </div>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
}
function inv_delete($db, $ir, $func)
{
    $_GET['user'] = array_key_exists('user', $_GET) && ctype_digit($_GET['user']) && $_GET['user'] > 0 ? $_GET['user'] : null;
    $row = null;
    if (null !== $_GET['ID']) {
        if (null !== $_GET['user']) {
            if ($func->userExists($_GET['user'])) {
                $target = $func->username($_GET['user']);
                $db->query('SELECT iv.inv_userid, iv.inv_qty, i.itmid, i.itmname
                    FROM inventory AS iv
                    INNER JOIN items AS i ON i.itmid = iv.inv_itemid
                    WHERE iv.inv_itemid = ? AND iv.inv_userid = ?
                ');
                $db->execute([$_GET['ID'], $_GET['user']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $log = 'removed '.$func->format($row['inv_qty']).'x '.$func->format($row['itmname']).' from the inventory of {user}';
                    $db->trans('start');
                    $db->query('DELETE FROM inventory WHERE inv_itemid = ? AND inv_userid = ?');
                    $db->execute([$_GET['ID'], $_GET['user']]);
                    $func->stafflog(ucfirst($log), $row['inv_userid']);
                    $db->trans('end');
                    $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $target, $log);
                } else {
                    $_SESSION['error'] = $target.' doesn\'t own '.$func->aAn($row['itmname'], true);
                }
            } else {
                $_SESSION['error'] = 'The player you selected doesn\'t exist';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t select a valid player';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid item';
    }
    exit(header('Location: /new_staff.php?action='.(null !== $row ? 'invuser&user='.$row['inv_userid'] : 'invbeg')));
}

function admin_user_record($db, $ir, $func)
{
    $redirect = true;
    $_GET['user1'] = array_key_exists('user1', $_GET) && ctype_digit($_GET['user1']) && $_GET['user1'] > 0 ? $_GET['user1'] : null;
    $_GET['user2'] = array_key_exists('user2', $_GET) && ctype_digit($_GET['user2']) && $_GET['user2'] > 0 ? $_GET['user2'] : null;
    $_GET['user'] = null === $_GET['user2'] ? $_GET['user1'] : $_GET['user2'];
    if (null !== $_GET['user']) {
        $db->query('SELECT u.bankmoney, u.brave, u.cdays, u.crystals, u.cybermoney, u.daysold, u.avatar, u.donatordays, u.duties, u.email, u.energy, u.exp, u.gender, u.hospital, u.hospreason, u.hp, u.lastip, u.laston, u.level, u.location, u.login_name, u.mailban,
            u.maxbrave, u.maxenergy, u.maxhp, u.maxwill, u.mb_reason, u.money, u.signedup, u.staffnotes, u.user_level, u.userid, u.username, u.will,
            us.strength, us.agility, us.guard, us.labour, us.IQ,
            h.hNAME,
            c.crNAME,
            f.fed_days, f.fed_jailedby, f.fed_reason
            FROM users AS u
            INNER JOIN userstats AS us ON u.userid = us.userid
            INNER JOIN houses AS h ON u.maxwill = h.hWILL
            LEFT JOIN courses AS c ON u.course = c.crID
            LEFT JOIN fedjail AS f ON u.userid = f.fed_userid
            WHERE u.userid = ?
        ');
        $db->execute([$_GET['user']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $redirect = false;
            $func->stafflog('Viewed user record on {user}', $row['userid']);
            staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">View User Record: <?php echo $func->username($row['userid']); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th colspan="4">General</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Username</th>
                        <td><?php echo $func->format($row['username']); ?></td>
                        <th>Login</th>
                        <td><?php echo $func->format($row['login_name']); ?></td>
                    </tr>
                    <tr>
                        <th>User ID</th>
                        <td><?php echo $func->format($row['userid']); ?></td>
                        <th>Level</th>
                        <td><?php echo $func->format($row['level']); ?></td>
                    </tr>
                    <tr>
                        <th>Exp</th>
                        <td><?php echo $func->format($row['exp']); ?></td>
                        <th>Money</th>
                        <td><?php echo $func->format($row['money']); ?></td>
                    </tr>
                    <tr>
                        <th>Crystals</th>
                        <td><?php echo $func->format($row['crystals']); ?></td>
                        <th>Last Active</th>
                        <td><?php echo $func->format($row['laston']); ?></td>
                    </tr>
                    <tr>
                        <th>Last IP</th>
                        <td><?php echo $func->format($row['lastip']); ?></td>
                        <th>Energy</th>
                        <td><?php echo $func->format($row['energy']); ?></td>
                    </tr>
                    <tr>
                        <th>Max Energy</th>
                        <td><?php echo $func->format($row['maxenergy']); ?></td>
                        <th>Health</th>
                        <td><?php echo $func->format($row['hp']); ?></td>
                    </tr>
                    <tr>
                        <th>Max Health</th>
                        <td><?php echo $func->format($row['maxhp']); ?></td>
                        <th>Will</th>
                        <td><?php echo $func->format($row['will']); ?></td>
                    </tr>
                    <tr>
                        <th>Max Will</th>
                        <td><?php echo $func->format($row['maxwill']); ?></td>
                        <th>Property</th>
                        <td><?php echo $func->format($row['hNAME']); ?></td>
                    </tr>
                    <tr>
                        <th>Brave</th>
                        <td><?php echo $func->format($row['brave']); ?></td>
                        <th>Max Brave</th>
                        <td><?php echo $func->format($row['maxbrave']); ?></td>
                    </tr>
                    <tr>
                        <th>Location</th>
                        <td><?php echo $func->format($row['location']); ?></td>
                        <th>Hospital</th>
                        <td><?php echo $func->format($row['hospital']); ?></td>
                    </tr>
                    <tr>
                        <th>Hosp Reason</th>
                        <td><?php echo $func->format($row['hospreason']); ?></td>
                        <th>User Level</th>
                        <td><?php echo $func->format($row['user_level']); ?></td>
                    </tr>
                    <tr>
                        <th>Duties</th>
                        <td><?php echo $func->format($row['duties']); ?></td>
                        <th>Gender</th>
                        <td><?php echo $func->format($row['gender']); ?></td>
                    </tr>
                    <tr>
                        <th>Course</th>
                        <td><?php echo $func->format($row['crNAME']); ?></td>
                        <th>Days Left</th>
                        <td><?php echo $func->format($row['cdays']); ?></td>
                    </tr>
                    <tr>
                        <th>Days Old</th>
                        <td><?php echo $func->format($row['daysold']); ?></td>
                        <th>Signed Up</th>
                        <td><?php echo $func->format($row['signedup']); ?></td>
                    </tr>
                    <tr>
                        <th>Donator</th>
                        <td><?php echo $func->format($row['donatordays']); ?></td>
                        <th>Email</th>
                        <td><?php echo $func->format($row['email']); ?></td>
                    </tr>
                    <tr>
                        <th>Pic</th>
                        <td><?php echo $func->format($row['avatar']); ?></td>
                        <th>Bank</th>
                        <td><?php echo $func->format($row['bankmoney']); ?></td>
                    </tr>
                    <tr>
                        <th>Cyber Bank</th>
                        <td><?php echo $func->format($row['cybermoney']); ?></td>
                        <th>Notes</th>
                        <td><?php echo $func->format($row['staffnotes']); ?></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="4">Stats</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Strength</th>
                        <td><?php echo $func->format($row['strength']); ?></td>
                        <th>Agility</th>
                        <td><?php echo $func->format($row['agility']); ?></td>
                    </tr>
                    <tr>
                        <th>Guard</th>
                        <td><?php echo $func->format($row['guard']); ?></td>
                        <th>Labour</th>
                        <td><?php echo $func->format($row['labour']); ?></td>
                    </tr>
                    <tr>
                        <th>IQ</th>
                        <td><?php echo $func->format($row['IQ']); ?></td>
                        <th>Total</th>
                        <td><?php echo $func->format($row['strength'] + $row['agility'] + $row['guard'] + $row['labour'] + $row['IQ']); ?></td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th colspan="4">Bans</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <th>Fed Jail</th>
                        <td><?php echo $func->format($row['fed_days']); ?></td>
                        <th>Reason</th>
                        <td><?php echo $func->format($row['fed_reason']); ?></td>
                    </tr>
                    <tr>
                        <th>Who</th>
                        <td colspan="3"><?php echo $func->format($row['fed_jailedby']); ?></td>
                    </tr>
                    <tr>
                        <th>Mail Banned</th>
                        <td><?php echo $func->format($row['mailban']); ?></td>
                        <th>Mail Ban Reason</th>
                        <td><?php echo $func->format($row['mb_reason']); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div><?php
        } else {
            $_SESSION['error'] = 'The player you selected doesn\'t exist';
        }
        if (true === $redirect) {
            exit(header('Location: /new_staff.php?action=record'));
        }
    } else {
        staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page=subtitle">View User Record</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php" method="get" class="form">
            <input type="hidden" name="action" value="record">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select player</label>
                        <?php echo $func->user_dropdown('user1'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="italic">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-search"></span>
                    View Record
                </button>
            </div>
        </form>
    </div>
</div><?php
    }
}
function admin_user_changeid($db, $ir, $func)
{
    if (array_key_exists('submit', $_POST)) {
        $_POST['user'] = array_key_exists('user', $_POST) && ctype_digit($_POST['user']) && $_POST['user'] > 0 ? $_POST['user'] : null;
        $_POST['newID'] = array_key_exists('newID', $_POST) && ctype_digit($_POST['newID']) && $_POST['newID'] > 0 ? $_POST['newID'] : null;
        if (null !== $_POST['user']) {
            if (null !== $_POST['newID']) {
                $db->query('SELECT userid, user_level FROM users WHERE userid = ?');
                $db->execute([$_POST['user']]);
                $existing = $db->fetch(true);
                if (null !== $existing) {
                    if (!in_array($existing['user_level'], [0, 2])) {
                        $db->query('SELECT COUNT(userid) FROM users WHERE userid = ?');
                        $db->execute([$_POST['newID']]);
                        if (!$db->result()) {
                            $queries = [
                                'users' => 'userid',
                                'userstats' => 'userid',
                                'adminlogs' => 'adUSER',
                                'attacklogs' => 'attacker',
                                'attacklogs' => 'attacked',
                                'blacklist' => 'bl_ADDED',
                                'blacklist' => 'bl_ADDER',
                                'cashxferlogs' => 'cxFROM',
                                'cashxferlogs' => 'cxTO',
                                'challengesbeaten' => 'userid',
                                'challengesbeaten' => 'npcid',
                                'coursesdone' => 'userid',
                                'crystalmarket' => 'cmADDER',
                                'dps_process' => 'dp_userid',
                                'events' => 'evUSER',
                                'fedjail' => 'fed_userid',
                                'fedjail' => 'fed_jailedby',
                                'friendslist' => 'fl_ADDER',
                                'friendslist' => 'fl_ADDED',
                                'imarketaddlogs' => 'imaADDER',
                                'imbuylogs' => 'imbADDER',
                                'imbuylogs' => 'imbBUYER',
                                'imremovelogs' => 'imrADDER',
                                'imremovelogs' => 'imrREMOVER',
                                'inventory' => 'inv_userid',
                                'itembuylogs' => 'ibUSER',
                                'itemmarket' => 'imADDER',
                                'itemselllogs' => 'isUSER',
                                'itemxferlogs' => 'ixFROM',
                                'itemxferlogs' => 'ixTO',
                                'jaillogs' => 'jaJAILER',
                                'jaillogs' => 'jaJAILED',
                                'mail' => 'mail_from',
                                'mail' => 'mail_to',
                                'mail' => 'mail_from',
                                'preports' => 'prREPORTED',
                                'preports' => 'prREPORTER',
                                'referrals' => 'refREFER',
                                'referrals' => 'refREFED',
                                'seclogs' => 'secUSER',
                                'staffnotelogs' => 'snCHANGER',
                                'staffnotelogs' => 'snCHANGED',
                                'unjaillogs' => 'ujaJAILER',
                                'unjaillogs' => 'ujaJAILED',
                                'votes' => 'userid',
                                'willplogs' => 'wp_userid',
                            ];
                            $log = 'changed the ID of {user} from '.$_POST['user'].' to '.$_POST['newID'];
                            $db->trans('start');
                            foreach ($queries as $table => $col) {
                                $db->query('UPDATE '.$table.' SET '.$col.' = ? WHERE '.$col.' = ?');
                                $db->execute([$_POST['newID'], $_POST['user']]);
                            }
                            $func->event_add($_POST['newID'], 'Your ID has been changed to '.$_POST['newID'].' by the Administration');
                            $func->stafflog(ucfirst($log), $_POST['newID']);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $func->username($_POST['newID']), $log);
                        } else {
                            $_SESSION['error'] = 'ID '.$_POST['newID'].' is already in use';
                        }
                    } else {
                        $_SESSION['error'] = 'You can\'t change the ID of an '.(2 == $existing['user_level'] ? 'Administrator' : 'NPC');
                    }
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t enter a valid new ID';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t select a valid player';
        }
        exit(header('Location: /new_staff.php?action=change_id'));
    } else {
        staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Change Player ID</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=change_id" method="post" class="form">
            <div class="form-group">
                <label for="user" class="form-label">Select player</label>
                <?php echo $func->user_dropdown('user'); ?>
            </div>
            <div class="form-group">
                <label for="newID" class="form-label">New ID</label>
                <input type="number" name="newID" id="newID" class="form-control bg-dark text-light" required>
            </div>
            <div class="form-controls">
                <button type="submit" name="submit" class="btn btn-primary">
                    <span class="fas fa-edit"></span>
                    Change ID
                </button>
            </div>
        </form>
    </div>
</div><?php
    }
}
function new_user_form($db, $ir, $func)
{
    global $defaults;
    staffMenu();
    newEditUserForm($func, $defaults);
}
function new_user_submit($db, $ir, $func)
{
    global $defaults;
    $toForm = true;
    $nums = ['level', 'money', 'crystals', 'donatordays', 'strength', 'agility', 'guard', 'labour', 'IQ'];
    $strs = ['username', 'login_name'];
    $_POST['email'] = array_key_exists('email', $_POST) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? $_POST['email'] : null;
    $_POST['userpass'] = array_key_exists('userpass', $_POST) && is_string($_POST['userpass']) && strlen($_POST['userpass']) > 0 ? $_POST['userpass'] : null;
    $_POST['gender'] = array_key_exists('gender', $_POST) && in_array($_POST['gender'], ['Male', 'Female']) ? $_POST['gender'] : null;
    $_POST['user_level'] = array_key_exists('user_level', $_POST) && in_array($_POST['user_level'], [0, 1]) ? $_POST['user_level'] : 1;
    foreach ($strs as $str) {
        $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? strip_tags(trim($_POST[$str])) : null;
    }
    foreach ($nums as $num) {
        $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : $defaults[$num];
    }
    if (null !== $_POST['username']) {
        $login_name = null !== $_POST['login_name'] ? $_POST['login_name'] : $_POST['username'];
        if (null !== $_POST['email']) {
            if (null !== $_POST['userpass']) {
                if (null !== $_POST['gender']) {
                    $db->query('SELECT COUNT(userid) FROM users WHERE ? IN (LOWER(username), LOWER(login_name))');
                    $db->execute([strtolower($_POST['username'])]);
                    if (!$db->result()) {
                        $db->query('SELECT COUNT(userid) FROM users WHERE LOWER(email) = ?');
                        $db->execute([strtolower($_POST['email'])]);
                        if (!$db->result()) {
                            $toForm = false;
                            $db->trans('start');
                            $log = 'created the account: {user}';
                            $db->query('INSERT INTO users (username, login_name, userpass, gender, email, level, money, crystals, donatordays, user_level, staffnotes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "")');
                            $db->execute([$_POST['username'], $login_name, password_hash($_POST['userpass'], PASSWORD_BCRYPT), $_POST['gender'], $_POST['email'], $_POST['level'], $_POST['money'], $_POST['crystals'], $_POST['donatordays'], $_POST['user_level']]);
                            $id = $db->insert_id();
                            $db->query('INSERT INTO userstats (userid, strength, agility, guard, labour, IQ) VALUES (?, ?, ?, ?, ?, ?)');
                            $db->execute([$id, $_POST['strength'], $_POST['agility'], $_POST['guard'], $_POST['labour'], $_POST['IQ']]);
                            $func->stafflog(ucfirst($log), $id);
                            $db->trans('end');
                            $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $func->username($id), $log);
                        } else {
                            $_SESSION['error'] = 'That email is already in use';
                        }
                    } else {
                        $_SESSION['error'] = 'That username is already in use';
                    }
                } else {
                    $_SESSION['error'] = 'You didn\'t select a valid gender'; // THERE ARE 2 GENDERS, FUCKO!
                }
            } else {
                $_SESSION['error'] = 'You didn\'t enter a valid password';
            }
        } else {
            $_SESSION['error'] = 'You didn\'t enter a valid email address';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t enter a valid username';
    }
    exit(header('Location: /new_staff.php'.(true === $toForm ? '?action=newuser' : '')));
}
function edit_user($db, $ir, $func)
{
    global $defaults;
    $_GET['step'] = array_key_exists('step', $_GET) ? $_GET['step'] : null;
    switch ($_GET['step']) {
        case 2:
            $toForm = true;
            $_POST['user'] = array_key_exists('user', $_POST) && ctype_digit($_POST['user']) && $_POST['user'] > 0 ? $_POST['user'] : null;
            if (null !== $_POST['user']) {
                if ($func->userExists($_POST['user'])) {
                    $nums = ['level', 'money', 'crystals', 'donatordays', 'strength', 'agility', 'guard', 'labour', 'IQ'];
                    $strs = ['username', 'login_name'];
                    $_POST['email'] = array_key_exists('email', $_POST) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ? $_POST['email'] : null;
                    $_POST['userpass'] = array_key_exists('userpass', $_POST) && is_string($_POST['userpass']) && strlen($_POST['userpass']) > 0 ? $_POST['userpass'] : null;
                    $_POST['gender'] = array_key_exists('gender', $_POST) && in_array($_POST['gender'], ['Male', 'Female']) ? $_POST['gender'] : null;
                    $_POST['user_level'] = array_key_exists('user_level', $_POST) && in_array($_POST['user_level'], [0, 1]) ? $_POST['user_level'] : 1;
                    foreach ($strs as $str) {
                        $_POST[$str] = array_key_exists($str, $_POST) && is_string($_POST[$str]) && strlen($_POST[$str]) > 0 ? strip_tags(trim($_POST[$str])) : null;
                    }
                    foreach ($nums as $num) {
                        $_POST[$num] = array_key_exists($num, $_POST) && ctype_digit($_POST[$num]) && $_POST[$num] > 0 ? $_POST[$num] : $defaults[$num];
                    }
                    if (null !== $_POST['username']) {
                        $login_name = null !== $_POST['login_name'] ? $_POST['login_name'] : $_POST['username'];
                        if (null !== $_POST['email']) {
                            if (null !== $_POST['gender']) {
                                $db->query('SELECT COUNT(userid) FROM users WHERE ? IN (LOWER(username), LOWER(login_name)) AND userid <> ?');
                                $db->execute([strtolower($_POST['username']), $_POST['user']]);
                                if (!$db->result()) {
                                    $db->query('SELECT COUNT(userid) FROM users WHERE LOWER(email) = ? AND userid <> ?');
                                    $db->execute([strtolower($_POST['email']), $_POST['user']]);
                                    if (!$db->result()) {
                                        $toForm = false;
                                        $db->trans('start');
                                        $log = 'edited the account: {user}';
                                        $db->query('UPDATE users AS u
                                                INNER JOIN userstats AS us ON u.userid = us.userid
                                                SET u.username = ?, u.login_name = ?, u.gender = ?, u.email = ?, u.level = ?, u.money = ?, u.crystals = ?, u.donatordays = ?, u.user_level = ?,
                                                us.strength = ?, us.agility = ?, us.guard = ?, us.labour = ?, us.IQ = ?
                                                WHERE u.userid = ?
                                            ');
                                        $db->execute([$_POST['username'], $login_name, $_POST['gender'], $_POST['email'], $_POST['level'], $_POST['money'], $_POST['crystals'], $_POST['donatordays'], $_POST['user_level'], $_POST['strength'], $_POST['agility'], $_POST['guard'], $_POST['labour'], $_POST['IQ'], $_POST['user']]);
                                        $func->stafflog(ucfirst($log), $_POST['user']);
                                        $db->trans('end');
                                        $_SESSION['success'] = 'You\'ve '.str_replace('{user}', $func->username($_POST['user']), $log);
                                    } else {
                                        $_SESSION['error'] = 'That email is already in use';
                                    }
                                } else {
                                    $_SESSION['error'] = 'That username is already in use';
                                }
                            } else {
                                $_SESSION['error'] = 'You didn\'t select a valid gender'; // THERE ARE 2 GENDERS, FUCKO!
                            }
                        } else {
                            $_SESSION['error'] = 'You didn\'t enter a valid email address';
                        }
                    } else {
                        $_SESSION['error'] = 'You didn\'t enter a valid username';
                    }
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid player';
            }
            header('Location: /new_staff.php'.(true === $toForm ? '?action=edituser' : ''));
            break;
        case 1:
            $redirect = true;
            $_POST['user1'] = array_key_exists('user1', $_POST) && ctype_digit($_POST['user1']) && $_POST['user1'] > 0 ? $_POST['user1'] : null;
            $_POST['user2'] = array_key_exists('user2', $_POST) && ctype_digit($_POST['user2']) && $_POST['user2'] > 0 ? $_POST['user2'] : null;
            $_POST['user'] = null === $_POST['user2'] ? $_POST['user1'] : $_POST['user2'];
            if (null !== $_POST['user']) {
                $db->query('SELECT u.userid, u.username, u.login_name, u.email, u.userpass, u.user_level, u.user_level, u.level, u.money, u.crystals, u.donatordays, u.gender,
                    us.strength, us.agility, us.guard, us.labour, us.IQ
                    FROM users AS u
                    INNER JOIN userstats AS us ON u.userid = us.userid
                    WHERE u.userid = ?
                ');
                $db->execute([$_POST['user']]);
                $row = $db->fetch(true);
                if (null !== $row) {
                    $redirect = false;
                    staffMenu();
                    newEditUserForm($func, $defaults, $row);
                } else {
                    $_SESSION['error'] = 'The player you selected doesn\'t exist';
                }
            } else {
                $_SESSION['error'] = 'You didn\'t select a valid player';
            }
            if (true === $redirect) {
                exit(header('Location: /new_staff.php?action=edituser'));
            }
            break;
        default:
            staffMenu(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Edit Account</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=edituser&amp;step=1" method="post" class="form">
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="user1" class="form-label">Select player</label>
                        <?php echo $func->user_dropdown('user1'); ?>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="user2" class="form-label"><strong class="italic">OR</strong> enter ID</label>
                        <input type="number" name="user2" id="user2" class="form-control bg-dark text-light">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-edit"></span>
                    Begin Edits
                </button>
            </div>
        </form>
    </div>
</div><?php
            break;
    }
}
function newEditUserForm($func, $defaults, $row = null)
{
    ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle"><?php echo null !== $row ? 'Edit' : 'Create'; ?> Account</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <form action="/new_staff.php?action=<?php echo null !== $row ? 'edituser&amp;step=2' : 'newusersub'; ?>" method="post" class="form"><?php
        if (null !== $row) {
            ?>
            <input type="hidden" name="user" value="<?php echo $row['userid']; ?>"><?php
        } ?>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" name="username" id="username" class="form-control bg-dark text-light" required autofocus<?php echo null !== $row ? ' value="'.$func->format($row['username']).'"' : ''; ?>>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="login_name" class="form-label">Login Name</label>
                        <input type="text" name="login_name" id="login_name" class="form-control bg-dark text-light" placeholder="Leave blank to use username"<?php echo null !== $row ? ' value="'.$func->format($row['login_name']).'"' : ''; ?>>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" name="email" id="email" class="form-control bg-dark text-light" required<?php echo null !== $row ? ' value="'.$func->format($row['email']).'"' : ''; ?>>
                    </div>
                </div><?php
    if (null === $row) {
        ?>
                <div class="col-6">
                    <div class="form-group">
                        <label for="userpass" class="form-label">Password</label>
                        <input type="text" name="userpass" id="userpass" class="form-control bg-dark text-light" required>
                    </div>
                </div><?php
    } ?>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        Account Type
                        <div class="form-check">
                            <input type="radio" name="user_level" id="npc" class="form-check-input" value="0"<?php echo null !== $row && !$row['user_level'] ? ' checked' : ''; ?>>
                            <label for="npc" class="form-check-label">NPC</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" name="user_level" id="reg" class="form-check-input" value="1"<?php echo null !== $row && $row['user_level'] ? ' checked' : ''; ?>>
                            <label for="reg" class="form-check-label">Standard</label>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="level" class="form-label">Level</label>
                        <input type="number" name="level" id="level" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['level'] : $defaults['level']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="money" class="form-label">Money</label>
                        <input type="number" name="money" id="money" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['money'] : $defaults['money']; ?>">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="crystals" class="form-label">Crystals</label>
                        <input type="number" name="crystals" id="crystals" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['crystals'] : $defaults['crystals']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="donatordays" class="form-label">Donator Days</label>
                        <input type="number" name="donatordays" id="donatordays" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['donatordays'] : $defaults['donatordays']; ?>">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select name="gender" id="gender" class="form-control bg-dark text-light">
                            <option value="Male"<?php echo null !== $row && 'Male' == $row['gender'] ? ' selected' : ''; ?>>Male</option>
                            <option value="Female"<?php echo null !== $row && 'Female' == $row['gender'] ? ' selected' : ''; ?>>Female</option>
                        </select>
                    </div>
                </div>
            </div>
            <legend>Stats</legend>
            <div class="form-row">
                <div class="col-4">
                    <div class="form-group">
                        <label for="strength" class="form-label">Strength</label>
                        <input type="number" name="strength" id="strength" min="10" step="0.0001" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['strength'] : $defaults['strength']; ?>">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="agility" class="form-label">Agility</label>
                        <input type="number" name="agility" id="agility" min="10" step="0.0001" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['agility'] : $defaults['agility']; ?>">
                    </div>
                </div>
                <div class="col-4">
                    <div class="form-group">
                        <label for="guard" class="form-label">Guard</label>
                        <input type="number" name="guard" id="guard" min="10" step="0.0001" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['guard'] : $defaults['guard']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <div class="form-group">
                        <label for="labour" class="form-label">Labour</label>
                        <input type="number" name="labour" id="labour" min="10" step="0.0001" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['labour'] : $defaults['labour']; ?>">
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label for="IQ" class="form-label">IQ</label>
                        <input type="number" name="IQ" id="IQ" min="10" step="0.0001" class="form-control bg-dark text-light" value="<?php echo null !== $row ? $row['IQ'] : $defaults['IQ']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-controls">
                <button type="submit" class="btn btn-primary">
                    <span class="fas fa-<?php echo null !== $row ? 'edit' : 'user-plus'; ?>"></span>
                    <?php echo null !== $row ? 'Edit' : 'Add'; ?> Account
                </button>
            </div>
        </form>
    </div>
</div><?php
}
