<?php
/*
MCCodes FREE
stafflist.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$time = time();
$staff = [
    'administrators' => [],
    'secretaries' => [],
    'assistants' => [],
];
$db->query('SELECT userid, laston, level, money, user_level, avatar FROM users WHERE user_level IN(2, 3, 5) ORDER BY user_level ASC, userid ASC');
$db->execute();
$rows = $db->fetch();
if (null === $rows) {
    $_SESSION['info'] = 'There are no staff members :o';
    exit(header('Location: /explore.php'));
}
foreach ($rows as $row) {
    if (2 == $row['user_level']) {
        $level = 'administrators';
    } elseif (3 == $row['user_level']) {
        $level = 'secretaries';
    } elseif (5 == $row['user_level']) {
        $level = 'assistants';
    }
    $staff[$level][] = $row;
}
$assCnt = 0; // lel
$secCnt = 0;
$adminCnt = 0;
foreach ($staff as $rank => $data) {
    if (count($data)) {
        ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle"><?php echo ucfirst($rank); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="row text-center"><?php
        foreach ($data as $row) {
            ++$adminCnt;
            $laston = strtotime($row['laston']);
            $timeCalc = $laston >= $time - 900; ?>
            <div class="col-3">
                <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                    <img src="<?php echo $row['avatar']; ?>" class="staff-list img-fluid <?php echo $timeCalc ? 'online-box' : 'offline-box'; ?>">
                </a><br>
                <?php echo $func->username($row['userid']); ?><br>
                Last seen: <?php echo $func->time_format($time - $laston, 'short', true, 2); ?>
            </div><?php
            $div = $adminCnt % 4;
            if (!$div) {
                ?>
        </div>
        <div class="row text-center"><?php
            }
        }
        if (1 == $div) {
            echo str_repeat('<div class="col-3">&nbsp;</div>', 3);
        } elseif (2 == $div) {
            echo str_repeat('<div class="col-3">&nbsp;</div>', 2);
        } elseif (3 == $div) {
            echo '<div class="col-3">&nbsp;</div>';
        } ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">&nbsp;</div>
</div><?php
    }
}
