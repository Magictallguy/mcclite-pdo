<?php
/*
MCCodes FREE
staffnotes.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

require_once __DIR__.'/lib/master.php';
$row = null;
if (in_array($ir['user_level'], [2, 3, 5])) {
    $_POST['ID'] = array_key_exists('ID', $_POST) && ctype_digit($_POST['ID']) && $_POST['ID'] > 0 ? $_POST['ID'] : null;
    $_POST['staffnotes'] = array_key_exists('staffnotes', $_POST) && is_string($_POST['staffnotes']) && strlen($_POST['staffnotes']) > 0 ? strip_tags(trim($_POST['staffnotes'])) : '';
    if (null !== $_POST['ID']) {
        $db->query('SELECT userid, staffnotes FROM users WHERE userid = ?');
        $db->execute([$_POST['ID']]);
        $row = $db->fetch(true);
        if (null !== $row) {
            $db->trans('start');
            $db->query('UPDATE users SET staffnotes = ? WHERE userid = ?');
            $db->execute([$_POST['staffnotes'], $row['userid']]);
            $db->query('INSERT INTO staffnotelogs (snCHANGER, snCHANGED, snOLD, snNEW) VALUES (?, ?, ?, ?)');
            $db->execute([$ir['userid'], $row['userid'], $row['staffnotes'], $_POST['staffnotes']]);
            $db->trans('end');
            $_SESSION['success'] = 'The staff notes on '.$func->username($row['userid']).' have been updated';
        } else {
            $_SESSION['error'] = 'That player doesn\'t exist';
        }
    } else {
        $_SESSION['error'] = 'You didn\'t select a valid player';
    }
} else {
    $_SESSION['error'] = 'You don\'t have access';
}
exit(header('Location: '.(null !== $row ? '/viewuser.php?u='.$row['userid'] : '/index.php')));
