<?php
require_once __DIR__.'/lib/master.php';
// Basic Stats (all users)
$db->query('SELECT COUNT(userid) FROM users WHERE crystals > 0');
$db->execute();
$crys = $db->result();
$db->query('SELECT COUNT(userid) AS c_users, SUM(money) AS s_money, SUM(crystals) AS s_crystals FROM users');
$db->execute();
$mem_info = $db->fetch(true);
$membs = $mem_info['c_users'];
$total = $mem_info['s_money'];
$avg = (int) ($total / ($membs > 1 ? $membs : 1));
$totalc = $mem_info['s_crystals'];
$avgc = (int) ($totalc / ($crys > 1 ? $crys : 1));
$db->query('SELECT COUNT(userid) AS c_users, SUM(bankmoney) AS s_bank FROM users WHERE bankmoney > -1');
$db->execute();
$bank_info = $db->fetch(true);
$db->query('SELECT COUNT(userid) AS c_users, SUM(cybermoney) AS s_cyber FROM users WHERE cybermoney > -1');
$db->execute();
$cyber_info = $db->fetch(true);
$banks = $bank_info['c_users'];
$totalb = $bank_info['s_bank'];
$avgb = (int) ($totalb / ($banks > 0 ? $banks : 1));
$cybers = $cyber_info['c_users'];
$totalcb = $cyber_info['s_cyber'];
$avgcb = (int) ($totalcb / ($cybers > 0 ? $cybers : 1));
$db->query('SELECT COUNT(userid) FROM users WHERE gender = "Male"');
$db->execute();
$male = $db->result();
$db->query('SELECT COUNT(userid) FROM users WHERE gender = "Female"');
$db->execute();
$fem = $db->result();
$db->query('SELECT SUM(inv_qty) FROM inventory');
$db->execute();
$totali = $db->result();
$db->query('SELECT COUNT(mail_id) FROM mail');
$db->execute();
$mail = $db->result();
$db->query('SELECT COUNT(evID) FROM events');
$db->execute();
$events = $db->result(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Country Statistics</h3>
        <p>
            You step into the Statistics Department and login to the service. You see some stats that interest you.
        </p>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th style="width:50%;">Users</th>
                        <th style="width:50%;">Finances</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            There are currently <?php echo $func->format($membs); ?> {GAME_NAME} players, <?php echo $func->format($male); ?> male<?php echo $func->s($male); ?> and <?php echo $func->format($fem); ?> female<?php echo $func->s($fem); ?>.
                        </td>
                        <td>
                            Amount of cash in circulation: <?php echo $func->money($total); ?><br>
                            The average player has: <?php echo $func->money($avg); ?><br>
                            Amount of cash in banks: <?php echo $func->money($totalb); ?><br>
                            Amount of players with bank accounts: <?php echo $func->format($banks); ?><br>
                            The average player has in their bank account: <?php echo $func->money($avgb); ?><br>
                            Amount of players with cyber accounts: <?php echo $func->format($cybers); ?><br>
                            The average player has in their cyber account: <?php echo $func->money($avgcb); ?><br>
                            Amount of crystals in circulation: <?php echo $func->crystals($totalc); ?><br>
                            The average player has: <?php echo $func->crystals($avgc); ?>
                        </td>
                    </tr>
                </tbody>
                <thead>
                    <tr>
                        <th>Correspondence</th>
                        <th>Items</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $func->format($mail); ?> message<?php echo $func->s($mail); ?> and <?php echo $func->format($events); ?> event<?php echo $func->s($events); ?> have been sent
                        </td>
                        <td>
                            There <?php echo 1 == $totali ? 'is' : 'are'; ?> currently <?php echo $func->format($totali); ?> item<?php echo $func->s($totali); ?> in circulation
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
