<?php
/*
MCCodes FREE
userlist.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
define('PAGINATION', true);
require_once __DIR__.'/lib/master.php';
$_GET['by'] = array_key_exists('by', $_GET) && in_array($_GET['by'], ['userid', 'username', 'level', 'money']) ? $_GET['by'] : 'userid';
$_GET['ord'] = array_key_exists('ord', $_GET) && in_array(strtolower($_GET['ord']), ['asc', 'desc']) ? $_GET['ord'] : 'asc';
$db->query('SELECT COUNT(userid) FROM users');
$db->execute();
$membs = $db->result();
$pages = new Paginator($membs);
$db->query('SELECT userid, money, level, gender, laston, avatar FROM users ORDER BY '.$_GET['by'].' '.strtoupper($_GET['ord']).$pages->limit);
$db->execute();
$rows = $db->fetch();
$no1 = $pages->current_page + 1;
$no2 = $pages->current_page + 100;
if (0 == $membs % 100) {
    --$pages->num_pages;
}
if ($membs % 100) {
    $no2 = ($pages->num_pages * 100) + ($membs - 100);
}
$indicator = [
    [
        'text' => 'success',
        'disp' => 'Online',
    ],
    [
        'text' => 'danger',
        'disp' => 'Offline',
    ],
]; ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Userlist</h3>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?php echo $pages->display_pages(); ?>
    </div>
    <div class="col-6">
        Order By: <a href="/userlist.php?st=<?php echo $pages->current_page; ?>&amp;by=userid&amp;ord=<?php echo $_GET['ord']; ?>"<?php echo 'userid' == $_GET['by'] ? ' class="bold"' : ''; ?>>ID</a> &middot;
        <a href="/userlist.php?st=<?php echo $pages->current_page; ?>&amp;by=username&amp;ord=<?php echo $_GET['ord']; ?>"<?php echo 'username' == $_GET['by'] ? ' class="bold"' : ''; ?>>Name</a> &middot;
        <a href="/userlist.php?st=<?php echo $pages->current_page; ?>&amp;by=level&amp;ord=<?php echo $_GET['ord']; ?>"<?php echo 'level' == $_GET['by'] ? ' class="bold"' : ''; ?>>Level</a> &middot;
        <a href="/userlist.php?st=<?php echo $pages->current_page; ?>&amp;by=money&amp;ord=<?php echo $_GET['ord']; ?>"<?php echo 'money' == $_GET['by'] ? ' class="bold"' : ''; ?>>Money</a><br />
        <a href="/userlist.php?st=<?php echo $pages->current_page; ?>&amp;by=<?php echo $_GET['by']; ?>&amp;ord=asc"<?php echo 'asc' == $_GET['ord'] ? ' class="bold"' : ''; ?>>Ascending</a> &middot;
        <a href="/userlist.php?st=<?php echo $pages->current_page; ?>&amp;by=<?php echo $_GET['by']; ?>&amp;ord=desc"<?php echo 'desc' == $_GET['ord'] ? ' class="bold"' : ''; ?>>Descending</a><br /><br />
    </div>
</div>
<div class="row">
    <div class="col">
        Showing users <?php echo $no1; ?> to <?php echo $no2; ?> by order of <?php echo $_GET['by'],' ',$_GET['ord']; ?>ending.
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th colspan="4">Players</th>
                    </tr>
                </thead>
                <tbody>
                    <tr><?php
if (null === $rows) {
    ?>
                        <td colspan="5" class="center">This page is empty</td>
                    </tr><?php
} else {
        $time = time();
        $cnt = 0;
        foreach ($rows as $row) {
            $laston = strtotime($row['laston']);
            $timeCalc = time() - 900 >= $laston;
            ++$cnt; ?>
                        <td class="text-center" style="width:25%">
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" title="avatar" alt="avatar" class="avatar-userlist img-fluid <?php echo $timeCalc ? 'off' : 'on'; ?>line-box">
                            </a><br>
                            <span class="fas fa-<?php echo 'male' == strtolower($row['gender']) ? 'mars' : 'venus'; ?>" title="This character is <?php echo $row['gender']; ?>"></span> <?php echo $func->username($row['userid']); ?><br>
                            Last seen: <?php echo $func->time_format($time - $laston, 'short', true, 1); ?><br>
                            Cash: <?php echo $func->money($row['money']); ?><br>
                            Level: <?php echo $func->format($row['level']); ?>
                        </td><?php
            if (!($cnt % 4)) {
                ?>
                    </tr>
                    <tr><?php
            }
        }
        $div = $cnt % 4;
        if (1 == $div) {
            echo '<td></td><td></td><td>';
        } elseif (2 == $div) {
            echo '<td></td><td>';
        } elseif (3 == $div) {
            echo '<td></td>';
        }
    } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
