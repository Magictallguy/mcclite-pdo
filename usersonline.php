<?php
/*
MCCodes FREE
usersonline.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$indicator = [
    [
        'text' => 'success',
        'disp' => 'Online',
    ],
    [
        'text' => 'danger',
        'disp' => 'Offline',
    ],
];
$db->query('SELECT u.userid, u.laston, u.avatar, u.gender, u.user_level, c.cityname
    FROM users AS u
    INNER JOIN cities AS c ON u.location = c.cityid
    WHERE u.laston >= DATE_SUB(NOW(), INTERVAL 15 MINUTE)
    ORDER BY u.laston DESC
');
$db->execute();
$rows = $db->fetch(); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Online</h3>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody><?php
if (null === $rows) {
    ?>
                    <tr>
                        <td colspan="3" class="text-center">There's no-one online! Wait.. wut?</td>
                    </tr><?php
} else {
        $time = time();
        $cnt = 0;
        $ranks = [
            1 => '',
            2 => ' admin-box',
            3 => ' secretary-box',
            5 => ' assistant-box',
        ]; ?>
                    <tr><?php
        foreach ($rows as $row) {
            $laston = strtotime($row['laston']);
            $timeCalc = time() - 900 >= $laston;
            ++$cnt;
            $div = $cnt % 4; ?>
                        <td class="text-center" style="width:25%">
                            <a href="<?php echo $row['avatar']; ?>" data-toggle="lightbox">
                                <img src="<?php echo $row['avatar']; ?>" title="avatar" alt="avatar" class="avatar-userlist img-fluid<?php echo $ranks[$row['user_level']]; ?>">
                            </a><br>
                            <span class="fas fa-<?php echo 'male' == strtolower($row['gender']) ? 'mars' : 'venus'; ?>" title="This character is <?php echo $row['gender']; ?>"></span> <?php echo $func->username($row['userid']); ?><br>
                            <?php echo $func->format($row['cityname']); ?><br>
                            <small>Last seen: <?php echo $func->time_format($time - $laston); ?></small>
                        </td><?php
            if (!$div) {
                ?>
                    </tr>
                    <tr><?php
            }
        }
        if (1 == $div) {
            echo '<td></td><td></td><td></td>';
        } elseif (2 == $div) {
            echo '<td></td><td></td>';
        } elseif (3 == $div) {
            echo '<td></td>';
        }
    } ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
