<?php
/*
MCCodes FREE
viewuser.php Rev 1.1.0c
Copyright (C) 2005-2012 Dabomstew

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
require_once __DIR__.'/lib/master.php';
$_GET['u'] = array_key_exists('u', $_GET) && ctype_digit($_GET['u']) && $_GET['u'] > 0 ? $_GET['u'] : $ir['userid'];
$db->query('SELECT u.userid, u.username, u.user_level, u.staffnotes, u.signedup, u.money, u.maxhp, u.level, u.laston, u.lastip, u.hp, u.hospreason, u.hospital, u.gender, u.fedjail, u.duties, u.donatordays, u.avatar, u.daysold, u.crystals,
    us.strength, us.agility, us.guard, us.labour, us.IQ,
    h.hNAME,
    c.cityname,
    f.fed_days, f.fed_reason
    FROM users AS u
    INNER JOIN userstats AS us ON u.userid = us.userid
    INNER JOIN houses AS h ON u.maxwill = h.hWILL
    INNER JOIN cities AS c ON u.location = c.cityid
    LEFT JOIN fedjail AS f ON u.userid = f.fed_userid
    WHERE u.userid = ?
');
$db->execute([$_GET['u']]);
$profile = $db->fetch(true);
if (null === $profile) {
    $_SESSION['error'] = 'The player you selected doesn\'t exist';
    exit(header('Location: /index.php'));
}
$user_levels = [
    0 => 'NPC',
    1 => $profile['donatordays'] > 0 ? '<span style="color:#df1414;">Donator</span>' : 'Member',
    2 => '<span style="color:#935ae4;">Administrator</span>',
    3 => '<span style="color:#07d410;">Secretary</span>',
    4 => 'IRC Operator',
    5 => '<span style="color:#e6de09;">Assistant</span>',
];
$userl = array_key_exists($profile['user_level'], $user_levels) ? $user_levels[$profile['user_level']] : '<em>Unknown</em>';
$time = time();
$laston = strtotime($profile['laston']);
$lastDate = new \DateTime($profile['laston']);
$signDate = new \DateTime($profile['signedup']);
$signedup = strtotime($profile['signedup']);
$lon = $lastDate->format('F j, Y g:i:s a');
$sup = $signDate->format('F j, Y g:i:s a');
$ts = $profile['strength'] + $profile['agility'] + $profile['guard'] + $profile['labour'] + $profile['IQ'];
$username = $func->username($profile['userid']);
$db->query('SELECT * FROM (
    (SELECT COUNT(refID) AS refs FROM referrals WHERE refREFER = ?) AS refs,
    (SELECT COUNT(fl_ID) AS friends FROM friendslist WHERE fl_ADDED = ?) AS friends,
    (SELECT COUNT(bl_ID) AS enemies FROM blacklist WHERE bl_ADDED = ?) AS enemies
)');
$db->execute([$profile['userid'], $profile['userid'], $profile['userid']]);
$cnts = $db->fetch(true); ?>
<div class="row">
    <div class="col">
        <h3 class="page-subtitle">Profile: <?php echo $username; ?></h3>
    </div>
</div>
<div class="row">
    <div class="col">
        &nbsp;
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-3">
                Name: <?php echo $username; ?><br>
                Rank: <?php echo $userl; ?><br>
                Duties: <?php echo $func->format($profile['duties']); ?><br>
                Gender: <?php echo $profile['gender']; ?><br>
                Signed Up: <?php echo $sup; ?><br>
                Last Active: <?php echo $lon; ?><br>
                Last Action: <?php echo $func->time_format($time - $laston); ?><br>
                Account Age: <?php echo $func->time_format($profile['daysold'] * 86400, 'long', false); ?>
            </div>
            <div class="col-3">
                Location: <?php echo $func->format($profile['cityname']); ?><br>
                Money: <?php echo $func->money($profile['money']); ?><br>
                Crystals: <?php echo $func->format($profile['crystals']); ?><br>
                Property: <?php echo $func->format($profile['hNAME']); ?><br>
                Referrals: <?php echo $func->format($cnts['refs']); ?><br>
                Friends: <?php echo $func->format($cnts['friends']); ?><br>
                Enemies: <?php echo $func->format($cnts['enemies']); ?><br>
                Level: <?php echo $func->format($profile['level']); ?><br>
                Health: <?php echo $func->format($profile['hp']),'/',$func->format($profile['maxhp']); ?>
            </div>
            <div class="col-6 h-100 align-self-center text-center">
                <a href="<?php echo $profile['avatar']; ?>" data-toggle="lightbox">
                    <img src="<?php echo $profile['avatar']; ?>" class="avatar-profile img-fluid <?php echo $laston >= $time - 900 ? 'online-box' : 'offline-box'; ?>">
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col">
                &nbsp;
            </div>
        </div><?php
if ($profile['fedjail'] > 0 or $profile['hospital'] > 0) {
    ?>
        <div class="row">
            <div class="col"><?php
    if ($profile['fedjail'] > 0) {
        ?>
                <br>
                <big class="text-danger">Federal Jailed for <?php echo $func->time_format($profile['fed_days'] * 86400, 'long', false); ?></big><br>
                <span class="text-danger">Reason: <?php echo $func->format($profile['fed_reason'], true); ?></span><br><?php
    }
    if ($profile['hospital'] > 0) {
        ?>
                <br>
                <big class="text-warning">Hospitalised for <?php echo $func->time_format($profile['hospital'] * 60, 'long', false); ?></big><br>
                <span class="text-warning">Reason: <?php echo $profile['hospreason']; ?></span><br><?php
    } ?>
            </div>
        </div><?php
}?>
        <div class="row">
            <div class="col">
                [<a href="/mailbox.php?action=compose&amp;ID=<?php echo $profile['userid']; ?>">Send Mail</a>] &middot;
                [<a href="/sendcash.php?ID=<?php echo $profile['userid']; ?>">Send Money</a>] &middot;
                [<a href="/sendcrystals.php?ID=<?php echo $profile['userid']; ?>">Send Crystals</a>] &middot;
                [<a href="/attack.php?ID=<?php echo $profile['userid']; ?>" class="text-danger">Attack</a>]<?php
if ($ir['donatordays'] > 0) {
        ?>
                <br>
                [<a href="/friendslist.php?action=add&amp;ID=<?php echo $profile['userid']; ?>">Add to Friends</a>] &middot;
                [<a href="/blacklist.php?action=add&amp;ID=<?php echo $profile['userid']; ?>">Add to Enemies</a>]<?php
    }
if (in_array($ir['user_level'], [2, 3, 5])) {
    ?>
                <br>
                [<a href="/jailuser.php?userid=<?php echo $profile['userid']; ?>">Account Ban</a>] &middot;
                [<a href="/mailban.php?userid=<?php echo $profile['userid']; ?>">Mail Ban</a>]<?php
} ?>
            </div><?php
if (in_array($ir['user_level'], [2, 3, 5])) {
        ?>
            <div class="col">
                <h4 class="page-subtitle">Staff Info</h4>
                <p>
                    Last known IP Address: <?php echo $func->format($profile['lastip']); ?>
                </p>
                <form action="/staffnotes.php" method="post" class="form">
                    <input type="hidden" name="ID" value="<?php echo $profile['userid']; ?>">
                    <div class="form-group">
                        <label for="staffnotes" class="form-label">Staff Notes</label>
                        <textarea name="staffnotes" id="staffnotes" rows="5" class="form-control bg-dark text-light"><?php echo $func->format($profile['staffnotes']); ?></textarea>
                    </div>
                    <div class="form-controls">
                        <button type="submit" class="btn btn-primary">
                            <span class="fas fa-edit"></span>
                            Update Notes
                        </button>
                    </div>
                </form>
            </div><?php
    } ?>
        </div>
    </div>
</div>
